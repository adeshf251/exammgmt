@extends('layouts.app')

@section('content')

<div class="panel-heading">Register</div>

<div class="panel-body">
     <form class="form-horizontal" method="POST" action="{{ route('resultdate.store') }}">
        {{ csrf_field() }}


        <div class="form-group{{ $errors->has('result_date') ? ' has-error' : '' }}">
            <label for="result_date" class="col-md-4 control-label">Result Date (DD/MM/YYYY, ex- 25/09/2017)</label>

            <div class="col-md-6">
                <input id="result_date" type="text" class="form-control" name="result_date" value="{{ old('result_date') }}" autofocus>

                @if ($errors->has('result_date'))
                    <span class="help-block">
                        <strong>{{ $errors->first('result_date') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <div class="form-group{{ $errors->has('term') ? ' has-error' : '' }}">
            <label for="term" class="col-md-4 control-label">Choose Applicable Term</label>

            <div class="col-md-6">
                <select  id="term" class="form-control" name="term" >
                         <option value="1"> Term 1 </option>
                         <option value="2"> Term 2 </option>
                </select>

                @if ($errors->has('term'))
                    <span class="help-block">
                        <strong>{{ $errors->first('term') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    Assign the Date
                </button>
            </div>
        </div>

    </form>
</div>

@endsection
