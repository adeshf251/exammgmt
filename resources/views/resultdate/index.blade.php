@extends('layouts.app')

@section('content')

<!-- will be used to show any messages -->
@if(Session('message'))
    <div class="alert alert-info">{{ Session('message') }}</div>
@endif

<h3>Showing the Details of Registered Result Dates  </h3>

<a href="{{ URL::to('resultdate/create') }}">
    <button type="button" class="btn btn-primary" style="margin: 10px 0px 0px 10px;">Register Result Date</button>
</a>

<div class="panel-body">
    <table width="100%" class="table table-striped table-bordered table-hover" id="AdvancedTable">
            <thead>
                <th> ID </th>
                <th></th>
                <th></th>
                <th> Session ID </th>
                <th> Session Year </th>
                <th> Term </th>
                <th> Result Date </th>
            </thead>
        @foreach ($resultdatelist as $element)
            <tr>
                <td> {{ $element->id }} </td> 

                    <td> 
                        <a href="{{ URL::to('resultdate/' . $element->id . '/edit') }}">
                            <input type="button" class="btn btn-info  btn-sm" name="" id="" value="Edit">
                        </a>   
                    
                    </td>

                    <td>
                    <form method="POST" action="{{ URL('resultdate') }}/{{$element->id}}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE">
                    <input type="submit" class="btn btn-danger  btn-sm" name="submit" id="$element->id" value="Delete">
                    </form> 

                    </td>
                    
                <td> {{ $element->sessionid }} </td> 
                <td> {{ $element->valid_period }} </td>
                <td> {{ $element->term }} </td>
                <td> {{ $element->result_date }} </td>
            </tr>
        @endforeach
    </table>
</div>


@endsection

