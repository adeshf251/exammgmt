@extends('layouts.app')

@section('content')

<div class="panel-heading">Register</div>

<div class="panel-body">
    <form class="form-horizontal" method="POST" action="{{ route('studentmgmt.store') }}">
        {{ csrf_field() }}


        <div class="form-group{{ $errors->has('admission_no') ? ' has-error' : '' }}">
            <label for="admission_no" class="col-md-4 control-label">Admission Number</label>

            <div class="col-md-6">
                <input id="admission_no" type="text" class="form-control" name="admission_no" value="{{ $studentdetail->admission_no }}" autofocus>

                @if ($errors->has('admission_no'))
                    <span class="help-block">
                        <strong>{{ $errors->first('admission_no') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('student_name') ? ' has-error' : '' }}">
            <label for="student_name" class="col-md-4 control-label">Student's Name</label>

            <div class="col-md-6">
                <input id="student_name" type="text" class="form-control" name="student_name" value="{{ $studentdetail->student_name }}" autofocus>

                @if ($errors->has('student_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('student_name') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <div class="form-group{{ $errors->has('student_class_section') ? ' has-error' : '' }}">
            <label for="student_class_section" class="col-md-4 control-label">Class Applicable</label>

            <div class="col-md-6">
                <select  id="student_class_section" class="form-control" name="student_class_section" >

                    @foreach ($classlist as $element)
                         <option value="{{$element}}"> {{$element}}</option>
                    @endforeach
                    <option value="{{ $studentdetail->student_class_section }}" selected> {{ $studentdetail->student_class_section }}</option>
                </select>

                @if ($errors->has('student_class_section'))
                    <span class="help-block">
                        <strong>{{ $errors->first('student_class_section') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <div class="form-group{{ $errors->has('student_mother_name') ? ' has-error' : '' }}">
            <label for="student_mother_name" class="col-md-4 control-label">Mother's Name</label>

            <div class="col-md-6">
                <input id="student_mother_name" type="text" class="form-control" name="student_mother_name" value="{{ $studentdetail->student_mother_name }}" autofocus>

                @if ($errors->has('student_mother_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('student_mother_name') }}</strong>
                    </span>
                @endif
            </div>
        </div>



        <div class="form-group{{ $errors->has('student_father_name') ? ' has-error' : '' }}">
            <label for="student_father_name" class="col-md-4 control-label">Father's Name</label>

            <div class="col-md-6">
                <input id="student_father_name" type="text" class="form-control" name="student_father_name" value="{{ $studentdetail->student_father_name }}" autofocus>

                @if ($errors->has('student_father_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('student_father_name') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <div class="form-group{{ $errors->has('student_address') ? ' has-error' : '' }}">
            <label for="student_address" class="col-md-4 control-label">Address</label>

            <div class="col-md-6">
                <input id="student_address" type="text" class="form-control" name="student_address" value="{{ $studentdetail->student_address }}" autofocus>

                @if ($errors->has('student_address'))
                    <span class="help-block">
                        <strong>{{ $errors->first('student_address') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <div class="form-group{{ $errors->has('student_joining_date') ? ' has-error' : '' }}">
            <label for="student_joining_date" class="col-md-4 control-label">Date of Joining</label>

            <div class="col-md-6">
                <input id="student_joining_date" type="text" class="form-control" name="student_joining_date" value="{{ $studentdetail->student_joining_date }}" autofocus>

                @if ($errors->has('student_joining_date'))
                    <span class="help-block">
                        <strong>{{ $errors->first('student_joining_date') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <div class="form-group{{ $errors->has('student_images') ? ' has-error' : '' }}">
            <label for="student_images" class="col-md-4 control-label">Student's Image</label>

            <div class="col-md-6">
                <input id="student_images" type="file" class="form-control" name="student_images" value="{{ $studentdetail->student_images }}" autofocus>

                @if ($errors->has('student_images'))
                    <span class="help-block">
                        <strong>{{ $errors->first('student_images') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('student_dob') ? ' has-error' : '' }}">
            <label for="student_dob" class="col-md-4 control-label">Date of Birth (dd-mm-yyyy)</label>

            <div class="col-md-6">
                <input id="student_dob" type="text" class="form-control" name="student_dob" value="{{ $studentdetail->student_dob }}" autofocus>

                @if ($errors->has('student_dob'))
                    <span class="help-block">
                        <strong>{{ $errors->first('student_dob') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('student_gender') ? ' has-error' : '' }}">
            <label for="student_gender" class="col-md-4 control-label">Student's Gender</label>

            <div class="col-md-6">
                <input id="student_gender" type="text" class="form-control" name="student_gender" value="{{ $studentdetail->student_gender }}" autofocus>

                @if ($errors->has('student_gender'))
                    <span class="help-block">
                        <strong>{{ $errors->first('student_gender') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('student_username') ? ' has-error' : '' }}">
            <label for="student_username" class="col-md-4 control-label">Student's Username</label>

            <div class="col-md-6">
                <input id="student_username" type="text" class="form-control" name="student_username" value="{{ $studentdetail->student_username }}" autofocus>

                @if ($errors->has('student_username'))
                    <span class="help-block">
                        <strong>{{ $errors->first('student_username') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('student_password') ? ' has-error' : '' }}">
            <label for="student_password" class="col-md-4 control-label">student's password</label>

            <div class="col-md-6">
                <input id="student_password" type="text" class="form-control" name="student_password" value="{{ $studentdetail->student_password }}" autofocus>

                @if ($errors->has('student_password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('student_password') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <div class="form-group{{ $errors->has('student_email') ? ' has-error' : '' }}">
            <label for="student_email" class="col-md-4 control-label">Student's Email</label>

            <div class="col-md-6">
                <input id="student_email" type="text" class="form-control" name="student_email" value="{{ $studentdetail->student_email }}" autofocus>

                @if ($errors->has('student_email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('student_email') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <div class="form-group{{ $errors->has('student_mob1') ? ' has-error' : '' }}">
            <label for="student_mob1" class="col-md-4 control-label">Student's Mobile 1</label>

            <div class="col-md-6">
                <input id="student_mob1" type="text" class="form-control" name="student_mob1" value="{{ $studentdetail->student_mob1 }}" autofocus>

                @if ($errors->has('student_mob1'))
                    <span class="help-block">
                        <strong>{{ $errors->first('student_mob1') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('student_mob2') ? ' has-error' : '' }}">
            <label for="student_mob2" class="col-md-4 control-label">Student's Mobile 2</label>

            <div class="col-md-6">
                <input id="student_mob2" type="text" class="form-control" name="student_mob2" value="{{ $studentdetail->student_mob2 }}" autofocus>

                @if ($errors->has('student_mob2'))
                    <span class="help-block">
                        <strong>{{ $errors->first('student_mob2') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('student_rollno') ? ' has-error' : '' }}">
            <label for="student_rollno" class="col-md-4 control-label">Students Roll No</label>

            <div class="col-md-6">
                <input id="student_rollno" type="text" class="form-control" name="student_rollno" value="{{ $studentdetail->student_rollno }}" autofocus>

                @if ($errors->has('student_rollno'))
                    <span class="help-block">
                        <strong>{{ $errors->first('student_rollno') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    Update it
                </button>
            </div>
        </div>

    </form>
</div>

@endsection
