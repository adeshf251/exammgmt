@extends('layouts.app')

@section('content')

<div class="panel-heading">Register</div>

<div class="panel-body">
    <form class="form-horizontal" method="POST" action="{{ URL('resultdate') }}/{{$id}}">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PUT">


        <div class="form-group{{ $errors->has('result_date') ? ' has-error' : '' }}">
            <label for="result_date" class="col-md-4 control-label">Result Date (DD/MM/YYYY, ex- 25/09/2017)</label>

            <div class="col-md-6">
                <input id="result_date" type="text" class="form-control" name="result_date" value="{{ $resultdatelist->result_date }}" autofocus>

                @if ($errors->has('result_date'))
                    <span class="help-block">
                        <strong>{{ $errors->first('result_date') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    Update it
                </button>
            </div>
        </div>

    </form>
</div>

@endsection
