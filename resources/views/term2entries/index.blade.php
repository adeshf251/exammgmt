@extends('layouts.app')

@section('content')

<!-- will be used to show any messages -->
@if(Session('message'))
    <div class="alert alert-info">{{ Session('message') }}</div>
@endif

<h3>Showing the Details of Uploaded Admission No with Subject Code  </h3>

<a href="{{ URL::to('term2entries/create/findstudent') }}">
    <button type="button" class="btn btn-primary" style="margin: 10px 0px 0px 10px;">Upload/Modify New Students Marks</button>
</a>



<div class="panel-body" style="overflow-x: scroll;">
    <table width="100%" class="table table-striped table-bordered table-hover" id="AdvancedTable">
            <thead>
                <th> ID </th>
                <th> Admission No </th>
                <th> Subject Code </th>
                <th> Subject Name </th>
                <th> Class </th>
                <th> Session Year </th>
                
            </thead>
        @foreach ($markslist as $element)
            <tr>
                <td> {{ $element->id }} </td> 
                <td> {{ $element->admission_no }} </td> 
                <td> {{ $element->subject_code }} </td> 
                <td> {{ $element->subject_name }} </td> 
                <td> {{ $element->student_class_section }} </td>
                <td> {{ $element->valid_period }} </td>
            </tr>
        @endforeach
    </table>
</div>


@endsection

