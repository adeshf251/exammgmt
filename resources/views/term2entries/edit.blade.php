<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
        div {
            text-transform: capitalize;
        }
    </style>
</head>
<body>

<form class="form-horizontal" method="POST" action="{{ URL('term2entries') }}/{{$id}}">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PUT">


    <div class="admitcard-mainblock" style="width: 250mm; height: 350mm;">
        <div class="admitcard-block"  style="background-image: url( '{{URL('/')}}/defaultimages/dellmond_term2.png' ); background-size: 100% 110%;">
                <div class="AdmitCardTop" style="width: 100%; height: 48mm;">

                </div>
                <div class="AdmitCardTop" style="width: 100%; height: 15mm; text-align: center;">
                   <span style="font-size: 20px; font-weight: bold;"> Class : {{$class_section}} </span> <br>
                   <span style="font-size: 20px; font-weight: bold;"> Academic Session : {{ Session('valid_period') }} (Term 2) </span>
                   <input id="hclass_section" type="hidden" name="hclass_section" value="{{$class_section}}" >
                </div>
                <div class="AdmitCardTop" style="width: 100%; height: 30mm;">

                    <div style="width: 60%; height: 100%; float: left;">
                      @foreach ($studentdetails as $element)
                        <table class="table" style="padding-left: 20px;">
                              <tr>
                                <td  style="border:none; width:125px">Students's Name</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">
                                   <input type="text" value="{{$element->student_name}}" disabled="disabled">
                                </td>
                              </tr>

                              <tr>
                                <td  style="border:none; width:125px">Mother's Name</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">
                                  <input type="text" value="{{$element->student_mother_name}}" disabled="disabled">
                                </td>
                              </tr>

                              <tr>
                                <td  style="border:none; width:125px">Father's Name</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">
                                <input type="text" value="{{$element->student_father_name}}" disabled="disabled">
                                </td>
                              </tr>

                              
                        </table>
                    </div>

                    <div style="width: 38%; height: 100%; float: left;">
                        <table class="table">
                              <tr>
                                <td  style="border:none; width:100px">Admission No</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">
                                  <input type="text" value="{{$element->admission_no}}" disabled="disabled">
                                  <input id="hadmission_no" type="hidden" name="hadmission_no" value="{{$element->admission_no}}" >
                                </td>
                              </tr>
                              <tr>
                                <td  style="border:none; width:100px">Roll No</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">
                                  <input type="text" value="{{$element->student_rollno}}" disabled="disabled">
                                </td>
                              </tr>

                              <tr>
                                <td  style="border:none; width:125px">Date of Birth</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">
                                  <input type="text" value="{{$element->student_dob}}" disabled="disabled">
                                </td>
                              </tr>
                        </table>
                    </div>
                  @endforeach
                </div>

                <div class="AdmitCardTop" style="width: 100%; height: 147mm;">
                    
                    <table style="width: 95%; margin-left: 20px; text-align: center; border-collapse: collapse; border: 1px solid black;">

                            <tr style="background-color: #4CAF50; border: 1px solid black;">
                              <td colspan="2" style="border: 1px solid black;">Scholastic Area</td>
                              <td colspan="6" style="border: 1px solid black;">Term 1 Marks</td>
                            </tr>

                            <tr style="background-color: skyblue; border: 1px solid black;">
                              <td  style="border: 1px solid black;">SNo.</td>
                              <td  style="border: 1px solid black; width:275px">Sub Name</td>
                              <td  colspan="2" style="border: 1px solid black;">Unit Test Marks</td>
                              <td  colspan="2" style="border: 1px solid black;">Exam Marks</td>
                              <td  colspan="2" style="border: 1px solid black;">Total</td>
                            </tr>

                            @foreach ($subjectlist as $element)
                                <tr>
                                    
                                    <td>
                                          <input style="width: 55px;" type="text" name="dsubject_code[]" value="{{$element->subject_code}}" disabled="disabled">
                                          <input style="width: 55px;" type="hidden" name="hsubject_code[]" value="{{$element->subject_code}}">


                                          <input style="width: 55px;" type="hidden" name="hmarks_id[]" value="{{$element->id}}">
                                    </td>
                                    <td>
                                          <input style="width: 275px;" type="text" name="subject_name[]" value="{{$element->subject_name}}" disabled="disabled">
                                          <input style="width: 275px;" type="hidden" name="hsubject_name[]" value="{{$element->subject_name}}" >
                                    </td>
                                    <td>
                                          <input style="width: 55px;" type="number" name="dunit[]" min="-1" max="{{$element->unittest_max_term2}}" value="{{$element->unittest_obtained_term2}}">
                                    </td>
                                    <td>
                                          <input style="width: 55px;" type="text" name="dunitmax[]" value="{{$element->unittest_max_term2}}" disabled="disabled">
                                          <input style="width: 55px;" type="hidden" name="hunitmax[]" value="{{$element->unittest_max_term2}}" >
                                    </td>
                                    <td>
                                          <input style="width: 55px;" type="number" name="dexam[]" min="-1" max="{{$element->exam_max_term2}}" value="{{$element->exam_obtained_term2}}">
                                    </td>
                                    <td>
                                          <input style="width: 55px;" type="text" name="dexammax[]" value="{{$element->exam_max_term2}}" disabled="disabled">
                                          <input style="width: 55px;" type="hidden" name="hexammax[]" value="{{$element->exam_max_term2}}">
                                    </td>
                                    <td>
                                          <input style="width: 55px;" type="text" name="dtotal[]" disabled="disabled">
                                    </td>
                                    <td>
                                          <input style="width: 55px;" type="text" name="dtotalmax[]" value="{{$element->total_max_term2}}" disabled="disabled">
                                    </td>
                                    
                                  </tr>
                            @endforeach
                    </table>
                    
                    <div style="width: 100%; height: 3mm;"> </div>

                    <div style="width: 100%; height: 80mm;">
                        <div style="width: 50%; float: left; height: 100%">

                            <table style="width: 90%; margin-left: 20px; text-align: center; border-collapse: collapse; border: 1px solid black; font-size: 13px; height: 100%">
                                    <tr style="background-color: #4CAF50; border: 1px solid black;">
                                      <td colspan="3" style="border: 1px solid black;">Co-Scholastic Area</td>
                                    </tr>

                                    <tr style="background-color: skyblue; border: 1px solid black;">
                                      <td  style="border: 1px solid black;">SNo.</td>
                                      <td  style="border: 1px solid black;">Elements</td>
                                      <td  style="border: 1px solid black;">Grade</td>
                                    </tr>

                                    @foreach ($coscholasticlist as $row)
                                <tr>
                                    
                                    <td>
                                          <input style="width: 50px;" type="text" name="dcoscholastic_code[]" value="{{$row->coscholastic_code}}" disabled="disabled">
                                          <input style="width: 50px;" type="hidden" name="hcoscholastic_code[]" value="{{$row->coscholastic_code}}">

                                          <input style="width: 50px;" type="hidden" name="hcoscholastic_id[]" value="{{$row->id}}">
                                    </td>
                                    <td>
                                          <input style="width: 200px;" type="text" name="coscholastic_name[]" value="{{$row->coscholastic_name}}" disabled="disabled">
                                          <input style="width: 200px;" type="hidden" name="hcoscholastic_name[]" value="{{$row->coscholastic_name}}" >
                                    </td>
                                    <td>
                                          <select style="width: 50px;" name="coscholastic_grade[]">
                                              <option value="{{$row->grade_term2}}" selected="selected"> {{$row->grade_term2}} </option>
                                              <option value="A"> A </option>
                                              <option value="B"> B </option>
                                              <option value="C"> C </option>
                                              <option value="D"> D </option>
                                          </select>
                                    </td>                                    
                                  </tr>

                              @endforeach
                            </table>

                        </div>
                        <div style="width: 50%; float: left; height: 100%;">
                            <table style="width: 90%; margin-left: 20px; text-align: center; border-collapse: collapse; border: 1px solid black; font-size: 13px; height: 100%">
                                    <tr style="background-color: #4CAF50; border: 1px solid black;">
                                      <td colspan="3" style="border: 1px solid black;">Discipline</td>
                                    </tr>

                                    <tr style="background-color: skyblue; border: 1px solid black;">
                                      <td  style="border: 1px solid black;">SNo.</td>
                                      <td  style="border: 1px solid black;">Activity</td>
                                      <td  style="border: 1px solid black;">Grade</td>
                                    </tr>

                                    @foreach ($disciplinelist as $row)
                                <tr>
                                    <td>
                                          <input style="width: 50px;" type="text" name="ddiscipline_code[]" value="{{$row->discipline_code}}" disabled="disabled">
                                          <input style="width: 50px;" type="hidden" name="hdiscipline_code[]" value="{{$row->discipline_code}}">

                                          <input style="width: 50px;" type="hidden" name="hdiscipline_id[]" value="{{$row->id}}">
                                    </td>
                                    <td>
                                          <input style="width: 200px;" type="text" name="discipline_name[]" value="{{$row->discipline_name}}" disabled="disabled">
                                          <input style="width: 200px;" type="hidden" name="hdiscipline_name[]" value="{{$row->discipline_name}}" >
                                    </td>
                                    <td>
                                          <select style="width: 50px;" name="discipline_grade[]">
                                              <option value="{{$row->grade_term2}}" selected="selected"> {{$row->grade_term2}} </option>
                                              <option value="A"> A </option>
                                              <option value="B"> B </option>
                                              <option value="C"> C </option>
                                              <option value="D"> D </option>
                                          </select>
                                    </td>                                    
                                </tr>
                              @endforeach
                            </table>
                        </div>
                    </div>

                     <div style="width: 190mm; height: 5mm;"> </div>

                     <div class="AdmitCardTop" style="width: 100%; height: 15mm;">

                    <div style="width: 60%; height: 100%; float: left; font-size:14px">
                        <table class="table" style="padding-left: 20px;">
                              <tr>
                                <td  style="border:none; width:200px">Overall Marks Obtained</td>
                                <td style="width:10px"> : </td>
                                <td>
                                      @foreach ($Overallmarksmgmt as $element)
                                                  {{ ($element->marks_term2_obtained) }}
                                      @endforeach
                                </td>
                              </tr>

                              <tr>
                                <td  style="border:none; width:100px">Percentage</td>
                                <td style="width:10px"> : </td>
                                <td>
                                      @foreach ($Overallmarksmgmt as $element)
                                                  @if ($element->marks_term2_total > 0)
                                                    {{ round((($element->marks_term2_obtained)/($element->marks_term2_total))*100,2) }}
                                                  @endif

                                                  <input type="hidden" name="hoverallmarksid" value="{{ $element->id }}">

                                                  <input type="hidden" name="hmarks_term1_obtained" value="{{ $element->marks_term1_obtained }}">
                                                  <input type="hidden" name="hmarks_term1_total" value="{{ $element->marks_term1_total }}">
                                      @endforeach
                                </td>
                              </tr>
                        </table>
                    </div>

                    <div style="width: 38%; height: 100%; float: left;  font-size:14px">
                        <table class="table">
                              <tr>
                                <td  style="border:none; width:70px">Attendance </td>
                                <td style="width:10px"> : </td>
                                <td>
                                    @foreach ($attendance as $element)
                                      Obt<input style="width: 20px;" type="text" name="attendence" min="0" max="100" value="{{ $element->attendance_obtained_term2 }}">

                                      Total<input style="width: 40px;" type="text" name="attendencetotal" value="{{ $element->attendance_total_term2 }}">



                                      <input type="hidden" name="hattendence_term1" value="{{ $element->attendance_obtained }}">

                                      <input type="hidden" name="hattendenceid" value="{{ $element->id }}">
                                      <input type="hidden" name="hattendence_term1_total" value="{{ $element->attendance_total }}">
                                    @endforeach
                                    
                                </td>
                              </tr>
                              <tr>
                                <td  style="border:none; width:100px">Class Rank</td>
                                <td style="width:10px"> : </td>
                                <td>1</td>
                              </tr>
                        </table>
                    </div>

                    <table style="width: 95%; margin-left: 20px; text-align: center; border-collapse: collapse; border: 1px solid black;">

                            <tr style="border: 1px solid black;"">
                              <td style="width: 20%; height: 50px; background-color: #4CAF50; " >Remark</td>
                              <td style="width: 80%; height: 50px;"> 
                                          <select name="resultstatus">
                                              <option value="Passed" selected="selected"> Passed </option>
                                              <option value="Promoted"> Promoted </option>
                                              <option value="Failed"> Failed </option>
                                              <option value="Passed with Distinction"> Passed with Distinction </option>
                                          </select>
                              </td>
                            </tr>

                    </table>
                </div>

                

                </div>

                <hr>

                <div class="AdmitCardTop" style="width: 100%; height: 40mm; float:left">
                    <div class="AdmitCardTop" style="width: 100%; height: 30mm;"> </div>
                    <div class="AdmitCardTop" style="width: 100%; height: 5mm; margin-left: 20px;">
                      Date : @foreach ($resultdate as $element)
                                        {{ $element->result_date }}
                            @endforeach
                    </div>
                </div>
                
        </div>
    </div>




                <button type="submit" style="margin-left: 75mm; height: 15mm; background-color: red" class="btn btn-primary">
                    Upload/Update MARKS
                </button>
            </form>
</body>
</html>
