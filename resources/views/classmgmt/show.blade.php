@extends('layouts.app')

@section('content')

                <div class="panel-heading">Showing the List of Registered Classes </div>

                <div class="panel-body">
                    <table class="table">
                            <thead>
                                <th> Class ID </th>
                                <th> Class Name </th>
                                <th> Description </th>
                                <th> Action </th>
                            </thead>
                        @foreach ($classes as $element)
                            <tr>
                                <td> {{ $element->id }} </td> 
                                <td> {{ $element->class_section }} </td> 
                                <td> {{ $element->description }} </td> 
                                <td> 
                                    <a href="{{ URL::to('classmgmt/' . $element->id . '/edit') }}"> <img src="{{URL('/')}}/icons/edit.png"> </a>  &nbsp; &nbsp; 
                                <img src="{{URL('/')}}/icons/delete.png"> </td> 
                            </tr>
                        @endforeach
                    </table>
                </div>

@endsection
