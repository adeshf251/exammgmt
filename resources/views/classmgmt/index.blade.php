@extends('layouts.app')

@section('content')

<!-- will be used to show any messages -->
@if(Session('message'))
    <div class="alert alert-info">{{ Session('message') }}</div>
@endif

<h3>Showing the List of Registered Classes </h3>

<a href="{{ URL::to('classmgmt/create') }}">
    <button type="button" class="btn btn-primary"  style="margin: 10px 0px 0px 10px;">Add New Class</button>
</a>

<div class="panel-body">

    <table width="100%" class="table table-striped table-bordered table-hover" id="AdvancedTable">

            <thead>
                <tr>
                    <th class="priority"> Class ID </th>
                    <th class="priority"> Class Name </th>
                    <th class="priority"> Description </th>
                    <th>  </th>
                    <th>  </th>
                </tr>
            </thead>
            <tbody>

           
        @foreach ($classmgmt as $element)
            
            <tr>
                <td> {{ $element->id }} </td> 
                <td> {{ $element->class_section }} </td> 
                <td> {{ $element->description }} </td> 
                <td> 
                    <a href="{{ URL::to('classmgmt/' . $element->id . '/edit') }}">
                   
                    <input type="button" class="btn btn-info  btn-sm" name="EDIT" id="" value="EDIT">
                    </a>  &nbsp; 
                
                </td>
                <td>
                <form method="POST" action="{{ URL('classmgmt') }}/{{$element->id}}">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="DELETE">
                <input type="submit" class="btn btn-danger  btn-sm" name="submit" id="{{$element->id}}" value="Delete">
                </form>

                </td>
                
            </tr>
            
        @endforeach
       
    </tbody>
    </table>
</div>
          

@endsection

