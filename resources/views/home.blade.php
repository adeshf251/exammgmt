<?php

use \App\Sessionmanager as SessionManager;

$session = SessionManager::all();

if( Session('valid_id') and Session('valid_period')  )
{
   redirect('dashboard');
}

?>


@extends('layouts.welcomelayout')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">Please Select Before Proceeding</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ URL('/') }}/security-check">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('activate_session') ? ' has-error' : '' }}">
                            <label for="activate_session" class="col-md-4 control-label">Select Session </label>

                            <div class="col-md-6">
                                <select  id="activate_session" class="form-control" name="activate_session" >
                                    @foreach ($session as $element)
                                         <option value="{{$element->id}}"> {{$element->session_period}}</option>
                                    @endforeach
                                                                   </select>

                                @if ($errors->has('activate_session'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('activate_session') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Proceed
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
