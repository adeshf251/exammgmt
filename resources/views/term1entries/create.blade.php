<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
        div {
            text-transform: capitalize;
        }
    </style>
</head>
<body>
<form class="form-horizontal" method="POST" action="{{ route('term1entries.store') }}">
        {{ csrf_field() }}
    <div class="admitcard-mainblock" style="width: 200mm; height: 280mm;">
        <div class="admitcard-block"  style="background-image: url( '{{URL('/')}}/defaultimages/dellmond_term1.png' ); background-size: 100% 100%;">
                <div class="AdmitCardTop" style="width: 100%; height: 48mm;">

                </div>
                <div class="AdmitCardTop" style="width: 100%; height: 15mm; text-align: center;">
                   <span style="font-size: 20px; font-weight: bold;"> Class : {{$class_section}} </span> <br>
                   <span style="font-size: 20px; font-weight: bold;"> Academic Session : {{ Session('valid_period') }} (Term 1) </span>
                   <input id="hclass_section" type="hidden" name="hclass_section" value="{{$class_section}}" >
                </div>
                <div class="AdmitCardTop" style="width: 100%; height: 30mm;">

                    <div style="width: 60%; height: 100%; float: left; font-size: 14px;">
                      @foreach ($studentdetails as $element)
                        <table class="table" style="padding-left: 20px;">
                              <tr>
                                <td  style="border:none; width:125px">Students's Name</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">
                                   <input type="text" value="{{$element->student_name}}" disabled="disabled">
                                </td>
                              </tr>

                              <tr>
                                <td  style="border:none; width:125px">Mother's Name</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">
                                  <input type="text" value="{{$element->student_mother_name}}" disabled="disabled">
                                </td>
                              </tr>

                              <tr>
                                <td  style="border:none; width:125px">Father's Name</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">
                                <input type="text" value="{{$element->student_father_name}}" disabled="disabled">
                                </td>
                              </tr>

                              
                        </table>
                    </div>

                    <div style="width: 38%; height: 100%; float: left; font-size: 14px;">
                        <table class="table">
                              <tr>
                                <td  style="border:none; width:100px">Admission No</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">
                                  <input type="text" value="{{$element->admission_no}}" disabled="disabled">
                                  <input id="hadmission_no" type="hidden" name="hadmission_no" value="{{$element->admission_no}}" >
                                </td>
                              </tr>
                              <tr>
                                <td  style="border:none; width:100px">Roll No</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">
                                  <input type="text" value="{{$element->student_rollno}}" disabled="disabled">
                                </td>
                              </tr>
                              <tr>
                                <td  style="border:none; width:125px">Date of Birth</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">
                                  <input type="text" value="{{$element->student_dob}}" disabled="disabled">
                                </td>
                              </tr>
                        </table>
                    </div>
                  @endforeach
                </div>

                <div class="AdmitCardTop" style="width: 100%; height: 147mm;">
                    
                    <table style="width: 95%; margin-left: 20px; text-align: center; border-collapse: collapse; border: 1px solid black;">

                            <tr style="background-color: #4CAF50; border: 1px solid black;">
                              <td colspan="2" style="border: 1px solid black;">Scholastic Area</td>
                              <td colspan="6" style="border: 1px solid black;">Term 1 Marks</td>
                            </tr>

                            <tr style="background-color: skyblue; border: 1px solid black;">
                              <td  style="border: 1px solid black;">SNo.</td>
                              <td  style="border: 1px solid black; width:275px">Sub Name</td>
                              <td  colspan="2" style="border: 1px solid black;">Unit Test Marks</td>
                              <td  colspan="2" style="border: 1px solid black;">Exam Marks</td>
                              <td  colspan="2" style="border: 1px solid black;">Total</td>
                            </tr>

                            @foreach ($subjectlist as $element)
                                <tr>
                                    
                                    <td>
                                          <input style="width: 55px;" type="text" name="dsubject_code[]" value="{{$element->subject_code}}" disabled="disabled">
                                          <input style="width: 55px;" type="hidden" name="hsubject_code[]" value="{{$element->subject_code}}">
                                    </td>
                                    <td>
                                          <input style="width: 225px;" type="text" name="subject_name[]" value="{{$element->subject_name}}" disabled="disabled">
                                          <input style="width: 225px;" type="hidden" name="hsubject_name[]" value="{{$element->subject_name}}" >
                                    </td>
                                    <td>
                                          <input style="width: 55px;" type="number" name="dunit[]" value="" min="-1" max="{{$element->term1_unittest}}">
                                    </td>
                                    <td>
                                          <input style="width: 55px;" type="text" name="dunitmax[]" value="{{$element->term1_unittest}}" disabled="disabled">
                                          <input style="width: 55px;" type="hidden" name="hunitmax[]" value="{{$element->term1_unittest}}" >
                                    </td>
                                    <td>
                                          <input style="width: 55px;" type="number" name="dexam[]" value="" min="-1" max="{{$element->term1_exam}}">
                                    </td>
                                    <td>
                                          <input style="width: 55px;" type="text" name="dexammax[]" value="{{$element->term1_exam}}" disabled="disabled">
                                          <input style="width: 55px;" type="hidden" name="hexammax[]" value="{{$element->term1_exam}}">
                                    </td>
                                    <td>
                                          <input style="width: 55px;" type="text" name="dtotal[]" value="" disabled="disabled">
                                    </td>
                                    <td>
                                          <input style="width: 55px;" type="text" name="dtotalmax[]" value="{{$element->term1_total}}" disabled="disabled">
                                    </td>
                                    
                                  </tr>

                                  <input type="hidden" name="hunitmax_term2[]" value="{{$element->term2_unittest}}" >
                                  <input type="hidden" name="hexammax_term2[]" value="{{$element->term2_exam}}">

                            @endforeach
                    </table>
                    
                    <div style="width: 100%; height: 3mm;"> </div>

                    <div style="width: 100%; height: 60mm;">
                        <div style="width: 50%; float: left; height: 100%">

                            <table style="width: 90%; margin-left: 20px; text-align: center; border-collapse: collapse; border: 1px solid black; font-size: 13px; height: 100%">
                                    <tr style="background-color: #4CAF50; border: 1px solid black;">
                                      <td colspan="3" style="border: 1px solid black;">Co-Scholastic Area</td>
                                    </tr>

                                    <tr style="background-color: skyblue; border: 1px solid black;">
                                      <td  style="border: 1px solid black;">SNo.</td>
                                      <td  style="border: 1px solid black;">Elements</td>
                                      <td  style="border: 1px solid black;">Grade</td>
                                    </tr>

                                    @foreach ($coscholasticlist as $row)
                                <tr>
                                    
                                    <td>
                                          <input style="width: 50px;" type="text" name="dcoscholastic_code[]" value="{{$row->coscholastic_code}}" disabled="disabled">
                                          <input style="width: 50px;" type="hidden" name="hcoscholastic_code[]" value="{{$row->coscholastic_code}}">
                                    </td>
                                    <td>
                                          <input style="width: 200px;" type="text" name="coscholastic_name[]" value="{{$row->coscholastic_name}}" disabled="disabled">
                                          <input style="width: 200px;" type="hidden" name="hcoscholastic_name[]" value="{{$row->coscholastic_name}}" >
                                    </td>
                                    <td>
                                          <select style="width: 50px;" name="coscholastic_grade[]">
                                              <option value="A"> A </option>
                                              <option value="B"> B </option>
                                              <option value="C"> C </option>
                                              <option value="D"> D </option>
                                          </select>
                                    </td>                                    
                                  </tr>

                              @endforeach
                            </table>

                        </div>
                        <div style="width: 50%; float: left; height: 100%;">
                            <table style="width: 90%; margin-left: 20px; text-align: center; border-collapse: collapse; border: 1px solid black; font-size: 13px; height: 100%">
                                    <tr style="background-color: #4CAF50; border: 1px solid black;">
                                      <td colspan="3" style="border: 1px solid black;">Discipline</td>
                                    </tr>

                                    <tr style="background-color: skyblue; border: 1px solid black;">
                                      <td  style="border: 1px solid black;">SNo.</td>
                                      <td  style="border: 1px solid black;">Activity</td>
                                      <td  style="border: 1px solid black;">Grade</td>
                                    </tr>

                                    @foreach ($disciplinelist as $row)
                                <tr>
                                    <td>
                                          <input style="width: 50px;" type="text" name="ddiscipline_code[]" value="{{$row->discipline_code}}" disabled="disabled">
                                          <input style="width: 50px;" type="hidden" name="hdiscipline_code[]" value="{{$row->discipline_code}}">
                                    </td>
                                    <td>
                                          <input style="width: 200px;" type="text" name="discipline_name[]" value="{{$row->discipline_name}}" disabled="disabled">
                                          <input style="width: 200px;" type="hidden" name="hdiscipline_name[]" value="{{$row->discipline_name}}" >
                                    </td>
                                    <td>
                                          <select style="width: 50px;" name="discipline_grade[]">
                                              <option value="A"> A </option>
                                              <option value="B"> B </option>
                                              <option value="C"> C </option>
                                              <option value="D"> D </option>
                                          </select>
                                    </td>                                    
                                </tr>
                              @endforeach
                            </table>
                        </div>
                    </div>

                     <div style="width: 190mm; height: 5mm;"> </div>

                     <div class="AdmitCardTop" style="width: 100%; height: 15mm;">

                  <div style="width: 100%; height: auto; margin-top: 30px; font-size: 12px">
                    <div style="width: 60%; height: 100%; float: left;">
                        <table class="table" style="padding-left: 20px;">
                              <tr>
                                <td  style="border:none; width:200px">Overall Marks Obtained</td>
                                <td style="width:10px"> : </td>
                                <td>******</td>
                              </tr>

                              <tr>
                                <td  style="border:none; width:200px">Percentage</td>
                                <td style="width:10px"> : </td>
                                <td>******</td>
                              </tr>
                        </table>
                    </div>

                    <div style="width: 38%; height: 100%; float: left;">
                        <table class="table">
                              <tr>
                                <td  style="border:none; width:70px">Attendance </td>
                                <td style="width:10px"> : </td>
                                <td>
                                    Obt<input style="width: 40px;" type="text" name="attendence" min="0">
                                    Total<input style="width: 40px;" type="text" name="attendencetotal" min="0">
                                </td>
                              </tr>
                              <tr>
                                <td  style="border:none; width:100px">Class Rank</td>
                                <td style="width:10px"> : </td>
                                <td>**</td>
                              </tr>
                        </table>
                    </div>
                  </div>

                    <table style="width: 95%; margin-left: 20px; text-align: center; border-collapse: collapse; border: 1px solid black;">

                            <tr style="border: 1px solid black;"">
                              <td style="width: 20%; height: 50px; background-color: #4CAF50; " >Remark</td>
                              <td style="width: 80%; height: 50px;"> </td>
                            </tr>

                    </table>
                </div>

                

                </div>
                <div class="AdmitCardTop" style="width: 100%; height: 40mm;">
                    <div class="AdmitCardTop" style="width: 100%; height: 30mm;"> </div>
                    <div class="AdmitCardTop" style="width: 100%; height: 5mm; margin-left: 20px;">
                      Date : @foreach ($resultdate as $element)
                                        {{ $element->result_date }}
                            @endforeach </div>
                </div>
                
        </div>
    </div>




                <button type="submit" style="margin-left: 75mm; height: 15mm; background-color: red" class="btn btn-primary">
                    Upload/Update MARKS
                </button>
            </form>
</body>
</html>
