@extends('layouts.app')

@section('content')

<!-- will be used to show any messages -->
@if(Session('message'))
    <div class="alert alert-info">{{ Session('message') }}</div>
@endif

<h3>Print Admit Cards </h3>

<a href="{{ URL::to('/') }}/customadmitcardmgmt"><button type="button" class="btn btn-primary">Generate Custom Admit Card by Values</button></a>



<div class="panel-body">
    <table width="100%" class="table table-striped table-bordered table-hover" id="AdvancedTable">
            <thead>
                <th> Class-Section </th>
                <th> Term1 Admit Card </th>
                <th> Term2 Admit Card </th>
            </thead>
        @foreach ($classlist as $element)
            <tr>
                <td> {{$element}} </td> 
                <td> <a href="{{ URL('/') }}/admit-card/{{$element}}/term1">{{$element}} Term 1 (Half Yearly Exam)</a>  </td> 
                <td> <a href="{{ URL('/') }}/admit-card/{{$element}}/term2">{{$element}} Term 2 (Annual Exam)</a> </td>                                 
            </tr>
        @endforeach
    </table>
</div>
            

@endsection

