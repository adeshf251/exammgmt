<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="{{URL('/')}}/css/admitcard.css">
    <style type="text/css">
        div {
            text-transform: capitalize;
        }
    </style>
</head>
<body>


<?php  $value='ABCDEFGHIJKLM NOPQRSTUVWXY ZABCDEFG HIJKLM NOPQRSTUVWXYZ'; ?>

    @foreach ($studentlist as $element)
        <div class="admitcard-mainblock">
        <div class="admitcard-block"  style="background-image: url( '{{URL('/')}}/images/watermark.png' );">
                <div class="AdmitCardTop">
                    <div  class="AdmitCardTop1" style="">
                        <img src="{{URL('/')}}/images/logo.jpg" style="width: auto; height: 100%;">
                    </div>
                    <div  class="AdmitCardTop2" style="">
                            <span class="SpanLine1" style=""> Dellmond International School </span> <br>
                            <span class="SpanLine2" style=""> Dehradun Road Near Fatehpur Thana Chhutmalpur </span>
                    </div>
                    <div  class="AdmitCardTop3" style="">
                         <table>
                             <tr> <td colspan="2" style="font-size: 19px; font-weight: bold;"> Admit Card </td></tr>
                             <tr> <td> Adm.No. </td> <td> {{$element->admission_no}} </td></tr>
                         </table>
                    </div>
                </div>
                <div class="AdmitCardContentLeft" style="">
                    <table style="font-size: 15px; width: 100%;">
                        
                    <tr> <td><b> St. Name   </b></td><td>:</td><td><b> {{ substr($element->student_name, 0, 30 )}} </b> </td> </tr>
                    <tr> <td><b> M. Name   </b></td><td>:</td><td><b> {{ substr($element->student_mother_name, 0, 30 )}} </b> </td> </tr>
                    <tr> <td><b> F. Name   </b></td><td>:</td><td><b> {{ substr($element->student_father_name, 0, 30 )}} </b> </td> </tr>
                    <tr> <td><b> Roll No   </b></td><td>:</td><td><b> {{ substr($element->student_rollno, 0, 30 )}} </b> </td> </tr>
                    <tr> <td><b> Class     </b></td><td>:</td><td><b> {{ substr($element->student_class_section, 0, 30 )}} </b> </td> </tr>
                    <tr> <td><b> Exam Term  </b></td><td>:</td><td><b> {{ substr('Half Yearly Exam', 0, 30 )}} </b> </td> </tr>
                    <tr> <td><b> Session    </b></td><td>:</td><td><b> {{ substr('2017-2018', 0, 30 )}} </b> </td> </tr>
                        
                    </table>
                    <div class="AdmitCardContentLeftbottom" style="">



                        <div class="AdmitCardContentLeftbottom1" style=""> </div>
                        <div class="AdmitCardContentLeftbottom2" style=""> <img src="{{URL('/')}}/images/principal.png" style="height: 100%;"> </div>
                        <div class="AdmitCardContentLeftbottom3" style=""> Student's Sign </div>
                        <div class="AdmitCardContentLeftbottom4" style=""> Principal's Sign </div>
                            
                    </div>
                </div>

                <div class="AdmitCardContentRight" style="">
                    <div class="r1" style="width: 89mm; height: 59mm; float: left;">
                        <table style="font-size: 13px; font-weight: bold; text-align:center; width: 100%;">
                        
                            <thead>
                                <tr>
                                    <td><b>     Subject          </b></td>
                                    <td></td>
                                    <td><b>     Date             </b></td>
                                </tr>
                            </thead>
                            <tbody>     <?php $counter = 0;?>
                                @foreach ($subjectlist as $subjectvalue)
                                    <?php $counter++; ?>
                                    @if ($counter<11)
                                        <tr>
                                            <td> {{ substr($subjectvalue->subject_name, 0, 23 )}} </td>
                                            <td> : </td>
                                            <td> {{ substr($subjectvalue->date_term1, 0, 12 )}} </td>
                                        </tr>
                                    @endif
                                    
                                @endforeach
                            </tbody>
                            


                        </table>
                    </div>
                    <div class="r2" style="width: 89mm; height: 6mm; float: left; text-align: center; ">
                        <span> School Seal </span>
                    </div>
                </div>
                
        </div>

        <div class="GapAfterAdmitCard" style="width: 210mm; height: 15mm; "> </div>
    </div>
    @endforeach
    





</body>
</html>
