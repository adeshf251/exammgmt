@extends('layouts.app')

@section('content')

<div class="panel-heading">Register</div>

<div class="panel-body">
     <form class="form-horizontal" method="POST" action="{{ URL('coscholastic') }}/{{$id}}">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PUT">

        <div class="form-group{{ $errors->has('coscholasticcode') ? ' has-error' : '' }}">
            <label for="coscholasticcode" class="col-md-4 control-label">Co-Scholastic Code</label>

            <div class="col-md-6">
                <input id="coscholasticcode" type="text" class="form-control" name="coscholasticcode" value="{{ $coscholasticdetail->coscholastic_code }}"  autofocus>

                @if ($errors->has('coscholasticcode'))
                    <span class="help-block">
                        <strong>{{ $errors->first('coscholasticcode') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('coscholasticname') ? ' has-error' : '' }}">
            <label for="coscholasticname" class="col-md-4 control-label">Co-Scholastic Name</label>

            <div class="col-md-6">
                <input id="coscholasticname" type="text" class="form-control" name="coscholasticname" value="{{ $coscholasticdetail->coscholastic_name }}"  autofocus>

                @if ($errors->has('coscholasticname'))
                    <span class="help-block">
                        <strong>{{ $errors->first('coscholasticname') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <div class="form-group{{ $errors->has('classapplicable') ? ' has-error' : '' }}">
            <label for="classapplicable" class="col-md-4 control-label">Class Applicable</label>

            <div class="col-md-6">
                <select  id="classapplicable" class="form-control" name="classapplicable" >
                         <option value="{{ $coscholasticdetail->class_applicable }}" selected="selected"> {{ $coscholasticdetail->class_applicable }}</option>
                    
                </select>

                @if ($errors->has('classapplicable'))
                    <span class="help-block">
                        <strong>{{ $errors->first('classapplicable') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('extra') ? ' has-error' : '' }}">
            <label for="extra" class="col-md-4 control-label">Any Comment</label>

            <div class="col-md-6">
                <input id="extra" type="text" class="form-control" name="extra" value="{{ $coscholasticdetail->extra }}"  autofocus>

                @if ($errors->has('extra'))
                    <span class="help-block">
                        <strong>{{ $errors->first('extra') }}</strong>
                    </span>
                @endif
            </div>
        </div>



        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    Update it
                </button>
            </div>
        </div>

    </form>
</div>

@endsection
