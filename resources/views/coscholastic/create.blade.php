@extends('layouts.app')

@section('content')

<div class="panel-heading">Register</div>

<div class="panel-body">
    <form class="form-horizontal" method="POST" action="{{ route('coscholastic.store') }}">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('coscholasticcode') ? ' has-error' : '' }}">
            <label for="coscholasticcode" class="col-md-4 control-label">Co-Scholastic Code</label>

            <div class="col-md-6">
                <input id="coscholasticcode" type="text" class="form-control" name="coscholasticcode" value="{{ old('coscholasticcode') }}" autofocus>

                @if ($errors->has('coscholasticcode'))
                    <span class="help-block">
                        <strong>{{ $errors->first('coscholasticcode') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('coscholasticname') ? ' has-error' : '' }}">
            <label for="coscholasticname" class="col-md-4 control-label">Co-Scholastic Name</label>

            <div class="col-md-6">
                <input id="coscholasticname" type="text" class="form-control" name="coscholasticname" value="{{ old('coscholasticname') }}" autofocus>

                @if ($errors->has('coscholasticname'))
                    <span class="help-block">
                        <strong>{{ $errors->first('coscholasticname') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <div class="form-group{{ $errors->has('classapplicable') ? ' has-error' : '' }}">
            <label for="classapplicable" class="col-md-4 control-label">Class Applicable</label>

            <div class="col-md-6">
                <select  id="classapplicable[]" class="form-control" name="classapplicable[]"  multiple="multiple" >
                    @foreach ($classlist as $element)
                         <option value="{{$element}}"> {{$element}}</option>
                    @endforeach
                </select>

                @if ($errors->has('classapplicable'))
                    <span class="help-block">
                        <strong>{{ $errors->first('classapplicable') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('extra') ? ' has-error' : '' }}">
            <label for="extra" class="col-md-4 control-label">Any Comment</label>

            <div class="col-md-6">
                <input id="extra" type="text" class="form-control" name="extra" value="{{ old('extra') }}" autofocus>

                @if ($errors->has('extra'))
                    <span class="help-block">
                        <strong>{{ $errors->first('extra') }}</strong>
                    </span>
                @endif
            </div>
        </div>



        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    Register
                </button>
            </div>
        </div>

    </form>
</div>

@endsection
