@extends('layouts.app')

@section('content')

<div class="panel-heading">Welcome To Dashboard</div>

<div class="panel-body">
  <div class="col-lg-3 ">
        <div class="panel panel-success">
            <div class="panel-heading" style="background-color: #f5b041; border-color: #5cb85c;color: white;">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-users fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div style="font-size: 40px;">{{$students}}</div>
                        <div>Total Students!</div>
                    </div>
                </div>
            </div>
              <a href="#">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>

    <div class="col-lg-3 ">
        <div class="panel panel-success">
            <div class="panel-heading" style="background-color: #2ecc71; border-color: #5cb85c;color: white;">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-book fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div style="font-size: 40px;">{{$subjects}}</div>
                        <div>Total Subjects!</div>
                    </div>
                </div>
            </div>
              <a href="#">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>

    <div class="col-lg-3 ">
        <div class="panel panel-success">
            <div class="panel-heading" style="background-color: maroon; border-color: #5cb85c;color: white;">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-paper-plane fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div style="font-size: 40px;">0</div>
                        <div>Email Sended!</div>
                    </div>
                </div>
            </div>
              <a href="#">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>

    <div class="col-lg-3 ">
        <div class="panel panel-success">
            <div class="panel-heading" style="background-color: #5dade2; border-color: #5cb85c;color: white;">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-comments fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div style="font-size: 40px;">0</div>
                        <div>Upcoming!</div>
                    </div>
                </div>
            </div>
              <a href="#">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
</div>
@endsection
