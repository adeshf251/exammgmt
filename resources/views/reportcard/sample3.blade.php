<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
        div {
            text-transform: capitalize;
        }
    </style>
</head>
<body>
    <div class="admitcard-mainblock" style="width: 200mm; height: 280mm;">
        <div class="admitcard-block"  style="background-image: url( '{{URL('/')}}/defaultimages/dellmond.png' ); background-size: 100% 100%;">
                <div class="AdmitCardTop" style="width: 100%; height: 48mm;">

                </div>
                <div class="AdmitCardTop" style="width: 100%; height: 15mm; text-align: center;">
                   <span style="font-size: 20px; font-weight: bold;"> Class : I-A </span> <br>
                    <span style="font-size: 20px; font-weight: bold;"> Academic Session : 2017-2018 (Term 1 & Term 2) </span>
                </div>
                <div class="AdmitCardTop" style="width: 100%; height: 30mm;">

                    <div style="width: 60%; height: 100%; float: left;">
                        <table class="table" style="padding-left: 20px;">
                              <tr>
                                <td  style="border:none; width:125px">Students's Name</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">Narendra Modi</td>
                              </tr>

                              <tr>
                                <td  style="border:none; width:125px">Mother's Name</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">Heeraben Modi</td>
                              </tr>

                              <tr>
                                <td  style="border:none; width:125px">Father's Name</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">Damodardas Mulchand Modi</td>
                              </tr>

                              <tr>
                                <td  style="border:none; width:125px">Date of Birth</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">10/09/2017</td>
                              </tr>
                        </table>
                    </div>

                    <div style="width: 38%; height: 100%; float: left;">
                        <table class="table">
                              <tr>
                                <td  style="border:none; width:100px">Admission No</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">123123</td>
                              </tr>
                              <tr>
                                <td  style="border:none; width:100px">Roll No</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">1</td>
                              </tr>
                        </table>
                    </div>
                
                </div>

                <div class="AdmitCardTop" style="width: 100%; height: 147mm;">
                    
                    <table style="width: 95%; margin-left: 20px; text-align: center; border-collapse: collapse; border: 1px solid black;">

                            <tr style="background-color: #4CAF50; border: 1px solid black;">
                              <td colspan="2" style="border: 1px solid black;">Scholastic Area</td>
                              <td colspan="3" style="border: 1px solid black;">Term 1 Marks</td>
                              <td colspan="3" style="border: 1px solid black;">Term 2 Marks</td>
                              <td             style="border: 1px solid black;">             </td>
                            </tr>

                            <tr style="background-color: skyblue; border: 1px solid black;">
                              <td  style="border: 1px solid black;">SNo.</td>
                              <td  style="border: 1px solid black; width:275px">Sub Name</td>
                              <td  style="border: 1px solid black;">Unit Test Marks</td>
                              <td  style="border: 1px solid black;">Exam Marks</td>
                              <td  style="border: 1px solid black;">Total</td>
                              <td  style="border: 1px solid black;">Unit Test Marks</td>
                              <td  style="border: 1px solid black;">Exam Marks</td>
                              <td  style="border: 1px solid black;">Total</td>
                              <td  style="border: 1px solid black;">Grand Total</td>
                            </tr>

                            <tr style="border: 1px solid black;">
                              <td  style="border: 1px solid black;">1</td>
                              <td  style="border: 1px solid black;">Doe</td>
                              <td  style="border: 1px solid black;">24</td>
                              <td  style="border: 1px solid black;">78</td>
                              <td  style="border: 1px solid black;">100</td>
                              <td  style="border: 1px solid black;">24</td>
                              <td  style="border: 1px solid black;">78</td>
                              <td  style="border: 1px solid black;">100</td>
                              <td  style="border: 1px solid black;">200</td>
                            </tr>

                            <tr style="border: 1px solid black;">
                              <td  style="border: 1px solid black;">1</td>
                              <td  style="border: 1px solid black;">History and Civics and Science</td>
                              <td  style="border: 1px solid black;">24</td>
                              <td  style="border: 1px solid black;">78</td>
                              <td  style="border: 1px solid black;">100</td>
                              <td  style="border: 1px solid black;">24</td>
                              <td  style="border: 1px solid black;">78</td>
                              <td  style="border: 1px solid black;">100</td>
                              <td  style="border: 1px solid black;">200</td>
                            </tr>

                            <tr style="border: 1px solid black;">
                              <td  style="border: 1px solid black;">1</td>
                              <td  style="border: 1px solid black;">Doe</td>
                              <td  style="border: 1px solid black;">24</td>
                              <td  style="border: 1px solid black;">78</td>
                              <td  style="border: 1px solid black;">100</td>
                              <td  style="border: 1px solid black;">24</td>
                              <td  style="border: 1px solid black;">78</td>
                              <td  style="border: 1px solid black;">100</td>
                              <td  style="border: 1px solid black;">200</td>
                            </tr>

                            <tr style="border: 1px solid black;">
                              <td  style="border: 1px solid black;">1</td>
                              <td  style="border: 1px solid black;">Doe</td>
                              <td  style="border: 1px solid black;">24</td>
                              <td  style="border: 1px solid black;">78</td>
                              <td  style="border: 1px solid black;">100</td>
                              <td  style="border: 1px solid black;">24</td>
                              <td  style="border: 1px solid black;">78</td>
                              <td  style="border: 1px solid black;">100</td>
                              <td  style="border: 1px solid black;">200</td>
                            </tr>

                            <tr style="border: 1px solid black;">
                              <td  style="border: 1px solid black;">1</td>
                              <td  style="border: 1px solid black;">Doe</td>
                              <td  style="border: 1px solid black;">24</td>
                              <td  style="border: 1px solid black;">78</td>
                              <td  style="border: 1px solid black;">100</td>
                              <td  style="border: 1px solid black;">24</td>
                              <td  style="border: 1px solid black;">78</td>
                              <td  style="border: 1px solid black;">100</td>
                              <td  style="border: 1px solid black;">200</td>
                            </tr>

                            <tr style="border: 1px solid black;">
                              <td  style="border: 1px solid black;">1</td>
                              <td  style="border: 1px solid black;">Doe</td>
                              <td  style="border: 1px solid black;">24</td>
                              <td  style="border: 1px solid black;">78</td>
                              <td  style="border: 1px solid black;">100</td>
                              <td  style="border: 1px solid black;">24</td>
                              <td  style="border: 1px solid black;">78</td>
                              <td  style="border: 1px solid black;">100</td>
                              <td  style="border: 1px solid black;">200</td>
                            </tr>

                            <tr style="background-color: ; border: 1px solid black;">
                              <td colspan="9" style="border: 1px solid black;">
                                    Unit Test Max Marks : 25, Exam Max Marks : 100, Total Max Marks : 125, Grand Total Max Marks: 250
                              </td>
                            </tr>

                            
                    </table>
                    
                    <div style="width: 100%; height: 3mm;"> </div>

                    <div style="width: 100%; height: 60mm;">
                        <div style="width: 50%; float: left; height: 100%">

                            <table style="width: 90%; margin-left: 20px; text-align: center; border-collapse: collapse; border: 1px solid black; font-size: 13px; height: 100%">
                                    <tr style="background-color: #4CAF50; border: 1px solid black;">
                                      <td colspan="4" style="border: 1px solid black;">Co-Scholastic Area</td>
                                    </tr>

                                    <tr style="background-color: skyblue; border: 1px solid black;">
                                      <td  style="border: 1px solid black;">SNo.</td>
                                      <td  style="border: 1px solid black;">Elements</td>
                                      <td  style="border: 1px solid black;">T1</td>
                                      <td  style="border: 1px solid black;">T2</td>
                                    </tr>

                                    <tr style="border: 1px solid black;">
                                      <td  style="border: 1px solid black;">1</td>
                                      <td  style="border: 1px solid black;">Health & Physical Education</td>
                                      <td  style="border: 1px solid black;">A</td>
                                      <td  style="border: 1px solid black;">B</td>
                                    </tr>

                                    <tr style="border: 1px solid black;">
                                      <td  style="border: 1px solid black;">1</td>
                                      <td  style="border: 1px solid black;">Health & Physical Education</td>
                                      <td  style="border: 1px solid black;">A</td>
                                      <td  style="border: 1px solid black;">B</td>
                                    </tr>

                                    <tr style="border: 1px solid black;">
                                      <td  style="border: 1px solid black;">1</td>
                                      <td  style="border: 1px solid black;">Health & Physical Education</td>
                                      <td  style="border: 1px solid black;">A</td>
                                      <td  style="border: 1px solid black;">B</td>
                                    </tr>

                                    <tr style="border: 1px solid black;">
                                      <td  style="border: 1px solid black;">1</td>
                                      <td  style="border: 1px solid black;">Health & Physical Education</td>
                                      <td  style="border: 1px solid black;">A</td>
                                      <td  style="border: 1px solid black;">B</td>
                                    </tr>

                                    <tr style="border: 1px solid black;">
                                      <td  style="border: 1px solid black;">1</td>
                                      <td  style="border: 1px solid black;">Health & Physical Education</td>
                                      <td  style="border: 1px solid black;">A</td>
                                      <td  style="border: 1px solid black;">B</td>
                                    </tr>

                                    <tr style="border: 1px solid black;">
                                      <td  style="border: 1px solid black;">1</td>
                                      <td  style="border: 1px solid black;">Health & Physical Education</td>
                                      <td  style="border: 1px solid black;">A</td>
                                      <td  style="border: 1px solid black;">B</td>
                                    </tr>

                                    <tr style="border: 1px solid black;">
                                      <td  style="border: 1px solid black;">1</td>
                                      <td  style="border: 1px solid black;">Health & Physical Education</td>
                                      <td  style="border: 1px solid black;">A</td>
                                      <td  style="border: 1px solid black;">B</td>
                                    </tr>

                                    <tr style="border: 1px solid black;">
                                      <td  style="border: 1px solid black;">1</td>
                                      <td  style="border: 1px solid black;">Health & Physical Education</td>
                                      <td  style="border: 1px solid black;">A</td>
                                      <td  style="border: 1px solid black;">B</td>
                                    </tr>
                                    
                            </table>

                        </div>
                        <div style="width: 50%; float: left; height: 100%;">
                            <table style="width: 90%; margin-left: 20px; text-align: center; border-collapse: collapse; border: 1px solid black; font-size: 13px; height: 100%">
                                    <tr style="background-color: #4CAF50; border: 1px solid black;">
                                      <td colspan="4" style="border: 1px solid black;">Discipline</td>
                                    </tr>

                                    <tr style="background-color: skyblue; border: 1px solid black;">
                                      <td  style="border: 1px solid black;">SNo.</td>
                                      <td  style="border: 1px solid black;">Activity</td>
                                      <td  style="border: 1px solid black;">T1</td>
                                      <td  style="border: 1px solid black;">T2</td>
                                    </tr>

                                    <tr style="border: 1px solid black;">
                                      <td  style="border: 1px solid black;">1</td>
                                      <td  style="border: 1px solid black;">Respectfulness for Rules & Regulations</td>
                                      <td  style="border: 1px solid black;">A</td>
                                      <td  style="border: 1px solid black;">C</td>
                                    </tr>

                                    <tr style="border: 1px solid black;">
                                      <td  style="border: 1px solid black;">1</td>
                                      <td  style="border: 1px solid black;">Respectfulness for Rules & Regulations</td>
                                      <td  style="border: 1px solid black;">A</td>
                                      <td  style="border: 1px solid black;">C</td>
                                    </tr>

                                    <tr style="border: 1px solid black;">
                                      <td  style="border: 1px solid black;">1</td>
                                      <td  style="border: 1px solid black;">Respectfulness for Rules & Regulations</td>
                                      <td  style="border: 1px solid black;">A</td>
                                      <td  style="border: 1px solid black;">C</td>
                                    </tr>

                                    <tr style="border: 1px solid black;">
                                      <td  style="border: 1px solid black;">1</td>
                                      <td  style="border: 1px solid black;">Respectfulness for Rules & Regulations</td>
                                      <td  style="border: 1px solid black;">A</td>
                                      <td  style="border: 1px solid black;">C</td>
                                    </tr>

                                    <tr style="border: 1px solid black;">
                                      <td  style="border: 1px solid black;">1</td>
                                      <td  style="border: 1px solid black;">Respectfulness for Rules & Regulations</td>
                                      <td  style="border: 1px solid black;">A</td>
                                      <td  style="border: 1px solid black;">C</td>
                                    </tr>

                                    <tr style="border: 1px solid black;">
                                      <td  style="border: 1px solid black;">1</td>
                                      <td  style="border: 1px solid black;">Respectfulness for Rules & Regulations</td>
                                      <td  style="border: 1px solid black;">A</td>
                                      <td  style="border: 1px solid black;">C</td>
                                    </tr>

                                    <tr style="border: 1px solid black;">
                                      <td  style="border: 1px solid black;">1</td>
                                      <td  style="border: 1px solid black;">Respectfulness for Rules & Regulations</td>
                                      <td  style="border: 1px solid black;">A</td>
                                      <td  style="border: 1px solid black;">C</td>
                                    </tr>
                                    
                            </table>
                        </div>
                    </div>

                     <div style="width: 190mm; height: 5mm;"> </div>

                     <div class="AdmitCardTop" style="width: 100%; height: 15mm;">

                    <div style="width: 60%; height: 100%; float: left;">
                        <table class="table" style="padding-left: 20px;">
                              <tr>
                                <td  style="border:none; width:125px">Overall Marks</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">1500</td>
                              </tr>

                              <tr>
                                <td  style="border:none; width:125px">Percentage</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">92%</td>
                              </tr>
                        </table>
                    </div>

                    <div style="width: 38%; height: 100%; float: left;">
                        <table class="table">
                              <tr>
                                <td  style="border:none; width:100px">Attendance</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">80.00 %</td>
                              </tr>
                              <tr>
                                <td  style="border:none; width:100px">Class Rank</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">1</td>
                              </tr>
                        </table>
                    </div>

                    <table style="width: 95%; margin-left: 20px; text-align: center; border-collapse: collapse; border: 1px solid black;">

                            <tr style="border: 1px solid black;"">
                              <td style="background-color: #4CAF50;  width:100px">Remark</td>
                              <td> You Need to be more dedicated towards yours Studies, Your Few Section are weeker </td>
                            </tr>

                    </table>
                </div>

                

                </div>
                <div class="AdmitCardTop" style="width: 100%; height: 40mm;">
                    <div class="AdmitCardTop" style="width: 100%; height: 30mm;"> </div>
                    <div class="AdmitCardTop" style="width: 100%; height: 5mm; margin-left: 20px;"> Date : 28/10/2017</div>
                </div>
                
        </div>
    </div>
</body>
</html>
