<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
        div {
            text-transform: capitalize;
        }
    </style>
</head>
<body>

    <div class="admitcard-mainblock" style="width: 200mm; height: 280mm;">
        <div class="admitcard-block"  style="background-image: url('{{URL('/')}}/defaultimages/dellmond_result.png'); background-size: 100% 100%;">
                <div class="AdmitCardTop" style="width: 100%; height: 48mm;">

                </div>
                <div class="AdmitCardTop" style="width: 100%; height: 15mm; text-align: center;">
                   <span style="font-size: 20px; font-weight: bold;"> Class : {{$class_section}} </span> <br>
                   <span style="font-size: 20px; font-weight: bold;"> Academic Session : {{ Session('valid_period') }} (Term 1) </span>
                   <input id="hclass_section" type="hidden" name="hclass_section" value="{{$class_section}}" >
                </div>
                <div class="AdmitCardTop" style="width: 100%; height: 30mm;">

                    <div style="width: 60%; height: 100%; float: left;">
                      @foreach ($studentdetails as $element)
                        <table class="table" style="padding-left: 20px;">
                              <tr>
                                <td  style="border:none; width:125px">Students's Name</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black; min-width:250px">
                                   {{ $element->student_name }}
                                </td>
                              </tr>

                              <tr>
                                <td  style="border:none; width:125px">Mother's Name</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">
                                    {{$element->student_mother_name}}
                                </td>
                              </tr>

                              <tr>
                                <td  style="border:none; width:125px">Father's Name</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">
                                    {{$element->student_father_name}}
                                </td>
                              </tr>

                              <tr>
                                <td  style="border:none; width:125px">Date of Birth</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">
                                      {{$element->student_dob}}
                                </td>
                              </tr>
                        </table>
                    </div>

                    <div style="width: 38%; height: 100%; float: left;">
                        <table class="table">
                              <tr>
                                <td  style="border:none; width:100px">Admission No</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black; min-width:100px;">
                                      {{$element->admission_no}}  
                                </td>
                              </tr>
                              <tr>
                                <td  style="border:none; width:100px">Roll No</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">
                                    {{$element->student_rollno}}  
                                </td>
                              </tr>
                        </table>
                    </div>
                  @endforeach
                </div>

                <div class="AdmitCardTop" style="width: 100%; height: 147mm;">
                    
                    <table style="width: 95%; margin-left: 20px; text-align: center; border-collapse: collapse; border: 1px solid black;">

                            <tr style="background-color: #4CAF50; border: 1px solid black;">
                              <td colspan="2" style="border: 1px solid black;">Scholastic Area</td>
                              <td colspan="6" style="border: 1px solid black;">Term 1 Marks</td>
                            </tr>

                            <tr style="background-color: skyblue; border: 1px solid black;">
                              <td  style="border: 1px solid black;">Code</td>
                              <td  style="border: 1px solid black; width:225px">Sub Name</td>
                              <td  style="border: 1px solid black;">Unit Test Max.</td>
                              <td  style="border: 1px solid black;">Unit Test Obt.</td>
                              
                              <td  style="border: 1px solid black;">Half Yearly Max</td>
                              <td  style="border: 1px solid black;">Half Yearly Obt.</td>
                              
                              <td  style="border: 1px solid black;">Total Max</td>
                              <td  style="border: 1px solid black;">Total Obt</td>
                              
                            </tr>

                            @foreach ($subjectlist as $element)
                                <tr style="border: 1px solid black;">
                                    
                                    <td style="border: 1px solid black;">
                                            {{$element->subject_code}}
                                    </td>
                                    
                                    <td style="border: 1px solid black;">
                                            {{$element->subject_name}}
                                    </td>


                                    <td style="border: 1px solid black;">
                                            @if ($element->unittest_max > 0)
                                              {{$element->unittest_max}} 
                                            @endif
                                            @if ($element->unittest_max == 0)
                                                  --
                                            @endif
                                            
                                    </td>
                                    
                                    <td style="border: 1px solid black;">
                                          @if ( $element->unittest_max > 0 )

                                            @if ($element->unittest_obtained == '-1')
                                                  Ab
                                            @endif

                                            @if ($element->unittest_obtained != '-1')
                                              @if ((($element->unittest_obtained/$element->unittest_max)*100<33))
                                                <span style="color: red;"> {{ $element->unittest_obtained }} </span>
                                              @else
                                                {{ $element->unittest_obtained }}
                                              @endif
                                              
                                            @endif

                                          @endif

                                          @if ( $element->unittest_max == 0 )
                                              --
                                          @endif
                                             
                                    </td>
                                    

                                    <td style="border: 1px solid black;">
                                            {{$element->exam_max}}
                                    </td>
                                    
                                    <td style="border: 1px solid black;">

                                            @if ( $element->exam_max >= 0 )

                                              @if ($element->exam_obtained == '-1')
                                                    Ab
                                              @endif

                                              @if ($element->exam_obtained != '-1')
                                                @if ((($element->exam_obtained/$element->exam_max)*100<33))
                                                  <span style="color: red;"> {{ $element->exam_obtained }} </span>
                                                @else
                                                  {{ $element->exam_obtained }}
                                                @endif
                                              @endif

                                          @endif 

                                          @if ( $element->exam_max == -1 )
                                              --
                                          @endif 
                                    </td>
                                    

                                    <td style="border: 1px solid black;">
                                            {{$element->total_max}} 
                                    </td>
                                    <td style="border: 1px solid black;">
                                        @if ((($element->total_obtained/$element->total_max)*100<33))
                                          <span style="color: red;"> {{ $element->total_obtained }} </span>
                                        @else
                                          {{ $element->total_obtained }}
                                        @endif
                                          
                                    </td>
                                    
                                    
                                  </tr>
                            @endforeach
                    </table>
                    
                    <div style="width: 100%; height: 3mm;"> </div>

                    <div style="width: 100%; height: 60mm;">
                        <div style="width: 50%; float: left; height: 100%">

                            <table style="width: 90%; margin-left: 20px; text-align: center; border-collapse: collapse; border: 1px solid black; font-size: ; height: 100%">
                                    <tr style="background-color: #4CAF50; border: 1px solid black;">
                                      <td colspan="3" style="border: 1px solid black;">Co-Scholastic Area</td>
                                    </tr>

                                    <tr style="background-color: skyblue; border: 1px solid black;">
                                      <td  style="border: 1px solid black;">Code</td>
                                      <td  style="border: 1px solid black;">Elements</td>
                                      <td  style="border: 1px solid black;">Grade</td>
                                    </tr>

                                    @foreach ($coscholasticlist as $row)
                                <tr style="border: 1px solid black;">
                                    
                                    <td style="border: 1px solid black;">
                                          {{$row->coscholastic_code}}
                                    </td>
                                    <td style="border: 1px solid black;">
                                          {{$row->coscholastic_name}}
                                    </td>
                                    <td style="border: 1px solid black;">
                                            {{$row->grade_term1}}
                                    </td>                                    
                                  </tr>

                              @endforeach
                            </table>

                        </div>
                        <div style="width: 50%; float: left; height: 100%;">
                            <table style="width: 90%; margin-left: 20px; text-align: center; border-collapse: collapse; border: 1px solid black; font-size: ; height: 100%">
                                    <tr style="background-color: #4CAF50; border: 1px solid black;">
                                      <td colspan="3" style="border: 1px solid black;">Discipline</td>
                                    </tr>

                                    <tr style="background-color: skyblue; border: 1px solid black;">
                                      <td  style="border: 1px solid black;">Code</td>
                                      <td  style="border: 1px solid black;">Activity</td>
                                      <td  style="border: 1px solid black;">Grade</td>
                                    </tr>

                                    @foreach ($disciplinelist as $row)
                                <tr style="border: 1px solid black;">
                                    <td style="border: 1px solid black;">
                                          {{$row->discipline_code}}
                                    </td>
                                    <td style="border: 1px solid black;">
                                          {{$row->discipline_name}}
                                    </td>
                                    <td style="border: 1px solid black;">
                                          {{$row->grade_term1}}
                                    </td>                                    
                                </tr>
                              @endforeach
                            </table>
                        </div>
                    </div>

                     <div style="width: 190mm; height: 5mm;"> </div>

                     <div class="AdmitCardTop" style="width: 100%; height: 15mm;">

                    <div style="width: 60%; height: 100%; float: left;">
                        <table class="table" style="padding-left: 20px;">
                              <tr>
                                <td  style="border:none; width:125px">Overall Marks </td>
                                <td style="width:10px"> : </td>
                                <td>
                                      @foreach ($Overallmarksmgmt as $element)
                                          @if ($element->marks_term1_total > 0)
                                            {{ ($element->marks_term1_obtained) }} / {{ ($element->marks_term1_total) }}
                                          @endif

                                          @if ($element->marks_term1_total <= 0)
                                            0
                                          @endif
                                                  
                                      @endforeach
                                </td>
                              </tr>

                              <tr>
                                <td  style="border:none; width:125px">Percentage</td>
                                <td style="width:10px"> : </td>
                                <td>
                                      @foreach ($Overallmarksmgmt as $element)
                                          @if ($element->marks_term1_total > 0)
                                            {{ round((($element->marks_term1_obtained)/($element->marks_term1_total))*100,2) }} %
                                          @endif

                                          @if ($element->marks_term1_total <= 0)
                                            0 %
                                          @endif
                                                  
                                      @endforeach
                                </td>
                              </tr>
                        </table>
                    </div>

                    <div style="width: 38%; height: 100%; float: left;">
                        <table class="table">
                              <tr>
                                <td  style="border:none; width:100px">Attendance </td>
                                <td style="width:10px"> : </td>
                                <td>
                                    @foreach ($attendance as $element)
                                          @if ($element->attendance_total > 0)
                                            {{ round((($element->attendance_obtained)/($element->attendance_total))*100,2) }} %
                                          @endif

                                          @if ($element->attendance_total <= 0)
                                            0 %
                                          @endif
                                       
                                    @endforeach
                                    
                                </td>
                              </tr>
                              <tr>
                                <td  style="border:none; width:100px">Class Rank</td>
                                <td style="width:10px"> : </td>
                                <td>
                                    @foreach ($Overallmarksmgmt as $element)
                                                  {{ ($element->rank_term1) }}
                                      @endforeach
                                </td>
                              </tr>
                        </table>
                    </div>

                    <table style="width: 65%; margin-left: 20px; border-collapse: collapse; line-height: 25px; ">

                            <tr>
                              <td style="width: 15%; " > Remark </td>
                              <td> : </td>
                              <td style="width: 85%; border-bottom: 1px solid black;"> </td>
                            </tr>

                            <tr>
                              <td style="width: 15%; " ></td>
                              <td> &nbsp; </td>
                              <td style="width: 85%; border-bottom: 1px solid black;"> </td>
                            </tr>

                    </table>

                </div>

                

                </div>
                <div class="AdmitCardTop" style="width: 100%; height: 40mm;">
                    <div class="AdmitCardTop" style="width: 100%; height: 30mm;"> </div>
                    <div class="AdmitCardTop" style="width: 100%; height: 5mm; margin-left: 20px;">
                      Date : @foreach ($resultdate as $element)
                                        {{ $element->result_date }}
                            @endforeach
                    </div>
                </div>
                
        </div>
    </div>


</body>
</html>
