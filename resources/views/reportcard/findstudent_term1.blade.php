@extends('layouts.app')

@section('content')


@if(Session('message'))
    <div class="alert alert-danger">{{ Session('message') }}</div>
@endif



<div class="panel-heading">Find By Admission Number Only</div>

<div class="panel-body">
    <form class="form-horizontal" method="POST" action="{{ URL('/') }}/reportcard/generate">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('admission_number') ? ' has-error' : '' }}">
            <label for="admission_number" class="col-md-4 control-label">Admission Number</label>

            <div class="col-md-6">
                <input id="admission_number" type="text" class="form-control" name="admission_number" value="{{ old('admission_number') }}" autofocus>

                @if ($errors->has('admission_number'))
                    <span class="help-block">
                        <strong>{{ $errors->first('admission_number') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <input type="hidden" class="form-control" name="resulttype" value="term1" autofocus>

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    Find And Fill Student Details
                </button>
            </div>
        </div>

    </form>
</div>


@endsection

