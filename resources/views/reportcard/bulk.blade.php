@extends('layouts.app')

@section('content')

<!-- will be used to show any messages -->
@if(Session('message'))
    <div class="alert alert-info">{{ Session('message') }}</div>
@endif

<h3>Showing the Details of Registered Subjects  </h3>

<a href="{{ URL::to('coscholastic/create') }}">
    <button type="button" class="btn btn-primary" style="margin: 10px 0px 0px 10px;">Register New Co-Scholastic</button>
</a>



<div class="panel-body" style="overflow-x: scroll;">
    <table width="100%" class="table table-striped table-bordered table-hover" id="AdvancedTable">
            <thead>
                <th> Class </th>
                <th> Half Yearly ( Term 1) </th>
                <th> Anual (Term 2)</th>
                <th> Final (Term 1 + Term 2) </th>                
            </thead>
        @foreach ($class_section as $element)
            <tr>
                <td> {{ $element->class_section }} </td> 

                <td> 
                    <a target="_blank" href="{{ URL::to('reportcard/bulk/' . $element->class_section . '/1' . '/generate') }}">
                        <input type="button" class="btn btn-info  btn-sm" name="" id="" value="{{ $element->class_section}} term1">
                    </a>   
                
                </td>

                <td> 
                    <a target="_blank" href="{{ URL::to('reportcard/bulk/' . $element->class_section . '/2' . '/generate') }}">
                        <input type="button" class="btn btn-info  btn-sm" name="" id="" value="{{ $element->class_section }} term 2">
                    </a>   
                
                </td>

                <td> 
                    <a target="_blank" href="{{ URL::to('reportcard/bulk/' . $element->class_section . '/3' . '/generate') }}">
                        <input type="button" class="btn btn-info  btn-sm" name="" id="" value="{{ $element->class_section }} final yearly">
                    </a>   
                
                </td>

            </tr>
        @endforeach
    </table>
</div>


@endsection

