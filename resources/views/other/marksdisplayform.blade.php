@extends('layouts.app')

@section('content')

<div class="panel-heading">Choose the Class-Section to Display Marks of All Subjects</div>

<div class="panel-body">
     <form class="form-horizontal" method="POST" action="{{URL('/')}}/showmarks-submit">
        {{ csrf_field() }}



        <div class="form-group{{ $errors->has('class_section') ? ' has-error' : '' }}">
            <label for="class_section" class="col-md-4 control-label">Class Applicable</label>

            <div class="col-md-6">
                <select  id="class_section" class="form-control" name="class_section" >
                    @foreach ($classlist as $element)
                         <option value="{{$element}}"> {{$element}}</option>
                    @endforeach
                </select>

                @if ($errors->has('class_section'))
                    <span class="help-block">
                        <strong>{{ $errors->first('class_section') }}</strong>
                    </span>
                @endif
            </div>
        </div>




        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    Submit
                </button>
            </div>
        </div>

    </form>
</div>

@endsection
