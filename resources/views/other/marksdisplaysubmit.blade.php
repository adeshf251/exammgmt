{{--  displaytabularmarksinfo.blade.php  --}}

@extends('layouts.app')

@section('content')

<!-- will be used to show any messages -->
@if(Session('message'))
    <div class="alert alert-info">{{ Session('message') }}</div>
@endif


<table class="table table-bordered" >
    <thead>
        <tr>
            <td>S.No [ class : {{$class}} and session: {{$session}} ] </td>
            <td>Admission.No</td>
            <td>Student Name</td>
            @foreach ($subjectslist as $key => $subjectname)
                <td> {{$subjectname}} unit test term-1</td>
                <td> {{$subjectname}} exam marks term-1</td>
                <td> {{$subjectname}} total marks term-1</td>
                <td> {{$subjectname}} unit test term-2</td>
                <td> {{$subjectname}} exam marks term-2</td>
                <td> {{$subjectname}} total marks term-2</td>
            @endforeach
        </tr>
       
        
    </thead>
    <tbody>
        @foreach ($arrayresult as $k1 => $studentmarks)
            <tr>
                <td>{{$k1+1}}</td>
                <td>{{$studentinfo[$k1]->admission_no}}</td>
                <td>{{$studentinfo[$k1]->student_name}}</td>
                @foreach ($studentmarks as $k2 => $subject)
                    <td>{{$subject->unittest_obtained}} </td>
                    <td>{{$subject->exam_obtained}} </td>
                    <td>{{$subject->total_obtained}} </td>
                    <td>{{$subject->unittest_obtained_term2}} </td>
                    <td>{{$subject->exam_obtained_term2}} </td>
                    <td>{{$subject->total_obtained_term2}} </td>
                @endforeach
            </tr>
        @endforeach
    </tbody>
</table>

@endsection

