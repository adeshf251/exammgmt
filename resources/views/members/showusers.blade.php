@extends('layouts.app')

@section('content')

<!-- will be used to show any messages -->
@if(Session('message'))
    <div class="alert alert-info">{{ Session('message') }}</div>
@endif

<h3>Showing the Details of Registered Subjects  </h3>

<a href="{{ URL::to('adduserform') }}">
    <button type="button" class="btn btn-primary" style="margin: 10px 0px 0px 10px;">Register New User</button>
</a>



<div class="panel-body" style="overflow-x: scroll;">
    <table width="100%" class="table table-striped table-bordered table-hover" id="AdvancedTable">
            <thead>
                <th> User Name </th>
                <th> Email ID  </th>
                
            </thead>
        @foreach ($userlist as $element)
            <tr>
                <td> {{ $element->name }} </td> 
                <td> {{ $element->email }} </td>               
                
            </tr>
        @endforeach
    </table>
</div>


@endsection

