<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="{{URL('/')}}/css/admitcard.css">
</head>
<body>


<?php  $value='ABCDEFGHIJKLM NOPQRSTUVWXY ZABCDEFG HIJKLM NOPQRSTUVWXYZ'; ?>

    @for ($i = 0; $i < 40; $i++)
        <div class="admitcard-mainblock">
        <div class="admitcard-block">
                <div class="AdmitCardTop">
                    <div  class="AdmitCardTop1" style="">
                        <img src="{{URL('/')}}/images/logo.jpg" style="width: auto; height: 100%;">
                    </div>
                    <div  class="AdmitCardTop2" style="">
                            <span class="SpanLine1" style=""> Dellmond International School </span> <br>
                            <span class="SpanLine2" style=""> Dehradun Road Near Fatehpur Thana Chhutmalpur </span>
                    </div>
                    <div  class="AdmitCardTop3" style="">
                         <table>
                             <tr> <td colspan="2" style="font-size: 19px; font-weight: bold;"> Admit Card </td></tr>
                             <tr> <td> Adm.No. </td> <td> 123456 </td></tr>
                         </table>
                    </div>
                </div>
                <div class="AdmitCardContentLeft" style="">
                    <table style="font-size: 15px;">
                        
                            <tr> <td><b> St. Name :   </b></td><td><b> {{ substr($value, 0, 30 )}} </b> </td> </tr>
                            <tr> <td><b> M. Name :    </b></td><td><b> {{ substr($value, 0, 30 )}} </b> </td> </tr>
                            <tr> <td><b> F. Name :    </b></td><td><b> {{ substr($value, 0, 30 )}} </b> </td> </tr>
                            <tr> <td><b> Roll No :    </b></td><td><b> {{ substr($value, 0, 30 )}} </b> </td> </tr>
                            <tr> <td><b> Class :      </b></td><td><b> {{ substr($value, 0, 30 )}} </b> </td> </tr>
                            <tr> <td><b> Exam Term :  </b></td><td><b> {{ substr($value, 0, 30 )}} </b> </td> </tr>
                            <tr> <td><b> Session  :   </b></td><td><b> {{ substr($value, 0, 30 )}} </b> </td> </tr>
                        
                    </table>
                    <div class="AdmitCardContentLeftbottom" style="">



                        <div class="AdmitCardContentLeftbottom1" style=""> </div>
                        <div class="AdmitCardContentLeftbottom2" style=""> <img src="{{URL('/')}}/images/principal.png" style="height: 100%;"> </div>
                        <div class="AdmitCardContentLeftbottom3" style=""> Teacher's Sign </div>
                        <div class="AdmitCardContentLeftbottom4" style=""> Principal's Sign </div>
                            
                    </div>
                </div>

                <div class="AdmitCardContentRight" style="">
                    <table style="font-size: 13px; text-align:center;">
                        
                            <tr> <td><b> Subject                </b></td><td><b>     Date             </b></td> </tr>
                            <tr> <td> {{ substr($value, 0, 25 )}} : &nbsp; </td><td> {{ substr($value, 0, 12 )}} </td> </tr>
                            <tr> <td> {{ substr($value, 0, 25 )}} : &nbsp; </td><td> {{ substr($value, 0, 12 )}} </td> </tr>
                            <tr> <td> {{ substr($value, 0, 25 )}} : &nbsp; </td><td> {{ substr($value, 0, 12 )}} </td> </tr>
                            <tr> <td> {{ substr($value, 0, 25 )}} : &nbsp; </td><td> {{ substr($value, 0, 12 )}} </td> </tr>
                            <tr> <td> {{ substr($value, 0, 25 )}} : &nbsp; </td><td> {{ substr($value, 0, 12 )}} </td> </tr>
                            <tr> <td> {{ substr($value, 0, 25 )}} : &nbsp; </td><td> {{ substr($value, 0, 12 )}} </td> </tr>
                            <tr> <td> {{ substr($value, 0, 25 )}} : &nbsp; </td><td> {{ substr($value, 0, 12 )}} </td> </tr>
                        
                    </table>
                </div>
        </div>

        <div class="GapAfterAdmitCard" style="width: 210mm; height: 15mm; "> </div>
    </div>
    @endfor
    





</body>
</html>
