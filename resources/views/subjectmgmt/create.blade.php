@extends('layouts.app')
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
@section('content')

<div class="panel-heading">Register</div>

<div class="panel-body">
    <form class="form-horizontal" method="POST" action="{{ route('subjectmgmt.store') }}">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('subjectcode') ? ' has-error' : '' }}">
            <label for="subjectcode" class="col-md-4 control-label">Subject Code</label>

            <div class="col-md-6">
                <input id="subjectcode" type="text" class="form-control" name="subjectcode" value="{{ old('subjectcode') }}" autofocus>

                @if ($errors->has('subjectcode'))
                    <span class="help-block">
                        <strong>{{ $errors->first('subjectcode') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('subject_name') ? ' has-error' : '' }}">
            <label for="subject_name" class="col-md-4 control-label">Subject Name</label>

            <div class="col-md-6">
                <input id="subject_name" type="text" class="form-control" name="subject_name" value="{{ old('subject_name') }}" autofocus>

                @if ($errors->has('subject_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('subject_name') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <div class="form-group{{ $errors->has('class_applicable') ? ' has-error' : '' }}">
            <label for="class_applicable" class="col-md-4 control-label">Class Applicable</label>

            <div class="col-md-6">
                <select  id="class_applicable" class="form-control" name="class_applicable" >
                    @foreach ($classlist as $element)
                         <option value="{{$element}}"> {{$element}}</option>
                    @endforeach
                </select>

                @if ($errors->has('class_applicable'))
                    <span class="help-block">
                        <strong>{{ $errors->first('class_applicable') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('extra') ? ' has-error' : '' }}">
            <label for="extra" class="col-md-4 control-label">Any Comment</label>

            <div class="col-md-6">
                <input id="extra" type="text" class="form-control" name="extra" value="{{ old('extra') }}" autofocus>

                @if ($errors->has('extra'))
                    <span class="help-block">
                        <strong>{{ $errors->first('extra') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <div class="form-group{{ $errors->has('term1_unittest') ? ' has-error' : '' }}">
            <label for="term1_unittest" class="col-md-4 control-label">Term1 Unit Test Max Marks</label>

            <div class="col-md-6">
                <input id="term1_unittest" type="text" class="form-control" name="term1_unittest" value="{{ old('term1_unittest') }}" autofocus>

                @if ($errors->has('term1_unittest'))
                    <span class="help-block">
                        <strong>{{ $errors->first('term1_unittest') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <div class="form-group{{ $errors->has('term1_exam') ? ' has-error' : '' }}">
            <label for="term1_exam" class="col-md-4 control-label">Term1 Exam Max Marks</label>

            <div class="col-md-6">
                <input id="term1_exam" type="text" class="form-control" name="term1_exam" value="{{ old('term1_exam') }}" autofocus>

                @if ($errors->has('term1_exam'))
                    <span class="help-block">
                        <strong>{{ $errors->first('term1_exam') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('term1_total') ? ' has-error' : '' }}">
            <label for="term1_total" class="col-md-4 control-label">Term1 Total Marks</label>

            <div class="col-md-6">
                <input id="term1_total" type="text" class="form-control" name="term1_total" value="{{ old('term1_total') }}" autofocus disabled>

                @if ($errors->has('term1_total'))
                    <span class="help-block">
                        <strong>{{ $errors->first('term1_total') }}</strong>
                    </span>
                @endif
            </div>
        </div>



        <div class="form-group{{ $errors->has('term2_unittest') ? ' has-error' : '' }}">
            <label for="term2_unittest" class="col-md-4 control-label">Term2 Unit Test Max Marks</label>

            <div class="col-md-6">
                <input id="term2_unittest" type="text" class="form-control" name="term2_unittest" value="{{ old('term2_unittest') }}" autofocus>

                @if ($errors->has('term2_unittest'))
                    <span class="help-block">
                        <strong>{{ $errors->first('term2_unittest') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        
        <div class="form-group{{ $errors->has('term2_exam') ? ' has-error' : '' }}">
            <label for="term2_exam" class="col-md-4 control-label">Term2 Exam Max Marks</label>

            <div class="col-md-6">
                <input id="term2_exam" type="text" class="form-control" name="term2_exam" value="{{ old('term2_exam') }}" autofocus>

                @if ($errors->has('term2_exam'))
                    <span class="help-block">
                        <strong>{{ $errors->first('term2_exam') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('term2_total') ? ' has-error' : '' }}">
            <label for="term2_total" class="col-md-4 control-label">Term2 Total Max Marks</label>

            <div class="col-md-6">
                <input id="term2_total" type="text" class="form-control" name="term2_total" value="{{ old('term2_total') }}" autofocus disabled>

                @if ($errors->has('term2_total'))
                    <span class="help-block">
                        <strong>{{ $errors->first('term2_total') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
            <label for="status" class="col-md-4 control-label">Status</label>

            <div class="col-md-6">
                <input id="status" type="text" class="form-control" name="status" value="{{ old('status') }}" autofocus>

                @if ($errors->has('status'))
                    <span class="help-block">
                        <strong>{{ $errors->first('status') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('grand_total_marks') ? ' has-error' : '' }}">
            <label for="grand_total_marks" class="col-md-4 control-label">Grand Total Max Marks</label>

            <div class="col-md-6">
                <input id="grand_total_marks" type="text" class="form-control" name="grand_total_marks" value="{{ old('grand_total_marks') }}" autofocus disabled>

                @if ($errors->has('grand_total_marks'))
                    <span class="help-block">
                        <strong>{{ $errors->first('grand_total_marks') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('optional') ? ' has-error' : '' }}">
            <label for="optional" class="col-md-4 control-label">Optional (If any)</label>

             <div class="col-md-6">
                 <select  id="optional" class="form-control" name="optional" >
                         <option value="both test and exam"> Both test and exam</option>
                         <option value="only test"> only test</option>
                         <option value="only exam"> only exam</option>
                                                 </select>

                @if ($errors->has('optional'))
                    <span class="help-block">
                        <strong>{{ $errors->first('optional') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('date_term1') ? ' has-error' : '' }}">
            <label for="date_term1" class="col-md-4 control-label">Term 1 Exam Date (dd-mm-yyyy)</label>

            <div class="col-md-6">
                <input id="date_term1" type="text" class="form-control" name="date_term1" value="{{ old('date_term1') }}" autofocus>

                @if ($errors->has('date_term1'))
                    <span class="help-block">
                        <strong>{{ $errors->first('date_term1') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('date_term2') ? ' has-error' : '' }}">
            <label for="date_term2" class="col-md-4 control-label">Term 2 Exam Date (dd-mm-yyyy)</label>

            <div class="col-md-6">
                <input id="date_term2" type="text" class="form-control" name="date_term2" value="{{ old('date_term2') }}" autofocus>

                @if ($errors->has('date_term2'))
                    <span class="help-block">
                        <strong>{{ $errors->first('date_term2') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    Register
                </button>
            </div>
        </div>

    </form>
</div>


<input id="term1_total_hidden" type="hidden" name="term1_total_hidden" value="0">
<input id="term2_total_hidden" type="hidden" name="term2_total_hidden" value="0">
<input id="grand_total_hidden" type="hidden" name="term2_total_hidden" value="0">

<script type="text/javascript">
  $(document).ready(function()
  {

  
        document.getElementById("term1_unittest").onkeyup = function() { myFunction(); };
        document.getElementById("term1_exam").onkeyup = function() { myFunction(); };

            function myFunction()
                {
                var x = document.getElementById("term1_unittest");
                var y = document.getElementById("term1_exam");
                var term1display = document.getElementById("term1_total");
                var term1hidden = document.getElementById("term1_total_hidden");
                var t1 = (parseInt(term1hidden.value) + parseInt(x.value) + parseInt(y.value));
                term1display.value = t1;

                var term2display = document.getElementById("term2_total");
                var grandhidden = document.getElementById("grand_total_hidden");
                var granddisplay = document.getElementById("grand_total_marks");
                var t2 = (parseInt(t1) + parseInt(grandhidden.value) + parseInt(term2display.value));
                granddisplay.value = t2;
                }



        document.getElementById("term2_unittest").onkeyup = function() { myFunction2(); };
        document.getElementById("term2_exam").onkeyup = function() { myFunction2(); };

            function myFunction2()
                {
                var x = document.getElementById("term2_unittest");
                var y = document.getElementById("term2_exam");
                var term2display = document.getElementById("term2_total");
                var term2hidden = document.getElementById("term2_total_hidden");
                var t1 = (parseInt(term2hidden.value) + parseInt(x.value) + parseInt(y.value));
                term2display.value = t1;

                var term1display = document.getElementById("term1_total");
                var grandhidden = document.getElementById("grand_total_hidden");
                var granddisplay = document.getElementById("grand_total_marks");
                var t2 = (parseInt(t1) + parseInt(grandhidden.value) + parseInt(term1display.value));
                granddisplay.value = t2;
                }   
    });    
</script>


@endsection
