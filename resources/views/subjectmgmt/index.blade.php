@extends('layouts.app')

@section('content')

<!-- will be used to show any messages -->
@if(Session('message'))
    <div class="alert alert-info">{{ Session('message') }}</div>
@endif

<h3>Showing the Details of Registered Subjects  </h3>

<a href="{{ URL::to('subjectmgmt/create') }}">
    <button type="button" class="btn btn-primary" style="margin: 10px 0px 0px 10px;">Register New Subject</button>
</a>



<div class="panel-body" style="overflow-x: scroll;">
    <table width="100%" class="table table-striped table-bordered table-hover" id="AdvancedTable">
            <thead>
                <th> ID </th>
                <th>  </th>
                <th></th>
                <th></th>
                <th> Subject Code </th>
                <th> Subject Name </th>
                <th> Class Applicable </th>
                <th> Term1 Unit Test Max </th>
                <th> Term1 Exam Max </th>
                <th> Term1 Total </th>
                <th> Term2 Unit Test Max </th>
                <th> Term2 Exam Max </th>
                <th> Term2 Total </th>
                <th> Status </th>
                <th> Grand Total Marks </th>
                <th> Exam Date Term1 </th>
                <th> Exam Date Term2 </th>
                <th> Session ID </th>
                
            </thead>
        @foreach ($subjectmgmt as $element)
            <tr>
                <td> {{ $element->id }} </td> 

                    <td> 
                        <a href="{{ URL::to('subjectmgmt/' . $element->id . '/edit') }}">
                            <input type="button" class="btn btn-info  btn-sm" name="" id="" value="Edit">
                        </a>   
                    
                    </td>

                    <td> 
                        <a href="{{ URL::to('subjectmgmt/' . $element->id . '/duplicate') }}">
                            <input type="button" class="btn btn-warning  btn-sm" name="" id="" value="Duplicate">
                        </a>  
                    
                    </td>

                    <td>
                    <form method="POST" action="{{ URL('subjectmgmt') }}/{{$element->id}}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE">
                    <input type="submit" class="btn btn-danger  btn-sm" name="submit" id="{{$element->id}}" value="Delete">
                    </form>

                    </td>
                    
                <td> {{ $element->subject_code }} </td> 
                <td> {{ $element->subject_name }} </td>
                <td> {{ $element->class_applicable }} </td>
                <td> {{ $element->term1_unittest }} </td>
                <td> {{ $element->term1_exam }} </td>
                <td> {{ $element->term1_total }} </td>
                <td> {{ $element->term2_unittest }} </td>
                <td> {{ $element->term2_exam }} </td>
                <td> {{ $element->term2_total }} </td>
                <td> {{ $element->status }} </td>
                <td> {{ $element->grand_total_marks }} </td>
                <td> {{ $element->date_term1 }} </td>
                <td> {{ $element->date_term2 }} </td>
                <td> {{ $element->sessionid }} </td>

                
                
            </tr>
        @endforeach
    </table>
</div>


@endsection

