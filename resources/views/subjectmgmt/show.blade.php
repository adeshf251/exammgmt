@extends('layouts.app')

@section('content')

<!-- will be used to show any messages -->
@if(Session('message'))
    <div class="alert alert-info">{{ Session('message') }}</div>
@endif


<a href="{{ URL::to('subjectmgmt/create') }}"><button type="button" class="btn btn-primary">Add New Class</button></a>

<div class="panel-heading">Showing the Details of Registered Subjects  </div>

<div class="panel-body">
    <table class="table">
            <thead>
                <th> ID </th>
                <th> Subject Code </th>
                <th> Subject Name </th>
                <th> Class Applicable </th>
                <th> Term1 Test Max </th>
                <th> Term1 Part1 Max </th>
                <th> Term1 Part2 Max </th>
                <th> Term1 Exam Max </th>
                <th> Term1 Total </th>
                <th> Term2 Test Max </th>
                <th> Term2 Part1 Max </th>
                <th> Term2 Part2 Max </th>
                <th> Term2 Exam Max </th>
                <th> Term2 Total </th>
                <th> Status </th>
                <th> Grand Total Marks </th>
                <th colspan="2"> Action </th>
            </thead>
        
            <tr>
                <td> {{ $subjectmgmt->id }} </td> 
                <td> {{ $subjectmgmt->subject_code }} </td> 
                <td> {{ $subjectmgmt->subject_name }} </td>
                <td> {{ $subjectmgmt->class_applicable }} </td>
                <td> {{ $subjectmgmt->term1_test }} </td>
                <td> {{ $subjectmgmt->term1_part1 }} </td> 
                <td> {{ $subjectmgmt->term1_part2 }} </td>
                <td> {{ $subjectmgmt->term1_exam }} </td>
                <td> {{ $subjectmgmt->term1_total }} </td>
                <td> {{ $subjectmgmt->term2_test }} </td>
                <td> {{ $subjectmgmt->term2_part1 }} </td> 
                <td> {{ $subjectmgmt->term2_part2 }} </td>
                <td> {{ $subjectmgmt->term2_exam }} </td>
                <td> {{ $subjectmgmt->term2_total }} </td>
                <td> {{ $subjectmgmt->status }} </td>
                <td> {{ $subjectmgmt->grand_total_marks }} </td>

                <td> 
                    <a href="{{ URL::to('subjectmgmt/' . $subjectmgmt->id . '/edit') }}"> <button type="button" class="btn btn-info  btn-sm">Edit </button> </a>  &nbsp; &nbsp; 
                
                </td>
                <td>
                <form method="POST" action="{{ URL('subjectmgmt') }}/{{$subjectmgmt->id}}">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="DELETE">
                <input type="submit" class="btn btn-danger  btn-sm" name="submit" id="$subjectmgmt->id" value="Delete">
                </form>

                </td>
                
            </tr>
        
    </table>
</div>

@endsection

