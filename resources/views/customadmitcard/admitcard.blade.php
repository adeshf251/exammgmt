<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="{{URL('/')}}/css/admitcard.css">
    <style type="text/css">
        div {
            text-transform: capitalize;
        }
    </style>
</head>
<body>

        <div class="admitcard-mainblock">
        <div class="admitcard-block"  style="background-image: url( '{{URL('/')}}/images/watermark.png' );">
                <div class="AdmitCardTop">
                    <div  class="AdmitCardTop1" style="">
                        <img src="{{URL('/')}}/images/logo.jpg" style="width: auto; height: 100%;">
                    </div>
                    <div  class="AdmitCardTop2" style="">
                            <span class="SpanLine1" style=""> Dellmond International School </span> <br>
                            <span class="SpanLine2" style=""> Dehradun Road Near Fatehpur Thana Chhutmalpur </span>
                    </div>
                    <div  class="AdmitCardTop3" style="">
                         <table>
                             <tr> <td colspan="2" style="font-size: 19px; font-weight: bold;"> Admit Card </td></tr>
                             <tr> <td> Adm.No. </td> <td> {{$customadmitcard->admission_no}} </td></tr>
                         </table>
                    </div>
                </div>
                <div class="AdmitCardContentLeft" style="">
                    <table style="font-size: 15px; width: 100%;">
                        
                    <tr> <td><b> St. Name   </b></td><td>:</td><td><b> {{ substr($customadmitcard->student_name, 0, 30 )}} </b> </td> </tr>
                    <tr> <td><b> M. Name   </b></td><td>:</td><td><b> {{ substr($customadmitcard->mother_name, 0, 30 )}} </b> </td> </tr>
                    <tr> <td><b> F. Name   </b></td><td>:</td><td><b> {{ substr($customadmitcard->father_name, 0, 30 )}} </b> </td> </tr>
                    <tr> <td><b> Roll No   </b></td><td>:</td><td><b> {{ substr($customadmitcard->roll_no, 0, 30 )}} </b> </td> </tr>
                    <tr> <td><b> Class     </b></td><td>:</td><td><b> {{ substr($customadmitcard->class_section, 0, 30 )}} </b> </td> </tr>
                    <tr> <td><b> Exam Term  </b></td><td>:</td><td><b> {{ substr($customadmitcard->exam_term, 0, 30 )}} </b> </td> </tr>
                    <tr> <td><b> Session    </b></td><td>:</td><td><b> {{ substr($customadmitcard->session, 0, 30 )}} </b> </td> </tr>
                        
                    </table>
                    <div class="AdmitCardContentLeftbottom" style="">



                        <div class="AdmitCardContentLeftbottom1" style=""> </div>
                        <div class="AdmitCardContentLeftbottom2" style=""> <img src="{{URL('/')}}/images/principal.png" style="height: 100%;"> </div>
                        <div class="AdmitCardContentLeftbottom3" style=""> Students's Sign </div>
                        <div class="AdmitCardContentLeftbottom4" style=""> Principal's Sign </div>
                            
                    </div>
                </div>

                <div class="AdmitCardContentRight" style="">
                    <div class="r1" style="width: 89mm; height: 59mm; float: left;">
                        <table style="font-size: 13px; font-weight: bold; text-align:center; width: 100%;">
                        
                            <thead>
                                <tr>
                                    <td><b>     Subject          </b></td>
                                   
                                    <td><b>     Date             </b></td>
                                </tr>
                            </thead>
                            <tbody>    
                                <tr>
                                    <td> {{ substr($customadmitcard->subject1, 0, 23 )}} </td>
                                    
                                    <td> {{ substr($customadmitcard->date1, 0, 12 )}} </td>
                                </tr>
                                <tr>
                                    <td> {{ substr($customadmitcard->subject2, 0, 23 )}} </td>
                                    
                                    <td> {{ substr($customadmitcard->date2, 0, 12 )}} </td>
                                </tr>
                                <tr>
                                    <td> {{ substr($customadmitcard->subject3, 0, 23 )}} </td>
                                    
                                    <td> {{ substr($customadmitcard->date3, 0, 12 )}} </td>
                                </tr>
                                <tr>
                                    <td> {{ substr($customadmitcard->subject4, 0, 23 )}} </td>
                                    
                                    <td> {{ substr($customadmitcard->date4, 0, 12 )}} </td>
                                </tr>
                                <tr>
                                    <td> {{ substr($customadmitcard->subject5, 0, 23 )}} </td>
                                    
                                    <td> {{ substr($customadmitcard->date5, 0, 12 )}} </td>
                                </tr>
                                <tr>
                                    <td> {{ substr($customadmitcard->subject6, 0, 23 )}} </td>
                                    
                                    <td> {{ substr($customadmitcard->date6, 0, 12 )}} </td>
                                </tr>
                                <tr>
                                    <td> {{ substr($customadmitcard->subject7, 0, 23 )}} </td>
                                    
                                    <td> {{ substr($customadmitcard->date7, 0, 12 )}} </td>
                                </tr>
                                <tr>
                                    <td> {{ substr($customadmitcard->subject8, 0, 23 )}} </td>
                                    
                                    <td> {{ substr($customadmitcard->date8, 0, 12 )}} </td>
                                </tr>
                                <tr>
                                    <td> {{ substr($customadmitcard->subject9, 0, 23 )}} </td>
                                    
                                    <td> {{ substr($customadmitcard->date9, 0, 12 )}} </td>
                                </tr>
                                <tr>
                                    <td> {{ substr($customadmitcard->subject10, 0, 23 )}} </td>
                                    
                                    <td> {{ substr($customadmitcard->date10, 0, 12 )}} </td>
                                </tr>
                                  
                            </tbody>
                            


                        </table>
                    </div>
                    <div class="r2" style="width: 89mm; height: 6mm; float: left; text-align: center; ">
                        <span> School Seal </span>
                    </div>
                </div>
                
        </div>

        <div class="GapAfterAdmitCard" style="width: 210mm; height: 15mm; "> </div>
    </div>
   
    





</body>
</html>
