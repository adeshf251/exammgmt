@extends('layouts.app')

@section('content')

<!-- will be used to show any messages -->
@if(Session('message'))
    <div class="alert alert-info">{{ Session('message') }}</div>
@endif

<h3>Showing the Details of Custom Admit Cards  </h3>

<a href="{{ URL::to('customadmitcardmgmt/create') }}">
    <button type="button" class="btn btn-primary" style="margin: 10px 0px 0px 10px;">Generate New Custom Admit Card</button>
</a>

<div class="panel-body" style="overflow-x: scroll;">
    <table width="100%" class="table table-striped table-bordered table-hover" id="AdvancedTable">
            <thead>
                <th> ID </th>
                <th>  </th>
                <th></th>
                <th></th>
                <th> student name </th>
                <th> mother name </th>
                <th> father name </th>
                <th> roll no </th>
                <th> class section </th>
                <th> Exam Term </th>
                <th> session </th>
                <th> subject 1 </th>
                <th> date 1 </th>
                <th> subject 2 </th>
                <th> date 2 </th>
                <th> subject 3 </th>
                <th> date 3 </th>
                <th> subject 4 </th>
                <th> date 4 </th>
                <th> subject 5 </th>
                <th> date 5 </th>
                <th> subject 6 </th>
                <th> date 6 </th>
                <th> subject 7 </th>
                <th> date 7 </th>
                <th> subject 8 </th>
                <th> date 8 </th>
                <th> subject 9 </th>
                <th> date 9 </th>
                <th> subject 10 </th>
                <th> date 10 </th>                              
                
            </thead>
        @foreach ($data as $element)
            <tr>
                <td> {{ $element->id }} </td> 

                    <td> 
                        <a href="{{ URL::to('customadmitcardmgmt/' . $element->id . '/edit') }}">
                            <input type="button" class="btn btn-info  btn-sm" name="" id="" value="Edit">
                        </a>  &nbsp;  
                    
                    </td>
                    <td>
                    <form method="POST" action="{{ URL('customadmitcardmgmt') }}/{{$element->id}}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE">
                    <input type="submit" class="btn btn-danger  btn-sm" name="submit" id="{{$element->id}}" value="Delete">
                    </form>

                    </td>
                    <td> 
                        <a href="{{ URL::to('customadmitcardmgmt/' . $element->id . '/display') }}">
                            <input type="button" class="btn btn-info  btn-sm" name="" id="" value="Print">
                        </a>  &nbsp; 
                    
                    </td>
                    
                <td> {{ $element->student_name }} </td> 
                <td> {{ $element->mother_name }} </td> 
                <td> {{ $element->father_name }} </td> 
                <td> {{ $element->roll_no }} </td> 
                <td> {{ $element->class_section }} </td> 
                <td> {{ $element->exam_term }} </td> 
                <td> {{ $element->session }} </td> 
                <td> {{ $element->subject1 }} </td> 
                <td> {{ $element->date1 }} </td> 
                <td> {{ $element->subject2 }} </td> 
                <td> {{ $element->date2 }} </td> 
                <td> {{ $element->subject3 }} </td> 
                <td> {{ $element->date3 }} </td> 
                <td> {{ $element->subject4 }} </td> 
                <td> {{ $element->date4 }} </td> 
                <td> {{ $element->subject5 }} </td> 
                <td> {{ $element->date5 }} </td> 
                <td> {{ $element->subject6 }} </td> 
                <td> {{ $element->date6 }} </td> 
                <td> {{ $element->subject7 }} </td> 
                <td> {{ $element->date7 }} </td> 
                <td> {{ $element->subject8 }} </td> 
                <td> {{ $element->date8 }} </td> 
                <td> {{ $element->subject9 }} </td> 
                <td> {{ $element->date9 }} </td> 
                <td> {{ $element->subject10 }} </td> 
                <td> {{ $element->date10 }} </td> 
                
                
                
            </tr>
        @endforeach
    </table>
</div>


@endsection

