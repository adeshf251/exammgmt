@extends('layouts.app')

@section('content')

<div class="panel-heading">Make a custom Admit Card</div>

<div class="panel-body">
    <form class="form-horizontal" method="POST" action="{{ route('customadmitcardmgmt.store') }}">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('admission_no') ? ' has-error' : '' }}">
            <label for="admission_no" class="col-md-4 control-label">Admission Number</label>

            <div class="col-md-6">
                <input id="admission_no" type="text" class="form-control" name="admission_no" value="{{ old('admission_no') }}" autofocus>

                @if ($errors->has('admission_no'))
                    <span class="help-block">
                        <strong>{{ $errors->first('admission_no') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('student_name') ? ' has-error' : '' }}">
            <label for="student_name" class="col-md-4 control-label">Student Name</label>

            <div class="col-md-6">
                <input id="student_name" type="text" class="form-control" name="student_name" value="{{ old('student_name') }}" autofocus>

                @if ($errors->has('student_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('student_name') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <div class="form-group{{ $errors->has('mother_name') ? ' has-error' : '' }}">
            <label for="mother_name" class="col-md-4 control-label">Mothers Name</label>

            <div class="col-md-6">
                <input id="mother_name" type="text" class="form-control" name="mother_name" value="{{ old('mother_name') }}" autofocus>

                @if ($errors->has('mother_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('mother_name') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <div class="form-group{{ $errors->has('father_name') ? ' has-error' : '' }}">
            <label for="father_name" class="col-md-4 control-label">Father's Name</label>

            <div class="col-md-6">
                <input id="father_name" type="text" class="form-control" name="father_name" value="{{ old('father_name') }}" autofocus>

                @if ($errors->has('father_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('father_name') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('roll_no') ? ' has-error' : '' }}">
            <label for="roll_no" class="col-md-4 control-label">Roll No</label>

            <div class="col-md-6">
                <input id="roll_no" type="text" class="form-control" name="roll_no" value="{{ old('roll_no') }}" autofocus>

                @if ($errors->has('roll_no'))
                    <span class="help-block">
                        <strong>{{ $errors->first('roll_no') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('class_section') ? ' has-error' : '' }}">
            <label for="class_section" class="col-md-4 control-label">Class-section</label>

            <div class="col-md-6">
                <input id="class_section" type="text" class="form-control" name="class_section" value="{{ old('class_section') }}" autofocus>

                @if ($errors->has('class_section'))
                    <span class="help-block">
                        <strong>{{ $errors->first('class_section') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('exam_term') ? ' has-error' : '' }}">
            <label for="exam_term" class="col-md-4 control-label">Exam Term (like- Half Yearly)</label>

            <div class="col-md-6">
                <input id="exam_term" type="text" class="form-control" name="exam_term" value="{{ old('exam_term') }}" autofocus>

                @if ($errors->has('exam_term'))
                    <span class="help-block">
                        <strong>{{ $errors->first('exam_term') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('session') ? ' has-error' : '' }}">
            <label for="session" class="col-md-4 control-label">Session (yyyy-yyyy)</label>

            <div class="col-md-6">
                <input id="session" type="text" class="form-control" name="session" value="{{ old('session') }}" autofocus >

                @if ($errors->has('session'))
                    <span class="help-block">
                        <strong>{{ $errors->first('session') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <div class="form-group{{ $errors->has('subject1') ? ' has-error' : '' }}">
            <label for="subject1" class="col-md-4 control-label">subject 1</label>

            <div class="col-md-6">
                <input id="subject1" type="text" class="form-control" name="subject1" value="{{ old('subject1') }}" autofocus>

                @if ($errors->has('subject1'))
                    <span class="help-block">
                        <strong>{{ $errors->first('subject1') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('date1') ? ' has-error' : '' }}">
            <label for="date1" class="col-md-4 control-label">date 1</label>

            <div class="col-md-6">
                <input id="date1" type="text" class="form-control" name="date1" value="{{ old('date1') }}" autofocus>

                @if ($errors->has('date1'))
                    <span class="help-block">
                        <strong>{{ $errors->first('date1') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('subject2') ? ' has-error' : '' }}">
            <label for="subject2" class="col-md-4 control-label">subject 2</label>

            <div class="col-md-6">
                <input id="subject2" type="text" class="form-control" name="subject2" value="{{ old('subject2') }}" autofocus>

                @if ($errors->has('subject2'))
                    <span class="help-block">
                        <strong>{{ $errors->first('subject2') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('date2') ? ' has-error' : '' }}">
            <label for="date2" class="col-md-4 control-label">date 2</label>

            <div class="col-md-6">
                <input id="date2" type="text" class="form-control" name="date2" value="{{ old('date2') }}" autofocus>

                @if ($errors->has('date2'))
                    <span class="help-block">
                        <strong>{{ $errors->first('date2') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <div class="form-group{{ $errors->has('subject3') ? ' has-error' : '' }}">
            <label for="subject3" class="col-md-4 control-label">subject 3</label>

            <div class="col-md-6">
                <input id="subject3" type="text" class="form-control" name="subject3" value="{{ old('subject3') }}" autofocus>

                @if ($errors->has('subject3'))
                    <span class="help-block">
                        <strong>{{ $errors->first('subject3') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('date3') ? ' has-error' : '' }}">
            <label for="date3" class="col-md-4 control-label">date 3</label>

            <div class="col-md-6">
                <input id="date3" type="text" class="form-control" name="date3" value="{{ old('date3') }}" autofocus>

                @if ($errors->has('date3'))
                    <span class="help-block">
                        <strong>{{ $errors->first('date3') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('subject4') ? ' has-error' : '' }}">
            <label for="subject4" class="col-md-4 control-label">subject 4</label>

            <div class="col-md-6">
                <input id="subject4" type="text" class="form-control" name="subject4" value="{{ old('subject4') }}" autofocus>

                @if ($errors->has('subject4'))
                    <span class="help-block">
                        <strong>{{ $errors->first('subject4') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('date4') ? ' has-error' : '' }}">
            <label for="date4" class="col-md-4 control-label">date 4</label>

            <div class="col-md-6">
                <input id="date4" type="text" class="form-control" name="date4" value="{{ old('date4') }}" autofocus>

                @if ($errors->has('date4'))
                    <span class="help-block">
                        <strong>{{ $errors->first('date4') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <div class="form-group{{ $errors->has('subject5') ? ' has-error' : '' }}">
            <label for="subject5" class="col-md-4 control-label">subject 5</label>

            <div class="col-md-6">
                <input id="subject5" type="text" class="form-control" name="subject5" value="{{ old('subject5') }}" autofocus>

                @if ($errors->has('subject5'))
                    <span class="help-block">
                        <strong>{{ $errors->first('subject5') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('date5') ? ' has-error' : '' }}">
            <label for="date5" class="col-md-4 control-label">date 5</label>

            <div class="col-md-6">
                <input id="date5" type="text" class="form-control" name="date5" value="{{ old('date5') }}" autofocus>

                @if ($errors->has('date5'))
                    <span class="help-block">
                        <strong>{{ $errors->first('date5') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <div class="form-group{{ $errors->has('subject6') ? ' has-error' : '' }}">
            <label for="subject6" class="col-md-4 control-label">subject 6</label>

            <div class="col-md-6">
                <input id="subject6" type="text" class="form-control" name="subject6" value="{{ old('subject6') }}" autofocus>

                @if ($errors->has('subject6'))
                    <span class="help-block">
                        <strong>{{ $errors->first('subject6') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('date6') ? ' has-error' : '' }}">
            <label for="date6" class="col-md-4 control-label">date 6</label>

            <div class="col-md-6">
                <input id="date6" type="text" class="form-control" name="date6" value="{{ old('date6') }}" autofocus>

                @if ($errors->has('date6'))
                    <span class="help-block">
                        <strong>{{ $errors->first('date6') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('subject7') ? ' has-error' : '' }}">
            <label for="subject7" class="col-md-4 control-label">subject 7</label>

            <div class="col-md-6">
                <input id="subject7" type="text" class="form-control" name="subject7" value="{{ old('subject7') }}" autofocus>

                @if ($errors->has('subject7'))
                    <span class="help-block">
                        <strong>{{ $errors->first('subject7') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('date7') ? ' has-error' : '' }}">
            <label for="date7" class="col-md-4 control-label">date 7</label>

            <div class="col-md-6">
                <input id="date7" type="text" class="form-control" name="date7" value="{{ old('date7') }}" autofocus>

                @if ($errors->has('date7'))
                    <span class="help-block">
                        <strong>{{ $errors->first('date7') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('subject8') ? ' has-error' : '' }}">
            <label for="subject8" class="col-md-4 control-label">subject 8</label>

            <div class="col-md-6">
                <input id="subject8" type="text" class="form-control" name="subject8" value="{{ old('subject8') }}" autofocus>

                @if ($errors->has('subject8'))
                    <span class="help-block">
                        <strong>{{ $errors->first('subject8') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('date8') ? ' has-error' : '' }}">
            <label for="date8" class="col-md-4 control-label">date 8</label>

            <div class="col-md-6">
                <input id="date8" type="text" class="form-control" name="date8" value="{{ old('date8') }}" autofocus>

                @if ($errors->has('date8'))
                    <span class="help-block">
                        <strong>{{ $errors->first('date8') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('subject9') ? ' has-error' : '' }}">
            <label for="subject9" class="col-md-4 control-label">subject 9</label>

            <div class="col-md-6">
                <input id="subject9" type="text" class="form-control" name="subject9" value="{{ old('subject9') }}" autofocus>

                @if ($errors->has('subject9'))
                    <span class="help-block">
                        <strong>{{ $errors->first('subject9') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('date9') ? ' has-error' : '' }}">
            <label for="date9" class="col-md-4 control-label">date 9</label>

            <div class="col-md-6">
                <input id="date9" type="text" class="form-control" name="date9" value="{{ old('date9') }}" autofocus>

                @if ($errors->has('date9'))
                    <span class="help-block">
                        <strong>{{ $errors->first('date9') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('subject10') ? ' has-error' : '' }}">
            <label for="subject10" class="col-md-4 control-label">subject 10</label>

            <div class="col-md-6">
                <input id="subject10" type="text" class="form-control" name="subject10" value="{{ old('subject10') }}" autofocus>

                @if ($errors->has('subject10'))
                    <span class="help-block">
                        <strong>{{ $errors->first('subject10') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('date10') ? ' has-error' : '' }}">
            <label for="date10" class="col-md-4 control-label">date 10</label>

            <div class="col-md-6">
                <input id="date10" type="text" class="form-control" name="date10" value="{{ old('date10') }}" autofocus>

                @if ($errors->has('date10'))
                    <span class="help-block">
                        <strong>{{ $errors->first('date10') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    Register
                </button>
            </div>
        </div>

    </form>
</div>

@endsection
