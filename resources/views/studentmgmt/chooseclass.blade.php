@extends('layouts.app')

@section('content')

<div class="panel-heading">Choose Class</div>

<div class="panel-body">
    <form class="form-horizontal" method="POST" action="{{ URL('/') }}/studentlist-classwise">
        {{ csrf_field() }}


        <div class="form-group{{ $errors->has('student_class_section') ? ' has-error' : '' }}">
            <label for="student_class_section" class="col-md-4 control-label">Class Applicable</label>

            <div class="col-md-6">
                <select  id="student_class_section" class="form-control" name="student_class_section" >

                    @foreach ($classlist as $element)
                         <option value="{{$element}}"> {{$element}}</option>
                    @endforeach

                </select>

                @if ($errors->has('student_class_section'))
                    <span class="help-block">
                        <strong>{{ $errors->first('student_class_section') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-success">
                    Proceed
                </button>
            </div>
        </div>

    </form>
</div>

@endsection
