@extends('layouts.app')

@section('content')

<!-- will be used to show any messages -->
@if(Session('message'))
    <div class="alert alert-info">{{ Session('message') }}</div>
@endif

<h3>Showing the Details of Registered Students  </h3>

<a href="{{ URL::to('studentmgmt/create') }}">
    <button type="button" class="btn btn-primary" style="margin: 10px 0px 0px 10px;">Register New Student</button>
</a>

<div class="panel-body">
    <table width="100%" class="table table-striped table-bordered table-hover" id="AdvancedTable">
            <thead>
                <th> ID </th>
                <th>  </th>
                <th></th>
                <th></th>
                <th> admission no </th>
                <th> student name </th>
                <th> student father name </th>
                <th> student mother name </th>
                <th> student class section </th>
                <th> student address </th>
                <th> student joining date </th>
                <th> student images </th>
                <th> student dob </th>
                <th> student gender </th>
                <th> student username </th>
                <th> student password </th>
                <th> sessionid </th>
                <th> student email </th>
                <th> student mob1 </th>
                <th> student mob2 </th>
                <th> student rollno </th>
                
                
            </thead>
        @foreach ($studentmgmt as $element)
            <tr>
                <td> {{ $element->id }} </td> 

                    <td> 
                        <a href="{{ URL::to('studentmgmt/' . $element->id . '/edit') }}">
                            <input type="button" class="btn btn-info  btn-sm" name="" id="" value="Edit">
                        </a>   
                    
                    </td>

                    <td> 
                        <a href="{{ URL::to('studentmgmt/' . $element->id . '/duplicate') }}">
                            <input type="button" class="btn btn-warning  btn-sm" name="" id="" value="Duplicate">
                        </a>  
                    
                    </td>

                    <td>
                    <form method="POST" action="{{ URL('studentmgmt') }}/{{$element->id}}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE">
                    <input type="submit" class="btn btn-danger  btn-sm" name="submit" id="$element->id" value="Delete">
                    </form> 

                    </td>
                    
                <td> {{ $element->admission_no }} </td> 
                <td> {{ $element->student_name }} </td>
                <td> {{ $element->student_father_name }} </td>
                <td> {{ $element->student_mother_name }} </td>
                <td> {{ $element->student_class_section }} </td>
                <td> {{ $element->student_address }} </td>
                <td> {{ $element->student_joining_date }} </td>
                <td> {{ $element->student_images }} </td>
                <td> {{ $element->student_dob }} </td>
                <td> {{ $element->student_gender }} </td>
                <td> {{ $element->student_username }} </td>
                <td> {{ $element->student_password }} </td>
                <td> {{ $element->sessionid }} </td>
                <td> {{ $element->student_email }} </td>
                <td> {{ $element->student_mob1 }} </td>
                <td> {{ $element->student_mob2 }} </td>
                <td> {{ $element->student_rollno }} </td>
                
            </tr>
        @endforeach
    </table>
</div>


@endsection

