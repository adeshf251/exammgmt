@extends('layouts.app')

@section('content')

<!-- will be used to show any messages -->
@if(Session('message'))
    <div class="alert alert-info">{{ Session('message') }}</div>
@endif

<h3>Showing the Details of Registered Discipline  </h3>

<a href="{{ URL::to('discipline/create') }}">
    <button type="button" class="btn btn-primary" style="margin: 10px 0px 0px 10px;">Register New Discipline</button>
</a>



<div class="panel-body" style="overflow-x: scroll;">
    <table width="100%" class="table table-striped table-bordered table-hover" id="AdvancedTable">
            <thead>
                <th> ID </th>
                <th></th>
                <th></th>
                <th> Discipline Code </th>
                <th> Discipline Name </th>
                <th> Class Applicable </th>
                <th> Session ID </th>
                
            </thead>
        @foreach ($disciplinelist as $element)
            <tr>
                <td> {{ $element->id }} </td> 

                    <td> 
                        <a href="{{ URL::to('discipline/' . $element->id . '/edit') }}">
                            <input type="button" class="btn btn-info  btn-sm" name="" id="" value="Edit">
                        </a>   
                    
                    </td>

                    <td>
                    <form method="POST" action="{{ URL('discipline') }}/{{$element->id}}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE">
                    <input type="submit" class="btn btn-danger  btn-sm" name="submit" id="{{$element->id}}" value="Delete">
                    </form>

                    </td>
                    
                <td> {{ $element->discipline_code }} </td> 
                <td> {{ $element->discipline_name }} </td>
                <td> {{ $element->class_applicable }} </td>
                <td> {{ $element->sessionid }} </td>                
            </tr>
        @endforeach
    </table>
</div>


@endsection

