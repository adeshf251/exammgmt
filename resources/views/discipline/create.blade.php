@extends('layouts.app')

@section('content')

<div class="panel-heading">Register</div>

<div class="panel-body">
    <form class="form-horizontal" method="POST" action="{{ route('discipline.store') }}">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('disciplinecode') ? ' has-error' : '' }}">
            <label for="disciplinecode" class="col-md-4 control-label">Discipline Code</label>

            <div class="col-md-6">
                <input id="disciplinecode" type="text" class="form-control" name="disciplinecode" value="{{ old('disciplinecode') }}" autofocus>

                @if ($errors->has('disciplinecode'))
                    <span class="help-block">
                        <strong>{{ $errors->first('disciplinecode') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('disciplinename') ? ' has-error' : '' }}">
            <label for="disciplinename" class="col-md-4 control-label">Discipline Name</label>

            <div class="col-md-6">
                <input id="disciplinename" type="text" class="form-control" name="disciplinename" value="{{ old('disciplinename') }}" autofocus>

                @if ($errors->has('disciplinename'))
                    <span class="help-block">
                        <strong>{{ $errors->first('disciplinename') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <div class="form-group{{ $errors->has('classapplicable') ? ' has-error' : '' }}">
            <label for="classapplicable" class="col-md-4 control-label">Class Applicable</label>

            <div class="col-md-6">
                <select  id="classapplicable[]" class="form-control" name="classapplicable[]"  multiple="multiple" >
                    @foreach ($classlist as $element)
                         <option value="{{$element}}"> {{$element}}</option>
                    @endforeach
                </select>

                @if ($errors->has('classapplicable'))
                    <span class="help-block">
                        <strong>{{ $errors->first('classapplicable') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('extra') ? ' has-error' : '' }}">
            <label for="extra" class="col-md-4 control-label">Any Comment</label>

            <div class="col-md-6">
                <input id="extra" type="text" class="form-control" name="extra" value="{{ old('extra') }}" autofocus>

                @if ($errors->has('extra'))
                    <span class="help-block">
                        <strong>{{ $errors->first('extra') }}</strong>
                    </span>
                @endif
            </div>
        </div>



        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    Register
                </button>
            </div>
        </div>

    </form>
</div>

@endsection
