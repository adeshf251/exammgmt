@extends('layouts.app')

@section('content')


@if(Session('message'))
    <div class="alert alert-danger">{{ Session('message') }}</div>
@endif



<div class="panel-body">
    <form class="form-horizontal" method="POST" action="{{ URL('/') }}/sendmailsubmit">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="col-md-4 control-label">Email Receiver (only one) </label>

            <div class="col-md-6">
                <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" autofocus>

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>



        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
            <label for="title" class="col-md-4 control-label"> Subject / Title </label>

            <div class="col-md-6">
                <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}" autofocus>

                @if ($errors->has('title'))
                    <span class="help-block">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
            <label for="content" class="col-md-4 control-label"> Message Body </label>

            <div class="col-md-6">
                <textarea name="content" id="content" value="{{ old('content') }}" class="form-control" cols="30" rows="10" required></textarea>
                @if ($errors->has('content'))
                    <span class="help-block">
                        <strong>{{ $errors->first('content') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <input type="hidden" class="form-control" name="resulttype" value="term1" autofocus>

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                   Send Email
                </button>
            </div>
        </div>

    </form>
</div>


@endsection

