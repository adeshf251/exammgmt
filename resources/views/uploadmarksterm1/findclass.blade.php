@extends('layouts.app')

@section('content')

<div class="panel-heading">Find By Admission Number Only</div>

<div class="panel-body">
    <form class="form-horizontal" method="POST" action="{{ URL('term1entries') }}/create/findstudent">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('class_applicable') ? ' has-error' : '' }}">
            <label for="class_applicable" class="col-md-4 control-label">Class Applicable</label>

            <div class="col-md-6">
                <select  id="class_applicable" class="form-control" name="class_applicable" >
                    @foreach ($classlist as $element)
                         <option value="{{$element}}"> {{$element}}</option>
                    @endforeach
                </select>

                @if ($errors->has('class_applicable'))
                    <span class="help-block">
                        <strong>{{ $errors->first('class_applicable') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('subject_type') ? ' has-error' : '' }}">
            <label for="subject_type" class="col-md-4 control-label">Subject Types</label>

            <div class="col-md-6">
                <select  id="subject_type" class="form-control" name="subject_type" >
                         <option value="1"> Scholastic </option>
                         <option value="2"> Co-Scholastic </option>
                         <option value="3"> Discipline </option>
                </select>

                @if ($errors->has('subject_type'))
                    <span class="help-block">
                        <strong>{{ $errors->first('subject_type') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    Find And Fill Student Marks
                </button>
            </div>
        </div>

    </form>
</div>


@endsection
