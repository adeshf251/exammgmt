<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
        div {
            text-transform: capitalize;
        }
    </style>
</head>
<body>
<form class="form-horizontal" method="POST" action="{{ route('term1entries.store') }}">
        {{ csrf_field() }}
    <div class="admitcard-mainblock" style="width: 200mm; height: 280mm;">
        <div class="admitcard-block"  style="background-image: url( '{{URL('/')}}/defaultimages/dellmond.png' ); background-size: 100% 100%;">
                <div class="AdmitCardTop" style="width: 100%; height: 48mm;">

                </div>
                <div class="AdmitCardTop" style="width: 100%; height: 15mm; text-align: center;">
                   <span style="font-size: 20px; font-weight: bold;"> Class : {{$class_section}} </span> <br>
                   <span style="font-size: 20px; font-weight: bold;"> Academic Session : 2017-2018 (Term 1) </span>
                   <input id="hclass_section" type="hidden" name="hclass_section" value="{{$class_section}}" >
                </div>
                <div class="AdmitCardTop" style="width: 100%; height: 30mm;">

                    <div style="width: 60%; height: 100%; float: left;">
                      @foreach ($studentdetails as $element)
                        <table class="table" style="padding-left: 20px;">
                              <tr>
                                <td  style="border:none; width:125px">Students's Name</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">
                                   <input type="text" value="{{$element->student_name}}" disabled="disabled">
                                </td>
                              </tr>

                              <tr>
                                <td  style="border:none; width:125px">Mother's Name</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">
                                  <input type="text" value="{{$element->student_mother_name}}" disabled="disabled">
                                </td>
                              </tr>

                              <tr>
                                <td  style="border:none; width:125px">Father's Name</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">
                                <input type="text" value="{{$element->student_father_name}}" disabled="disabled">
                                </td>
                              </tr>

                              <tr>
                                <td  style="border:none; width:125px">Date of Birth</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">
                                  <input type="text" value="{{$element->student_dob}}" disabled="disabled">
                                </td>
                              </tr>
                        </table>
                    </div>

                    <div style="width: 38%; height: 100%; float: left;">
                        <table class="table">
                              <tr>
                                <td  style="border:none; width:100px">Admission No</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">
                                  <input type="text" value="{{$element->admission_no}}" disabled="disabled">
                                  <input id="hadmission_no" type="hidden" name="hadmission_no" value="{{$element->admission_no}}" >
                                </td>
                              </tr>
                              <tr>
                                <td  style="border:none; width:100px">Roll No</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">
                                  <input type="text" value="{{$element->student_rollno}}" disabled="disabled">
                                </td>
                              </tr>
                        </table>
                    </div>
                  @endforeach
                </div>

                <div class="AdmitCardTop" style="width: 100%; height: 147mm;">
                    
                    <table style="width: 95%; margin-left: 20px; text-align: center; border-collapse: collapse; border: 1px solid black;">

                            <tr style="background-color: #4CAF50; border: 1px solid black;">
                              <td colspan="2" style="border: 1px solid black;">Scholastic Area</td>
                              <td colspan="6" style="border: 1px solid black;">Term 1 Marks</td>
                            </tr>

                            <tr style="background-color: skyblue; border: 1px solid black;">
                              <td  style="border: 1px solid black;">SNo.</td>
                              <td  style="border: 1px solid black; width:275px">Sub Name</td>
                              <td  colspan="2" style="border: 1px solid black;">Unit Test Marks</td>
                              <td  colspan="2" style="border: 1px solid black;">Exam Marks</td>
                              <td  colspan="2" style="border: 1px solid black;">Total</td>
                            </tr>

                            @foreach ($subjectlist as $element)
                                <tr>
                                    
                                    <td>
                                          <input style="width: 55px;" type="text" name="dsubject_code[]" value="{{$element->subject_code}}" disabled="disabled">
                                          <input style="width: 55px;" type="hidden" name="hsubject_code[]" value="{{$element->subject_code}}">
                                    </td>
                                    <td>
                                          <input style="width: 275px;" type="text" name="subject_name[]" value="{{$element->subject_name}}" disabled="disabled">
                                          <input style="width: 275px;" type="hidden" name="hsubject_name[]" value="{{$element->subject_name}}" >
                                    </td>
                                    <td>
                                          <input style="width: 55px;" type="number" name="dunit[]" value="" min="-1" max="{{$element->term1_unittest}}">
                                    </td>
                                    <td>
                                          <input style="width: 55px;" type="text" name="dunitmax[]" value="{{$element->term1_unittest}}" disabled="disabled">
                                          <input style="width: 55px;" type="hidden" name="hunitmax[]" value="{{$element->term1_unittest}}" >
                                    </td>
                                    <td>
                                          <input style="width: 55px;" type="number" name="dexam[]" value="" min="-1" max="{{$element->term1_exam}}">
                                    </td>
                                    <td>
                                          <input style="width: 55px;" type="text" name="dexammax[]" value="{{$element->term1_exam}}" disabled="disabled">
                                          <input style="width: 55px;" type="hidden" name="hexammax[]" value="{{$element->term1_exam}}">
                                    </td>
                                    <td>
                                          <input style="width: 55px;" type="text" name="dtotal[]" value="" disabled="disabled">
                                    </td>
                                    <td>
                                          <input style="width: 55px;" type="text" name="dtotalmax[]" value="{{$element->term1_total}}" disabled="disabled">
                                    </td>
                                    
                                  </tr>

                                  <input type="hidden" name="hunitmax_term2[]" value="{{$element->term2_unittest}}" >
                                  <input type="hidden" name="hexammax_term2[]" value="{{$element->term2_exam}}">

                            @endforeach
                    </table>
                    
                    <div style="width: 100%; height: 3mm;"> </div>

                    <div style="width: 100%; height: 60mm;">
                        <div style="width: 50%; float: left; height: 100%">

                            <table style="width: 90%; margin-left: 20px; text-align: center; border-collapse: collapse; border: 1px solid black; font-size: 13px; height: 100%">
                                    <tr style="background-color: #4CAF50; border: 1px solid black;">
                                      <td colspan="3" style="border: 1px solid black;">Co-Scholastic Area</td>
                                    </tr>

                                    <tr style="background-color: skyblue; border: 1px solid black;">
                                      <td  style="border: 1px solid black;">SNo.</td>
                                      <td  style="border: 1px solid black;">Elements</td>
                                      <td  style="border: 1px solid black;">Grade</td>
                                    </tr>

                              @foreach ($coscholasticlist as $row)
                                <tr>
                                    
                                    <td>
                                          <input style="width: 25px;" type="text" name="dcoscholastic_code[]" value="{{$row->coscholastic_code}}" disabled="disabled">
                                          <input style="width: 25px;" type="hidden" name="hcoscholastic_code[]" value="{{$row->coscholastic_code}}">
                                    </td>
                                    <td>
                                          <input style="width: 50px;" type="text" name="coscholastic_name[]" value="{{$row->coscholastic_name}}" disabled="disabled">
                                          <input style="width: 50px;" type="hidden" name="hcoscholastic_name[]" value="{{$row->coscholastic_name}}" >
                                    </td>
                                    <td>
                                          <select style="width: 25px;" name="coscholastic_grade[]">
                                              <option value="A"> A </option>
                                              <option value="B"> B </option>
                                              <option value="C"> C </option>
                                              <option value="D"> D </option>
                                          </select>
                                    </td>                                    
                                  </tr>

                              @endforeach
                                    {{-- <tr style="border: 1px solid black;">
                                      <td  style="border: 1px solid black;">2</td>
                                      <td  style="border: 1px solid black;">Doe</td>
                                      <td  style="border: 1px solid black;">24</td>
                                    </tr>
                                    <tr style="border: 1px solid black;">
                                      <td  style="border: 1px solid black;">3</td>
                                      <td  style="border: 1px solid black;">Doe</td>
                                      <td  style="border: 1px solid black;">24</td>
                                    </tr>
                                    <tr style="border: 1px solid black;">
                                      <td  style="border: 1px solid black;">4</td>
                                      <td  style="border: 1px solid black;">Doe</td>
                                      <td  style="border: 1px solid black;">24</td>
                                    </tr>
                                    <tr style="border: 1px solid black;">
                                      <td  style="border: 1px solid black;">5</td>
                                      <td  style="border: 1px solid black;">Doe</td>
                                      <td  style="border: 1px solid black;">24</td>
                                    </tr>
                                    <tr style="border: 1px solid black;">
                                      <td  style="border: 1px solid black;">6</td>
                                      <td  style="border: 1px solid black;">Doe</td>
                                      <td  style="border: 1px solid black;">24</td>
                                    </tr>
                                    <tr style="border: 1px solid black;">
                                      <td  style="border: 1px solid black;">7</td>
                                      <td  style="border: 1px solid black;">Doe</td>
                                      <td  style="border: 1px solid black;">24</td>
                                    </tr>
                                    <tr style="border: 1px solid black;">
                                      <td  style="border: 1px solid black;">8</td>
                                      <td  style="border: 1px solid black;">Doe</td>
                                      <td  style="border: 1px solid black;">24</td>
                                    </tr> --}}
                            </table>

                        </div>
                        <div style="width: 50%; float: left; height: 100%;">
                            <table style="width: 90%; margin-left: 20px; text-align: center; border-collapse: collapse; border: 1px solid black; font-size: 13px; height: 100%">
                                    <tr style="background-color: #4CAF50; border: 1px solid black;">
                                      <td colspan="3" style="border: 1px solid black;">Discipline</td>
                                    </tr>

                                    <tr style="background-color: skyblue; border: 1px solid black;">
                                      <td  style="border: 1px solid black;">SNo.</td>
                                      <td  style="border: 1px solid black;">Activity</td>
                                      <td  style="border: 1px solid black;">Grade</td>
                                    </tr>

                              @foreach ($disciplinelist as $row)
                                <tr>
                                    <td>
                                          <input style="width: 25px;" type="text" name="ddiscipline_code[]" value="{{$row->discipline_code}}" disabled="disabled">
                                          <input style="width: 25px;" type="hidden" name="hdiscipline_code[]" value="{{$row->discipline_code}}">
                                    </td>
                                    <td>
                                          <input style="width: 50px;" type="text" name="discipline_name[]" value="{{$row->discipline_name}}" disabled="disabled">
                                          <input style="width: 50px;" type="hidden" name="hdiscipline_name[]" value="{{$row->discipline_name}}" >
                                    </td>
                                    <td>
                                          <select style="width: 25px;" name="discipline_grade[]">
                                              <option value="A"> A </option>
                                              <option value="B"> B </option>
                                              <option value="C"> C </option>
                                              <option value="D"> D </option>
                                          </select>
                                    </td>                                    
                                </tr>
                              @endforeach
                              
                            </table>
                        </div>
                    </div>

                     <div style="width: 190mm; height: 5mm;"> </div>

                     <div class="AdmitCardTop" style="width: 100%; height: 15mm;">

                    <div style="width: 60%; height: 100%; float: left;">
                        <table class="table" style="padding-left: 20px;">
                              <tr>
                                <td  style="border:none; width:125px">Overall Marks</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">Narendra Modi</td>
                              </tr>

                              <tr>
                                <td  style="border:none; width:125px">Percentage</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">Heeraben Modi</td>
                              </tr>
                        </table>
                    </div>

                    <div style="width: 38%; height: 100%; float: left;">
                        <table class="table">
                              <tr>
                                <td  style="border:none; width:100px">Attendance</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">80.00 %</td>
                              </tr>
                              <tr>
                                <td  style="border:none; width:100px">Class Rank</td>
                                <td style="width:10px"> : </td>
                                <td style="border-bottom: 1px solid black">1</td>
                              </tr>
                        </table>
                    </div>

                    <table style="width: 95%; margin-left: 20px; text-align: center; border-collapse: collapse; border: 1px solid black;">

                            <tr style="border: 1px solid black;"">
                              <td style="background-color: #4CAF50;  width:100px">Remark</td>
                              <td> You Need to be more dedicated towards yours Studies, Your Few Section are weeker </td>
                            </tr>

                    </table>
                </div>

                

                </div>
                <div class="AdmitCardTop" style="width: 100%; height: 40mm;">
                    <div class="AdmitCardTop" style="width: 100%; height: 30mm;"> </div>
                    <div class="AdmitCardTop" style="width: 100%; height: 5mm; margin-left: 20px;"> Date : 28/10/2017</div>
                </div>
                
        </div>
    </div>




                <button type="submit" style="margin-left: 75mm; height: 15mm; background-color: red" class="btn btn-primary">
                    Upload/Update MARKS
                </button>
            </form>
</body>
</html>
