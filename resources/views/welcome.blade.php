<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Exam-Management</title>

        <!-- Fonts -->

        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <!-- Custom Fonts --><link href="{{URL('/')}}/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


        <!-- Styles -->
        <style>
            html, body {
                background-image: url('{{ url('/') }}/images/sc.jpg');
                background-size: 100%;
                background-position:left center;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 20px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .btn {
                width: 150px;
            }
            .box {
                background-color: white;
                padding-top: 40px;
                padding-bottom: 40px;
                padding-left: 100px;
                padding-right: 100px;
                border-radius: 20px;
                text-align: center;
                opacity: 0.9;
                box-shadow: 10px 10px 0px 0px rgb(185, 179, 179);
            }
            h2 {
                font-weight: 800;
                color: rgb(136, 50, 10);
            }
        </style>
    </head>
    <body>

        <div class="Container">
            <div class="row">
                <div class="flex-center position-ref full-height bg-white">
                    <div class="box">
                        <strong><h2>Dellmond International School</h2> </strong>
                        <h4>Welcome to Exam Management Section</h4>
                        <i> <h6>* Any unauthorized access attempts will not be entertained</h6> </i> 
                        <h4>Your IP Address is  <?php echo $_SERVER['REMOTE_ADDR'];
                        ?> </h4><hr>
                        @if (Route::has('login'))
                            <div class="links">
                                @if (Auth::check())
                                    <a href="{{ url('/home') }}"><button type="button" class="btn btn-primary">Home</button></a>
                                @else
                                    <a href="{{ url('/login') }}"><button type="button" class="btn btn-primary">Login <i class="fa fa-sign-in" aria-hidden="true"></i></button></a>
                                    <a href="{{ url('/register') }}"><button type="button" class="btn btn-primary">Register <i class="fa fa-user-plus" aria-hidden="true"></i></button></a>
                                @endif
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        
    </body>
</html>
