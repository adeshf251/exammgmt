@extends('layouts.app')

@section('content')
<div style="text-align:center;">
    <div class="col-md-12">
        <h3>
            Your Working Scope has been Modified to Admin Control.
        </h3>
        <h5> Please make only required modification. As any modification are irreversible. </h5>
    </div>
    <div class="col-md-12">
        <h1><i class="fa fa-unlock fa-4x" aria-hidden="true" style="color:green"></i></h1>
    </div>

</div>

@endsection

