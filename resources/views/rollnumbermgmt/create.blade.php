@extends('layouts.app')
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
@section('content')

<div class="panel-heading">Re-Initialize Roll Number</div>

<div class="panel-body">
    <form class="form-horizontal" method="POST" action="{{ route('rollnumbermgmt.store') }}">
        {{ csrf_field() }}

        

        <div class="form-group{{ $errors->has('class_applicable') ? ' has-error' : '' }}">
            <label for="class_applicable" class="col-md-4 control-label">Class Applicable</label>

            <div class="col-md-6">
                <select  id="class_applicable" class="form-control" name="class_applicable" >
                    @foreach ($classlist as $element)
                         <option value="{{$element}}"> {{$element}}</option>
                    @endforeach
                                                   </select>

                @if ($errors->has('class_applicable'))
                    <span class="help-block">
                        <strong>{{ $errors->first('class_applicable') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    Proceed
                </button>
            </div>
        </div>

    </form>
</div>


@endsection
