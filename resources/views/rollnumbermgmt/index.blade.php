@extends('layouts.app')

@section('content')

<!-- will be used to show any messages -->
@if(Session('message'))
    <div class="alert alert-info">{{ Session('message') }}</div>
@endif

<h3>Showing the Details of Registered Roll Number of Students  </h3>

<a href="{{ URL::to('rollnumbermgmt/create') }}">
    <button type="button" class="btn btn-primary" style="margin: 10px 0px 0px 10px;">Re-initialize the Roll Number </button>
</a>

<div class="panel-body" style="overflow-x: scroll;">
    <table width="100%" class="table table-striped table-bordered table-hover" id="AdvancedTable">
            <thead>
                <th> Admission Number </th>
                <th> Action </th>
                <th> Student Name </th>
                <th> Mother Name </th>
                <th> Father Name </th>
                <th> Class-Section </th>
                <th> Rollno </th>
                
            </thead>
        @foreach ($studentlist as $element)
            <tr>
                <td> {{ $element->admission_no }} </td> 

                    <td> 
                        <a href="{{ URL::to('rollnumbermgmt/' . $element->id . '/edit') }}">
                            <input type="button" class="btn btn-info  btn-sm" name="" id="" value="Edit">
                        </a>  
                    
                    </td>
                    
                    
                <td> {{ $element->student_name }} </td> 
                <td> {{ $element->student_mother_name }} </td>
                <td> {{ $element->student_father_name }} </td>
                <td> {{ $element->student_class_section }} </td>
                <td> {{ $element->student_rollno }} </td>
                                                
                
            </tr>
        @endforeach
    </table>
</div>


@endsection

