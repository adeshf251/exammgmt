@extends('layouts.app')

@section('content')

<!-- will be used to show any messages -->
@if(Session('message'))
    <div class="alert alert-info">{!! Session('message') !!}</div>
@endif

<h3>Showing the Details of Registered Session  </h3>

<a href="{{ URL::to('sessionmgmt/create') }}">
    <button type="button" class="btn btn-primary" style="margin: 10px 0px 0px 10px;">Register New Session</button>
</a>

<div class="panel-body" style="overflow-x: scroll;">
    <table width="100%" class="table table-striped table-bordered table-hover" id="AdvancedTable">
            <thead>
                <th> ID </th>
                <th> Session Name </th>
                <th> Status </th>
                <th> Description </th>
                
            </thead>
        @foreach ($data as $element)
            <tr>
                <td> {{ $element->id }} </td> 
                <td> {{ $element->session_period }} </td> 
                <td> {{ $element->status }} </td>
                <td> {{ $element->description }} </td>
            </tr>
        @endforeach
    </table>
</div>


@endsection

