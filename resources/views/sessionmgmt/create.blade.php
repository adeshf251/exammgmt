@extends('layouts.app')
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
@section('content')

<div class="panel-heading">Register The New Session</div>

<div class="panel-body">
    <form class="form-horizontal" method="POST" action="{{ route('sessionmgmt.store') }}">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('session_period') ? ' has-error' : '' }}">
            <label for="session_period" class="col-md-4 control-label">Session Period (YYYY-YYYY)</label>

            <div class="col-md-6">
                <input id="session_period" type="text" class="form-control" name="session_period" value="{{ old('session_period') }}" autofocus>

                @if ($errors->has('session_period'))
                    <span class="help-block">
                        <strong>{{ $errors->first('session_period') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
            <label for="description" class="col-md-4 control-label">Session Description</label>

            <div class="col-md-6">
                <input id="description" type="text" class="form-control" name="description" value="{{ old('description') }}" autofocus>

                @if ($errors->has('description'))
                    <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    Register this session
                </button>
            </div>
        </div>

    </form>
</div>


@endsection
