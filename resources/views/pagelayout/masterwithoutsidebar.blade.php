



<!DOCTYPE html>
<html lang="en">

<head>

    @include('pagelayout.header')




</head>

<body >

<div id="overlay" style="position:fixed;
                        z-index:10;
                        background-color: white;
                        width: 100%;
                        height: 100%;">
     <img src="{{URL('/')}}/defaultimages/loader.gif" alt="Loading" style="position: fixed;
  width: 300px;
  height: 200px;
  z-index: 15;
  top: 50%;
  left: 50%;" />
</div>


    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            
            <!-- /.navbar-top-links -->
            @include('pagelayout.navtopbar')
            
            
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper" style="background-image: url('{{URL('/')}}/defaultimages/background.png');
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-position: center; 
    background-size: cover; min-height: 650px; width: 100%;">
            <div class="container-fluid">
                <div class="row">

                    <div id="space-generator" style="width: 100%; height: 5px;"></div>

                    <div class="col-lg-12 col-md-12 col-sm-12" style="height: 100%; width: 100%; opacity: 0.96; background-color: white;">
                        
                        <!-- to load my custom application contents -->
                        @yield('content')
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    @include('pagelayout.footerscripts')

</body>

</html>
