<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">



        <ul class="nav" id="side-menu">

            <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <span style="color: grey">
                       <b> Action Menu </b>
                    </span>
                </div>
            </li>



            <li>
                <a href="{{URL('/')}}/dashboard">
                    <i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>



            <li>
                <a href="#">
                    <i class="fa fa-sitemap fa-fw"></i> Management
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{URL('/')}}/rollnumbermgmt">
                            <i class="fa fa-list-ol" aria-hidden="true"></i> RollNo Management</a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-users" aria-hidden="true"></i> Student Management
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{{URL('/')}}/studentmgmt">View All Students</a>
                            </li>
                            <li>
                                <a href="{{URL('/')}}/studentmgmt-classchoose">Classwise Students</a>
                            </li>
                            <li>
                                <a href="{{URL('/')}}/studentmgmt/create">Add New Student</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-list-alt" aria-hidden="true"></i> Class Management
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{{URL('/')}}/classmgmt">View All Classes</a>
                            </li>
                            <li>
                                <a href="{{URL('/')}}/classmgmt/create">Add New Class</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-book" aria-hidden="true"></i> Subject Management
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{{URL('/')}}/subjectmgmt">View All Subject</a>
                            </li>
                            <li>
                                <a href="{{URL('/')}}/subjectmgmt/create">Add New Subject</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>




            <!-- <li>
                <a href="{{URL('/')}}/uploadmarksterm1">
                    <i class="fa fa-edit fa-fw"></i> Upload Term1 Marks</a>
            </li> -->


            <li>
                <a href="#">
                    <i class="fa fa-edit fa-fw"></i> Entries
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{URL('/')}}/showmarks">
                            <i class="fa fa-television" aria-hidden="true"></i> Display Classwise Marks</a>
                    </li>
                    <li>
                        <a href="{{URL('/')}}/rankcalculator">
                            <i class="fa fa-calculator" aria-hidden="true"></i> Rank Calculator </a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Term 1 Marks Entries
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="{{URL('/')}}/term1entries">View All Entries</a>
                            </li>
                            <li>
                                <a href="{{URL('/')}}/term1entries/create/findstudent">Add/Update Studentwise</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Term 2 Marks Entries
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="{{URL('/')}}/term2entries">View All Entries</a>
                            </li>
                            <li>
                                <a href="{{URL('/')}}/term2entries/create/findstudent">Add/Update Studentwise</a>
                            </li>
                        </ul>
                    </li>

                </ul>

            </li>


            <li>
                <a href="#">
                    <i class="fa fa-sitemap fa-fw"></i> Cards
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{URL('/')}}/admit-card">
                            <i class="fa fa-user" aria-hidden="true"></i> Admit Cards</a>
                    </li>

                    <li>
                        <a href="{{URL('/')}}/customadmitcardmgmt">
                            <i class="fa fa-user" aria-hidden="true"></i> Custom Admit Cards</a>
                    </li>

                    <li>
                        <a href="#">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i> Report Card
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{{URL('/')}}/resultdate">Assign Result Dates</a>
                            </li>
                            <li>
                                <a href="{{URL('/')}}/reportcard/term1"><i class="fa fa-file" aria-hidden="true"></i> Term 1 Result </a>
                            </li>
                            <li>
                                <a href="{{URL('/')}}/reportcard/term2"><i class="fa fa-file" aria-hidden="true"></i> Term 2 Result </a>
                            </li>
                            <li>
                                <a href="{{URL('/')}}/reportcard/final"><i class="fa fa-file" aria-hidden="true"></i> Final Yearly Result</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="{{URL('/')}}/reportcard/bulk">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Bulk Report Card Print </a>
                    </li>

                </ul>

            </li>











            <li>
                <a href="#">
                    <i class="fa fa-futbol-o" aria-hidden="true"></i> Co-Scholastic Area
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{URL('/')}}/coscholastic">View All Co-Scholastic</a>
                    </li>
                    <li>
                        <a href="{{URL('/')}}/coscholastic/create">Add New Co-Scholastic</a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="#">
                    <i class="fa fa-eye" aria-hidden="true"></i> Discipline
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{URL('/')}}/discipline">View All Discipline</a>
                    </li>
                    <li>
                        <a href="{{URL('/')}}/discipline/create">Add New Discipline</a>
                    </li>
                </ul>
            </li>












            <li>
                <a href="#">
                    <i class="fa fa-cog" aria-hidden="true"></i> Settings
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">

                    <li>
                        <a href="#"><i class="fa fa-hourglass-end" aria-hidden="true"></i> Session
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="{{URL('/')}}/sessionmgmt">View ALL Session</a>
                            </li>
                            <li>
                                <a href="{{URL('/')}}/changesession">Change Working Session</a>
                            </li>
                            <li>
                                <a href="{{URL('/')}}/upgrade"><i class="fa fa-files-o" aria-hidden="true"></i> Upgrade to New Session</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>


            <li>
                <a href="{{URL('/')}}/sendmailform">
                    <i class="fa fa-paper-plane-o" aria-hidden="true"></i> Send Email </a>
            </li>

            <li>
                <a href="{{URL('/')}}/admin/login">
                    <i class="fa fa-sign-in" aria-hidden="true"></i> Admin Login </a>
            </li>

            <li>
                <a href="{{URL('/')}}/showallusers">
                    <i class="fa fa-user" aria-hidden="true"></i> User lists </a>
            </li>

            <li>
                <a href="{{URL('/')}}/showalladmins">
                    <i class="fa fa-user" aria-hidden="true"></i> Admin lists </a>
            </li>

        </ul>
    </div>
</div>