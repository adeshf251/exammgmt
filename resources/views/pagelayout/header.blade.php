<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title> &copy; Dellmond International School </title>

<!-- Bootstrap Core CSS -->
<link href="{{URL('/')}}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="{{URL('/')}}/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="{{URL('/')}}/dist/css/sb-admin-2.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="{{URL('/')}}/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- DataTables CSS -->
<link href="{{URL('/')}}/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="{{URL('/')}}/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

 <!-- jQuery -->
<script src="{{URL('/')}}/vendor/jquery/jquery.min.js"></script>
  

<link rel="stylesheet" href="{{ url('/') }}/css/buttons.dataTables.min.css">
  <link rel="stylesheet" href="{{ url('/') }}/css/jquery.dataTables.min.css">


    
