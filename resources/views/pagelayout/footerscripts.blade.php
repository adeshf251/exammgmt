

<!-- Bootstrap Core JavaScript -->
<script src="{{URL('/')}}/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="{{URL('/')}}/vendor/metisMenu/metisMenu.min.js"></script>

{{--  <script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>  --}}

<!-- DataTables JavaScript -->
<script src="{{URL('/')}}/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="{{URL('/')}}/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="{{URL('/')}}/vendor/datatables-responsive/dataTables.responsive.js"></script>

<!-- Custom Theme JavaScript -->
<script src="{{URL('/')}}/dist/js/sb-admin-2.js"></script>



    
    <script src="{{ url('/') }}/js/dataTables.buttons.min.js"></script>
    <script src="{{ url('/') }}/js/jszip.min.js"></script>
    <script src="{{ url('/') }}/js/pdfmake.min.js"></script>
    <script src="{{ url('/') }}/js/vfs_fonts.js"></script>
    <script src="{{ url('/') }}/js/buttons.html5.min.js"></script>

<!--

    <script>
    $(document).ready(function() {
    	
		   $('#overlay').fadeOut();
								

        $('#AdvancedTable').DataTable({
            responsive: true,
            lengthChange : true,
            searching : true,
            buttons: ['copyHtml5', 'excelHtml5', 'csvHtml5', 'pdfHtml5' , 'pageLength'],
        });
    });
    </script>

-->



<!--
<script type="text/javascript">
    $(document).ready(function(){ 
        $('#overlay').fadeOut();
    var table =  $('.table').DataTable({
        "paging": true,
            "lengthChange": true,
            "dom": 'Bfrtip',
            "searching": true,
            "responsive": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
             "buttons": ['copyHtml5', 'excelHtml5', 'csvHtml5', 'pdfHtml5' , 'pageLength', ],
             "exportOptions": {
                    'columns': ':visible'
                },
             "lengthMenu": [
                                [ 10, 25, 50, -1 ],
                                [ '10 rows', '25 rows', '50 rows', 'Show all' ]
                            ],
            "columnDefs": [{
              "defaultContent": "-",
              "targets": "_all",
               "visible": false,
            }],

    });
    var data = table.column( 0 ).data();
});
</script>

-->

 <script type="text/javascript">
    $(document).ready(function(){ 
        $('#overlay').fadeOut();
    $('.table').DataTable({
        "paging": true,
            "lengthChange": true,
            "dom": 'Bfrtip',
            "searching": true,
            "responsive": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
             "buttons": ['copyHtml5', 'excelHtml5', 'csvHtml5', 'pdfHtml5' , 'pageLength', ],
             
             "lengthMenu": [
                                [ 10, 25, 50, -1 ],
                                [ '10 rows', '25 rows', '50 rows', 'Show all' ]
                            ],
            "columnDefs": [{
              "defaultContent": "-",
              "targets": "_all",
              "visible" : false,
            }],

    })
});
</script>




<!--
    <script type="text/javascript">
    $(document).ready(function(){ 
        $('#overlay').fadeOut();
    $('.table').DataTable({
        "paging": true,
            "lengthChange": true,
            "dom": 'Bfrtip',
            "searching": true,
            "responsive": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
             "buttons": ['copyHtml5', 'excelHtml5', 'csvHtml5', 'pdfHtml5' , 'pageLength', ],
             "lengthMenu": [
                                [ 10, 25, 50, -1 ],
                                [ '10 rows', '25 rows', '50 rows', 'Show all' ]
                            ],
            "columnDefs": [{
              "defaultContent": "-",
              "targets": "_all"
            }],

    })
});
</script>


-->


<!--

    <script type="text/javascript">
    $(window).load(function(){
   // PAGE IS FULLY LOADED  
   // FADE OUT YOUR OVERLAYING DIV
   $('#overlay').fadeOut();
});
</script>
-->