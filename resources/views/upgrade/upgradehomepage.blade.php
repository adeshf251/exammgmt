@extends('layouts.app')

@section('content')

<h6><strong>*</strong>Any Upgrade Changes are Irreversible, Please take backup before starting upgrade</h6>

<div class="col-md-6">
    <h2> Upcoming Session : {{ $session->session_period }}</h2>
</div>

<div class="col-md-6">
    <label>Apply to all</label>
    <select  id="nextclass" class="form-control" name="nextclass" >
        @foreach ($classlist as $element)
            <option value="{{$element}}"> {{$element}}</option>
        @endforeach
    </select>
</div>



<hr>

<form class="form-horizontal" method="POST" action="{{URL('/')}}/upgradestudentsubmit">
        {{ csrf_field() }}


<table class="table table-bordered" style="width: 100%;">
    <thead>
        <td>Admission No</td>
        <td>Student Name</td>
        <td>Father Name</td>
        <td>current class section</td>
        <td>upcoming class section</td>
    </thead>
    @foreach ($students as $item)
        <tr>
            
                <td>{{$item->admission_no}} <input type="hidden" value="{{$item->admission_no}}" name="admission_no[]"> </td>
                <td>{{$item->student_name}}</td>
                <td>{{$item->student_father_name}}</td>
                <td>{{$item->student_class_section}}</td>
                <td>
                    <select  id="student_class_section" class="form-control opt" name="student_class_section[]" >
                        @foreach ($classlist as $element)
                            <option value="{{$element}}"> {{$element}}</option>
                        @endforeach
                    </select>
                </td>
            
        </tr>
    @endforeach
</table>

<div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        <button type="submit" class="btn btn-danger" style="width: 250px; height: 55px;">
            Upgrade Submit <i class="fa fa-arrow-right" aria-hidden="true"></i>
        </button>
    </div>
</div>

</form>



<script>
    var sel = document.getElementsByClassName('opt');
    document.getElementById('nextclass').onclick = function() {
        for (var j = 0; s = sel[j]; j++) {
            s.value  = document.getElementById('nextclass').value;
        }
        alert( "Class of all the students has been changed, Please review once manually" );
    }
</script>

@endsection

