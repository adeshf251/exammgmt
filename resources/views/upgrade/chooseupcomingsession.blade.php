@extends('layouts.app')

@section('content')

<div class="panel panel-default" style="margin-top:25px;">
    <div class="panel-heading">Please Select Before Proceeding</div>

    <div class="panel-body">
        <form class="form-horizontal" method="POST" action="{{ URL('/') }}/upcomingsession">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('nextsession') ? ' has-error' : '' }}">
                <label for="nextsession" class="col-md-4 control-label">Select Session </label>

                <div class="col-md-6">
                    <select  id="nextsession" class="form-control" name="nextsession" >
                        @foreach ($session as $element)
                                <option value="{{$element->id}}"> {{$element->session_period}}</option>
                        @endforeach
                                                        </select>

                    @if ($errors->has('nextsession'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nextsession') }}</strong>
                        </span>
                    @endif
                </div>
            </div>



            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        Proceed
                    </button>
                </div>
            </div>

        </form>
    </div>
</div>

@endsection
