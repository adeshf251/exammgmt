@extends('layouts.app')

@section('content')

<div class="panel panel-default" style="margin-top:25px;">
    <div class="panel-heading"> Step 1 : Please Select Before Proceeding  </div>

    <div class="panel-body">
        <table class="table table-bordered">
            <tr>
                <td> Upgrade All Subjects to New Session </td>
                <td> <a target="blank"href="{{URL('/')}}/upgradesubjectsubmit"> <button class="btn btn-primary">Upgrade <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button> </a> </td>
            </tr>
            <tr>
                <td> Upgrade All Disciplines to New Session </td>
                <td> <a target="blank"href="{{URL('/')}}/upgradedisciplinesubmit"> <button class="btn btn-primary">Upgrade <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button> </a> </td>
            </tr>
            <tr>
                <td> Upgrade All Co-scholastics to New Session </td>
                <td> <a target="blank"href="{{URL('/')}}/upgradecoscholasticsubmit"> <button class="btn btn-primary">Upgrade <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button> </a> </td>
            </tr>
        </table>
    </div>
</div>


<div class="panel panel-default" style="margin-top:25px;">
    <div class="panel-heading">step 2 : Please Select Before Proceeding  </div>

    <div class="panel-body">
        <form class="form-horizontal" method="POST" action="{{ URL('/') }}/upgradestudentform">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('class_section') ? ' has-error' : '' }}">
                <label for="class_section" class="col-md-4 control-label">Select class </label>

                <div class="col-md-6">
                    <select  id="class_section" class="form-control" name="class_section" >
                        @foreach ($classlist as $element)
                                <option value="{{$element}}"> {{$element}}</option>
                        @endforeach
                                                        </select>

                    @if ($errors->has('class_section'))
                        <span class="help-block">
                            <strong>{{ $errors->first('class_section') }}</strong>
                        </span>
                    @endif
                </div>
            </div>



            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        Proceed <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
                    </button>
                </div>
            </div>

        </form>
    </div>
</div>

@endsection
