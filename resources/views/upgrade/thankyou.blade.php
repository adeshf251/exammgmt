@extends('layouts.app')

@section('content')
<div style="text-align:center;">
    <div class="col-md-12">
        <h3>
            {{$added}} records are added for the next session
                and
            {{$skipped}} records are skipped
        </h3> 
    </div>
    <div class="col-md-12">
        <h1><i class="fa fa-check fa-4x" aria-hidden="true" style="color:green"></i></h1>
    </div>

</div>

@endsection

