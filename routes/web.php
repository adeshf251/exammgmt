<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//------------------------- WELCOME PAGE -----------------------------------
Route::get('/', function () {
    return view('welcome');
});


//----------------------------- Authentication -----------------------------
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::any('/security-check', 'SessionController@switchSession');


Route::group(['middleware' => ['auth', 'CheckLoginStatus']], function ()
{

	Route::any('/dashboard', 'DashboardController@index');
	// -----------------------------------------------------------------------------
	//								RESOURCE GROUPS
	//------------------------------------------------------------------------------


	//*********************************** Subject Management *************************
	Route::resource('/subjectmgmt', 				'SubjectmgmtController');
	Route::get('/subjectmgmt/{id}/duplicate',       'SubjectmgmtController@duplicate' );


	//*********************************** Syudent Management *************************
	Route::resource('/studentmgmt', 'StudentmgmtController');
	Route::get('/studentmgmt/{id}/duplicate',       'StudentmgmtController@duplicate' );
	Route::get('/studentmgmt-classchoose', 'StudentmgmtController@chooseClass');
	Route::post('/studentlist-classwise', 'StudentmgmtController@generateStudentsClasswise');
	


	//*********************************** Class Management *************************
	Route::resource('/classmgmt', 'ClassmgmtController');


	//*********************************** Roll Number Management *************************
	Route::resource('/rollnumbermgmt', 'RollNumberController');



	//*********************************** Rank Calculator *************************
	Route::resource('/rankcalculator', 'RankCalculator');
	Route::get('/rankcalculator/{class}/{term}/generate', 'RankCalculator@rankCalculate');


	//*********************************** Mail *************************
	Route::get('/sendmailform', 'SendMail@sendmailreq');
	Route::post('/sendmailsubmit', 'SendMail@sendmailsubmit');


	//*********************************** Session Management *************************
	Route::resource('/sessionmgmt', 'SessionController');
	Route::get('/changesession', function (Request $request) {
													$request->session()->forget('valid_id');
												    return view('home');
												});

	//*********************************** CUSTOM ADMIT CARD *************************
	Route::resource('/customadmitcardmgmt', 'CustomAdmitCardController');
	Route::get('/customadmitcardmgmt/{id}/display',  'CustomAdmitCardController@display' );

	//*********************************** Upload Marks Term 1 *************************
	Route::resource('/uploadmarksterm1', 'UploadMarksTerm1Controller');
	Route::get('/term1entries/create/findclass',  'Term1EntriesController@findclass' );
	Route::post('/term1entries/create/findclass',  'Term1EntriesController@checkpresence' );


	//*********************************** Term 1 Marks Upload *************************
	Route::resource('/term1entries', 'Term1EntriesController');
	Route::get('/term1entries/create/findstudent',  'Term1EntriesController@findstudent' );
	Route::post('/term1entries/create/findstudent',  'Term1EntriesController@checkpresence' );

	//*********************************** Term 2 Marks Upload *************************
	Route::resource('/term2entries', 'Term2EntriesController');
	Route::get('/term2entries/create/findstudent',  'Term2EntriesController@findstudent' );
	Route::post('/term2entries/create/findstudent',  'Term2EntriesController@checkpresence' );

	//***************************** Result Date Management *************************
	Route::resource('/coscholastic', 'CoScholasticController');

	//*********************************** Result Date Management *************************
	Route::resource('/discipline', 'DisciplineController');

	//*********************************** Result Date Management *************************
	Route::resource('/resultdate', 'ResultDateController');





	//*************************************** ADMIT CARD ***************************
	Route::get('/admit-card',                	 'AdmitCardController@index' );
	Route::get('/admit-card/{id}/term1',         'AdmitCardController@term1' );
	Route::get('/admit-card/{id}/term2',         'AdmitCardController@term2' );
	// Route::get('/admit-card/custom-generate',    'AdmitCardController@createIndex' );



	//*************************************** ADMIT CARD ***************************
	Route::get('/reportcard/term1',                	'ReportCardController@term1' );
	Route::get('/reportcard/term2',                	'ReportCardController@term2' );
	Route::get('/reportcard/final',                	'ReportCardController@finalreportcard' );
	Route::any('/reportcard/generate',         		'ReportCardController@generateReportCard' );


	Route::any('/reportcard/bulk',         		'ReportCardController@bulk' );
	Route::any('/reportcard/bulk/{class}/{term}/generate',         		'ReportCardController@bulkgenerate' );



	//**************************Other Functionalities and Upgrade *********** */
	Route::any('/showmarks',         		'MarksController@index' );
	Route::any('/showmarks-submit',        	'MarksController@showTabulation' );
	// Route::any('/backup',        	        'BackupController@backup_tables' );
	
	Route::get('admin/login', 'AdminLoginController@getAdminLogin');
	Route::post('admin/login', ['as'=>'admin.auth','uses'=>'AdminLoginController@adminAuth']);
	
	Route::group(['middleware' => ['admin']], function () {
	Route::get('admin/dashboard', ['as'=>'admin.dashboard','uses'=>'AdminLoginController@dashboard']);
		
	Route::any('/upgrade',        	        'UpgradeController@index' );
	Route::any('/upcomingsession',        	 'UpgradeController@setupcomingsession' );
	Route::any('/upgradeclassselect',        	 'UpgradeController@showclasslist' );
	Route::any('/upgradestudentform',        	 'UpgradeController@showclasslistgenerate' );
	Route::any('/upgradestudentsubmit',        	 'UpgradeController@upgradestudents' );
	
	Route::any('/upgradesubjectsubmit',        	 'UpgradeController@upgradesubjects' );
	Route::any('/upgradedisciplinesubmit',       'UpgradeController@upgradedisciplines' );
	Route::any('/upgradecoscholasticsubmit',     'UpgradeController@upgradecoscholastics' );


	Route::get('/showallusers',     'MembersController@showusers' );
	Route::get('/adduserform',     'MembersController@adduserform' );
	Route::post('/addusersubmit',     'MembersController@adduser' );

	Route::get('/showalladmins',     'MembersController@showadmins' );
	Route::get('/addadminform',     'MembersController@addadminform' );
	Route::post('/addadminsubmit',     'MembersController@addadmin' );
	});
	
});




//------------------------------------------------------------------------------
//                               TEMPORARY ROUTES
//------------------------------------------------------------------------------
Route::get('/admit-card-sample', function()
	{
		return view('sample.admitcard');
	});

Route::get('/arrayinput', function () {  return view('sample.arrayinputs');  });
Route::post('/arrayinput',        'SampleController@store' );


Route::get('/setting', function()
	{
		
		return view('term1entries.createwithall');
	});


Route::get('/path', function()
	{
		return value(function() { return 'bar'; });
	});

Route::get('/testpage', function()
	{
		return view('sample.samplepage');
	});



Route::any( '{all}', function ( $missingRoute) {
    return 'Please go back and Proceed Again';
} )->where('all', '(.*)');