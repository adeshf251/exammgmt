-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 13, 2017 at 07:38 AM
-- Server version: 10.1.10-MariaDB-log
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `school28`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendancemgmts`
--

CREATE TABLE `attendancemgmts` (
  `id` int(10) UNSIGNED NOT NULL,
  `admission_no` text COLLATE utf8_unicode_ci,
  `sessionid` text COLLATE utf8_unicode_ci,
  `term` text COLLATE utf8_unicode_ci,
  `class_applicable` text COLLATE utf8_unicode_ci,
  `code` varchar(190) COLLATE utf8_unicode_ci DEFAULT NULL,
  `valid_period` text COLLATE utf8_unicode_ci,
  `attendance_obtained` text COLLATE utf8_unicode_ci,
  `attendance_total` text COLLATE utf8_unicode_ci,
  `attendance_obtained_term2` text COLLATE utf8_unicode_ci,
  `attendance_total_term2` text COLLATE utf8_unicode_ci,
  `attendance_obtained_overall` text COLLATE utf8_unicode_ci,
  `attendance_total_overall` text COLLATE utf8_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `coscholasticmarksmgmts`
--

CREATE TABLE `coscholasticmarksmgmts` (
  `id` int(10) UNSIGNED NOT NULL,
  `admission_no` text COLLATE utf8_unicode_ci,
  `sessionid` text COLLATE utf8_unicode_ci,
  `term` text COLLATE utf8_unicode_ci,
  `coscholastic_code` text COLLATE utf8_unicode_ci,
  `code` varchar(190) COLLATE utf8_unicode_ci DEFAULT NULL,
  `valid_period` text COLLATE utf8_unicode_ci,
  `coscholastic_name` text COLLATE utf8_unicode_ci,
  `class_applicable` text COLLATE utf8_unicode_ci,
  `grade_term1` text COLLATE utf8_unicode_ci,
  `grade_term2` text COLLATE utf8_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `coscholasticmgmts`
--

CREATE TABLE `coscholasticmgmts` (
  `id` int(10) UNSIGNED NOT NULL,
  `coscholastic_code` text COLLATE utf8_unicode_ci,
  `coscholastic_name` text COLLATE utf8_unicode_ci,
  `class_applicable` text COLLATE utf8_unicode_ci,
  `code` text COLLATE utf8_unicode_ci,
  `extra` text COLLATE utf8_unicode_ci,
  `grade` text COLLATE utf8_unicode_ci,
  `sessionid` text COLLATE utf8_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `disciplinemarksmgmts`
--

CREATE TABLE `disciplinemarksmgmts` (
  `id` int(10) UNSIGNED NOT NULL,
  `admission_no` text COLLATE utf8_unicode_ci,
  `sessionid` text COLLATE utf8_unicode_ci,
  `term` text COLLATE utf8_unicode_ci,
  `discipline_code` text COLLATE utf8_unicode_ci,
  `code` varchar(190) COLLATE utf8_unicode_ci DEFAULT NULL,
  `valid_period` text COLLATE utf8_unicode_ci,
  `discipline_name` text COLLATE utf8_unicode_ci,
  `class_applicable` text COLLATE utf8_unicode_ci,
  `grade_term1` text COLLATE utf8_unicode_ci,
  `grade_term2` text COLLATE utf8_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `disciplinemgmts`
--

CREATE TABLE `disciplinemgmts` (
  `id` int(10) UNSIGNED NOT NULL,
  `discipline_code` text COLLATE utf8_unicode_ci,
  `discipline_name` text COLLATE utf8_unicode_ci,
  `class_applicable` text COLLATE utf8_unicode_ci,
  `code` text COLLATE utf8_unicode_ci,
  `extra` text COLLATE utf8_unicode_ci,
  `grade` text COLLATE utf8_unicode_ci,
  `sessionid` text COLLATE utf8_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `marksmgmts`
--

CREATE TABLE `marksmgmts` (
  `id` int(10) UNSIGNED NOT NULL,
  `admission_no` text COLLATE utf8_unicode_ci,
  `sessionid` text COLLATE utf8_unicode_ci,
  `term` text COLLATE utf8_unicode_ci,
  `subject_code` text COLLATE utf8_unicode_ci,
  `code` varchar(190) COLLATE utf8_unicode_ci DEFAULT NULL,
  `valid_period` text COLLATE utf8_unicode_ci,
  `subject_name` text COLLATE utf8_unicode_ci,
  `unittest_obtained` text COLLATE utf8_unicode_ci,
  `unittest_max` text COLLATE utf8_unicode_ci,
  `exam_obtained` text COLLATE utf8_unicode_ci,
  `exam_max` text COLLATE utf8_unicode_ci,
  `total_obtained` text COLLATE utf8_unicode_ci,
  `total_max` text COLLATE utf8_unicode_ci,
  `student_class_section` text COLLATE utf8_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `unittest_obtained_term2` text COLLATE utf8_unicode_ci,
  `unittest_max_term2` text COLLATE utf8_unicode_ci,
  `exam_obtained_term2` text COLLATE utf8_unicode_ci,
  `exam_max_term2` text COLLATE utf8_unicode_ci,
  `total_obtained_term2` text COLLATE utf8_unicode_ci,
  `total_max_term2` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `overallmarksmgmts`
--

CREATE TABLE `overallmarksmgmts` (
  `id` int(10) UNSIGNED NOT NULL,
  `admission_no` text COLLATE utf8_unicode_ci,
  `sessionid` text COLLATE utf8_unicode_ci,
  `term` text COLLATE utf8_unicode_ci,
  `code` varchar(190) COLLATE utf8_unicode_ci DEFAULT NULL,
  `valid_period` text COLLATE utf8_unicode_ci,
  `class_applicable` text COLLATE utf8_unicode_ci,
  `marks_term1_obtained` text COLLATE utf8_unicode_ci,
  `marks_term1_total` text COLLATE utf8_unicode_ci,
  `rank_term1` text COLLATE utf8_unicode_ci,
  `marks_term2_obtained` text COLLATE utf8_unicode_ci,
  `marks_term2_total` text COLLATE utf8_unicode_ci,
  `rank_term2` text COLLATE utf8_unicode_ci,
  `marks_overall_obtained` text COLLATE utf8_unicode_ci,
  `marks_overall_total` text COLLATE utf8_unicode_ci,
  `rank_overall` text COLLATE utf8_unicode_ci,
  `result_status` text COLLATE utf8_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `resultdatemgmts`
--

CREATE TABLE `resultdatemgmts` (
  `id` int(10) UNSIGNED NOT NULL,
  `sessionid` text COLLATE utf8_unicode_ci,
  `term` text COLLATE utf8_unicode_ci,
  `extra` text COLLATE utf8_unicode_ci,
  `code` varchar(190) COLLATE utf8_unicode_ci DEFAULT NULL,
  `valid_period` text COLLATE utf8_unicode_ci,
  `result_date` text COLLATE utf8_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendancemgmts`
--
ALTER TABLE `attendancemgmts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `attendancemgmts_code_unique` (`code`);

--
-- Indexes for table `coscholasticmarksmgmts`
--
ALTER TABLE `coscholasticmarksmgmts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `coscholasticmarksmgmts_code_unique` (`code`);

--
-- Indexes for table `coscholasticmgmts`
--
ALTER TABLE `coscholasticmgmts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `disciplinemarksmgmts`
--
ALTER TABLE `disciplinemarksmgmts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `disciplinemarksmgmts_code_unique` (`code`);

--
-- Indexes for table `disciplinemgmts`
--
ALTER TABLE `disciplinemgmts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `marksmgmts`
--
ALTER TABLE `marksmgmts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `marksmgmts_code_unique` (`code`);

--
-- Indexes for table `overallmarksmgmts`
--
ALTER TABLE `overallmarksmgmts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `overallmarksmgmt_code_unique` (`code`);

--
-- Indexes for table `resultdatemgmts`
--
ALTER TABLE `resultdatemgmts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `resultdatemgmts_code_unique` (`code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendancemgmts`
--
ALTER TABLE `attendancemgmts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `coscholasticmarksmgmts`
--
ALTER TABLE `coscholasticmarksmgmts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `coscholasticmgmts`
--
ALTER TABLE `coscholasticmgmts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `disciplinemarksmgmts`
--
ALTER TABLE `disciplinemarksmgmts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `disciplinemgmts`
--
ALTER TABLE `disciplinemgmts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `marksmgmts`
--
ALTER TABLE `marksmgmts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `overallmarksmgmts`
--
ALTER TABLE `overallmarksmgmts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `resultdatemgmts`
--
ALTER TABLE `resultdatemgmts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
