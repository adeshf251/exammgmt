<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Studentmgmt;
use App\Subjectmgmt;

class DashboardController extends Controller
{
    public function index()
    {
        $students = Studentmgmt::where('sessionid', Session('valid_id') )->count('id');
        $subjects = Subjectmgmt::where('sessionid', Session('valid_id') )->count('id');
        $session = Session('valid_period');
        return view('dashboard')->with([
            'students' => $students,
            'subjects' => $subjects,
            'session' => $session,
        ]);
    }
}
