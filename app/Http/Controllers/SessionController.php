<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sessionmanager;

class SessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Sessionmanager::all();
        return View('sessionmgmt.index')
            ->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('sessionmgmt.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'session_period' => 'required|unique:sessionmanagers|min:9|max:10',
            'description' => 'required',
        ]);

        $x = new Sessionmanager;
        $x->session_period        =       $request->input('session_period');
        $x->description           =       $request->input('description');
        $x->status                =       '-';
        $x->save();

        $request->session()->flash('message', 'Successfully Created new Session! Now You Can Perform Tash in it! Please remember that any changes made are reflected in thae session only ');
        return Redirect('sessionmgmt');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function switchSession(Request $request)
    {
        $this->validate($request, [
            'activate_session' => 'required',
        ]);

        $val        =       $request->input('activate_session');
        $x = SessionManager::find($val);
        
        session(['valid_id' => $x->id ]);
        session(['valid_period' => $x->session_period]);
                    //echo session('valid_period', 'not-present');
        return redirect('dashboard');
    }
}
