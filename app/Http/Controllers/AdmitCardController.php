<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subjectmgmt;
use App\Classmgmt;
use App\Studentmgmt;

class AdmitCardController extends Controller
{
    public function index()
    {
    	$classlist = Classmgmt::pluck('class_section');
        return View('admitcard.index')
            ->with('classlist', $classlist);
    }

    public function term1(Request $request, $id)
    {
    	$studentlist = Studentmgmt::where('student_class_section', $id)
                ->where('sessionid', Session('valid_id') )
               ->orderBy('student_name', 'asc')
               ->get();

        $subjectlist = Subjectmgmt::where('class_applicable', $id)
                ->where('sessionid', Session('valid_id') )
               ->get();
        
        //dd($subjectlist);
        return View('admitcard.classwise_admitcard_term1')->with(['studentlist' => $studentlist , 'subjectlist' => $subjectlist, ]);
    }

    public function term2(Request $request, $id)
    {
    	$studentlist = Studentmgmt::where('student_class_section', $id)
                ->where('sessionid', Session('valid_id') )
               ->orderBy('student_name', 'asc')
               ->get();

        $subjectlist = Subjectmgmt::where('class_applicable', $id)
                ->where('sessionid', Session('valid_id') )
               ->get();
        
        //dd($subjectlist);
        return View('admitcard.classwise_admitcard_term2')->with([
            'studentlist' => $studentlist ,
            'subjectlist' => $subjectlist, ]);
    }
}
