<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Coscholasticmgmt;
use App\Classmgmt;

class CoScholasticController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $coscholasticlist = Coscholasticmgmt::where('sessionid', Session('valid_id') )->get();
        return View('coscholastic.index')
            ->with('coscholasticlist', $coscholasticlist);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $classlist = Classmgmt::pluck('class_section');
        return view('coscholastic.create')->with(['classlist' => $classlist ,]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'coscholasticcode' => 'required|max:190',
            'coscholasticname' => 'required|max:50',
            'classapplicable' => 'required',
        ]);

        $sessionid = Session('valid_id');
        $coscholastic_code = $request->input('coscholasticcode');
        $class_applicable = $request->input('classapplicable');
        

        foreach ($class_applicable as $key => $class)
        {
            $code = $sessionid."-".$coscholastic_code."-".$class;

            if (Coscholasticmgmt::where('code', $code)->exists())
            {
               echo 'exist';
            }
            else
            {
                $x = new Coscholasticmgmt;
                $x->coscholastic_code           =       $coscholastic_code;
                $x->coscholastic_name           =       $request->input('coscholasticname');
                $x->class_applicable            =       $class;
                $x->code                        =       $code;
                $x->extra                       =       $request->input('extra');
                $x->sessionid                   =       $sessionid;

                $x->save();
            }
        }
        

        $request->session()->flash('message', 'Successfully Created new Co-Scholastic Detail!');
        return Redirect('coscholastic');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $classlist = Classmgmt::pluck('class_section');
        $coscholasticdetail = Coscholasticmgmt::find($id);
        return view('coscholastic.edit')->with(['coscholasticdetail' => $coscholasticdetail, 'id' => $id, 'classlist' => $classlist, ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'coscholasticcode' => 'required|max:190',
            'coscholasticname' => 'required|max:50',
            'classapplicable' => 'required',
        ]);

        $sessionid              = Session('valid_id');
        $coscholastic_code      = $request->input('coscholasticcode');
        $class_applicable       = $request->input('classapplicable');

        $code = $sessionid."-".$coscholastic_code."-".$class_applicable;

        $x = Coscholasticmgmt::find($id);
        $x->coscholastic_code           =       $coscholastic_code;
        $x->coscholastic_name           =       $request->input('coscholasticname');
        $x->code                        =       $code;
        $x->extra                       =       $request->input('extra');
        $x->sessionid                   =       $sessionid;

        $x->save();

        $request->session()->flash('message', 'Successfully Created new Co-Scholastic Detail!');
        return Redirect('coscholastic');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $x = Coscholasticmgmt::find($id);
        $x->delete();
        
        $request->session()->flash('message', 'Successfully Deleted the Co-Scholastic Details!');
        return Redirect('coscholastic');
    }
}
