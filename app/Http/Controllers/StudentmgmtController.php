<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Studentmgmt;
use App\Classmgmt;

class StudentmgmtController extends Controller
{
    /**
     * Display a listing of the resource.
     * where('sessionid', Session('valid_id') )->get();
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $studentmgmt = Studentmgmt::where('sessionid', Session('valid_id') )->get();
        return View('studentmgmt.index')
            ->with('studentmgmt', $studentmgmt);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $classlist = Classmgmt::pluck('class_section');
        return view('studentmgmt.create')->with(['classlist' => $classlist ,]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'student_name' => 'required|min:3|max:150',
            'student_father_name' => 'required|min:3|max:150',
            'student_mother_name' => 'required|min:3|max:150',
            'student_address' => 'required|min:3|max:1000',
            'student_joining_date' => 'required',
            'student_dob' => 'required|min:6|max:15',
            'student_gender' => 'required|min:1|max:10',
            'student_mob1' => 'required|numeric|min:1000000000|max:9999999999',
        ]);

        $code = $request->input('admission_no')."-".Session('valid_id');
        if (Studentmgmt::where('code', $code)->exists())
        {
           $request->session()->flash('message', 'Requested Admission Number Already Exists, Please Verify it Before Proceeding!');
            return Redirect('studentmgmt');
        }
        else
        {
            $x = new Studentmgmt;
            $x->admission_no                        =       $request->input('admission_no');
            $x->student_name                        =       $request->input('student_name');
            $x->student_father_name                 =       $request->input('student_father_name');
            $x->student_mother_name                 =       $request->input('student_mother_name');
            $x->student_class_section               =       $request->input('student_class_section');
            $x->student_address                     =       $request->input('student_address');
            $x->student_joining_date                =       $request->input('student_joining_date');
            $x->student_images                      =       $request->input('student_images');
            $x->student_dob                         =       $request->input('student_dob');
            $x->student_gender                      =       $request->input('student_gender');
            $x->student_username                    =       $request->input('student_username');
            $x->student_password                    =       $request->input('student_password');
            $x->sessionid                           =       Session('valid_id');
            $x->student_email                       =       $request->input('student_email');
            $x->student_mob1                        =       $request->input('student_mob1');
            $x->student_mob2                        =       $request->input('student_mob2');
            $x->student_rollno                      =       $request->input('student_rollno');
            $x->code                                =       $code;

            $x->save();
        }

        $request->session()->flash('message', 'Successfully Created new Student Detail!');
        return Redirect('studentmgmt');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return 'Under Process';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $classlist = Classmgmt::pluck('class_section');
        $studentdetail = Studentmgmt::find($id);
        return view('studentmgmt.edit')->with(['classlist' => $classlist ,'id' => $id, 'studentdetail' => $studentdetail,]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'admission_no' => 'required|min:0|max:99999',
            'student_name' => 'required|min:3|max:150',
            'student_father_name' => 'required|min:3|max:150',
            'student_mother_name' => 'required|min:3|max:150',
            'student_address' => 'required|min:3|max:1000',
            'student_joining_date' => 'required',
            'student_dob' => 'required|min:6|max:15',
            'student_gender' => 'required|min:1|max:10',
            'student_mob1' => 'required|numeric|min:1000000000|max:9999999999',
        ]);

        $code = $request->input('admission_no')."-".Session('valid_id');
        $x = Studentmgmt::find($id);
        $x->admission_no                        =       $request->input('admission_no');
        $x->student_name                        =       $request->input('student_name');
        $x->student_father_name                 =       $request->input('student_father_name');
        $x->student_mother_name                 =       $request->input('student_mother_name');
        $x->student_class_section               =       $request->input('student_class_section');
        $x->student_address                     =       $request->input('student_address');
        $x->student_joining_date                =       $request->input('student_joining_date');
        $x->student_images                      =       $request->input('student_images');
        $x->student_dob                         =       $request->input('student_dob');
        $x->student_gender                      =       $request->input('student_gender');
        $x->student_username                    =       $request->input('student_username');
        $x->student_password                    =       $request->input('student_password');
        $x->sessionid                           =       Session('valid_id');
        $x->student_email                       =       $request->input('student_email');
        $x->student_mob1                        =       $request->input('student_mob1');
        $x->student_mob2                        =       $request->input('student_mob2');
        $x->student_rollno                      =       $request->input('student_rollno');
        $x->code                                =       $code;

        $x->save();

        $request->session()->flash('message', 'Successfully Updated the Student Detail!');
        return Redirect('studentmgmt');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $x = Studentmgmt::find($id);
        $x->delete();
        
        $request->session()->flash('message', 'Successfully Deleted the Student Details!');
        return Redirect('studentmgmt');
    }

    public function duplicate($id)
    {
        $classlist = Classmgmt::pluck('class_section');
        $studentdetail = Studentmgmt::find($id);
        return view('studentmgmt.duplicate')->with(['classlist' => $classlist ,'id' => $id, 'studentdetail' => $studentdetail,]);

    }

    public function chooseClass() {
        $classlist = Classmgmt::pluck('class_section');
        return view('studentmgmt.chooseclass')->with('classlist', $classlist);
    }

    public function generateStudentsClasswise(Request $request) {
        $cl = $request->input('student_class_section');
        $studentslist = Studentmgmt::where('student_class_section', $cl)->where('sessionid', Session('valid_id') )->get();
        return view('studentmgmt.index')
        ->with('studentmgmt', $studentslist);
    }
}
