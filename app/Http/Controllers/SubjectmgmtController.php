<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subjectmgmt;
use App\Classmgmt;

class SubjectmgmtController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjectmgmt = Subjectmgmt::where('sessionid', Session('valid_id') )->get();
        return View('subjectmgmt.index')
            ->with('subjectmgmt', $subjectmgmt);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $classlist = Classmgmt::pluck('class_section');
        return view('subjectmgmt.create')->with(['classlist' => $classlist ,]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'subjectcode' => 'required|max:190',
            'subject_name' => 'required|max:50',
            'class_applicable' => 'required',
            'term1_unittest' => 'required|numeric|min:0|max:1000',
            'term1_exam' => 'required|numeric|min:0|max:1000',
            'term2_unittest' => 'required|numeric|min:0|max:1000',
            'term2_exam' => 'required|numeric|min:0|max:1000',
        ]);

        $x = new Subjectmgmt;
        $x->subject_code                =       $request->input('subjectcode');
        $x->subject_name                =       $request->input('subject_name');
        $x->class_applicable            =       $request->input('class_applicable');
        $x->extra                       =       $request->input('extra');
        $x->term1_unittest              =       $request->input('term1_unittest');
        $x->term1_exam                  =       $request->input('term1_exam');
        $x->term1_total                 =       $request->input('term1_unittest')
                                                + $request->input('term1_exam');
        $x->term2_unittest              =       $request->input('term2_unittest');
        $x->term2_exam                  =       $request->input('term2_exam');
        $x->term2_total                 =       $request->input('term2_unittest')
                                                + $request->input('term2_exam'); 
        $x->status                      =       $request->input('status');
        $x->optional                    =       $request->input('optional');
        $x->grand_total_marks           =       $request->input('term1_unittest')
                                                + $request->input('term1_exam')
                                                + $request->input('term2_unittest')
                                                + $request->input('term2_exam');
        $x->date_term1                  =       $request->input('date_term1');
        $x->date_term2                  =       $request->input('date_term2');
        $x->sessionid                   =       Session('valid_id');

        $x->save();

        $request->session()->flash('message', 'Successfully Created new Subject Detail!');
        return Redirect('subjectmgmt');
    }

    /**Session('valid_id') and Session('valid_period')
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $subjectmgmt = Subjectmgmt::find($id);
        return view('subjectmgmt.show')->with(['subjectmgmt' => $subjectmgmt,]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $classlist = Classmgmt::pluck('class_section');
        $subjectmgmt = Subjectmgmt::find($id);
        return view('subjectmgmt.edit')->with(['subjectmgmt' => $subjectmgmt, 'id' => $id, 'classlist' => $classlist, ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'subjectcode' => 'required|max:190',
            'subject_name' => 'required|max:50',
            'class_applicable' => 'required',
            'term1_unittest' => 'required|numeric|min:0|max:1000',
            'term1_exam' => 'required|numeric|min:0|max:1000',
            'term2_unittest' => 'required|numeric|min:0|max:1000',
            'term2_exam' => 'required|numeric|min:0|max:1000',
        ]);

        $x = Subjectmgmt::find($id);
        $x->subject_code                =       $request->input('subjectcode');
        $x->subject_name                =       $request->input('subject_name');
        $x->class_applicable            =       $request->input('class_applicable');
        $x->extra                       =       $request->input('extra');
        $x->term1_unittest              =       $request->input('term1_unittest');
        $x->term1_exam                  =       $request->input('term1_exam');
        $x->term1_total                 =       $request->input('term1_unittest')
                                                + $request->input('term1_exam');
        $x->term2_unittest              =       $request->input('term2_unittest');
        $x->term2_exam                  =       $request->input('term2_exam');
        $x->term2_total                 =       $request->input('term2_unittest')
                                                + $request->input('term2_exam'); 
        $x->status                      =       $request->input('status');
        $x->optional                    =       $request->input('optional');
        $x->grand_total_marks           =       $request->input('term1_unittest')
                                                + $request->input('term1_exam')
                                                + $request->input('term2_unittest')
                                                + $request->input('term2_exam');
        $x->date_term1                  =       $request->input('date_term1');
        $x->date_term2                  =       $request->input('date_term2');
        $x->sessionid                   =       Session('valid_id');
        $x->save();

        $request->session()->flash('message', 'Successfully Updated the Subject Details!');
        return Redirect('subjectmgmt');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $x = Subjectmgmt::find($id);
        $x->delete();
        
        $request->session()->flash('message', 'Successfully Deleted the Subject Details!');
        return Redirect('subjectmgmt');
    }

    public function duplicate($id)
    {
        $classlist = Classmgmt::pluck('class_section');
        $subjectmgmt = Subjectmgmt::find($id);
        return view('subjectmgmt.duplicate')->with(['subjectmgmt' => $subjectmgmt, 'id' => $id, 'classlist' => $classlist, ]);
    }

}
