<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Resultdatemgmt;
use App\Classmgmt;

class ResultDateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resultdatelist = Resultdatemgmt::where('sessionid', Session('valid_id') )->get();
        return View('resultdate.index')
            ->with('resultdatelist', $resultdatelist);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('resultdate.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'result_date' => 'required|min:10|max:10',
        ]);

        $result_date = $request->input('result_date');
        $term = $request->input('term');
        $sessionid = Session('valid_id');
        $valid_period = Session('valid_period');

        $code = $sessionid."-".$term;
        
        if (Resultdatemgmt::where('code', $code)->exists())
        {
            $request->session()->flash('message', 'Date Already Exists!! Please Use Edit Option');
            return Redirect('resultdate');
        }
        else
        {
            $x = new Resultdatemgmt;
            $x->sessionid                       =       $sessionid;
            $x->term                            =       $term ;
            $x->code                            =       $code;
            $x->valid_period                    =       $valid_period;
            $x->result_date                    =        $result_date;
            $x->save();
        }

        $request->session()->flash('message', 'Successfully Created new Result Date!');
        return Redirect('resultdate');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $resultdatelist = Resultdatemgmt::find($id);
        return view('resultdate.edit')->with(['resultdatelist' => $resultdatelist ,'id' => $id, ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'result_date' => 'required|min:10|max:10',
        ]);

        $result_date = $request->input('result_date');
        
        $x = Resultdatemgmt::find($id);
        $x->result_date                    =        $result_date;
        $x->save();
        

        $request->session()->flash('message', 'Successfully Updated the Result Date!');
        return Redirect('resultdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
