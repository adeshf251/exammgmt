<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Subjectmgmt;
use App\Studentmgmt;
use App\Classmgmt;

use App\Marksmgmt;

use App\Coscholasticmgmt;
use App\Disciplinemgmt;
use App\Attendancemgmt;
use App\Resultdatemgmt;
use App\Overallmarksmgmt;

use App\Coscholasticmarksmgmt;
use App\Disciplinemarksmgmt;

class ReportCardController extends Controller
{
    public function term1(Request $request)
    {
        // session(['reportcardtype' => 'term1']);
        return view('reportcard.findstudent_term1');
    }
    public function term2(Request $request)
    {
        // session(['reportcardtype' => 'term2']);
        return view('reportcard.findstudent_term2');
    }
    public function finalreportcard(Request $request)
    {
        // session(['reportcardtype' => 'finalreportcard']);
        return view('reportcard.findstudent_yearly');
    }


    public function generateReportCard(Request $request)
    {
        //$reportcardtype = session('reportcardtype');
        $reportcardtype             =       $request->input('resulttype');
        $admission_number           =       $request->input('admission_number');
        $sessionid                  =       Session('valid_id');
        $studentdetails = Studentmgmt::where('sessionid', $sessionid )->where('admission_no', $admission_number)->get();
        $stuclass = "";
        foreach ($studentdetails as $key => $value) {
            $student_class_section = $value->student_class_section;
            $stuclass = $student_class_section;
        }
        if($stuclass == "")
            {
                $request->session()->flash('message', 'Admission no :'. $admission_number .'  does not exists!');
                return back();
            }


        $subjectlist                  = Marksmgmt::where('sessionid', $sessionid )
                                                    ->where('admission_no', $admission_number )
                                                    ->get();
        $Overallmarksmgmt      = Overallmarksmgmt::where('sessionid', $sessionid )
                                                    ->where('admission_no', $admission_number )
                                                    ->get();
        $coscholasticlist = Coscholasticmarksmgmt::where('sessionid', $sessionid )
                                                    ->where('admission_no', $admission_number )
                                                    ->get();
        $disciplinelist     = Disciplinemarksmgmt::where('sessionid', $sessionid )
                                                    ->where('admission_no', $admission_number )
                                                    ->get();
        $attendance              = Attendancemgmt::where('sessionid', $sessionid )
                                                    ->where('admission_no', $admission_number )
                                                    ->get();

        try
        {
            if($reportcardtype == 'term1')
            {
            	$resultdate              = Resultdatemgmt::where('sessionid', $sessionid )->where('term', '1')->get();
            	return view('reportcard.term1reportcard')->with(['studentdetails'    => $studentdetails ,
                                                    'subjectlist'       => $subjectlist ,
                                                    'Overallmarksmgmt'  => $Overallmarksmgmt ,
                                                    'coscholasticlist'  => $coscholasticlist ,
                                                    'disciplinelist'    => $disciplinelist ,
                                                    'attendance'        => $attendance ,
                                                    'resultdate'        => $resultdate ,
                                                    'class_section'     => $student_class_section ,
                                                    ]);
            }
            else if($reportcardtype == 'term2')
            {
            	$resultdate              = Resultdatemgmt::where('sessionid', $sessionid )->where('term', '2')->get();
            	return view('reportcard.term2reportcard')->with(['studentdetails'    => $studentdetails ,
                                                    'subjectlist'       => $subjectlist ,
                                                    'Overallmarksmgmt'  => $Overallmarksmgmt ,
                                                    'coscholasticlist'  => $coscholasticlist ,
                                                    'disciplinelist'    => $disciplinelist ,
                                                    'attendance'        => $attendance ,
                                                    'resultdate'        => $resultdate ,
                                                    'class_section'     => $student_class_section ,
                                                    ]);
            }
            else if($reportcardtype == 'finalreportcard')
            {
            	$resultdate              = Resultdatemgmt::where('sessionid', $sessionid )->where('term', '2')->get();
            	return view('reportcard.finalreportcard')->with(['studentdetails'    => $studentdetails ,
                                                    'subjectlist'       => $subjectlist ,
                                                    'Overallmarksmgmt'  => $Overallmarksmgmt ,
                                                    'coscholasticlist'  => $coscholasticlist ,
                                                    'disciplinelist'    => $disciplinelist ,
                                                    'attendance'        => $attendance ,
                                                    'resultdate'        => $resultdate ,
                                                    'class_section'     => $student_class_section ,
                                                    ]);
            }
            else 
            {
            	return 'No Result Exists in this session, Please Log-out and Log-in again OR switch to valid session ';
            }
        }
        catch (Exception $e)
        {
            return 'Unknown Error Encountered ! Please, input all the valid entries!! ';
        }
    }



    public function bulk(Request $request)
    {       //class_section
        $class_section  = Classmgmt::all();
        return view('reportcard.bulk')->with(['class_section' =>  $class_section , ]);
    }

    public function bulkgenerate(Request $request, $class, $term)
    {    
        $sessionid                  =       Session('valid_id');
        $students = Studentmgmt::where('sessionid', $sessionid )
                                ->where('student_class_section', $class)
                                ->get(['admission_no']);

    $studentdetails_array               =       array();
    $subjectlist_array                  =       array();
    $Overallmarksmgmt_array             =       array();
    $coscholasticlist_array             =       array();
    $disciplinelist_array               =       array();
    $attendance_array                   =       array();
    $resultdate_array                   =       array();
    $class_section_array                =       array();

        foreach ($students as $key => $data)
        {       
            
            $admission_number           =       $data->admission_no;
            $sessionid                  =       Session('valid_id');
            $studentdetails = Studentmgmt::where('sessionid', $sessionid )->where('admission_no', $admission_number)->get();

            foreach ($studentdetails as $key => $value) {
                $student_class_section = $value->student_class_section;
            }

            $subjectlist                  = Marksmgmt::where('sessionid', $sessionid )->where('admission_no', $admission_number )->get();
            $Overallmarksmgmt      = Overallmarksmgmt::where('sessionid', $sessionid )->where('admission_no', $admission_number )->get();
            $coscholasticlist = Coscholasticmarksmgmt::where('sessionid', $sessionid )->where('admission_no', $admission_number )->get();
            $disciplinelist     = Disciplinemarksmgmt::where('sessionid', $sessionid )->where('admission_no', $admission_number )->get();
            $attendance              = Attendancemgmt::where('sessionid', $sessionid )->where('admission_no', $admission_number )->get();
            

            if($term == '1')
            {
                $resultdate              = Resultdatemgmt::where('sessionid', $sessionid )->where('term', '1')->get();
                array_push( $studentdetails_array           , $studentdetails        );
                array_push( $subjectlist_array              , $subjectlist           );
                array_push( $Overallmarksmgmt_array         , $Overallmarksmgmt      );
                array_push( $coscholasticlist_array         , $coscholasticlist      );
                array_push( $disciplinelist_array           , $disciplinelist        );
                array_push( $attendance_array               , $attendance            );
                array_push( $resultdate_array               , $resultdate            );
                array_push( $class_section_array            , $class                 );
            }
            else if($term == '2')
            {
                $resultdate              = Resultdatemgmt::where('sessionid', $sessionid )->where('term', '2')->get();
                array_push( $studentdetails_array           , $studentdetails        );
                array_push( $subjectlist_array              , $subjectlist           );
                array_push( $Overallmarksmgmt_array         , $Overallmarksmgmt      );
                array_push( $coscholasticlist_array         , $coscholasticlist      );
                array_push( $disciplinelist_array           , $disciplinelist        );
                array_push( $attendance_array               , $attendance            );
                array_push( $resultdate_array               , $resultdate            );
                array_push( $class_section_array            , $class                 );
            }
            else if($term == '3')
            {
                $resultdate              = Resultdatemgmt::where('sessionid', $sessionid )->where('term', '2')->get();
                array_push( $studentdetails_array           , $studentdetails        );
                array_push( $subjectlist_array              , $subjectlist           );
                array_push( $Overallmarksmgmt_array         , $Overallmarksmgmt      );
                array_push( $coscholasticlist_array         , $coscholasticlist      );
                array_push( $disciplinelist_array           , $disciplinelist        );
                array_push( $attendance_array               , $attendance            );
                array_push( $resultdate_array               , $resultdate            );
                array_push( $class_section_array            , $class                 );
            }

            // return view('reportcard.term1reportcard')->with(['studentdetails'    => $studentdetails ,
            //                                         'subjectlist'       => $subjectlist ,
            //                                         'Overallmarksmgmt'  => $Overallmarksmgmt ,
            //                                         'coscholasticlist'  => $coscholasticlist ,
            //                                         'disciplinelist'    => $disciplinelist ,
            //                                         'attendance'        => $attendance ,
            //                                         'resultdate'        => $resultdate ,
            //                                         'class_section'     => $student_class_section ,
            //                                         ]);
        }

        //print_r($studentdetails_array);
        if($term == '1')
            {
                return view('reportcard.bulkreportterm1')->with([
                                                    'studentdetails_array'    => $studentdetails_array ,
                                                    'subjectlist_array'       => $subjectlist_array ,
                                                    'Overallmarksmgmt_array'  => $Overallmarksmgmt_array ,
                                                    'coscholasticlist_array'  => $coscholasticlist_array ,
                                                    'disciplinelist_array'    => $disciplinelist_array ,
                                                    'attendance_array'        => $attendance_array ,
                                                    'resultdate_array'        => $resultdate_array ,
                                                    'class_section_array'     => $class_section_array ,
                                                    ]);
            }
        else if($term == '2')
            {
                return view('reportcard.bulkreportterm2')->with([
                                                    'studentdetails_array'    => $studentdetails_array ,
                                                    'subjectlist_array'       => $subjectlist_array ,
                                                    'Overallmarksmgmt_array'  => $Overallmarksmgmt_array ,
                                                    'coscholasticlist_array'  => $coscholasticlist_array ,
                                                    'disciplinelist_array'    => $disciplinelist_array ,
                                                    'attendance_array'        => $attendance_array ,
                                                    'resultdate_array'        => $resultdate_array ,
                                                    'class_section_array'     => $class_section_array ,
                                                    ]);
            }
        else if($term == '3')
            {
                return view('reportcard.bulkreporttermfinal')->with([
                                                    'studentdetails_array'    => $studentdetails_array ,
                                                    'subjectlist_array'       => $subjectlist_array ,
                                                    'Overallmarksmgmt_array'  => $Overallmarksmgmt_array ,
                                                    'coscholasticlist_array'  => $coscholasticlist_array ,
                                                    'disciplinelist_array'    => $disciplinelist_array ,
                                                    'attendance_array'        => $attendance_array ,
                                                    'resultdate_array'        => $resultdate_array ,
                                                    'class_section_array'     => $class_section_array ,
                                                    ]);
            }
        
        
    }

}
