<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class SendMail extends Controller
{
    public function sendmailreq()
    {
        return view('emails.emailform');
    }
    public function sendmailsubmit2(Request $request)
    {
        $title = $request->input('title');
        $content = $request->input('content');

        $data['title'] = $title;
        $data['content'] = $content;
        Mail::send('emails.sample', $data , function($message)
        {
            // $message->from('us@example.com', 'Laravel');

            // $message->to('adeupm@gmail.com')->cc('bar@example.com');
            $message->to('adeupm@gmail.com');
        });

        return view('emails.sended');
    }

    public function sendmailsubmit(Request $request)
    {
        // send mail via hitting to sendmail page at dellmond domain
        $email   =           $request->input('email');
        $subject =           $request->input('title');
        $body    =           $request->input('content');

        $postdata = http_build_query(
            array(
                'email'     => $email,
                'subject'   => $subject,
                'body'      => $body,
            )
        );

        $opts = array('http' =>
            array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => $postdata
            )
        );

        $context  = stream_context_create($opts);

        $result = @file_get_contents('http://central.dellmondinternational.in/sendmail.php', false, $context);

        // if($http_response_header[0]=="HTTP/1.1 404 Not Found"):

        //             return "iam 404";

        // elseif($http_response_header[0]=="HTTP/1.1 200 OK"):

        //             return view('emails.sended');

        // else:

        //             return "unknown error"; 

        // endif;
    }
}
