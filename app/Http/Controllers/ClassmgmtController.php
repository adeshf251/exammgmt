<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classmgmt;
use App\Http\Controllers\View;

class ClassmgmtController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classmgmt = Classmgmt::all();

        return View('classmgmt.index')
            ->with('classmgmt', $classmgmt);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('classmgmt.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'class_section' => 'required|unique:classmgmts|max:190',
            'description' => 'required',
        ]);

            $x = new Classmgmt;
            $x->class_section                =       $request->input('class_section');
            $x->description                  =       $request->input('description');
            $x->save();

            $request->session()->flash('message', 'Successfully Created new Class!');
            return Redirect('classmgmt');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $classes = Classmgmt::all();
        return view('classmgmt.show')->with(['classes' => $classes,]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $classmgmt = Classmgmt::find($id);

        return View('classmgmt.edit')
            ->with(['classmgmt'=> $classmgmt, 'id' => $id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'class_section' => 'required|max:190',
            'description' => 'required',
        ]);

        $x = Classmgmt::find($id);
        $x->class_section                =       $request->input('class_section');
        $x->description                  =       $request->input('description');
        $x->save();

        $request->flash('message', 'Successfully created Class!');
        return Redirect('classmgmt');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $x = Classmgmt::find($id);
        $x->delete();
        
        $request->session()->flash('message', 'Successfully Deleted the Class!');
        return Redirect('classmgmt');
    }
}
