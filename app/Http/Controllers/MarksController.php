<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \DB;
use App\Subjectmgmt;
use App\Marksmgmt;
use App\Studentmgmt;
use App\Classmgmt;

class MarksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classlist = Classmgmt::pluck('class_section');
        return View('other.marksdisplayform')
            ->with([
                'classlist' => $classlist,
                ]);
    }


    public function showTabulation(Request $request) {
        $class = $request->input('class_section');
        $session  =   Session('valid_period');

        $allsubjects = Subjectmgmt::where('sessionid', Session('valid_id') )->where('class_applicable', $class )->pluck('subject_code');
        $allstudents = Studentmgmt::where('sessionid', Session('valid_id') )->where('student_class_section', $class )->pluck('admission_no');
        // dd($allstudents);
        $arrayresult = Array();
        $studentinfo = Array();
        $subjectslist = Array();

        foreach ($allsubjects as $key => $subcode) {
            $subname = Subjectmgmt::where('sessionid', Session('valid_id') )
                                    ->where('class_applicable', $class )
                                    ->where('subject_code', $subcode)->pluck('subject_name');
            $subjectslist[$key] = $subname[0];
        }

        foreach ($allstudents as $k1 => $admission) {
            $stu = Studentmgmt::where('sessionid', Session('valid_id') )
                            ->where('admission_no', $admission)->get();
            $studentinfo[$k1] = $stu[0];
            foreach ($allsubjects as $k2 => $subject_code) {
                $marksrow = Marksmgmt::where('sessionid', Session('valid_id') )
                                    ->where('admission_no', $admission)
                                    ->where('subject_code', $subject_code)
                                    ->get();
                $arrayresult[$k1][$k2] = $marksrow[0];
            } 
        }
        return View('other.marksdisplaysubmit')
            ->with([
                'arrayresult' => $arrayresult,
                'studentinfo' => $studentinfo,
                'subjectslist'=> $subjectslist,
                'class' => $class,
                'session' => $session,
                ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
