<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subjectmgmt;
use App\Studentmgmt;
use App\Classmgmt;

use App\Marksmgmt;

use App\Coscholasticmgmt;
use App\Disciplinemgmt;
use App\Attendancemgmt;
use App\Resultdatemgmt;
use App\Overallmarksmgmt;

use App\Coscholasticmarksmgmt;
use App\Disciplinemarksmgmt;


class RankCalculator extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $class_section  = Classmgmt::all();
        return view('rankcalculator.index')->with(['class_section' =>  $class_section , ]);
    }

    public function rankCalculate(Request $request, $class, $term)
    {
        $sessionid                  =       Session('valid_id');
        $markslist = Overallmarksmgmt::where('class_applicable', $class)
                                    ->where('sessionid', $sessionid)
                                    ->get();

        if($term == '1')
            {
                for ($i=0; $i <  sizeof($markslist) ; $i++) 
                { 
                    for ($j=$i; $j < sizeof($markslist) ; $j++) 
                    { 
                        if( $markslist[$j]->marks_term1_obtained >=  $markslist[$i]->marks_term1_obtained  ) 
                        {
                            $temp = $markslist[$i];
                            $markslist[$i] = $markslist[$j];
                            $markslist[$j] = $temp;
                        }
                    }
                }


                $rank = 0;
                $previousmarks = 99999;
                foreach ($markslist as $key => $value) {
            
                    $position = Overallmarksmgmt::find($value->id);
                    if($position->marks_term1_obtained == $previousmarks ) {
                        $position->rank_term1 = $rank;
                    } else {
                        $previousmarks = $position->marks_term1_obtained;
                        $rank++;
                        $position->rank_term1 = $rank;
                    }
                    $position->save();
                }
            }
            else if($term == '2')
            {
                for ($i=0; $i <  sizeof($markslist) ; $i++) 
                { 
                    for ($j=$i; $j < sizeof($markslist) ; $j++) 
                    { 
                        if( $markslist[$j]->marks_term2_obtained >=  $markslist[$i]->marks_term2_obtained  ) 
                        {
                            $temp = $markslist[$i];
                            $markslist[$i] = $markslist[$j];
                            $markslist[$j] = $temp;
                        }
                    }
                }


                $rank = 0;
                $previousmarks = 99999;
                foreach ($markslist as $key => $value) {
            
                    $position = Overallmarksmgmt::find($value->id);
                    if($position->marks_term2_obtained == $previousmarks ) {
                        $position->rank_term2 = $rank;
                        // echo $value->id."(". $value->marks_term2_obtained .")(". $rank .") &nbsp; &nbsp; ";
                    } else {
                        $previousmarks = $position->marks_term2_obtained;
                        $rank++;
                        $position->rank_term2 = $rank;
                        // echo $value->id."(". $value->marks_term2_obtained .")(". $rank .") &nbsp; &nbsp; ";
                    }
                    $position->save();
                }
            }
            else if($term == '3')
            {
                for ($i=0; $i <  sizeof($markslist) ; $i++) 
                { 
                    for ($j=$i; $j < sizeof($markslist) ; $j++) 
                    { 
                        if( $markslist[$j]->marks_overall_obtained >=  $markslist[$i]->marks_overall_obtained  ) 
                        {
                            $temp = $markslist[$i];
                            $markslist[$i] = $markslist[$j];
                            $markslist[$j] = $temp;
                        }
                    }
                }


                $rank = 0;
                $previousmarks = 99999;
                foreach ($markslist as $key => $value) {
            
                    $position = Overallmarksmgmt::find($value->id);
                    if($position->marks_overall_obtained == $previousmarks ) {
                        $position->rank_overall = $rank;
                    } else {
                        $previousmarks = $position->marks_overall_obtained;
                        $rank++;
                        $position->rank_overall = $rank;
                    }
                    $position->save();
                }
            }

        return view('rankcalculator.rankreport')->with([
            'class' =>  $class,
            'countrecords' =>  sizeof($markslist) ,
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
