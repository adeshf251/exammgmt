<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Admin;

class MembersController extends Controller
{
    public function showusers()
    {
        $userlist = User::all();
        return view('members.showusers')->with(['userlist' => $userlist , ]);
    }
    public function adduserform()
    {
        return view('members.addusers');
    }
    public function adduser(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);


        $name = $request->input('name');
        $email = $request->input('email');
        $password = $request->input('password');

        $x = new User;
        $x->name = $name;
        $x->role_id = 1;
        $x->email = $email;
        $x->avatar = 'users/default.png';
        $x->password = bcrypt($password) ;
        $x->save();

        return view('members.thanks');
    }


    public function showadmins()
    {
        $userlist = Admin::all();
        return view('members.showadmins')->with(['userlist' => $userlist , ]);
    }
    public function addadminform()
    {
        return view('members.addadmins');
    }
    public function addadmin(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:admins',
            'password' => 'required|string|min:6|confirmed',
        ]);


        $name = $request->input('name');
        $email = $request->input('email');
        $password = $request->input('password');
        
        $x = new Admin;
        $x->name = $name;
        $x->role_id = 1;
        $x->email = $email;
        $x->avatar = 'users/default.png';
        $x->password = bcrypt($password) ;
        $x->save();

        return view('members.thanks');
    }
}
