<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subjectmgmt;
use App\Classmgmt;
use App\Studentmgmt;

class RollNumberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $studentlist = Studentmgmt::where('sessionid', Session('valid_id') )->get();
        return View('rollnumbermgmt.index')
            ->with('studentlist', $studentlist);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $classlist = Classmgmt::pluck('class_section');
        return view('rollnumbermgmt.create')->with(['classlist' => $classlist ,]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $class = $request->input('class_applicable');
        $studentlist = Studentmgmt::where('student_class_section', $class)
                                    ->where('sessionid', Session('valid_id') )
                                    ->orderBy('student_name', 'asc')
                                    ->pluck('id');
        $counter = 0;
        foreach ($studentlist as $admission_no) 
            {
                echo $counter ."<br>";
                $counter ++ ;
                $x = Studentmgmt::find($admission_no);
                $x->student_rollno  = $counter;
                $x->save();
            }
        
        $request->session()->flash('message', 'Successfully Alloted the Roll Numbers!');
        return Redirect('rollnumbermgmt');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
