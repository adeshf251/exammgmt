<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customadmitcard;

class CustomAdmitCardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Customadmitcard::where('sessionid', Session('valid_id') )->get();
        return View('customadmitcard.index')
            ->with('data', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customadmitcard.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'admission_no' => 'required',
            'student_name' => 'required',
            'mother_name' => 'required',
            'father_name' => 'required',
            'roll_no' => 'required',
            'class_section' => 'required',
            'exam_term' => 'required',
            'session' => 'required',
            'subject1' => 'required',
            'date1' => 'required',
        ]);

        $x = new Customadmitcard;
        $x->admission_no                  =       $request->input('admission_no');
        $x->student_name                  =       $request->input('student_name');
        $x->mother_name                   =       $request->input('mother_name');
        $x->father_name                   =       $request->input('father_name');
        $x->roll_no                       =       $request->input('roll_no');
        $x->class_section                 =       $request->input('class_section');
        $x->exam_term                     =       $request->input('exam_term');
        $x->session                       =       $request->input('session');
        $x->subject1                      =       $request->input('subject1');
        $x->date1                         =       $request->input('date1');
        $x->subject2                      =       $request->input('subject2');
        $x->date2                         =       $request->input('date2');
        $x->subject3                      =       $request->input('subject3');
        $x->date3                         =       $request->input('date3');
        $x->subject4                      =       $request->input('subject4');
        $x->date4                         =       $request->input('date4');
        $x->subject5                      =       $request->input('subject5');
        $x->date5                         =       $request->input('date5');
        $x->subject6                      =       $request->input('subject6');
        $x->date6                         =       $request->input('date6');
        $x->subject7                      =       $request->input('subject7');
        $x->date7                         =       $request->input('date7');
        $x->subject8                      =       $request->input('subject8');
        $x->date8                         =       $request->input('date8');
        $x->subject9                      =       $request->input('subject9');
        $x->date9                         =       $request->input('date9');
        $x->subject10                     =       $request->input('subject10');
        $x->date10                        =       $request->input('date10');
        $x->sessionid                     =       Session('valid_id');
        $x->save();

        $request->session()->flash('message', 'Successfully Created new custom admit card!');
        return Redirect('customadmitcardmgmt');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customadmitcard = Customadmitcard::find($id);
        return view('customadmitcard.edit')->with(['customadmitcard' => $customadmitcard, 'id' => $id, ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'admission_no' => 'required',
            'student_name' => 'required',
            'mother_name' => 'required',
            'father_name' => 'required',
            'roll_no' => 'required',
            'class_section' => 'required',
            'session' => 'required',
            'subject1' => 'required',
            'date1' => 'required',
        ]);

        $x = Customadmitcard::find($id);
        $x->admission_no                  =       $request->input('admission_no');
        $x->student_name                  =       $request->input('student_name');
        $x->mother_name                   =       $request->input('mother_name');
        $x->father_name                   =       $request->input('father_name');
        $x->roll_no                       =       $request->input('roll_no');
        $x->class_section                 =       $request->input('class_section');
        $x->exam_term                     =       $request->input('exam_term');
        $x->session                       =       $request->input('session');
        $x->subject1                      =       $request->input('subject1');
        $x->date1                         =       $request->input('date1');
        $x->subject2                      =       $request->input('subject2');
        $x->date2                         =       $request->input('date2');
        $x->subject3                      =       $request->input('subject3');
        $x->date3                         =       $request->input('date3');
        $x->subject4                      =       $request->input('subject4');
        $x->date4                         =       $request->input('date4');
        $x->subject5                      =       $request->input('subject5');
        $x->date5                         =       $request->input('date5');
        $x->subject6                      =       $request->input('subject6');
        $x->date6                         =       $request->input('date6');
        $x->subject7                      =       $request->input('subject7');
        $x->date7                         =       $request->input('date7');
        $x->subject8                      =       $request->input('subject8');
        $x->date8                         =       $request->input('date8');
        $x->subject9                      =       $request->input('subject9');
        $x->date9                         =       $request->input('date9');
        $x->subject10                     =       $request->input('subject10');
        $x->date10                        =       $request->input('date10');
        $x->sessionid                     =       Session('valid_id');
        $x->save();

        $request->session()->flash('message', 'Successfully Updated the Custom Admit card Detail!');
        return Redirect('customadmitcardmgmt');
    }

    /**
     * Remove the specified resource from storage.
     *  where('sessionid', Session('valid_id') )->get();
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $x = Customadmitcard::find($id);
        $x->delete();
        
        $request->session()->flash('message', 'Successfully Deleted the custom admit cards!');
        return Redirect('customadmitcardmgmt');
    }


    public function display(Request $request, $id)
    {
        $x = Customadmitcard::find($id);
        return view('customadmitcard.admitcard')->with(['customadmitcard' => $x, 'id' => $id, ]);
    }


   
}
