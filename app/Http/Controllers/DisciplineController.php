<?php

namespace App\Http\Controllers;
use App\Disciplinemgmt;
use App\Classmgmt;

use Illuminate\Http\Request;

class DisciplineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $disciplinelist = Disciplinemgmt::where('sessionid', Session('valid_id') )->get();
        return View('discipline.index')
            ->with('disciplinelist', $disciplinelist);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $classlist = Classmgmt::pluck('class_section');
        return view('discipline.create')->with(['classlist' => $classlist ,]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'disciplinecode' => 'required|max:190',
            'disciplinename' => 'required|max:50',
            'classapplicable' => 'required',
        ]);

        $sessionid = Session('valid_id');
        $discipline_code = $request->input('disciplinecode');
        $class_applicable = $request->input('classapplicable');
        

        foreach ($class_applicable as $key => $class)
        {
            $code = $sessionid."-".$discipline_code."-".$class;

            if (Disciplinemgmt::where('code', $code)->exists())
            {
               echo 'exist';
            }
            else
            {
                $x = new Disciplinemgmt;
                $x->discipline_code             =       $discipline_code;
                $x->discipline_name             =       $request->input('disciplinename');
                $x->class_applicable            =       $class;
                $x->code                        =       $code;
                $x->extra                       =       $request->input('extra');
                $x->sessionid                   =       $sessionid;

                $x->save();
            }
        }
        

        $request->session()->flash('message', 'Successfully Created new Co-Scholastic Detail!');
        return Redirect('discipline');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $classlist = Classmgmt::pluck('class_section');
        $disciplinedetail = Disciplinemgmt::find($id);
        return view('discipline.edit')->with(['disciplinedetail' => $disciplinedetail, 'id' => $id, 'classlist' => $classlist, ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'disciplinecode' => 'required|max:190',
            'disciplinename' => 'required|max:50',
            'classapplicable' => 'required',
        ]);

        $sessionid              = Session('valid_id');
        $discipline_code      = $request->input('disciplinecode');
        $class_applicable       = $request->input('classapplicable');

        $code = $sessionid."-".$discipline_code."-".$class_applicable;

        $x = Disciplinemgmt::find($id);
        $x->discipline_code           =       $discipline_code;
        $x->discipline_name           =       $request->input('disciplinename');
        $x->code                        =       $code;
        $x->extra                       =       $request->input('extra');
        $x->sessionid                   =       $sessionid;

        $x->save();

        $request->session()->flash('message', 'Successfully Created new Co-Scholastic Detail!');
        return Redirect('discipline');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $x = Disciplinemgmt::find($id);
        $x->delete();
        
        $request->session()->flash('message', 'Successfully Deleted the Co-Scholastic Details!');
        return Redirect('discipline');
    }
}
