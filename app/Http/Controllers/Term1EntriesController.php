<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Subjectmgmt;
use App\Studentmgmt;
use App\Classmgmt;

use App\Marksmgmt;

use App\Coscholasticmgmt;
use App\Disciplinemgmt;
use App\Attendancemgmt;
use App\Resultdatemgmt;
use App\Overallmarksmgmt;

use App\Coscholasticmarksmgmt;
use App\Disciplinemarksmgmt;

class Term1EntriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $markslist = Marksmgmt::where('sessionid', Session('valid_id'))->get();
        return view('term1entries.index')->with(['markslist' => $markslist , ]);
    }

    public function findstudent(Request $request)
    {
        
        return view('term1entries.findstudent');
    }

    public function checkpresence(Request $request)
    {
        $adm = $request->input('admission_number');
        session(['admission_number' => $adm]);
        // return view('term1entries.findstudent');
        //return redirect('/term1entries/create');

        if (Marksmgmt::where('sessionid' , Session('valid_id') )->where('admission_no', $adm)->exists())
            {
               return redirect('/term1entries/'.$adm.'/edit');
            }
            else
            {
                if (Studentmgmt::where('sessionid' , Session('valid_id') )->where('admission_no', $adm)->exists())
                {
                   return redirect('/term1entries/create');
                }
                else
                {
                    echo 'Student with Admission Number ='.$adm.' does not exists in session='.Session('valid_period');
                }
                
            }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $admission_number = session('admission_number');
        $studentdetails = Studentmgmt::where('admission_no', $admission_number)->where('sessionid', Session('valid_id') )->get();
        foreach ($studentdetails as $key => $value) {
            $student_class_section = $value->student_class_section;
        }
        //$student_class_section = Studentmgmt::where('admission_no', $admission_number);

        $subjectlist = Subjectmgmt::where('sessionid', Session('valid_id') )
                                        ->where('class_applicable', $student_class_section )
                                        ->get();
        $coscholasticlist = Coscholasticmgmt::where('sessionid', Session('valid_id') )
                                        ->where('class_applicable', $student_class_section )
                                        ->get();
        $disciplinelist = Disciplinemgmt::where('sessionid', Session('valid_id') )
                                        ->where('class_applicable', $student_class_section )
                                        ->get();
        $resultdate  = Resultdatemgmt::where('sessionid', Session('valid_id') )
                                        ->where('term', '1' )
                                        ->get();

        //print_r($coscholasticlist);
        return view('term1entries.create')->with(['studentdetails'      => $studentdetails ,
                                                'subjectlist'           => $subjectlist ,
                                                'coscholasticlist'      => $coscholasticlist ,
                                                'disciplinelist'        => $disciplinelist ,
                                                'resultdate'            => $resultdate ,
                                                'class_section'         => $student_class_section ,
                                                ]);
        //print_r($studentdetails);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $count_total_obtained = 0;
        $count_total_max      = 0;

        $admission_no = $request->input('hadmission_no');
        $sessionid = Session('valid_id');
        $term = '1';
        $valid_period = Session('valid_period');

        //****************************** UPLOADING SCHOLASTIC *********************************

        $subject_code = $request->input('hsubject_code'); //array preholded
        $subject_name = $request->input('hsubject_name'); //array preholded
        $dunit = $request->input('dunit'); // array
        $dunitmax = $request->input('hunitmax');  // array preholded
        $dexam = $request->input('dexam'); // array
        $dexammax = $request->input('hexammax'); //array preholded
        $class_section = $request->input('hclass_section'); // preholded


            $unit_max_term2 = $request->input('hunitmax_term2'); // preholded
            $exam_max_term2 = $request->input('hexammax_term2'); // preholded

        foreach ($subject_code as $key => $subcode)
        {
           $code = $admission_no."-".$sessionid."-".$term."-".$subcode;
           
           $unittest_obtained = $dunit[$key];
           $unittest_max      = $dunitmax[$key];
           $exam_obtained     = $dexam[$key];
           $exam_max          = $dexammax[$key];
           $subname           = $subject_name[$key];
           $total_obtained = 0;

           if($unittest_obtained == '-1'){} else { $total_obtained += $unittest_obtained ; }
           if($exam_obtained == '-1'){} else { $total_obtained += $exam_obtained ; }
           // $total_obtained = $unittest_obtained + $exam_obtained ;
           $total_max      = $unittest_max      + $exam_max      ;

           if (Marksmgmt::where('code', $code)->exists())
            {
               echo 'already exist';
            }
            else
            {
                $x = new Marksmgmt;
                $x->admission_no                    =       $admission_no;
                $x->sessionid                       =       $sessionid;
                $x->term                            =       $term ;
                $x->subject_code                    =       $subcode;
                $x->code                            =       $code;
                $x->valid_period                    =       $valid_period;
                $x->subject_name                    =       $subname;
                $x->unittest_obtained               =       $unittest_obtained;
                $x->unittest_max                    =       $unittest_max;
                $x->exam_obtained                   =       $exam_obtained; 
                $x->exam_max                        =       $exam_max;
                $x->total_obtained                  =       $total_obtained;
                $x->total_max                       =       $total_max;
                $x->student_class_section           =       $class_section;
                $x->unittest_max_term2              =       $unit_max_term2[$key];
                $x->exam_max_term2                  =       $exam_max_term2[$key];
                $x->total_max_term2                 =       $unit_max_term2[$key] + $exam_max_term2[$key];
                $x->save();

                $count_total_obtained += $total_obtained ;
                $count_total_max      += $total_max ;
            }
        }

        //************************** Uploading Overall marks **************************
        $term = '1' ;
        $code = $admission_no."-".$sessionid."-".$term;
        if (Overallmarksmgmt::where('code', $code)->exists())
        {
           echo 'already exist';
        }
        else
        {
            $x = new Overallmarksmgmt;
            $x->admission_no                    =       $admission_no;
            $x->sessionid                       =       $sessionid;
            $x->term                            =       $term ;
            $x->class_applicable                =       $class_section;
            $x->code                            =       $code;
            $x->valid_period                    =       $valid_period;
            $x->marks_term1_obtained            =       $count_total_obtained;
            $x->marks_term1_total               =       $count_total_max;
            $x->save();
        }


        //************************** Uploading Co-scholastic **************************


        $hcoscholastic_code = $request->input('hcoscholastic_code'); // array
        $hcoscholastic_name = $request->input('hcoscholastic_name'); // array
        $coscholastic_grade = $request->input('coscholastic_grade'); // array
        foreach ($hcoscholastic_code as $key => $cos_code)
        {
           $code = $admission_no."-".$sessionid."-".$term."-".$cos_code;
           
           $grade                       = $coscholastic_grade[$key];
           $coscholastic_name           = $hcoscholastic_name[$key];

           if (Coscholasticmarksmgmt::where('code', $code)->exists())
            {
               echo 'already exist';
            }
            else
            {
                $x = new Coscholasticmarksmgmt;
                $x->admission_no                    =       $admission_no;
                $x->sessionid                       =       $sessionid;
                $x->term                            =       $term ;
                $x->coscholastic_code               =       $cos_code;
                $x->code                            =       $code;
                $x->valid_period                    =       $valid_period;
                $x->coscholastic_name               =       $coscholastic_name;
                $x->class_applicable                =       $class_section;
                $x->grade_term1                     =       $grade;
                $x->grade_term2                     =       $grade; 
                $x->save();
            }
        }

        //************************** Uploading Discipline **************************


        $hdiscipline_code = $request->input('hdiscipline_code'); // array
        $hdiscipline_name = $request->input('hdiscipline_name'); // array
        $discipline_grade = $request->input('discipline_grade'); // array
        foreach ($hdiscipline_code as $key => $disp_code)
        {
           $code = $admission_no."-".$sessionid."-".$term."-".$disp_code;
           
           $grade                       = $discipline_grade[$key];
           $discipline_name           = $hdiscipline_name[$key];

           if (Disciplinemarksmgmt::where('code', $code)->exists())
            {
               echo 'already exist';
            }
            else
            {
                $x = new Disciplinemarksmgmt;
                $x->admission_no                    =       $admission_no;
                $x->sessionid                       =       $sessionid;
                $x->term                            =       $term ;
                $x->discipline_code                 =       $disp_code;
                $x->code                            =       $code;
                $x->valid_period                    =       $valid_period;
                $x->discipline_name                 =       $discipline_name;
                $x->class_applicable                =       $class_section;
                $x->grade_term1                     =       $grade;
                $x->grade_term2                     =       $grade; 
                $x->save();
            }
        }

        
        //****************************** ATTENDANCE UPLOADING ************************
        $attendence = $request->input('attendence'); 
        $attendencetotal    = $request->input('attendencetotal'); 
        $term = '1' ;
        $code = $admission_no."-".$sessionid."-".$term;
        if (Attendancemgmt::where('code', $code)->exists())
        {
           echo 'already exist';
        }
        else
        {
            $x = new Attendancemgmt;
            $x->admission_no                    =       $admission_no;
            $x->sessionid                       =       $sessionid;
            $x->term                            =       $term ;
            $x->class_applicable                =       $class_section;
            $x->code                            =       $code;
            $x->valid_period                    =       $valid_period;
            $x->attendance_obtained             =       $attendence;
            $x->attendance_total                =       $attendencetotal;
            $x->save();
        }

        //****************************** RANK ALLOTMENT *****************************
        
        $studentlist = Overallmarksmgmt::where('sessionid', Session('valid_id') )
                                    ->where('class_applicable', $class_section )
                                    ->orderBy('marks_term1_obtained', 'asc')
                                    ->pluck('id', 'marks_term1_obtained');

        $allids = array();
        foreach ($studentlist as $k => $id)
        {
            $allids[(int)$k]=(int)$id;
        }
        krsort($allids);

        $counter = 0;

        foreach ($allids as $idpointer) 
            {
                $counter ++ ;
                $x = Overallmarksmgmt::find($idpointer);
                $x->rank_term1  = $counter;
                $x->save();
            }


        $request->session()->flash('message', 'Successfully Uploaded Students Marks!');
        return Redirect('term1entries');
        
    }

    /**Session('valid_id') and Session('valid_period')
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $subjectmgmt = Subjectmgmt::find($id);
        return view('subjectmgmt.show')->with(['subjectmgmt' => $subjectmgmt,]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admission_number           =       session('admission_number');
        $sessionid                  =       Session('valid_id');
        $studentdetails = Studentmgmt::where('admission_no', $admission_number)->where('sessionid', Session('valid_id') )->get();

        foreach ($studentdetails as $key => $value) {
            $student_class_section = $value->student_class_section;
        }

        $subjectlist                  = Marksmgmt::where('sessionid', $sessionid )->where('admission_no', $admission_number )->get();
        $Overallmarksmgmt      = Overallmarksmgmt::where('sessionid', $sessionid )->where('admission_no', $admission_number )->get();
        $coscholasticlist = Coscholasticmarksmgmt::where('sessionid', $sessionid )->where('admission_no', $admission_number )->get();
        $disciplinelist     = Disciplinemarksmgmt::where('sessionid', $sessionid )->where('admission_no', $admission_number )->get();
        $attendance              = Attendancemgmt::where('sessionid', $sessionid )->where('admission_no', $admission_number )->get();
        $resultdate              = Resultdatemgmt::where('sessionid', $sessionid )->where('term', '1')->get();

        return view('term1entries.edit')->with(['studentdetails'    => $studentdetails ,
                                                'subjectlist'       => $subjectlist ,
                                                'Overallmarksmgmt'  => $Overallmarksmgmt ,
                                                'coscholasticlist'  => $coscholasticlist ,
                                                'disciplinelist'    => $disciplinelist ,
                                                'attendance'        => $attendance ,
                                                'resultdate'        => $resultdate ,
                                                'class_section'     => $student_class_section ,
                                                'id'                => $id ,
                                                ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //###########################################################################################
        $count_total_obtained = 0;
        $count_total_max      = 0;

        $admission_no = $request->input('hadmission_no');
        $sessionid = Session('valid_id');
        $term = '1';
        $valid_period = Session('valid_period');

        //****************************** UPLOADING SCHOLASTIC *********************************
        $marks_id = $request->input('hmarks_id'); //getting predefined id
        $subject_code = $request->input('hsubject_code'); //array preholded
        $subject_name = $request->input('hsubject_name'); //array preholded
        $dunit = $request->input('dunit'); // array
        $dunitmax = $request->input('hunitmax');  // array preholded
        $dexam = $request->input('dexam'); // array
        $dexammax = $request->input('hexammax'); //array preholded
        $class_section = $request->input('hclass_section'); // preholded


            $unit_max_term2 = $request->input('hunitmax_term2'); // preholded
            $exam_max_term2 = $request->input('hexammax_term2'); // preholded

        foreach ($subject_code as $key => $subcode)
        {
           $code = $admission_no."-".$sessionid."-".$term."-".$subcode;
           
           $unittest_obtained = $dunit[$key];
           $unittest_max      = $dunitmax[$key];
           $exam_obtained     = $dexam[$key];
           $exam_max          = $dexammax[$key];
           $subname           = $subject_name[$key];
           $total_obtained = 0;

           $individual_subject_marks_id = $marks_id[$key];

           if($unittest_obtained == '-1'){} else { $total_obtained += $unittest_obtained ; }
           if($exam_obtained == '-1'){} else { $total_obtained += $exam_obtained ; }
           // $total_obtained = $unittest_obtained + $exam_obtained ;
           $total_max      = $unittest_max      + $exam_max      ;

           if (Marksmgmt::where('code', $code)->exists())
            {
                $x = Marksmgmt::find($individual_subject_marks_id);
                $x->unittest_obtained               =       $unittest_obtained;
                $x->exam_obtained                   =       $exam_obtained; 
                $x->total_obtained                  =       $total_obtained;
                $x->save();

                $count_total_obtained += $total_obtained ;
            }
            else
            {
                echo 'problem occured while modification';
            }
        }

        //************************** Uploading Overall marks **************************
        $term = '1' ;
        $code = $admission_no."-".$sessionid."-".$term;
        $overallmarksid = $request->input('hoverallmarksid'); // preholded

        if (Overallmarksmgmt::where('code', $code)->exists())
        {
            $x = Overallmarksmgmt::find($overallmarksid);
            $x->marks_term1_obtained            =       $count_total_obtained;
            $x->save();
        }
        else
        {
            echo 'problem occured while modification';
        }


        //************************** Uploading Co-scholastic **************************


        $hcoscholastic_code = $request->input('hcoscholastic_code'); // array
        $hcoscholastic_name = $request->input('hcoscholastic_name'); // array
        $coscholastic_grade = $request->input('coscholastic_grade'); // array
        $coscholastic_grade = $request->input('coscholastic_grade'); // array
        $coscholastic_id    = $request->input('hcoscholastic_id'); //getting predefined id

        foreach ($hcoscholastic_code as $key => $cos_code)
        {
           $code = $admission_no."-".$sessionid."-".$term."-".$cos_code;
           
           $grade                       = $coscholastic_grade[$key];
           $coscholastic_name           = $hcoscholastic_name[$key];
           $individual_coscholastic_id  = $coscholastic_id[$key];

           if (Coscholasticmarksmgmt::where('code', $code)->exists())
            {
                $x = Coscholasticmarksmgmt::find($individual_coscholastic_id);
                $x->grade_term1                     =       $grade;
                $x->grade_term2                     =       $grade; 
                $x->save();
            }
            else
            {
                echo 'problem occured while modification';
            }
        }

        //************************** Uploading Discipline **************************


        $hdiscipline_code = $request->input('hdiscipline_code'); // array
        $hdiscipline_name = $request->input('hdiscipline_name'); // array
        $discipline_grade = $request->input('discipline_grade'); // array
        $discipline_id    = $request->input('hdiscipline_id'); //getting predefined id

        foreach ($hdiscipline_code as $key => $disp_code)
        {
           $code = $admission_no."-".$sessionid."-".$term."-".$disp_code;
           
           $grade                       = $discipline_grade[$key];
           $discipline_name             = $hdiscipline_name[$key];
           $individual_discipline_id    = $discipline_id[$key];

           if (Disciplinemarksmgmt::where('code', $code)->exists())
            {
                $x = Disciplinemarksmgmt::find($individual_discipline_id);
                $x->grade_term1                     =       $grade;
                $x->grade_term2                     =       $grade; 
                $x->save();
            }
            else
            {
                echo 'problem occured while modification';
            }
        }

        
        //****************************** ATTENDANCE UPLOADING ************************
        $attendence         = $request->input('attendence'); 
        $attendencetotal    = $request->input('attendencetotal'); 
        $attendenceid = $request->input('hattendenceid');
        $term = '1' ;
        $code = $admission_no."-".$sessionid."-".$term;
        if (Attendancemgmt::where('code', $code)->exists())
        {
            $x = Attendancemgmt::find($attendenceid);
            $x->attendance_obtained             =       $attendence;
            $x->attendance_total             =       $attendencetotal;
            $x->save();
        }
        else
        {
            echo 'problem occured while modification';
        }

        //****************************** RANK ALLOTMENT *****************************
        $studentlist = Overallmarksmgmt::where('sessionid', Session('valid_id') )
                                    ->where('class_applicable', $class_section )
                                    ->orderBy('marks_term1_obtained', 'asc')
                                    ->pluck('id', 'marks_term1_obtained');

        $allids = array();
        foreach ($studentlist as $k => $id)
        {
            $allids[(int)$k]=(int)$id;
        }
        krsort($allids);

        $counter = 0;

        foreach ($allids as $idpointer) 
            {
                $counter ++ ;
                $x = Overallmarksmgmt::find($idpointer);
                $x->rank_term1  = $counter;
                $x->save();
            }

        $request->session()->flash('message', 'Successfully Updated the Students Subjects Marks!');
        return Redirect('term1entries');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $request->session()->flash('message', 'Illegal Access Encountered!');
        return Redirect('term1entries');
    }

    public function duplicate($id)
    {
        $request->session()->flash('message', 'Illegal Access Encountered!');
        return Redirect('term1entries');
    }

}
