<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sessionmanager as SessionManager;
use App\Studentmgmt;
use App\Classmgmt;
use App\Subjectmgmt;
use App\Disciplinemgmt;
use App\Coscholasticmgmt;

class UpgradeController extends Controller
{
    public function index() {

        // Mail::send('emails.send', ['title' => $title, 'content' => $content], function ($message)
        // {

        //     $message->from('me@gmail.com', 'Christian Nwamba');

        //     $message->to('chrisn@scotch.io');

        // });


        $session = SessionManager::where('id','>', Session('valid_id') )->get();
        return view('upgrade.chooseupcomingsession')->with([
            'session' => $session,
        ]);
    }

    public function setupcomingsession(Request $req) {
        $nextsession = $req->input('nextsession');
        session(['nextsession' => $nextsession ]);
        $session = SessionManager::find($nextsession);

        $students = Studentmgmt::where('sessionid', Session('valid_id') )->limit(5)->get();
        $classlist = Classmgmt::pluck('class_section');

        // dd($students, $classlist, $session);
        return view('upgrade.upgradeclassselect')->with([
            'session' => $session,
            'students' => $students,
            'classlist' => $classlist,
        ]);
    }

    public function showclasslist() {
        $classlist = Classmgmt::pluck('class_section');

        return view('upgrade.upgradeclasslist')->with([
            'classlist' => $classlist,
        ]);
    }
    public function showclasslistgenerate(Request $req) {
        $nextsession = Session('nextsession');
        $session = SessionManager::find($nextsession);
        $class     = $req->input('class_section');

        $students = Studentmgmt::where('sessionid', Session('valid_id') )
                            ->where('student_class_section', $class)->get();
        $classlist = Classmgmt::pluck('class_section');

        return view('upgrade.upgradehomepage')->with([
            'session' => $session,
            'students' => $students,
            'classlist' => $classlist,
        ]);
    }
    public function upgradestudents(Request $req) {
        $nextsession = Session('nextsession');
        $admission_no               = $req->input('admission_no');
        $next_class_section      = $req->input('student_class_section');
        $added = 0; $skipped = 0;

        foreach ($admission_no as $k1 => $v1) {
            $students = Studentmgmt::where('admission_no', $v1 )->where('sessionid', Session('valid_id') )->get();
            $count = Studentmgmt::where('admission_no', $v1 )->where('sessionid', $nextsession )->count('id');

            if($count < 1 ) {
                $x = new Studentmgmt;
                $x = $students[0]->replicate();
                $x->student_class_section = $next_class_section[$k1];
                $x->sessionid = $nextsession;
                $x->save();
                $added = $added + 1;
            } else {
                $skipped = $skipped + 1;
            }

            
        }
        return view('upgrade.thankyou')->with([
            'added' => $added,
            'skipped' => $skipped,
        ]);
    }

    public function upgradesubjects(Request $req) {
        $nextsession = Session('nextsession');

        $added = 0; $skipped = 0;
        $subjectlist = Subjectmgmt::pluck('id');
        foreach ($subjectlist as $k1 => $v1) {
            $subjectdetails = Subjectmgmt::where('id', $v1 )->where('sessionid', Session('valid_id') )->get();
            $count = Subjectmgmt::where('extra', $v1 )->where('sessionid', $nextsession )->count('id');

            if($count < 1 ) {
                $x = new Subjectmgmt;
                $x = $subjectdetails[0]->replicate();
                $x->sessionid = $nextsession;
                $x->extra = $v1;
                $x->date_term1 = '';
                $x->date_term2 = '';
                $x->save();
                $added = $added + 1;
            } else {
                $skipped = $skipped + 1;
            }   
        }
        return view('upgrade.thankyou')->with([
            'added' => $added,
            'skipped' => $skipped,
        ]);
    }

    public function upgradedisciplines(Request $req) {
        $nextsession = Session('nextsession');

        $added = 0; $skipped = 0;
        $subjectlist = Disciplinemgmt::pluck('id');
        foreach ($subjectlist as $k1 => $v1) {
            $subjectdetails = Disciplinemgmt::where('id', $v1 )->where('sessionid', Session('valid_id') )->get();
            $count = Disciplinemgmt::where('extra', $v1 )->where('sessionid', $nextsession )->count('id');

            if($count < 1 ) {
                $x = new Disciplinemgmt;
                $x = $subjectdetails[0]->replicate();
                $x->sessionid = $nextsession;
                $x->extra = $v1;
                $x->save();
                $added = $added + 1;
            } else {
                $skipped = $skipped + 1;
            }   
        }
        return view('upgrade.thankyou')->with([
            'added' => $added,
            'skipped' => $skipped,
        ]);
    }
    public function upgradecoscholastics(Request $req) {
        $nextsession = Session('nextsession');

        $added = 0; $skipped = 0;
        $subjectlist = Coscholasticmgmt::pluck('id');
        foreach ($subjectlist as $k1 => $v1) {
            $subjectdetails = Coscholasticmgmt::where('id', $v1 )->where('sessionid', Session('valid_id') )->get();
            $count = Coscholasticmgmt::where('extra', $v1 )->where('sessionid', $nextsession )->count('id');

            if($count < 1 ) {
                $x = new Coscholasticmgmt;
                $x = $subjectdetails[0]->replicate();
                $x->sessionid = $nextsession;
                $x->extra = $v1;
                $x->save();
                $added = $added + 1;
            } else {
                $skipped = $skipped + 1;
            }   
        } 
        return view('upgrade.thankyou')->with([
            'added' => $added,
            'skipped' => $skipped,
        ]);
    }
}
