<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subjectmgmt;
use App\Studentmgmt;
use App\Classmgmt;
use App\Marksmgmt;

class UploadMarksTerm1Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $markslist = Marksmgmt::where('sessionid', Session('valid_id'))->get();
        return view('uploadmarksterm1.index')->with(['markslist' => $markslist , ]);
    }

    public function findclass(Request $request)
    {
         return view('uploadmarksterm1.findclass');
    }

    public function checkpresence(Request $request)
    {
        $class_applicable       =   $request->input('class_applicable');
        $subject_type           =   $request->input('subject_type');
        session(['class_applicable' => $class_applicable]);
        session(['subject_type'     => $subject_type]);

        if (Marksmgmt::where('sessionid' , Session('valid_id') )->where('student_class_section', $class_applicable)->exists())
            {
               return redirect('/uploadmarksterm1/1/edit');
            }
            else
            {
                return redirect('/uploadmarksterm1/create');
            }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $class_applicable       =   session('class_applicable');
        $subject_type           =   session('subject_type');
        $sessionid              =   Session('valid_id');

        $studentdetails = Studentmgmt::where('sessionid', $sessionid )->where('student_class_section', $class_applicable)->get();
        
        $subjectlist = Subjectmgmt::where('class_applicable', $class_applicable )->get();

        return view('uploadmarksterm1.create')->with(['studentdetails' => $studentdetails ,
                                                'subjectlist' => $subjectlist ,
                                                'class_section' => $class_applicable ,
                                                ]);
        //print_r($studentdetails);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        $admission_no = $request->input('hadmission_no');
        $sessionid = Session('valid_id');
        $term = '1';
        $subject_code = $request->input('hsubject_code'); //array preholded

        $valid_period = Session('valid_period');
        $subject_name = $request->input('hsubject_name'); //array preholded
        $dunit = $request->input('dunit'); // array
        $dunitmax = $request->input('hunitmax');  // array preholded
        $dexam = $request->input('dexam'); // array
        $dexammax = $request->input('hexammax'); //array preholded
        $class_section = $request->input('hclass_section'); // preholded


            $unit_max_term2 = $request->input('hunitmax_term2'); // preholded
            $exam_max_term2 = $request->input('hexammax_term2'); // preholded

        foreach ($subject_code as $key => $subcode)
        {
           $code = $admission_no."-".$sessionid."-".$term."-".$subcode;
           
           $unittest_obtained = $dunit[$key];
           $unittest_max      = $dunitmax[$key];
           $exam_obtained     = $dexam[$key];
           $exam_max          = $dexammax[$key];
           $subname           = $subject_name[$key];
           $total_obtained = 0;

           if($unittest_obtained == '-1'){} else { $total_obtained += $unittest_obtained ; }
           if($exam_obtained == '-1'){} else { $total_obtained += $exam_obtained ; }
           // $total_obtained = $unittest_obtained + $exam_obtained ;
           $total_max      = $unittest_max      + $exam_max      ;

           if (Marksmgmt::where('code', $code)->exists())
            {
               echo 'already exist';
            }
            else
            {
                $x = new Marksmgmt;
                $x->admission_no                    =       $admission_no;
                $x->sessionid                       =       $sessionid;
                $x->term                            =       $term ;
                $x->subject_code                    =       $subcode;
                $x->code                            =       $code;
                $x->valid_period                    =       $valid_period;
                $x->subject_name                    =       $subname;
                $x->unittest_obtained               =       $unittest_obtained;
                $x->unittest_max                    =       $unittest_max;
                $x->exam_obtained                   =       $exam_obtained; 
                $x->exam_max                        =       $exam_max;
                $x->total_obtained                  =       $total_obtained;
                $x->total_max                       =       $total_max;
                $x->student_class_section           =       $class_section;

                $x->unittest_max_term2              =       $unit_max_term2[$key];
                $x->exam_max_term2                  =       $exam_max_term2[$key];
                $x->total_max_term2                 =       $unit_max_term2[$key] + $exam_max_term2[$key];
                $x->save();
            }
        }

        $request->session()->flash('message', 'Successfully Uploaded Students Marks!');
        return Redirect('uploadmarksterm1');
        
    }

    /**Session('valid_id') and Session('valid_period')
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $subjectmgmt = Subjectmgmt::find($id);
        return view('subjectmgmt.show')->with(['subjectmgmt' => $subjectmgmt,]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admission_number           =       session('admission_number');
        $sessionid                  =       Session('valid_id');
        $studentdetails = Studentmgmt::where('admission_no', $admission_number)->get();

        foreach ($studentdetails as $key => $value) {
            $student_class_section = $value->student_class_section;
        }
        
        $subjectlist = Marksmgmt::where('sessionid', $sessionid )->where('admission_no', $admission_number )->get();

        return view('uploadmarksterm1.edit')->with(['studentdetails' => $studentdetails ,
                                                'subjectlist' => $subjectlist ,
                                                'class_section' => $student_class_section ,
                                                'id' => $id ,
                                                ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $admission_no = $request->input('hadmission_no');
        $sessionid = Session('valid_id');
        $term = '1';
        $subject_code = $request->input('hsubject_code'); //array preholded

        $valid_period = Session('valid_period');
        $marks_id = $request->input('hmarks_id'); //array preholded
        $subject_name = $request->input('hsubject_name'); //array preholded
        $dunit = $request->input('dunit'); // array
        $dunitmax = $request->input('hunitmax');  // array preholded
        $dexam = $request->input('dexam'); // array
        $dexammax = $request->input('hexammax'); //array preholded
        $class_section = $request->input('hclass_section'); // preholded

        foreach ($subject_code as $key => $subcode)
        {
           $code = $admission_no."-".$sessionid."-".$term."-".$subcode;
           
           $individual_subject_marks_id = $marks_id[$key];

           $unittest_obtained = $dunit[$key];
           $unittest_max      = $dunitmax[$key];
           $exam_obtained     = $dexam[$key];
           $exam_max          = $dexammax[$key];
           $subname           = $subject_name[$key];
           $total_obtained = 0;

           if($unittest_obtained == '-1'){} else { $total_obtained += $unittest_obtained ; }
           if($exam_obtained == '-1'){} else { $total_obtained += $exam_obtained ; }
           // $total_obtained = $unittest_obtained + $exam_obtained ;
           $total_max      = $unittest_max      + $exam_max      ;

           if (Marksmgmt::where('code', $code)->exists())
            {
               //$x = Marksmgmt::where('sessionid', $sessionid)->where('admission_no', $admission_no)->where('subject_code', $subcode)->get();
               $x = Marksmgmt::find($individual_subject_marks_id);
                $x->unittest_obtained               =       $unittest_obtained;
                $x->exam_obtained                   =       $exam_obtained; 
                $x->total_obtained                  =       $total_obtained;
                $x->save();

                
            }
            else
            {
                echo "Does Not Exists OR ERROR Encountered, please contact to Admin if encountered again";
            }
        }

        $request->session()->flash('message', 'Successfully Updated the Students Subjects Marks!');
        return Redirect('uploadmarksterm1');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $x = Subjectmgmt::find($id);
        $x->delete();
        
        $request->session()->flash('message', 'Successfully Deleted the Subject Details!');
        return Redirect('subjectmgmt');
    }

    public function duplicate($id)
    {
        $classlist = Classmgmt::pluck('class_section');
        $subjectmgmt = Subjectmgmt::find($id);
        return view('subjectmgmt.duplicate')->with(['subjectmgmt' => $subjectmgmt, 'id' => $id, 'classlist' => $classlist, ]);
    }

}
