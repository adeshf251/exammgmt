<?php

namespace App\Http\Middleware;

use Closure;

class CheckLoginStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!($request->session()->has('valid_period')) )
        {
            return redirect('/home');
        }

        return $next($request);
    }
}
