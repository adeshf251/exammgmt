-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 13, 2017 at 07:16 AM
-- Server version: 10.1.10-MariaDB-log
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `school28`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendancemgmts`
--

CREATE TABLE `attendancemgmts` (
  `id` int(10) UNSIGNED NOT NULL,
  `admission_no` text COLLATE utf8_unicode_ci,
  `sessionid` text COLLATE utf8_unicode_ci,
  `term` text COLLATE utf8_unicode_ci,
  `class_applicable` text COLLATE utf8_unicode_ci,
  `code` varchar(190) COLLATE utf8_unicode_ci DEFAULT NULL,
  `valid_period` text COLLATE utf8_unicode_ci,
  `attendance_obtained` text COLLATE utf8_unicode_ci,
  `attendance_total` text COLLATE utf8_unicode_ci,
  `attendance_obtained_term2` text COLLATE utf8_unicode_ci,
  `attendance_total_term2` text COLLATE utf8_unicode_ci,
  `attendance_obtained_overall` text COLLATE utf8_unicode_ci,
  `attendance_total_overall` text COLLATE utf8_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'Category 1', 'category-1', '2017-08-29 15:05:39', '2017-08-29 15:05:39'),
(2, NULL, 1, 'Category 2', 'category-2', '2017-08-29 15:05:39', '2017-08-29 15:05:39');

-- --------------------------------------------------------

--
-- Table structure for table `classmgmts`
--

CREATE TABLE `classmgmts` (
  `id` int(10) UNSIGNED NOT NULL,
  `class_section` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `classmgmts`
--

INSERT INTO `classmgmts` (`id`, `class_section`, `description`, `created_at`, `updated_at`) VALUES
(3, 'PRE NUR--', 'section A', '2017-06-30 14:53:59', '2017-09-01 07:58:00'),
(4, 'NUR-A', 'sec A', '2017-06-30 14:54:39', '2017-06-30 14:54:39'),
(5, 'NUR-B', 'sec B', '2017-06-30 14:54:26', '2017-06-30 14:54:26'),
(6, 'LKG-A', 'sec A', '2017-06-30 14:55:09', '2017-06-30 14:55:09'),
(7, 'LKG-B', 'sec B', '2017-06-30 14:54:54', '2017-06-30 14:54:54'),
(8, 'UKG-A', 'UKG-A', '2017-06-30 14:56:00', '2017-06-30 14:56:00'),
(9, 'UKG-B', 'UKG-B', '2017-06-30 14:55:47', '2017-06-30 14:55:47'),
(23, 'I-A', 'I-A', '2017-06-30 14:53:06', '2017-06-30 14:53:06'),
(24, 'I-B', 'I-B', '2017-06-30 14:53:19', '2017-06-30 14:53:19'),
(25, 'II-A', 'A', '2017-06-30 14:52:44', '2017-06-30 14:52:44'),
(27, 'III-A', 'section A', '2017-06-30 14:52:12', '2017-06-30 14:52:12'),
(28, 'III-B', 'section B', '2017-06-30 14:51:54', '2017-06-30 14:51:54'),
(30, 'IV-A', 'sec A', '2017-06-30 14:51:37', '2017-06-30 14:51:37'),
(31, 'IV-B', 'IV-B', '2017-06-30 14:51:24', '2017-06-30 14:51:24'),
(32, 'V-A', 'V-A', '2017-06-30 14:50:32', '2017-06-30 14:50:32'),
(33, 'V-B', 'section B', '2017-06-30 14:50:46', '2017-06-30 14:50:46'),
(34, 'VI-A', 'sec A', '2017-06-30 14:49:56', '2017-06-30 14:49:56'),
(35, 'VI-B', 'B', '2017-06-30 14:50:09', '2017-06-30 14:50:09'),
(36, 'II-B', 'sec B', '2017-06-30 14:52:34', '2017-06-30 14:52:34'),
(37, 'VII-A', 'sec A', '2017-06-30 14:49:42', '2017-06-30 14:49:42'),
(38, 'VII-B', 'sec B', '2017-06-30 14:49:22', '2017-06-30 14:49:22'),
(39, 'VIII-A', 'A', '2017-06-30 14:49:07', '2017-06-30 14:49:07'),
(40, 'VIII-B', 'VIII-B', '2017-06-30 14:48:55', '2017-06-30 14:48:55'),
(41, 'IX-A', 'IX-A', '2017-06-30 14:48:27', '2017-06-30 14:48:27'),
(42, 'IX-B', 'IX-B', '2017-06-30 14:48:39', '2017-06-30 14:48:39'),
(43, 'X-A', 'X-A', '2017-06-30 14:45:58', '2017-06-30 14:45:58'),
(44, 'X-B', 'tenth', '2017-06-30 14:45:42', '2017-06-30 14:45:42'),
(45, 'XI SCI-A', 'Science Group', '2017-06-30 14:42:54', '2017-09-01 07:59:44'),
(47, 'XI COM-B', 'Commerce Group', '2017-06-30 14:44:31', '2017-09-01 08:00:22'),
(49, 'XII SCI-A', 'Science Group', '2017-06-30 14:42:15', '2017-09-01 08:00:50'),
(51, 'XII COM-B', 'Commerce Group', '2017-06-30 14:42:35', '2017-09-01 08:01:09');

-- --------------------------------------------------------

--
-- Table structure for table `coscholasticmarksmgmts`
--

CREATE TABLE `coscholasticmarksmgmts` (
  `id` int(10) UNSIGNED NOT NULL,
  `admission_no` text COLLATE utf8_unicode_ci,
  `sessionid` text COLLATE utf8_unicode_ci,
  `term` text COLLATE utf8_unicode_ci,
  `coscholastic_code` text COLLATE utf8_unicode_ci,
  `code` varchar(190) COLLATE utf8_unicode_ci DEFAULT NULL,
  `valid_period` text COLLATE utf8_unicode_ci,
  `coscholastic_name` text COLLATE utf8_unicode_ci,
  `class_applicable` text COLLATE utf8_unicode_ci,
  `grade_term1` text COLLATE utf8_unicode_ci,
  `grade_term2` text COLLATE utf8_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `coscholasticmgmts`
--

CREATE TABLE `coscholasticmgmts` (
  `id` int(10) UNSIGNED NOT NULL,
  `coscholastic_code` text COLLATE utf8_unicode_ci,
  `coscholastic_name` text COLLATE utf8_unicode_ci,
  `class_applicable` text COLLATE utf8_unicode_ci,
  `code` text COLLATE utf8_unicode_ci,
  `extra` text COLLATE utf8_unicode_ci,
  `grade` text COLLATE utf8_unicode_ci,
  `sessionid` text COLLATE utf8_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customadmitcards`
--

CREATE TABLE `customadmitcards` (
  `id` int(10) UNSIGNED NOT NULL,
  `admission_no` text COLLATE utf8_unicode_ci,
  `student_name` text COLLATE utf8_unicode_ci,
  `mother_name` text COLLATE utf8_unicode_ci,
  `father_name` text COLLATE utf8_unicode_ci,
  `roll_no` text COLLATE utf8_unicode_ci,
  `class_section` text COLLATE utf8_unicode_ci,
  `exam_term` text COLLATE utf8_unicode_ci,
  `session` text COLLATE utf8_unicode_ci,
  `subject1` text COLLATE utf8_unicode_ci,
  `date1` text COLLATE utf8_unicode_ci,
  `subject2` text COLLATE utf8_unicode_ci,
  `date2` text COLLATE utf8_unicode_ci,
  `subject3` text COLLATE utf8_unicode_ci,
  `date3` text COLLATE utf8_unicode_ci,
  `subject4` text COLLATE utf8_unicode_ci,
  `date4` text COLLATE utf8_unicode_ci,
  `subject5` text COLLATE utf8_unicode_ci,
  `date5` text COLLATE utf8_unicode_ci,
  `subject6` text COLLATE utf8_unicode_ci,
  `date6` text COLLATE utf8_unicode_ci,
  `subject7` text COLLATE utf8_unicode_ci,
  `date7` text COLLATE utf8_unicode_ci,
  `subject8` text COLLATE utf8_unicode_ci,
  `date8` text COLLATE utf8_unicode_ci,
  `subject9` text COLLATE utf8_unicode_ci,
  `date9` text COLLATE utf8_unicode_ci,
  `subject10` text COLLATE utf8_unicode_ci,
  `date10` text COLLATE utf8_unicode_ci,
  `sessionid` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(2, 1, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, '', 2),
(3, 1, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, '', 3),
(4, 1, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '', 4),
(5, 1, 'excerpt', 'text_area', 'excerpt', 1, 0, 1, 1, 1, 1, '', 5),
(6, 1, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, '', 6),
(7, 1, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '\n{\n    "resize": {\n        "width": "1000",\n        "height": "null"\n    },\n    "quality": "70%",\n    "upsize": true,\n    "thumbnails": [\n        {\n            "name": "medium",\n            "scale": "50%"\n        },\n        {\n            "name": "small",\n            "scale": "25%"\n        },\n        {\n            "name": "cropped",\n            "crop": {\n                "width": "300",\n                "height": "250"\n            }\n        }\n    ]\n}', 7),
(8, 1, 'slug', 'text', 'slug', 1, 0, 1, 1, 1, 1, '\n{\n    "slugify": {\n        "origin": "title",\n        "forceUpdate": true\n    }\n}', 8),
(9, 1, 'meta_description', 'text_area', 'meta_description', 1, 0, 1, 1, 1, 1, '', 9),
(10, 1, 'meta_keywords', 'text_area', 'meta_keywords', 1, 0, 1, 1, 1, 1, '', 10),
(11, 1, 'status', 'select_dropdown', 'status', 1, 1, 1, 1, 1, 1, '\n{\n    "default": "DRAFT",\n    "options": {\n        "PUBLISHED": "published",\n        "DRAFT": "draft",\n        "PENDING": "pending"\n    }\n}', 11),
(12, 1, 'created_at', 'timestamp', 'created_at', 0, 1, 1, 0, 0, 0, '', 12),
(13, 1, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '', 13),
(14, 2, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, '', 1),
(15, 2, 'author_id', 'text', 'author_id', 1, 0, 0, 0, 0, 0, '', 2),
(16, 2, 'title', 'text', 'title', 1, 1, 1, 1, 1, 1, '', 3),
(17, 2, 'excerpt', 'text_area', 'excerpt', 1, 0, 1, 1, 1, 1, '', 4),
(18, 2, 'body', 'rich_text_box', 'body', 1, 0, 1, 1, 1, 1, '', 5),
(19, 2, 'slug', 'text', 'slug', 1, 0, 1, 1, 1, 1, '{"slugify":{"origin":"title"}}', 6),
(20, 2, 'meta_description', 'text', 'meta_description', 1, 0, 1, 1, 1, 1, '', 7),
(21, 2, 'meta_keywords', 'text', 'meta_keywords', 1, 0, 1, 1, 1, 1, '', 8),
(22, 2, 'status', 'select_dropdown', 'status', 1, 1, 1, 1, 1, 1, '{"default":"INACTIVE","options":{"INACTIVE":"INACTIVE","ACTIVE":"ACTIVE"}}', 9),
(23, 2, 'created_at', 'timestamp', 'created_at', 1, 1, 1, 0, 0, 0, '', 10),
(24, 2, 'updated_at', 'timestamp', 'updated_at', 1, 0, 0, 0, 0, 0, '', 11),
(25, 2, 'image', 'image', 'image', 0, 1, 1, 1, 1, 1, '', 12),
(26, 3, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, '', 1),
(27, 3, 'name', 'text', 'name', 1, 1, 1, 1, 1, 1, '', 2),
(28, 3, 'email', 'text', 'email', 1, 1, 1, 1, 1, 1, '', 3),
(29, 3, 'password', 'password', 'password', 1, 0, 0, 1, 1, 0, '', 4),
(30, 3, 'remember_token', 'text', 'remember_token', 0, 0, 0, 0, 0, 0, '', 5),
(31, 3, 'created_at', 'timestamp', 'created_at', 0, 1, 1, 0, 0, 0, '', 6),
(32, 3, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '', 7),
(33, 3, 'avatar', 'image', 'avatar', 0, 1, 1, 1, 1, 1, '', 8),
(34, 5, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, '', 1),
(35, 5, 'name', 'text', 'name', 1, 1, 1, 1, 1, 1, '', 2),
(36, 5, 'created_at', 'timestamp', 'created_at', 0, 0, 0, 0, 0, 0, '', 3),
(37, 5, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '', 4),
(38, 4, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, '', 1),
(39, 4, 'parent_id', 'select_dropdown', 'parent_id', 0, 0, 1, 1, 1, 1, '{"default":"","null":"","options":{"":"-- None --"},"relationship":{"key":"id","label":"name"}}', 2),
(40, 4, 'order', 'text', 'order', 1, 1, 1, 1, 1, 1, '{"default":1}', 3),
(41, 4, 'name', 'text', 'name', 1, 1, 1, 1, 1, 1, '', 4),
(42, 4, 'slug', 'text', 'slug', 1, 1, 1, 1, 1, 1, '', 5),
(43, 4, 'created_at', 'timestamp', 'created_at', 0, 0, 1, 0, 0, 0, '', 6),
(44, 4, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '', 7),
(45, 6, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, '', 1),
(46, 6, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
(47, 6, 'created_at', 'timestamp', 'created_at', 0, 0, 0, 0, 0, 0, '', 3),
(48, 6, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '', 4),
(49, 6, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, '', 5),
(50, 1, 'seo_title', 'text', 'seo_title', 0, 1, 1, 1, 1, 1, '', 14),
(51, 1, 'featured', 'checkbox', 'featured', 1, 1, 1, 1, 1, 1, '', 15),
(52, 3, 'role_id', 'text', 'role_id', 1, 1, 1, 1, 1, 1, '', 9);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `controller`, `description`, `generate_permissions`, `server_side`, `created_at`, `updated_at`) VALUES
(1, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', '', '', 1, 0, '2017-08-29 15:05:34', '2017-08-29 15:05:34'),
(2, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', '', '', 1, 0, '2017-08-29 15:05:34', '2017-08-29 15:05:34'),
(3, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', '', '', 1, 0, '2017-08-29 15:05:34', '2017-08-29 15:05:34'),
(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', '', '', 1, 0, '2017-08-29 15:05:34', '2017-08-29 15:05:34'),
(5, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', '', '', 1, 0, '2017-08-29 15:05:34', '2017-08-29 15:05:34'),
(6, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', '', '', 1, 0, '2017-08-29 15:05:34', '2017-08-29 15:05:34');

-- --------------------------------------------------------

--
-- Table structure for table `disciplinemarksmgmts`
--

CREATE TABLE `disciplinemarksmgmts` (
  `id` int(10) UNSIGNED NOT NULL,
  `admission_no` text COLLATE utf8_unicode_ci,
  `sessionid` text COLLATE utf8_unicode_ci,
  `term` text COLLATE utf8_unicode_ci,
  `discipline_code` text COLLATE utf8_unicode_ci,
  `code` varchar(190) COLLATE utf8_unicode_ci DEFAULT NULL,
  `valid_period` text COLLATE utf8_unicode_ci,
  `discipline_name` text COLLATE utf8_unicode_ci,
  `class_applicable` text COLLATE utf8_unicode_ci,
  `grade_term1` text COLLATE utf8_unicode_ci,
  `grade_term2` text COLLATE utf8_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `disciplinemgmts`
--

CREATE TABLE `disciplinemgmts` (
  `id` int(10) UNSIGNED NOT NULL,
  `discipline_code` text COLLATE utf8_unicode_ci,
  `discipline_name` text COLLATE utf8_unicode_ci,
  `class_applicable` text COLLATE utf8_unicode_ci,
  `code` text COLLATE utf8_unicode_ci,
  `extra` text COLLATE utf8_unicode_ci,
  `grade` text COLLATE utf8_unicode_ci,
  `sessionid` text COLLATE utf8_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `marksmgmts`
--

CREATE TABLE `marksmgmts` (
  `id` int(10) UNSIGNED NOT NULL,
  `admission_no` text COLLATE utf8_unicode_ci,
  `sessionid` text COLLATE utf8_unicode_ci,
  `term` text COLLATE utf8_unicode_ci,
  `subject_code` text COLLATE utf8_unicode_ci,
  `code` varchar(190) COLLATE utf8_unicode_ci DEFAULT NULL,
  `valid_period` text COLLATE utf8_unicode_ci,
  `subject_name` text COLLATE utf8_unicode_ci,
  `unittest_obtained` text COLLATE utf8_unicode_ci,
  `unittest_max` text COLLATE utf8_unicode_ci,
  `exam_obtained` text COLLATE utf8_unicode_ci,
  `exam_max` text COLLATE utf8_unicode_ci,
  `total_obtained` text COLLATE utf8_unicode_ci,
  `total_max` text COLLATE utf8_unicode_ci,
  `student_class_section` text COLLATE utf8_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `unittest_obtained_term2` text COLLATE utf8_unicode_ci,
  `unittest_max_term2` text COLLATE utf8_unicode_ci,
  `exam_obtained_term2` text COLLATE utf8_unicode_ci,
  `exam_max_term2` text COLLATE utf8_unicode_ci,
  `total_obtained_term2` text COLLATE utf8_unicode_ci,
  `total_max_term2` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2017-08-29 15:05:36', '2017-08-29 15:05:36');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '/admin', '_self', 'voyager-boat', NULL, NULL, 1, '2017-08-29 15:05:36', '2017-08-29 15:05:36', NULL, NULL),
(2, 1, 'Media', '/admin/media', '_self', 'voyager-images', NULL, NULL, 5, '2017-08-29 15:05:36', '2017-08-29 15:05:36', NULL, NULL),
(3, 1, 'Posts', '/admin/posts', '_self', 'voyager-news', NULL, NULL, 6, '2017-08-29 15:05:36', '2017-08-29 15:05:36', NULL, NULL),
(4, 1, 'Users', '/admin/users', '_self', 'voyager-person', NULL, NULL, 3, '2017-08-29 15:05:36', '2017-08-29 15:05:36', NULL, NULL),
(5, 1, 'Categories', '/admin/categories', '_self', 'voyager-categories', NULL, NULL, 8, '2017-08-29 15:05:36', '2017-08-29 15:05:36', NULL, NULL),
(6, 1, 'Pages', '/admin/pages', '_self', 'voyager-file-text', NULL, NULL, 7, '2017-08-29 15:05:36', '2017-08-29 15:05:36', NULL, NULL),
(7, 1, 'Roles', '/admin/roles', '_self', 'voyager-lock', NULL, NULL, 2, '2017-08-29 15:05:37', '2017-08-29 15:05:37', NULL, NULL),
(8, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 9, '2017-08-29 15:05:37', '2017-08-29 15:05:37', NULL, NULL),
(9, 1, 'Menu Builder', '/admin/menus', '_self', 'voyager-list', NULL, 8, 10, '2017-08-29 15:05:37', '2017-08-29 15:05:37', NULL, NULL),
(10, 1, 'Database', '/admin/database', '_self', 'voyager-data', NULL, 8, 11, '2017-08-29 15:05:37', '2017-08-29 15:05:37', NULL, NULL),
(11, 1, 'Settings', '/admin/settings', '_self', 'voyager-settings', NULL, NULL, 12, '2017-08-29 15:05:37', '2017-08-29 15:05:37', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_01_01_000000_create_pages_table', 1),
(6, '2016_01_01_000000_create_posts_table', 1),
(7, '2016_02_15_204651_create_categories_table', 1),
(8, '2016_05_19_173453_create_menu_table', 1),
(9, '2016_10_21_190000_create_roles_table', 1),
(10, '2016_10_21_190000_create_settings_table', 1),
(11, '2016_11_30_135954_create_permission_table', 1),
(12, '2016_11_30_141208_create_permission_role_table', 1),
(13, '2016_12_26_201236_data_types__add__server_side', 1),
(14, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(15, '2017_01_14_005015_create_translations_table', 1),
(16, '2017_01_15_000000_add_permission_group_id_to_permissions_table', 1),
(17, '2017_01_15_000000_create_permission_groups_table', 1),
(18, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(19, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(20, '2017_04_21_000000_add_order_to_data_rows_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `overallmarksmgmts`
--

CREATE TABLE `overallmarksmgmts` (
  `id` int(10) UNSIGNED NOT NULL,
  `admission_no` text COLLATE utf8_unicode_ci,
  `sessionid` text COLLATE utf8_unicode_ci,
  `term` text COLLATE utf8_unicode_ci,
  `code` varchar(190) COLLATE utf8_unicode_ci DEFAULT NULL,
  `valid_period` text COLLATE utf8_unicode_ci,
  `class_applicable` text COLLATE utf8_unicode_ci,
  `marks_term1_obtained` text COLLATE utf8_unicode_ci,
  `marks_term1_total` text COLLATE utf8_unicode_ci,
  `rank_term1` text COLLATE utf8_unicode_ci,
  `marks_term2_obtained` text COLLATE utf8_unicode_ci,
  `marks_term2_total` text COLLATE utf8_unicode_ci,
  `rank_term2` text COLLATE utf8_unicode_ci,
  `marks_overall_obtained` text COLLATE utf8_unicode_ci,
  `marks_overall_total` text COLLATE utf8_unicode_ci,
  `rank_overall` text COLLATE utf8_unicode_ci,
  `result_status` text COLLATE utf8_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o''nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/AAgCCnqHfLlRub9syUdw.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2017-08-29 15:05:40', '2017-08-29 15:05:40');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `permission_group_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`, `permission_group_id`) VALUES
(1, 'browse_admin', NULL, '2017-08-29 15:05:37', '2017-08-29 15:05:37', NULL),
(2, 'browse_database', NULL, '2017-08-29 15:05:37', '2017-08-29 15:05:37', NULL),
(3, 'browse_media', NULL, '2017-08-29 15:05:37', '2017-08-29 15:05:37', NULL),
(4, 'browse_settings', NULL, '2017-08-29 15:05:37', '2017-08-29 15:05:37', NULL),
(5, 'browse_menus', 'menus', '2017-08-29 15:05:37', '2017-08-29 15:05:37', NULL),
(6, 'read_menus', 'menus', '2017-08-29 15:05:37', '2017-08-29 15:05:37', NULL),
(7, 'edit_menus', 'menus', '2017-08-29 15:05:37', '2017-08-29 15:05:37', NULL),
(8, 'add_menus', 'menus', '2017-08-29 15:05:37', '2017-08-29 15:05:37', NULL),
(9, 'delete_menus', 'menus', '2017-08-29 15:05:37', '2017-08-29 15:05:37', NULL),
(10, 'browse_pages', 'pages', '2017-08-29 15:05:37', '2017-08-29 15:05:37', NULL),
(11, 'read_pages', 'pages', '2017-08-29 15:05:37', '2017-08-29 15:05:37', NULL),
(12, 'edit_pages', 'pages', '2017-08-29 15:05:37', '2017-08-29 15:05:37', NULL),
(13, 'add_pages', 'pages', '2017-08-29 15:05:38', '2017-08-29 15:05:38', NULL),
(14, 'delete_pages', 'pages', '2017-08-29 15:05:38', '2017-08-29 15:05:38', NULL),
(15, 'browse_roles', 'roles', '2017-08-29 15:05:38', '2017-08-29 15:05:38', NULL),
(16, 'read_roles', 'roles', '2017-08-29 15:05:38', '2017-08-29 15:05:38', NULL),
(17, 'edit_roles', 'roles', '2017-08-29 15:05:38', '2017-08-29 15:05:38', NULL),
(18, 'add_roles', 'roles', '2017-08-29 15:05:38', '2017-08-29 15:05:38', NULL),
(19, 'delete_roles', 'roles', '2017-08-29 15:05:38', '2017-08-29 15:05:38', NULL),
(20, 'browse_users', 'users', '2017-08-29 15:05:38', '2017-08-29 15:05:38', NULL),
(21, 'read_users', 'users', '2017-08-29 15:05:38', '2017-08-29 15:05:38', NULL),
(22, 'edit_users', 'users', '2017-08-29 15:05:38', '2017-08-29 15:05:38', NULL),
(23, 'add_users', 'users', '2017-08-29 15:05:38', '2017-08-29 15:05:38', NULL),
(24, 'delete_users', 'users', '2017-08-29 15:05:38', '2017-08-29 15:05:38', NULL),
(25, 'browse_posts', 'posts', '2017-08-29 15:05:38', '2017-08-29 15:05:38', NULL),
(26, 'read_posts', 'posts', '2017-08-29 15:05:38', '2017-08-29 15:05:38', NULL),
(27, 'edit_posts', 'posts', '2017-08-29 15:05:38', '2017-08-29 15:05:38', NULL),
(28, 'add_posts', 'posts', '2017-08-29 15:05:38', '2017-08-29 15:05:38', NULL),
(29, 'delete_posts', 'posts', '2017-08-29 15:05:38', '2017-08-29 15:05:38', NULL),
(30, 'browse_categories', 'categories', '2017-08-29 15:05:38', '2017-08-29 15:05:38', NULL),
(31, 'read_categories', 'categories', '2017-08-29 15:05:38', '2017-08-29 15:05:38', NULL),
(32, 'edit_categories', 'categories', '2017-08-29 15:05:38', '2017-08-29 15:05:38', NULL),
(33, 'add_categories', 'categories', '2017-08-29 15:05:38', '2017-08-29 15:05:38', NULL),
(34, 'delete_categories', 'categories', '2017-08-29 15:05:38', '2017-08-29 15:05:38', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_groups`
--

CREATE TABLE `permission_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
(1, 0, NULL, 'Lorem Ipsum Post', NULL, 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/nlje9NZQ7bTMYOUG4lF1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2017-08-29 15:05:40', '2017-08-29 15:05:40'),
(2, 0, NULL, 'My Sample Post', NULL, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\n                <h2>We can use all kinds of format!</h2>\n                <p>And include a bunch of other stuff.</p>', 'posts/7uelXHi85YOfZKsoS6Tq.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2017-08-29 15:05:40', '2017-08-29 15:05:40'),
(3, 0, NULL, 'Latest Post', NULL, 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/9txUSY6wb7LTBSbDPrD9.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2017-08-29 15:05:40', '2017-08-29 15:05:40'),
(4, 0, NULL, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/yuk1fBwmKKZdY2qR1ZKM.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2017-08-29 15:05:40', '2017-08-29 15:05:40');

-- --------------------------------------------------------

--
-- Table structure for table `resultdatemgmts`
--

CREATE TABLE `resultdatemgmts` (
  `id` int(10) UNSIGNED NOT NULL,
  `sessionid` text COLLATE utf8_unicode_ci,
  `term` text COLLATE utf8_unicode_ci,
  `extra` text COLLATE utf8_unicode_ci,
  `code` varchar(190) COLLATE utf8_unicode_ci DEFAULT NULL,
  `valid_period` text COLLATE utf8_unicode_ci,
  `result_date` text COLLATE utf8_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2017-08-29 15:05:37', '2017-08-29 15:05:37'),
(2, 'user', 'Normal User', '2017-08-29 15:05:37', '2017-08-29 15:05:37');

-- --------------------------------------------------------

--
-- Table structure for table `sessionmanagers`
--

CREATE TABLE `sessionmanagers` (
  `id` int(11) NOT NULL,
  `session_period` varchar(190) NOT NULL,
  `status` varchar(500) DEFAULT '-',
  `description` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sessionmanagers`
--

INSERT INTO `sessionmanagers` (`id`, `session_period`, `status`, `description`, `created_at`, `updated_at`) VALUES
(1, '2017-2018', '-', 'first session', '2017-08-31 14:47:44', '2017-08-31 14:47:44'),
(2, '2018-2019', '-', 'second session', '2017-08-31 14:48:05', '2017-08-31 14:48:05'),
(3, '2019-2020', '-', 'third session', '2017-08-31 14:48:24', '2017-08-31 14:48:24');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`) VALUES
(1, 'title', 'Site Title', 'Site Title', '', 'text', 1),
(2, 'description', 'Site Description', 'Site Description', '', 'text', 2),
(3, 'logo', 'Site Logo', '', '', 'image', 3),
(4, 'admin_bg_image', 'Admin Background Image', '', '', 'image', 9),
(5, 'admin_title', 'Admin Title', 'Voyager', '', 'text', 4),
(6, 'admin_description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 5),
(7, 'admin_loader', 'Admin Loader', '', '', 'image', 6),
(8, 'admin_icon_image', 'Admin Icon Image', '', '', 'image', 7),
(9, 'google_analytics_client_id', 'Google Analytics Client ID', '', '', 'text', 9);

-- --------------------------------------------------------

--
-- Table structure for table `studentmgmts`
--

CREATE TABLE `studentmgmts` (
  `id` int(10) UNSIGNED NOT NULL,
  `admission_no` text COLLATE utf8_unicode_ci,
  `student_name` text COLLATE utf8_unicode_ci,
  `student_father_name` text COLLATE utf8_unicode_ci,
  `student_mother_name` text COLLATE utf8_unicode_ci,
  `student_class_section` text COLLATE utf8_unicode_ci,
  `student_address` text COLLATE utf8_unicode_ci,
  `student_joining_date` text COLLATE utf8_unicode_ci,
  `student_images` text COLLATE utf8_unicode_ci,
  `student_dob` text COLLATE utf8_unicode_ci,
  `student_gender` text COLLATE utf8_unicode_ci,
  `student_username` text COLLATE utf8_unicode_ci,
  `student_password` text COLLATE utf8_unicode_ci,
  `student_email` text COLLATE utf8_unicode_ci,
  `student_mob1` text COLLATE utf8_unicode_ci,
  `student_mob2` text COLLATE utf8_unicode_ci NOT NULL,
  `student_rollno` text COLLATE utf8_unicode_ci,
  `sessionid` text COLLATE utf8_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `code` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `studentmgmts`
--

INSERT INTO `studentmgmts` (`id`, `admission_no`, `student_name`, `student_father_name`, `student_mother_name`, `student_class_section`, `student_address`, `student_joining_date`, `student_images`, `student_dob`, `student_gender`, `student_username`, `student_password`, `student_email`, `student_mob1`, `student_mob2`, `student_rollno`, `sessionid`, `deleted_at`, `created_at`, `updated_at`, `code`) VALUES
(1, '1025', 'Aarav Saini', 'Sundar Lal', 'Deepa', 'I-B', 'Vill. Barkala, Post Chhutmalpur', '19-Jul-14', NULL, '14-Aug-11', 'M', '28', NULL, NULL, '9837550890', '9758749378', '1', '1', NULL, NULL, '2017-08-31 13:08:31', NULL),
(2, '979', 'Aastha Chaudhary', 'Prashant Chaudhary', 'Neelam Chaudhary', 'I-B', 'Vill-Alawalpur, Post-Chhutmalpur, Distt-Haridwar', '12-May-14', NULL, '14-Jun-11', 'F', '42', NULL, NULL, '9536684113', '9411424274', '2', '1', NULL, NULL, '2017-08-31 13:08:31', NULL),
(3, '1516', 'Aayush Saini', 'Dinesh Saini', 'Babita Saini', 'I-B', 'Amardeep Colony, Chhutmalpur, Distt-Saharanpur', '08-04-2017', NULL, '17-10-2010', 'M', '51', NULL, NULL, '9759053375', '7037201201', '3', '1', NULL, NULL, '2017-08-31 13:08:31', NULL),
(4, '1114', 'Abhinav', 'Sandeep Kumar', 'Sangeeta', 'I-A', 'Vill & Post-Halwana, Distt-SRE', '06-Apr-15', NULL, '', 'M', '70', NULL, NULL, '9759595062', '9627478988', '1', '1', NULL, NULL, '2017-08-31 11:28:19', NULL),
(5, '860', 'Abhinav Kamboj', 'Brijesh Kumar', 'Vandana Kamboj', 'I-B', 'Vill. Meerpur, Sherpur', '03-Apr-14', NULL, '30-Apr-11', 'F', '72', NULL, NULL, '9627055209', '9759077155', '4', '1', NULL, NULL, '2017-08-31 13:08:31', NULL),
(6, '909', 'Adhvika Bhardwaj', 'Dinesh Sharma', 'Sarita Sharma', 'I-B', 'Vill-Fatehpur Bhado, Shiv Colony, Chhutmalpur', '07-Apr-14', NULL, '23-Sep-11', 'F', '100', NULL, NULL, '9761414141', '9675634123', '5', '1', NULL, NULL, '2017-08-31 13:08:31', NULL),
(7, '570', 'Aditya ', 'Kuldeep Yadav', 'Soniya Devi', 'I-A', 'Khajuri Akbarpur', '25-Mar-13', NULL, '20-Aug-10', 'M', '107', NULL, NULL, '9761336113', '9719359915', '2', '1', NULL, NULL, '2017-08-31 11:28:19', NULL),
(8, '1034', 'Afsha', 'Afjaal', 'Khushnuma', 'I-A', 'Vill. Rehri, Post Chhutmalpur', '21-Aug-14', NULL, '02-Dec-09', 'F', '115', NULL, NULL, '8057110715', '8865982219', '3', '1', NULL, NULL, '2017-08-31 11:28:19', NULL),
(9, '816', 'Akshara', 'Brijesh Kumar', 'Padma', 'I-B', 'Vill. Cholli, Sahbuddipur, Distt. Haridwar', '31-Mar-14', NULL, '', 'F', '136', NULL, NULL, '7417070805', '9719190063', '6', '1', NULL, NULL, '2017-08-31 13:08:31', NULL),
(10, '1083', 'Akshat Dhiman', 'Vipin Dhiman', 'Poonam Dhiman', 'I-B', 'Gandhi Colony, Chhutmalpur, SRE', '01-Apr-15', NULL, '02-Aug-12', 'M', '140', NULL, NULL, '8954568201', '8954568203', '7', '1', NULL, NULL, '2017-08-31 13:08:31', NULL),
(11, '974', 'Alfiya Rao', 'Rao Mutayyab', 'Kharunisha', 'I-B', 'Vill. & Post Khujnawar', '06-May-14', NULL, '', 'F', '148', NULL, NULL, '9759363535', '9412016128', '8', '1', NULL, NULL, '2017-08-31 13:08:31', NULL),
(12, '831', 'Anant Kumar ', 'Geeta Ram Saini', 'Suman Lata', 'I-B', 'Kamaalpur Road, Shanti Nagar, Near Indian Culture Academy, Chhutmalpur', '31-Mar-14', NULL, '17-Sep-10', 'M', '181', NULL, NULL, '9759693790', '9675562612', '9', '1', NULL, NULL, '2017-08-31 13:08:31', NULL),
(13, '585', 'Anant Pahwa', 'Varun Pahwa', 'Shikha Pahwa', 'I-A', 'Punjabi Colony Chhutmalpur', '01-Apr-13', NULL, '14-Jan-11', 'M', '182', NULL, NULL, '9997153718', '9634656591', '4', '1', NULL, NULL, '2017-08-31 11:28:19', NULL),
(14, '820', 'Anant Sharma', 'Vipin Kumar Sharma', 'Ruby Sharma', 'I-B', 'Vill-Rehri, Post-Chhutmalpur, Distt-Saharanpur', '31-Mar-14', NULL, '02-Nov-10', 'M', '185', NULL, NULL, '9058870007', '9319297573', '10', '1', NULL, NULL, '2017-08-31 13:08:31', NULL),
(15, '1515', 'Ananya Saini', 'Neeraj Kumar', 'Deepa Saini', 'I-B', 'Amardeep Colony, Chhutmalpur, Distt-Saharanpur', '08-04-2017', NULL, '28-02-2011', 'F', '188', NULL, NULL, '9897632376', '9760370694', '11', '1', NULL, NULL, '2017-08-31 13:08:31', NULL),
(16, '981', 'Ansh Pundir ', 'Mudit Pundir', 'Anjali Pundir ', 'I-A', 'Vill-Mandawar, Post-Khubbanpur, Haridwar', '14-May-14', NULL, '29-Jun-11', 'M', '209', NULL, NULL, '9045111715', '956811008', '5', '1', NULL, NULL, '2017-08-31 11:28:19', NULL),
(17, '1349', 'Anshika', 'Dilawar Singh', 'Pooja', 'I-A', 'Dhara masala Factory, Near Nau gaja Peer, Saharanpur', '04-Apr-16', NULL, '', 'M', '215', NULL, NULL, '9012521830', '8193954095', '6', '1', NULL, NULL, '2017-08-31 11:28:20', NULL),
(18, '582', 'Anukalp Mittal', 'Shailendra Mittal', 'Seema Mittal', 'I-A', 'Chhutmalpur, Gandhi Colony', '30-Mar-13', NULL, '09-Dec-10', 'M', '233', NULL, NULL, '9319151659', '9837726062', '7', '1', NULL, NULL, '2017-08-31 11:28:20', NULL),
(19, '583', 'Anurag', 'Pramod Kumar', 'Sarita', 'I-A', 'Vill. Gandevara, Chhutmalpur', '01-Apr-13', NULL, '22-Nov-09', 'M', '235', NULL, NULL, '9759562195', '9927360848', '8', '1', NULL, NULL, '2017-08-31 11:28:20', NULL),
(20, '756', 'Anushka Saini', 'Pradeep Kumar', 'Soniya Saini', 'I-A', 'Kamalpur Road, Arya Nagar, Chhutmalpur', '19-Jul-13', NULL, '26-Feb-11', 'F', '241', NULL, NULL, '9412873567', '9410786054', '9', '1', NULL, NULL, '2017-08-31 11:28:20', NULL),
(21, '918', 'Anya', 'Aatish', 'Saloni', 'I-A', 'Punjabi Colony Chhutmalpur', '09-Apr-14', NULL, '02-Dec-11', 'F', '245', NULL, NULL, '9758007008', '9627251790', '10', '1', NULL, NULL, '2017-08-31 11:28:20', NULL),
(22, '1330', 'Arohi', 'Nitin Saini', 'Deepa Saini', 'I-B', 'Vill-Mandawar Post-Cholli, Distt-Haridwar', '05-Apr-16', NULL, '21-Oct-11', 'F', '267', NULL, NULL, '9411184911', '8439400100', '12', '1', NULL, NULL, '2017-08-31 13:08:31', NULL),
(23, '899', 'Arth Saini', 'Arvind Kumar Saini', 'Anju Saini', 'I-B', 'Vill-Alawalpur, Post-Chhutmalpur, Distt-Haridwar', '07-Apr-14', NULL, '28-Jun-10', 'M', '280', NULL, NULL, '9536909092', '9457370339', '13', '1', NULL, NULL, '2017-08-31 13:08:31', NULL),
(24, '911', 'Arush Kamboj', 'Neeraj Kumar', 'Ritu Kamboj', 'I-B', 'Rana Colony, Chhutmalpur', '09-Apr-14', NULL, '24-Nov-10', 'M', '282', NULL, NULL, '7830292930', '8171523156', '14', '1', NULL, NULL, '2017-08-31 13:08:32', NULL),
(25, '1369', 'Chetna', 'Praveen kumar', 'Sangeeta', 'I-A', 'Vill-Chamarikhera Distt-SRE', '09-Apr-16', NULL, '12-May-12', 'F', '367', NULL, NULL, '8859165022', '9761290639', '11', '1', NULL, NULL, '2017-08-31 11:28:20', NULL),
(26, '882', 'Dev Saini', 'Ashwani Saini', 'Gopi Saini', 'I-B', 'Vill-Alawalpur, Post-Chhutmalpur, Distt-Haridwar', '05-Apr-14', NULL, '24-11-10', 'M', '385', NULL, NULL, '9719870220', '9536069070', '15', '1', NULL, NULL, '2017-08-31 13:08:32', NULL),
(27, '999', 'Devansh Maan', 'Magan Singh', 'Nishu Maan', 'I-A', 'Near Gandhi  Ashram Punjabi colony Chhutmalpur', '02-Jul-14', NULL, '', 'M', '391', NULL, NULL, '9675949645', '', '12', '1', NULL, NULL, '2017-08-31 11:28:20', NULL),
(28, '687', 'Diya Khurana', 'Sachin Khurana', 'Kamiya Khurana', 'I-A', 'Punjabi Colony Chhutmalpur', '11-Apr-13', NULL, '11-Nov-10', 'F', '413', NULL, NULL, '9359597000', '8449184444', '13', '1', NULL, NULL, '2017-08-31 11:28:20', NULL),
(29, '752', 'Harek Jyot Singh', 'Herpal Singh', 'Balvinder Kaur', 'I-A', 'Harnam Singh Nagar, SRE Road, Chhutmalpur', '13-Jul-13', NULL, '15-Sep-10', 'M', '443', NULL, NULL, '9411079026', '', '14', '1', NULL, NULL, '2017-08-31 11:28:20', NULL),
(30, '765', 'Honey Saini', 'Rishi Saini', 'Seema Saini', 'I-B', 'Vill Badakla, Distt- Saharanpur', '28-Feb-14', NULL, '06-Jul-10', 'M', '480', NULL, NULL, '9758026200', '9927141872', '16', '1', NULL, NULL, '2017-08-31 13:08:32', NULL),
(31, '648', 'Jahnvi', 'Munish Kumar', 'Mamta', 'I-A', 'Mandawar, Post-Khubbannpur', '05-Apr-13', NULL, '06-Oct-10', 'F', '493', NULL, NULL, '9761246959', '', '15', '1', NULL, NULL, '2017-08-31 11:28:20', NULL),
(32, '647', 'Jasmin', 'Jogendra Singh', 'Sangeeta Devi', 'I-A', 'Mandawar, Post-Khubbannpur', '05-Apr-13', NULL, '11-Sep-10', 'F', '497', NULL, NULL, '9761246959', '', '16', '1', NULL, NULL, '2017-08-31 11:28:20', NULL),
(33, '879', 'Jessika Verma', 'Sandeep Kumar ', 'Anjali Verma', 'I-B', 'Amardeep Colony, Chhutmalpur', '04-Apr-14', NULL, '01-Apr-10', 'F', '504', NULL, NULL, '9897565782', '', '17', '1', NULL, NULL, '2017-08-31 13:08:32', NULL),
(34, '785', 'Kanishka Kamboj', 'Pardeep Kamboj', 'Aadesh Kamboj', 'I-B', 'Vill-Aurangabad, Post-Sherpur Khanazadpur, Distt-SRE', '18-Mar-14', NULL, '16-Feb-11', 'F', '517', NULL, NULL, '9720652719', '9536412277', '18', '1', NULL, NULL, '2017-08-31 13:08:32', NULL),
(35, '1029', 'Keshav Kumar', 'Pradeep', 'Baby', 'I-B', 'Vill. Rehri, Post Chhutmalpur', '22-Jul-14', NULL, '21-May-10', 'M', '527', NULL, NULL, '9720321627', '9759701755', '19', '1', NULL, NULL, '2017-08-31 13:08:32', NULL),
(36, '715', 'Latika Saini', 'Kulbhushan Saini', 'Deepa Saini', 'I-A', 'Vill. & Post Muzaffarabad', '23-Apr-13', NULL, '14-Mar-10', 'F', '569', NULL, NULL, '9719446546', '7830545670', '17', '1', NULL, NULL, '2017-08-31 11:28:20', NULL),
(37, '1010', 'Mahima', 'Avnish Kumar', 'Indra', 'I-A', 'Vill. Sahidwala Grant, Post Buggawala, Distt. Haridwar', '07-Jul-14', NULL, '28-Dec-10', 'F', '585', NULL, NULL, '9634131733', '9456130729', '18', '1', NULL, NULL, '2017-08-31 11:28:20', NULL),
(38, '881', 'Mahul Chaudhary', 'Ashok Kumar', 'Sanjeev Lata', 'I-B', 'Vill. Alawalpur', '05-Apr-14', NULL, '02-Aug-10', 'M', '586', NULL, NULL, '8057916621', '7830279044', '20', '1', NULL, NULL, '2017-08-31 13:08:32', NULL),
(39, '850', 'Manvi Chaudhary', 'Sohanveer Singh', 'Geeta Chaudhary', 'I-B', 'Vill. Alliwala Post Muzaffarabad', '02-Apr-14', NULL, '14-Jul-10', 'F', '603', NULL, NULL, '9917550028', '9759129632', '21', '1', NULL, NULL, '2017-08-31 13:08:32', NULL),
(40, '1268', 'Manya Singh', 'Ajay singh', 'Meenu Chaudhary', 'I-A', 'Vill-Alawalpur, post-Chhutmalpur, Distt-Haridwar', '22-Jul-15', NULL, '16-Nov-10', 'F', '609', NULL, NULL, '9466132234', '9050026354', '19', '1', NULL, NULL, '2017-08-31 11:28:20', NULL),
(41, '658', 'Mohd. Altamash', 'Mohd. Imran', 'Reshma', 'I-A', 'Muslim Colony, Chhutmalpur', '06-Apr-13', NULL, '20-Mar-10', 'M', '645', NULL, NULL, '9720650030', '', '20', '1', NULL, NULL, '2017-08-31 11:28:20', NULL),
(42, '1026', 'Mohd. Aqdas', 'Rashid', 'Aarfa', 'I-A', 'Vill. & Post Sherpur, Khanazadpur', '19-Jul-14', NULL, '06-Apr-10', 'M', '648', NULL, NULL, '9837583633', '9719961124', '21', '1', NULL, NULL, '2017-08-31 11:28:20', NULL),
(43, '1095', 'Mohd. Ibraheem', 'Mohd. Javed', 'Ishrat', 'I-A', 'Vill-Mathana, Post-Chhutmalpur, Distt-SRE', '02-Apr-15', NULL, '27-Nov-10', 'M', '658', NULL, NULL, '9758909358', '9720504146', '22', '1', NULL, NULL, '2017-08-31 11:28:20', NULL),
(44, '952', 'Nabiya Ali', 'Tahir Ali', 'Rubina Ali', 'I-A', 'Dehradun Road, Biharigarh', '21-Apr-14', NULL, '22-Jul-10', 'F', '684', NULL, NULL, '9675194409', '9058771236', '23', '1', NULL, NULL, '2017-08-31 11:28:20', NULL),
(45, '1556', 'Nandini Chauhan', 'Satender Chauhan', 'Monika Chauhan', 'I-B', 'Vill-Khubbanpur, Post-Bhagwanpur, Distt-Haridwar', '12-04-2017', NULL, '22-11-2010', 'F', '690', NULL, NULL, '9717743005', '9837464596', '22', '1', NULL, NULL, '2017-08-31 13:08:32', NULL),
(46, '564', 'Navya Pahuja', 'Sunil Kumar Pahuja', 'Nidhi Pahuja', 'I-A', 'Punjabi Colony Chhutmalpur', '13-Mar-13', NULL, '14-Sep-10', 'F', '698', NULL, NULL, '9927126426', '9927151033', '24', '1', NULL, NULL, '2017-08-31 11:28:20', NULL),
(47, '1124', 'Pari', 'Rupendra Saini', 'Upasna Devi', 'I-B', 'Vill-Mirzapur Poul, Post-Mirzapur, Distt-SRE', '06-Apr-15', NULL, '', 'F', '720', NULL, NULL, '9761231986', '8954775516', '23', '1', NULL, NULL, '2017-08-31 13:08:32', NULL),
(48, '1521', 'Priyanka Chauhan', 'Padam Singh', 'Babli', 'I-B', 'Vill-Khubbanpur, Post-Bhagwanpur, Distt-Haridwar', '08-04-2017', NULL, '05-04-2017', 'F', '750', NULL, NULL, '8393063078', '9536286359', '24', '1', NULL, NULL, '2017-08-31 13:08:32', NULL),
(49, '760', 'Rao Junaid', 'Rao Shameem Ahmad', 'Sahiba Rao', 'I-A', 'Vill. Gudam, Post Chhutmalpur', '29-Jul-13', NULL, '21-Dec-10', 'M', '787', NULL, NULL, '9927091670', '', '25', '1', NULL, NULL, '2017-08-31 11:28:21', NULL),
(50, '1001', 'Reeda Rao', 'Shafat Ali', 'Foziya Rao', 'I-B', 'Vill. & Post Kheri Shikohpur, Haridwar', '03-Jul-14', NULL, '10-Jan-11', 'F', '793', NULL, NULL, '9058787891', '9758651515', '25', '1', NULL, NULL, '2017-08-31 13:08:32', NULL),
(51, '766', 'Ronik Kumar', 'Jasveer Singh', 'Nisha Devi', 'I-B', 'Vill-Rehri, Post-Chhutmalpur, Distt-Saharanpur', '28-Feb-14', NULL, '05-Feb-10', 'M', '816', NULL, NULL, '9758866746', '9719166047', '26', '1', NULL, NULL, '2017-08-31 13:08:32', NULL),
(52, '819', 'Sameeksha Kamboj', 'Amit Kamboj', 'Neelam Kamboj', 'I-B', 'Vill. Aurangabad, Post Sherpur', '31-Mar-14', NULL, '07-Dec-10', 'F', '847', NULL, NULL, '9759544820', '9720058343', '27', '1', NULL, NULL, '2017-08-31 13:08:32', NULL),
(53, '1475', 'Sara Khan', 'Nadeem Ahmad Khan', 'Lubna', 'I-B', 'Bharat Dharam Kanta Saharanpur road', '02-Sep-16', NULL, '31-Jul-09', 'F', '861', NULL, NULL, '9997005658', '8394813590', '28', '1', NULL, NULL, '2017-08-31 13:08:32', NULL),
(54, '1366', 'Saurabh Pal', 'Sandeep Kumar', 'Babli', 'I-A', 'Gagalheri, D.dun Road, Distt-SRE', '09-Apr-16', NULL, '06-Aug-11', 'M', '873', NULL, NULL, '9690262681', '', '26', '1', NULL, NULL, '2017-08-31 11:28:21', NULL),
(55, '787', 'Shivang Bhardwaj', 'Kamal Kishore Sharma', 'Suman lata Sharma', 'I-B', 'Vill & Post Khubbanpur, Distt-Haridwar', '19-Mar-14', NULL, '02-Jan-11', 'M', '903', NULL, NULL, '9761336927', '9759434934', '29', '1', NULL, NULL, '2017-08-31 13:08:32', NULL),
(56, '665', 'Shruti Kamboj', 'Amrish Kumar', 'Rashi Kamboj', 'I-A', 'Vill. Sherpur Khanazadpur, Distt. Saharanpur', '08-Apr-13', NULL, '15-Mar-10', 'F', '915', NULL, NULL, '9761920631', '9458882958', '27', '1', NULL, NULL, '2017-08-31 11:28:21', NULL),
(57, '973', 'Suhani', 'Arvind Kumar', 'Ravita', 'I-B', 'Vill. & Post Halwana', '05-May-14', NULL, '09-Sep-10', 'F', '946', NULL, NULL, '9917073583', '8057613990', '30', '1', NULL, NULL, '2017-08-31 13:08:32', NULL),
(58, '600', 'Suryansh Kamboj', 'Amit Kumar', 'Rachna', 'I-A', 'Auranzebpur, Post-Biharigarh', '02-Apr-13', NULL, '22-Oct-10', 'M', '956', NULL, NULL, '9719447331', '9758694895', '28', '1', NULL, NULL, '2017-08-31 11:28:21', NULL),
(59, '1348', 'Tanishka', 'Parveen Kumar (Late)', 'Aarti Pundir', 'I-A', 'Dhara masala Factory, Near Nau gaja Peer, Saharanpur', '04-Apr-16', NULL, '', 'F', '965', NULL, NULL, '9012521830', '8193954095', '29', '1', NULL, NULL, '2017-08-31 11:28:21', NULL),
(60, '983', 'Tanishq Chawariya', 'Anil Kumar', 'Pinky ', 'I-B', 'Kalsiya Raod, Fatehpur Bhado, Post-Chhutmalpur', '15-May-14', NULL, '23-Jun-10', 'M', '966', NULL, NULL, '9758056973', '9760961282', '31', '1', NULL, NULL, '2017-08-31 13:08:32', NULL),
(61, '788', 'Tejas Kapil', 'Ravinder Kapil', 'Sharda Kapil', 'I-B', 'Vill Fatehpur Bhado, Near Police Station, Distt-Saharanpur', '19-Mar-14', NULL, '04-Nov-11', 'M', '975', NULL, NULL, '9719241046', '8869042485', '32', '1', NULL, NULL, '2017-08-31 13:08:32', NULL),
(62, '1175', 'Yashika Pundir', 'Shiv Sagar Pundir', 'Kavita Pundir', 'I-A', 'Vill & Post- Bahera Sandal Singh, Distt-SRE', '08-Apr-15', NULL, '12-Oct-10', 'F', '1068', NULL, NULL, '7500800468', '9456293442', '30', '1', NULL, NULL, '2017-08-31 11:28:21', NULL),
(63, '562', 'Aanya Singh', 'Shekher Singh', 'Ritu Singh', 'II-B', 'Alawalpur,Chhutmalpur', '23-Jan-13', NULL, '29-Dec-09', 'F', '19', NULL, NULL, '7409775403', '9917631925', '1', '1', NULL, NULL, '2017-09-01 08:02:58', NULL),
(64, '1200', 'Aaquib Chaudhary', 'Shakil Chaudhary', 'Firdous', 'II-B', 'Roorkee Road, Opp. PNB, Chhutmalpur,Distt-SRE', '15-Apr-15', NULL, '05-Feb-09', 'M', '20', NULL, NULL, '8439048586', '9045683719', '2', '1', NULL, NULL, '2017-09-01 08:02:58', NULL),
(65, '714', 'Aarav Goyal', 'Sunny Goyal', 'Vandana Goyal', 'II-A', 'Dehradun Road, Chhutmalpur', '23-Apr-13', NULL, '10-Jun-10', 'M', '23', NULL, NULL, '9927234275', '9837618343', '1', '1', NULL, NULL, '2017-09-01 08:02:50', NULL),
(66, '591', 'Aarav Saini', 'Yogesh Saini', 'Sapna Saini', 'II-A', 'Vijay Beej Bhandar, Buggawala Road, Biharigarh', '01-Apr-13', NULL, '04-Nov-09', 'M', '25', NULL, NULL, '9719654259', '7830199248', '2', '1', NULL, NULL, '2017-09-01 08:02:50', NULL),
(67, '587', 'Aarush Chaudhary', 'Manoj Kumar', 'Meenakshi', 'II-B', 'Vill-Chaura Khurd, Post Gagalheri', '01-Apr-13', NULL, '25-Mar-10', 'M', '32', NULL, NULL, '9759494306', '9759878056', '3', '1', NULL, NULL, '2017-09-01 08:02:58', NULL),
(68, '369', 'Aarv Pal', 'Devender Pal ', 'Laxmi Pal', 'II-A', 'Vill Gandewara Post Chhutamalpur', '03-Apr-12', NULL, '07-Jun-10', 'M', '35', NULL, NULL, '9927957907', '', '3', '1', NULL, NULL, '2017-09-01 08:02:50', NULL),
(69, '862', 'Abdul Musheeb', 'Abdul Aleem', 'Gulista Parveen', 'II-B', 'Vill-Kheri Shikohpur, Post-Khas, Haridwar', '03-Apr-14', NULL, '27-Nov-09', 'M', '58', NULL, NULL, '9720484703', '9837488947', '4', '1', NULL, NULL, '2017-09-01 08:02:58', NULL),
(70, '1283', 'Abhigyan Bieber', 'Surender Kumar', 'Sunita', 'II-B', 'Jain Bagh, Chhutmalpur, Distt-SRE', '16-Mar-16', NULL, '14-08-2011', 'M', '63', NULL, NULL, '9759627498', '9359207270', '5', '1', NULL, NULL, '2017-09-01 08:02:58', NULL),
(71, '907', 'Abhiman Saini', 'Naveen Saini', 'Luxmi Saini', 'II-B', 'Vill. & Post Muzaffarabad', '07-Apr-14', NULL, '', 'M', '67', NULL, NULL, '9639141915', '9675949400', '6', '1', NULL, NULL, '2017-09-01 08:02:58', NULL),
(72, '626', 'Abhinav Saini', 'Amit Kumar saini', 'Anjali Saini', 'II-A', 'Roorkee Road, Chhutmalpur', '03-Apr-13', NULL, '14-Nov-09', 'M', '74', NULL, NULL, '9719447321', '8859825004', '4', '1', NULL, NULL, '2017-09-01 08:02:50', NULL),
(73, '894', 'Abhinav Saini', 'Arjun Saini', 'Seema Saini', 'II-B', 'Suraj Vihar Colony,Chhutmalpur', '07-Apr-14', NULL, '02-Dec-09', 'M', '75', NULL, NULL, '9897325801', '', '7', '1', NULL, NULL, '2017-09-01 08:02:58', NULL),
(74, '1344', 'Abhinav Saini', 'Aditya Saini', 'Vinita Singh', 'II-B', 'Bansal Vihar Colony, D.dun Road, Chhutmalpur, Distt-SRE', '06-Apr-16', NULL, '01-Dec-10', 'M', '76', NULL, NULL, '9719567198', '9536687092', '8', '1', NULL, NULL, '2017-09-01 08:02:58', NULL),
(75, '555', 'Abhiraj kumar', 'Kuldeep kumar', 'Pinki', 'II-A', 'VILL-FATEHPUR', '09-Aug-12', NULL, '06-Jan-11', 'M', '85', NULL, NULL, '9759556644', '8057554466', '5', '1', NULL, NULL, '2017-09-01 08:02:50', NULL),
(76, '466', 'Afshan Ahmed', 'Rao Zeeshan', 'Faheem Zeeshan', 'II-A', 'Near Bharat Dharam Kanta, Haqeem Shekhupur Street, Dehadun Road, Chhutmalpur', '19-Apr-12', NULL, '16-Nov-09', 'M', '116', NULL, NULL, '9761868666', '8410310592', '6', '1', NULL, NULL, '2017-09-01 08:02:50', NULL),
(77, '516', 'Aish', 'Amit Kumar', 'Isha', 'II-A', 'Punjabi Colony, Chhutmalpur', '03-Jul-12', NULL, '11-Mar-10', 'F', '120', NULL, NULL, '9758408051', '8869047110', '7', '1', NULL, NULL, '2017-09-01 08:02:50', NULL),
(78, '373', 'Aksh Jain', 'Mohit Jain', 'Neha Jain', 'II-A', 'Chhutmalpur, Gandhi Colony', '03-Apr-12', NULL, '22-Jul-09', 'M', '133', NULL, NULL, '8439495984', '9259405315', '8', '1', NULL, NULL, '2017-09-01 08:02:50', NULL),
(79, '512', 'Akshaj Sharma', 'Tarang Sharma', 'Asha Dhiman', 'II-A', 'Chhutmalpur, Near Vaidik School, Saharanpur Road ', '28-Jun-12', NULL, '17-Jan-10', 'M', '135', NULL, NULL, '7500894448', '7830998641', '9', '1', NULL, NULL, '2017-09-01 08:02:50', NULL),
(80, '563', 'Akshara Chauhan', 'Abhishek Chauhan', 'Soniya Chauhan', 'II-A', 'Vill-Khubbanpur', '22-Feb-13', NULL, '24-Feb-10', 'F', '137', NULL, NULL, '9719672257', '8954818680', '10', '1', NULL, NULL, '2017-09-01 08:02:51', NULL),
(81, '606', 'Akshika Kamboj', 'Anuj Kamboj', 'Ranjeeta Kamboj', 'II-A', 'Chanchak, Post.-Sherpur', '02-Apr-13', NULL, '27-Sep-10', 'F', '142', NULL, NULL, '9457048049', '9627977740', '11', '1', NULL, NULL, '2017-09-01 08:02:51', NULL),
(82, '1385', 'Alkamar', 'Mohd. Wajid', 'Husna Malik', 'II-B', 'H. no.-580 Muzaffarabad, Distt-SRE', '11-Apr-16', NULL, '23-Jan-10', 'F', '158', NULL, NULL, '9675335996', '9761742060', '9', '1', NULL, NULL, '2017-09-01 08:02:58', NULL),
(83, '933', 'Anirudh Rana', 'Rajender Singh', 'Sushila Rani', 'II-B', 'Vill & Post-Badhedi Ghoghu, Distt-SRE', '16-Apr-14', NULL, '22-Aug-10', 'M', '194', NULL, NULL, '9756119511', '7500852870', '10', '1', NULL, NULL, '2017-09-01 08:02:58', NULL),
(84, '556', 'Ansh Dhingra', 'Alok Dhingra', 'Rozy dhingra', 'II-B', 'Chhutmalpur', '14-Aug-12', NULL, '16-Apr-10', 'M', '206', NULL, NULL, '9927173371', '9548521555', '11', '1', NULL, NULL, '2017-09-01 08:02:58', NULL),
(85, '829', 'Ansh Kaushik', 'Shrikant Sharma', 'Rakhi Sharma (Late)', 'II-B', 'Hind Vihar Colony, Chhutmalpur', '31-Mar-14', NULL, '27-Oct-10', 'M', '208', NULL, NULL, '9536912345', '', '12', '1', NULL, NULL, '2017-09-01 08:02:58', NULL),
(86, '1492', 'Anshi Saini', 'Sahadev Singh', 'Reeta Saini', 'II-A', 'Suraj Vihar Colony, Chhutmalpur, Distt-Saharanpur', '31-03-2017', NULL, '11-09-2011', 'F', '214', NULL, NULL, '9897325801', '9719325801', '12', '1', NULL, NULL, '2017-09-01 08:02:51', NULL),
(87, '1234', 'Anshul Gautam', 'Pradeep Gautam', 'Bala Devi', 'II-A', 'Vill-Alawalpur, Post-Chhutmalpur, Distt-Haridwar', '06-May-15', NULL, '21-Sep-09', 'M', '225', NULL, NULL, '7500004066', '8859046610', '13', '1', NULL, NULL, '2017-09-01 08:02:51', NULL),
(88, '1017', 'Aradhya Laman', 'Sunil Kumar Laman', 'Anita Laman', 'II-B', 'Vill-Manduwala, Post-Khujnawar, Distt-SRE', '14-Jul-14', NULL, '14-Apr-10', 'F', '251', NULL, NULL, '9720005020', '9639869477', '13', '1', NULL, NULL, '2017-09-01 08:02:58', NULL),
(89, '1563', 'Arham', 'Julfikar', 'Shaheen Jahan', 'II-B', 'Vill-Mukarrampur Urf Kalewala, Chhutmalpur, Distt-Saharanpyur', '04-04-2017', NULL, '05-09-2010', 'M', '255', NULL, NULL, '9411077562', '9456294521', '14', '1', NULL, NULL, '2017-09-01 08:02:58', NULL),
(90, '697', 'Ariba Rao', 'Ragib Ali', 'Reshma Rao', 'II-A', 'Vill. Sikanderpur Bhainswal, Bhagwanpur', '15-Apr-13', NULL, '11-Aug-09', 'F', '256', NULL, NULL, '9759077810', '8393820399', '14', '1', NULL, NULL, '2017-09-01 08:02:51', NULL),
(91, '604', 'Arpit Kumar', 'Lalit Kumar', 'Asha Devi', 'II-A', 'Budhakhera Pundir, Chamarikhera', '02-Apr-13', NULL, '27-Aug-09', 'M', '270', NULL, NULL, '9720512341', '', '15', '1', NULL, NULL, '2017-09-01 08:02:51', NULL),
(92, '403', 'Arpit Singh', 'Jasvinder Singh', 'Priya', 'II-A', 'Vill-Alawalpur', '09-Apr-12', NULL, '16-Jan-10', 'M', '277', NULL, NULL, '9756657238', '8192044705', '16', '1', NULL, NULL, '2017-09-01 08:02:51', NULL),
(93, '807', 'Asmith chauhan ', 'Reshu chauhan ', 'Kamini Chauhan', 'II-B', 'Vill. & post khubbanpur', '26-Mar-14', NULL, '04-Dec-10', 'M', '307', NULL, NULL, '9627911246', '9627405401', '15', '1', NULL, NULL, '2017-09-01 08:02:59', NULL),
(94, '1433', 'Insha Rao', 'Rao Mannan', 'Shahjan', 'II-B', 'Chhutmalpur', '21-Mar-15', NULL, '25-07-2001', 'M', '338', NULL, NULL, '9837415425', '0132-23206797', '16', '1', NULL, NULL, '2017-09-09 03:11:27', NULL),
(95, '738', 'Dev Dhiman', 'Amit Kumar Dhiman', 'Manisha Dhiman', 'II-B', 'Thapul Road, Biharigarh', '01-Jul-13', NULL, '10-Jan-10', 'M', '379', NULL, NULL, '9412557168', '9720122210', '17', '1', NULL, NULL, '2017-09-01 08:02:59', NULL),
(96, '729', 'Dev Yadav', 'Pankaj Yadav', 'Vimlesh Yadav', 'II-A', 'Vill. Mandebas, Post Harora', '10-May-13', NULL, '03-07-2010', 'M', '386', NULL, NULL, '9720769611', '9797836828', '17', '1', NULL, NULL, '2017-09-01 08:02:51', NULL),
(97, '554', 'Dhruv Kamboj', 'Digvijay Singh ', 'DEEPA KAMBOJ', 'II-A', 'VILL-SHERPUR', '31-Jul-12', NULL, '08-Sep-10', 'M', '398', NULL, NULL, '9627053753', '9720256204', '18', '1', NULL, NULL, '2017-09-01 08:02:51', NULL),
(98, '747', 'Divya Saini', 'Sanjay Saini', 'Manoj Kumari', 'II-A', 'Vill. Shekhowala Takipur, Post Sherpur', '08-Jul-13', NULL, '21-Mar-09', 'F', '409', NULL, NULL, '9837512018', '9690142911', '19', '1', NULL, NULL, '2017-09-01 08:02:51', NULL),
(99, '1043', 'Hardik Kamboj', 'Umesh Kumar', 'Rachna Devi', 'II-A', 'Vill- Aurangabad, Post-Sherpur Khanazadpur, Distt-SRE', '10-Mar-15', NULL, '08-Nov-09', 'M', '442', NULL, NULL, '9719562294', '9758610008', '20', '1', NULL, NULL, '2017-09-01 08:02:51', NULL),
(100, '374', 'Harsh Pal', 'Kuldeep Pal', 'Kavita Pal', 'II-A', 'Vill.Gagaleheri', '04-Apr-12', NULL, '21-Nov-08', 'M', '450', NULL, NULL, '9720509435', '9761801356', '21', '1', NULL, NULL, '2017-09-01 08:02:51', NULL),
(101, '599', 'Hiba Rao', 'Abdul Kadir', 'Rahil', 'II-A', 'Near Union Bank, Dehradun Road, Chhutmalpur', '02-Apr-13', NULL, '06-Sep-09', 'F', '468', NULL, NULL, '9897516130', '', '22', '1', NULL, NULL, '2017-09-01 08:02:51', NULL),
(102, '321', 'Jhalak Khurana', 'Sandeep Gaurav Khurana', 'Radha Khurana', 'II-A', 'Punjabi Colony, Chhutmalpur', '19-Mar-12', NULL, '03-Oct-09', 'F', '506', NULL, NULL, '9837602131', '7417509811', '23', '1', NULL, NULL, '2017-09-01 08:02:51', NULL),
(103, '445', 'Kirti Saini', 'Dr. Mukesh Saini', 'Neeta Saini', 'II-A', 'Satish Colony, Behind Punjab & Sindh Bank, Vill & Post Gagalheri, Distt. Saharanpur', '14-Apr-12', NULL, '28-Aug-09', 'F', '540', NULL, NULL, '9528186584', '7669440633', '24', '1', NULL, NULL, '2017-09-01 08:02:51', NULL),
(104, '361', 'Krishna Pahwa', 'Sachin Pahwa', 'Monika Pahwa', 'II-A', 'Punjabi Colony, Chhutmalpur', '02-Apr-12', NULL, '27-Nov-09', 'M', '548', NULL, NULL, '8979201200', '9761010976', '25', '1', NULL, NULL, '2017-09-01 08:02:51', NULL),
(105, '753', 'Lavi Rana', 'Sushil Rana', 'Kavita Rana', 'II-B', 'Vill. Sunderpur, Biharigarh', '15-Jul-13', NULL, '16-Jul-10', 'F', '572', NULL, NULL, '9761690122', '', '18', '1', NULL, NULL, '2017-09-01 08:02:59', NULL),
(106, '718', 'Lavishna', 'Sajid Ali', 'Gulishta', 'II-B', 'Vill. Cholli Shahabuddinpur', '29-Apr-13', NULL, '02-Feb-10', 'F', '575', NULL, NULL, '9808281111', '8273871114', '19', '1', NULL, NULL, '2017-09-01 08:02:59', NULL),
(107, '1244', 'Mohd. Asjad', 'Mohd. Aslam', 'Shabana', 'II-B', 'Vill & Post-Bahera Khurd, Distt-SRE', '14-May-15', NULL, '', 'M', '651', NULL, NULL, '9411484424', '9997503535', '20', '1', NULL, NULL, '2017-09-01 08:02:59', NULL),
(108, '350', 'Mohd. Umar', 'Abdul Raouf', 'Shama Parveen', 'II-A', 'Vill Mathana, Post Chhutmalpur, Distt Saharanpur', '31-Mar-12', NULL, '20-Jan-09', 'M', '665', NULL, NULL, '9759006006', '9758703090', '26', '1', NULL, NULL, '2017-09-01 08:02:51', NULL),
(109, '722', 'Mohd. Uvaish', 'Mohd. Irshad', 'Rashida', 'II-B', 'Vill. & Post Gagalheri', '30-Apr-13', NULL, '15-Mar-09', 'M', '667', NULL, NULL, '9897495286', '8439064888', '21', '1', NULL, NULL, '2017-09-01 08:02:59', NULL),
(110, '740', 'Noor Mohammad', 'Sanawwar Ahmed', 'Rehmani', 'II-B', 'Vill. Sayed Mazra, Post Harora', '04-Jul-13', NULL, '14-Apr-09', 'M', '714', NULL, NULL, '9927824275', '9720884778', '22', '1', NULL, NULL, '2017-09-01 08:02:59', NULL),
(111, '1565', 'Payal', 'Sonu', 'Rajnish', 'II-B', 'Vill-Buddhakhera Pundir, Post-Chmarikhera, distt-Saharanpur', '14-04-2017', NULL, '', 'F', '724', NULL, NULL, '8864986429', '9759979141', '23', '1', NULL, NULL, '2017-09-01 08:02:59', NULL),
(112, '628', 'Prachi Rawat', 'Pushpender Singh Rawat', 'Umesh Rawat', 'II-B', 'Vill. Pathlokar, Post Nyamatpur', '03-Apr-13', NULL, '12-Nov-09', 'F', '730', NULL, NULL, '9720228836', '', '24', '1', NULL, NULL, '2017-09-01 08:02:59', NULL),
(113, '680', 'Pratigya Saini', 'Sandeep Saini', 'Mamta Saini', 'II-A', 'Vill. Fatehpur', '10-Apr-13', NULL, '20-Jun-10', 'F', '741', NULL, NULL, '9927140838', '', '27', '1', NULL, NULL, '2017-09-01 08:02:51', NULL),
(114, '490', 'Preet Kumar', 'Lalit Kumar', 'Asha', 'II-A', 'Budha Khera Pundir, Post Chamarikhera', '07-May-12', NULL, '08-Oct-09', 'M', '743', NULL, NULL, '9720512341', '', '28', '1', NULL, NULL, '2017-09-01 08:02:51', NULL),
(115, '685', 'Rao Ashad', 'Rao Sajid', 'Nazma', 'II-A', 'Rao Market, Chhutmalpur', '11-Apr-13', NULL, 'NA', 'M', '777', NULL, NULL, '8868952998', '', '29', '1', NULL, NULL, '2017-09-01 08:02:51', NULL),
(116, '588', 'Ria Saini', 'Anup Kumar', 'Annu Saini', 'II-A', 'Mandawar, Post.-Chhutmapur', '01-Apr-13', NULL, '03-Mar-09', 'F', '796', NULL, NULL, '9837577038', '', '30', '1', NULL, NULL, '2017-09-01 08:02:51', NULL),
(117, '428', 'Rishu Saini', 'Arvind Saini', 'Anju Saini', 'II-A', 'Alawalpur, Post Chhutmalpur', '11-Apr-12', NULL, '08-Feb-09', 'M', '798', NULL, NULL, '8474991224', '9536909092', '31', '1', NULL, NULL, '2017-09-01 08:02:51', NULL),
(118, '789', 'Rudransh Choudhary', 'Yograj Singh', 'Neetu', 'II-B', 'Vill-Abha, Post-Chudiala, Distt-SRE', '19-Mar-14', NULL, '22-Sep-10', 'M', '817', NULL, NULL, '9761476094', '9720652745', '25', '1', NULL, NULL, '2017-09-01 08:02:59', NULL),
(119, '454', 'Saad Mohd', 'Ishtyaq Ali', 'Khalida Begam', 'II-A', 'Vill & Post Khujnawar', '16-Apr-12', NULL, '29-Aug-09', 'M', '818', NULL, NULL, '9719106522', '7409188471', '32', '1', NULL, NULL, '2017-09-01 08:02:51', NULL),
(120, '1347', 'Sagar Pundir', 'Parveen Kumar (Late)', 'Aarti Pundir', 'II-B', 'Dhara masala Factory, Near Nau gaja Peer, Saharanpur', '04-Apr-16', NULL, '', 'M', '825', NULL, NULL, '9012521830', '8193954095', '26', '1', NULL, NULL, '2017-09-01 08:02:59', NULL),
(121, '767', 'Saim Ansari', 'Naim Ansari', 'Huma Ansari', 'II-B', 'Harijan Colony,Chhutmalpur', '28-Feb-14', NULL, '28-Sep-10', 'M', '832', NULL, NULL, '9897723429', '9358090058', '27', '1', NULL, NULL, '2017-09-01 08:02:59', NULL),
(122, '533', 'Sarthak Kamboj', 'Anil Kamboj', 'Poonam Kamboj', 'II-A', 'Fatehpur, Near Nari  Niketan Uddhar Kendra', '11-Jul-12', NULL, '20-Dec-09', 'M', '866', NULL, NULL, '9927172474', '', '33', '1', NULL, NULL, '2017-09-01 08:02:51', NULL),
(123, '874', 'Sarthak Karnwal', 'Mangheram Karnwal', 'Suman Karnwal', 'II-A', 'Vill. Gangali, Post Chhutmalpur', '04-Apr-14', NULL, '27-Feb-09', 'M', '867', NULL, NULL, '9469239516', '9756657531', '34', '1', NULL, NULL, '2017-09-01 08:02:51', NULL),
(124, '739', 'Sheefa Rao', 'Rao Shafat', 'Fozia', 'II-B', 'Vill. Kheri Shikohpur, Distt. Haridwar', '02-Jul-13', NULL, '23-Apr-09', 'F', '886', NULL, NULL, '7520221208', '', '28', '1', NULL, NULL, '2017-09-01 08:02:59', NULL),
(125, '638', 'Shiv Ansh', 'Harbeer Singh', 'Deepa', 'II-A', 'Vill. Chaura Khurd, Post Chauradev', '03-Apr-13', NULL, '12-Feb-10', 'M', '889', NULL, NULL, '9761584828', '9720443464', '35', '1', NULL, NULL, '2017-09-01 08:02:51', NULL),
(126, '421', 'Shivam Kumar Saini', 'Sunil Kumar Saini', 'Manju Devi', 'II-B', 'Fatehpur, Chhutmalpur', '10-Apr-12', NULL, '19-May-09', 'M', '898', NULL, NULL, '9927919617', '9837648387', '29', '1', NULL, NULL, '2017-09-01 08:02:59', NULL),
(127, '565', 'Shourya Aggarwal', 'Amit Aggarwal', 'Kavita Aggarwal', 'II-A', 'Chhutmalpur, Kamalpur Road', '18-Mar-13', NULL, '07-Jan-10', 'M', '907', NULL, NULL, '9412650471', '8171509091', '36', '1', NULL, NULL, '2017-09-01 08:02:51', NULL),
(128, '558', 'Shourya Garg', 'Mr. Jitender Gupta', 'Mrs. Laxmi Gupta', 'II-B', 'Chhutmalpur', '07-Sep-12', NULL, '27-Oct-09', 'M', '910', NULL, NULL, '9759260537', '', '30', '1', NULL, NULL, '2017-09-01 08:02:59', NULL),
(129, '608', 'Shourya Kamboj', 'Umesh Kamboj', 'Areena Kamboj', 'II-A', 'Chanchak, Post.-Sherpur', '02-Apr-13', NULL, '24-Jan-11', 'M', '911', NULL, NULL, '9456271319', '', '37', '1', NULL, NULL, '2017-09-01 08:02:51', NULL),
(130, '1384', 'Sofia', 'Mohd. Ayub Ansari', 'Anwari', 'II-B', 'Vill-Harora, Distt-SRE', '11-Apr-16', NULL, '09-Jun-09', 'F', '934', NULL, NULL, '9756496326', '', '31', '1', NULL, NULL, '2017-09-01 08:02:59', NULL),
(131, '548', 'Suhasi Kamboj', 'Anil Kamboj', 'Suman Kamboj', 'II-B', 'Vill Takipur, Post Sherpur Khanaazadpur', '21-Jul-12', NULL, '13-Mar-10', 'F', '948', NULL, NULL, '9720557568', '8650003531', '32', '1', NULL, NULL, '2017-09-01 08:02:59', NULL),
(132, '1281', 'Urvi Chaudhary', 'Amit Kumar Chaudhary', 'Rishu', 'II-A', 'Arya Nagar, Chhutmalpur, Distt-SRE', '16-Mar-16', NULL, '27-Jul-09', 'F', '990', NULL, NULL, '9837547398', '9690181198', '38', '1', NULL, NULL, '2017-09-01 08:02:51', NULL),
(133, '962', 'Usman Gani', 'Mukeem Ahmad', 'Farzana Parveen', 'II-B', 'D.Dun Road, Near Bharat Dharm Kanta, Chhutmalpur', '25-Apr-14', NULL, '', 'M', '991', NULL, NULL, '9917870970', '7500433732', '33', '1', NULL, NULL, '2017-09-01 08:02:59', NULL),
(134, '349', 'Uzair Rana', 'Israr Rana', 'Afsha Rana', 'II-B', 'Vill Gandewara Post Chhutamalpur', '31-Mar-12', NULL, '25-Aug-08', 'M', '994', NULL, NULL, '9997999555', '8449755585', '34', '1', NULL, NULL, '2017-09-01 08:02:59', NULL),
(135, '615', 'Vansh Parashar', 'Neeraj Sharma', 'Suman Sharma', 'II-B', 'Near PNB, D.Dun Road, Chhutmalpur', '02-Apr-13', NULL, '03-Sep-10', 'M', '1016', NULL, NULL, '9760083829', '', '35', '1', NULL, NULL, '2017-09-01 08:02:59', NULL),
(136, '610', 'Vansh Saini', 'Sunil Saini', 'Poonam Saini', 'II-B', 'Sona Sayed Majra', '02-Apr-13', NULL, '25-Mar-10', 'M', '1018', NULL, NULL, '9719474450', '', '36', '1', NULL, NULL, '2017-09-01 08:02:59', NULL),
(137, '730', 'Vedansh Chaudhary', 'Chaudhary Raj Singh', 'Ranjeeta', 'II-B', 'Vill. Abha, Post Chudiala, Distt. SRE', '10-May-13', NULL, '01-Mar-10', 'M', '1038', NULL, NULL, '9758723509', '9627896562', '37', '1', NULL, NULL, '2017-09-01 08:02:59', NULL),
(138, '1012', 'Vishesh Kumar', 'Rajan Kumar', 'Poonam Devi', 'II-B', 'Vill. Rajapur, Post Chhutmalpur', '09-Jul-14', NULL, '', 'M', '1058', NULL, NULL, '9557916824', '', '38', '1', NULL, NULL, '2017-09-01 08:02:59', NULL),
(139, '553', 'Yuvraj Chandna', 'Pawan Chandna', 'Reena Chandna', 'II-A', '17, opp Anandpur Kutia, Punjabi Colony, Chhutmalpur', '24-Jul-12', NULL, '16-Jan-10', 'M', '1069', NULL, NULL, '9808593993', '8909961227', '39', '1', NULL, NULL, '2017-09-01 08:02:51', NULL),
(140, '277', 'Aarohi Arora', 'Vinay Arora', 'Ruby Arora', 'III-A', 'Punjabi Colony, Chhutmalpur', '07-Apr-11', NULL, '24-Feb-09', 'F', '31', NULL, NULL, '9897336449', '9897659420', '1', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(141, '279', 'Abhijeet Saini', 'Sandeep Saini', 'Deepa Saini', 'III-A', 'Chhutmapur, Near Cinema Hall', '08-Jul-11', NULL, '29-Aug-08', 'M', '65', NULL, NULL, '9286852932', '9675134082', '2', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(142, '1100', 'Abhinav Yadav', 'Anil Kumar', 'Anju Rani', 'III-A', 'Vill-Rajapur, Post-Chhutmalpur, Distt-SRE', '04-Apr-15', NULL, '30-Oct-08', 'M', '83', NULL, NULL, '9897345257', '9837413132', '3', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(143, '1442', 'Mohd. Afnan', 'Rao Manna', 'Shahjan', 'III-B', 'Chhutmalpur', '09-Apr-16', NULL, '20-12-2010', 'M', '84', NULL, NULL, '9720400255', '9675972670', '1', '1', NULL, NULL, '2017-09-09 02:23:44', NULL),
(144, '623', 'Akash Gupta', 'Sameer Gupta', 'Dixa Gupta', 'III-B', 'Fatehpur C/O Shivalik X-Ray', '03-Apr-13', NULL, '13-Apr-08', 'M', '127', NULL, NULL, '9897472682', '8006440815', '2', '1', NULL, NULL, '2017-09-01 08:03:21', NULL),
(145, '559', 'Akash Karnwal', 'Mr. Mahesh Karanwal', 'Mrs. Sunita Karnwal', 'III-A', 'Nanka', '07-Sep-12', NULL, '17-Apr-08', 'M', '128', NULL, NULL, '9675060297', '8839139123', '4', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(146, '289', 'Akshita Saini', 'Amrish Saini', 'Meenakshi Saini', 'III-A', 'Vill Fatehpur', '15/7/11', NULL, '08-Jun-09', 'F', '146', NULL, NULL, '9411295794', '', '5', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(147, '447', 'Akul Saini', 'Sanjay Kumar', 'Suman', 'III-B', 'Vill.Bhasrao, Post Chhutmslpur', '16-Apr-12', NULL, '06-Oct-08', 'M', '147', NULL, NULL, '9639142135', '9690896034', '3', '1', NULL, NULL, '2017-09-01 08:03:21', NULL),
(148, '353', 'Alisha Parveen', 'Mohd. Dilshad', 'Shabana', 'III-A', 'Vill.Kheri', '02-Apr-12', NULL, '30-Jun-09', 'F', '156', NULL, NULL, '9761007644', '9761336962', '6', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(149, '456', 'Amrit Gupta', 'Brijesh Gupta', 'Ashu Gupta', 'III-A', 'Chhutmalpur, Maharanapratap colony ', '17-Apr-12', NULL, '01-Jul-09', 'M', '177', NULL, NULL, '9761248148', '', '7', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(150, '390', 'Anjali Saini', 'Dinesh Saini', 'Babita Saini', 'III-A', 'Amardeep Colony, Chhutmalpur', '06-Apr-12', NULL, '25-Mar-08', 'F', '198', NULL, NULL, '9759053375', '9286760490', '8', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(151, '593', 'Ansh Chaudhary', 'Amit Kumar', 'Ritu  chaudhary', 'III-A', 'Vill-Chaura Khurd, Post.-Gagalheri', '01-Apr-13', NULL, '24-Dec-08', 'M', '205', NULL, NULL, '9720753911', '', '9', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(152, '476', 'Anshika Baliyan', 'Dushyant Kumar', 'Deepmala', 'III-A', 'Vill & Post Gagalheri', '23-Apr-12', NULL, '21-Mar-09', 'F', '217', NULL, NULL, '9759834740', '9997524099', '10', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(153, '1091', 'Anshuman Bhardwaj', 'Sumant Bharat Sharma', 'Renu Sharma', 'III-B', 'Maharana Pratap Colony, Strreet No.3, Chhutmalpur, SRE', '02-Apr-15', NULL, '09-Aug-08', 'M', '227', NULL, NULL, '9759998687', '8266805631', '4', '1', NULL, NULL, '2017-09-01 08:03:21', NULL),
(154, '670', 'Apurva Chauhan', 'Maneet Chauhan', 'Babita Chauhan', 'III-A', 'Near Shiv Mandir, Biharigarh', '08-Apr-13', NULL, '01-Feb-10', 'F', '249', NULL, NULL, '9719831616', '', '11', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(155, '491', 'Arjun Singh Chaudhary', 'Vinod Kumar Chaudhary', 'Gurmeet Kaur', 'III-A', 'Vill Hussainpur Niwada, Post Chhutmalpur', '07-May-12', NULL, '21-Sep-09', 'M', '260', NULL, NULL, '9675661154', '9761958118', '12', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(156, '1142', 'Arnav Kamboj', 'Manoj Kumar', 'Rajni', 'III-B', 'Vill-Ganja Mazra, Post-Biharigarh, Distt-Haridwar', '07-Apr-15', NULL, '19-Dec-09', 'M', '262', NULL, NULL, '9761350592', '8006264865', '5', '1', NULL, NULL, '2017-09-01 08:03:21', NULL),
(157, '283', 'Arpit Saini', 'Manjeet Saini', 'Mera Saini', 'III-B', 'Vill Fatehpur', '13/7/11', NULL, '06-May-08', 'M', '272', NULL, NULL, '9536845036', '', '6', '1', NULL, NULL, '2017-09-01 08:03:21', NULL),
(158, '446', 'Arpit Saini', 'Manish Kumar', 'Reena Devi', 'III-A', 'Vill.Bhaisrao, Post Chhutmalpur', '16-Apr-12', NULL, '03-Feb-09', 'M', '273', NULL, NULL, '9759098949', '', '13', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(159, '1322', 'Arpit Saini', 'Sandeep Saini', 'Kavita ', 'III-B', 'Vill-Dada jalalpur P-Hallumajra, Distt-Haridwar', '04-Apr-16', NULL, '09-Mar-09', 'M', '275', NULL, NULL, '8958042756', '8906141268', '7', '1', NULL, NULL, '2017-09-01 08:03:21', NULL),
(160, '157', 'Aryan Khurana', 'Amit Khurana', 'Meenu Khurana', 'III-A', 'Punjabi colony, Chhutmalpur', '13/4/11', NULL, '07-Apr-09', 'M', '291', NULL, NULL, '9358188554', '8449184444', '14', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(161, '404', 'Asad Rao', 'Manawar Ali Khan', 'Rahat Rao', 'III-A', 'Vill & Post Harora, Distt. Saharanpur', '09-Apr-12', NULL, '21-Sep-09', 'M', '299', NULL, NULL, '9837752620', '9837577974', '15', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(162, '611', 'Avi Saini', 'Sandeep Saini', 'Anjali Saini', 'III-B', 'Sona Sayed Majra', '02-Apr-13', NULL, '07-Sep-09', 'M', '320', NULL, NULL, '9837776217', '9759631188', '8', '1', NULL, NULL, '2017-09-01 08:03:22', NULL),
(163, '352', 'Ayush Kamboj', 'Arvind Kamboj', 'Ujla Rani', 'III-B', 'Takipur,Post-Sherpur', '02-Apr-12', NULL, '11-Dec-08', 'M', '336', NULL, NULL, '9675820836', '7830839109', '9', '1', NULL, NULL, '2017-09-01 08:03:22', NULL),
(164, '461', 'Ayush Raj', 'Ashwani Raj', 'Reena Rani', 'III-B', 'Vill Amarpur Begampur, Post Chauradev, Distt. Saharanpur', '18-Apr-12', NULL, '26-Sep-08', 'M', '341', NULL, NULL, '9760273255', '9761436562', '10', '1', NULL, NULL, '2017-09-01 08:03:22', NULL),
(165, '4', 'Ayushi', 'Rajendra Singh', 'Minakshi', 'III-B', 'Village Fatehpur', '25/3/11', NULL, '07-Nov-08', 'F', '346', NULL, NULL, '9761020414', '9720065837', '11', '1', NULL, NULL, '2017-09-01 08:03:22', NULL),
(166, '1561', 'Azmal', 'Zulfkar', 'Shaheen Jahan', 'III-A', 'Vill-Mukarrampur Urf Kalewala, Chhutmalpur, Distt-Saharanpyur', '03-04-2017', NULL, '21-06-2009', 'M', '354', NULL, NULL, '9411077562', '9456294521', '16', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(167, '412', 'Daksh Saini', 'Sundar Lal Saini', 'Deepa Saini', 'III-B', 'Vill Badkla, Post Chhutmalpur', '09-Apr-12', NULL, '20-Jan-09', 'M', '373', NULL, NULL, '8410745070', '9837550890', '12', '1', NULL, NULL, '2017-09-01 08:03:22', NULL),
(168, '305', 'Deepak Kumar', 'Suresh Kumar', 'Ruma Devi', 'III-A', 'Nanka', '18/10/11', NULL, '20-Dec-07', 'M', '376', NULL, NULL, '9758090478', '', '17', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(169, '1530', 'Devanshi Kamboj', 'Avdhesh Kamboj', 'Soniya Rani', 'III-A', 'Shiv Colony,Fatehpur, Chhutmalpur, Distt-Saharanpur', '10-04-2017', NULL, '26-02-2009', 'F', '392', NULL, NULL, '9927035711', '9917269080', '18', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(170, '525', 'Devesh Chaudhary', 'Ashish Kumar', 'Rashmi Chaudhary', 'III-A', 'Chhutmalpur, Hind Vihar Colony, Halwana Road', '07-Jul-12', NULL, '04-Jan-10', 'M', '394', NULL, NULL, '9837421910', '9997811177', '19', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(171, '449', 'Dhanisha Bhardwaj', 'Dinesh Bhardwaj', 'Sarita Bhardwaj', 'III-A', 'Opp Bank Of Baroda, Vill Fatehpur', '16-Apr-12', NULL, '27-Oct-09', 'F', '395', NULL, NULL, '9761414141', '9368955757', '20', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(172, '1397', 'Dhruv Saini', 'Umesh Saini', 'Anju Saini', 'III-B', 'Vill-Main Bazar Biharigarh, Distt-SRE', '12-Apr-16', NULL, '18-Dec-09', 'M', '399', NULL, NULL, '9759255930', '9720798756', '13', '1', NULL, NULL, '2017-09-01 08:03:22', NULL),
(173, '427', 'Hemant Kumar', 'Jogendra Kumar', 'Sangita', 'III-A', 'Vill Mandawar, Post Chhutmalpur, Distt. Haridwar', '11-Apr-12', NULL, '02-Sep-08', 'M', '467', NULL, NULL, '9761246959', '9760091864', '21', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(174, '225', 'Insha Rao', 'Rao Fasahat', 'Raziya Rao', 'III-B', 'Village Kheri Shikohpur', '29/4/11', NULL, '', 'F', '484', NULL, NULL, '9897202070', '', '14', '1', NULL, NULL, '2017-09-01 08:03:22', NULL),
(175, '471', 'Janshi Parle', 'Jitender Parle', 'Preeti Parle', 'III-A', 'Vill & Post Gagalehri, Distt. Saharanpur', '20-Apr-12', NULL, '21-Apr-08', 'F', '494', NULL, NULL, '9761333313', '7409077688', '22', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(176, '437', 'Kartik Sharma', 'Brijesh Sharma', 'Beena Sharma', 'III-A', 'Shiv Colony, Near Jain Bagh,Vill Fatehpur, Chhutmalpur', '11-Apr-12', NULL, '15-Nov-08', 'M', '523', NULL, NULL, '9759999152', '9412558030', '23', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(177, '306', 'Kavyansh Batra', 'Harish Batra', 'Savita Batra', 'III-A', 'Punjabi Colony, Chhutmalpur', '18/10/11', NULL, '19-Dec-08', 'M', '526', NULL, NULL, '9719370260', '7500838333', '24', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(178, '542', 'Khushi Mirza', 'Mohd Imran', 'Shahjahan Parveen', 'III-A', 'Chhutmalpur, Harijan Colony, Str. No. 1', '19-Jul-12', NULL, '15-Nov-08', 'F', '532', NULL, NULL, '9917756378', '9675933802', '25', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(179, '856', 'Krishna Singhal', 'Nakul Singhal', 'Renu Singhal', 'III-B', 'Maharana Pratap Colony, Chhutmalpur', '03-Apr-14', NULL, '26-Feb-09', 'M', '551', NULL, NULL, '9760007130', '9058769633', '15', '1', NULL, NULL, '2017-09-01 08:03:22', NULL),
(180, '441', 'Kush Tyagi', 'Nikunj Tyagi', 'Monika Tyagi', 'III-B', 'Near Tel Exchg. Shiv Colony Dehradun Road Chhumalpur', '12-Apr-12', NULL, '07-Jul-09', 'M', '559', NULL, NULL, '8445478364', '9927028537', '16', '1', NULL, NULL, '2017-09-01 08:03:22', NULL),
(181, '149', 'Mohd Kashif', 'Kamroon Ali', 'Shabana', 'III-A', 'Village Kalalhatti, Post Chhutmalpur, Saharanpur', '12-Apr-11', NULL, '08-Apr-08', 'M', '633', NULL, NULL, '9219490170', '', '26', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(182, '487', 'Mohd Sameer', 'Nafees Ahmed', 'Sajda Praveen', 'III-A', 'Vill Gandewara Post Chhutmalpur', '03-May-12', NULL, '06-Jan-08', 'M', '634', NULL, NULL, '9997159672', '9557004723', '27', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(183, '472', 'Mohini Saini', 'Vipin Kumar Saini', 'Rita Saini', 'III-B', 'Alawalpur, Post Chhutmalpur', '20-Apr-12', NULL, '15-Jan-10', 'F', '674', NULL, NULL, '9758069238', '0132 2782838', '17', '1', NULL, NULL, '2017-09-01 08:03:22', NULL),
(184, '949', 'Mueez Alam', 'Parvez Alam', 'Ashiya Naaz', 'III-B', 'Dehradun Road, Chhutmalpur', '21-Apr-14', NULL, '07-Aug-08', 'M', '677', NULL, NULL, '9758985555', '8394926262', '18', '1', NULL, NULL, '2017-09-01 08:03:22', NULL),
(185, '528', 'Payal Tiwari', 'B K Tiwari', 'Seema Tiwari', 'III-B', 'Vill Kunwhalia, Post Kanchanpur, Distt. Dewaria', '07-Jul-12', NULL, '14-Feb-09', 'F', '725', NULL, NULL, '9927712284', '8449866963', '19', '1', NULL, NULL, '2017-09-01 08:03:22', NULL),
(186, '482', 'Prashant Rana', 'Amrish Rana', 'Sunita Pundir', 'III-A', 'Budha Khera Pundir, Post Chamarikhera', '30-Apr-12', NULL, '26-02-2011', 'M', '738', NULL, NULL, '8449164444', '9759790381', '28', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(187, '1093', 'Prithvi', 'L. Mr. Sanjay Kumar', 'Madhu Devi', 'III-A', 'Dharamveer Colony, Gagalheri, SRE', '02-Apr-15', NULL, '07-Jan-09', 'M', '748', NULL, NULL, '8859624520', '9759655607', '29', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(188, '1419', 'Priyanshu Pundir', 'Shiv Charan Singh', 'Ashu Devi', 'III-B', 'Vill-Battanwala post-Chamarikhera,Distt.Saharanpur', '18-Apr-16', NULL, '08-Sep-09', 'M', '755', NULL, NULL, '9837335592', '8192045120', '20', '1', NULL, NULL, '2017-09-01 08:03:22', NULL),
(189, '173', 'Rao Zubair Ahmad', 'Rao Sammun', 'Reshma Rao', 'III-B', 'Village Gudam, Post Chhutmalpur, SRE', '15/4/11', NULL, '15-Sep-08', 'M', '791', NULL, NULL, '9927091670', '9837792200', '21', '1', NULL, NULL, '2017-09-01 08:03:22', NULL),
(190, '172', 'Safiya Rao', 'Rao Mutayyab', 'Harunnisa', 'III-B', 'Village Khujnawar', '15/4/11', NULL, '10-Jun-08', 'F', '822', NULL, NULL, '9759363535', '', '22', '1', NULL, NULL, '2017-09-01 08:03:22', NULL),
(191, '1520', 'Sarthak Chauhan', 'Padam Singh', 'Babli', 'III-B', 'Vill-Khubbanpur, Post-Bhagwanpur, Distt-Haridwar', '08-04-2017', NULL, '11-02-2009', 'M', '864', NULL, NULL, '8393063078', '9536286359', '23', '1', NULL, NULL, '2017-09-01 08:03:22', NULL),
(192, '1545', 'Sarthak Chauhan', 'Kulwant Singh', 'Anshu', 'III-B', 'Shiv Colony,Fatehpur, Chhutmalpur, Distt-Saharanpur', '12-04-2017', NULL, '11-04-2009', 'M', '865', NULL, NULL, '8894788344', '8894965244', '24', '1', NULL, NULL, '2017-09-01 08:03:22', NULL),
(193, '315', 'Satyam Saini (Nishu Saini)', 'Parveen Kumar Saini', 'Kavita Saini', 'III-B', 'Vill Mandawar', '14-Mar-12', NULL, '16-Jun-09', 'M', '872', NULL, NULL, '9917936337', '', '25', '1', NULL, NULL, '2017-09-01 08:03:22', NULL),
(194, '307', 'Sharvi Kamboj', 'Sumit Kamboj', 'Poonam Kamboj', 'III-B', 'Vill- Chanchak, Post- Sherpur', '16-Jan-12', NULL, '08-Jul-09', 'F', '884', NULL, NULL, '9758840372', '9627100089', '26', '1', NULL, NULL, '2017-09-01 08:03:22', NULL),
(195, '1346', 'Shivam Pundir', 'Dilawar Singh', 'Pooja', 'III-A', 'Dhara masala Factory, Near Nau gaja Peer, Saharanpur', '04-Apr-16', NULL, '', 'M', '900', NULL, NULL, '9012521830', '8193954095', '30', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(196, '584', 'Sivansh Choudhary', 'Anuraj Singh', 'Monika', 'III-A', 'Abha, Post-Chudiala, Dist. Saharanpur', '01-Apr-13', NULL, '18-01-2010', 'M', '932', NULL, NULL, '9761476094', '7830272531', '31', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(197, '520', 'Suhani Kamboj', 'Parveen Kamboj', 'Meenu Kamboj', 'III-B', 'Vill & Post Sherpur Khanaajadpur', '05-Jul-12', NULL, '21-Apr-09', 'F', '947', NULL, NULL, '9720458064', '9759339425', '27', '1', NULL, NULL, '2017-09-01 08:03:22', NULL),
(198, '494', 'Surya Pratap Singh Tomar', 'Mukesh Kumar Tomar', 'Soniya Tomar', 'III-B', 'Sardar Kartar Singh Marg, Near Rashtriya Vidyapeeth, Chhutmalpur', '08-May-12', NULL, '03-Oct-09', 'M', '954', NULL, NULL, '9719751136', '9045594129', '28', '1', NULL, NULL, '2017-09-01 08:03:22', NULL),
(199, '1122', 'Sushant Rana', 'Vijay Rana', 'Reena chouhan', 'III-A', 'Vill-Kamalpur, Post-Chhutmalpur,Distt-SRE', '06-Apr-15', NULL, '', 'M', '958', NULL, NULL, '9897282903', '', '32', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(200, '749', 'Tabish', 'Rashid', 'Tarannum', 'III-B', 'Vill. Kalewala, Mukarrampur', '10-Jul-13', NULL, '', 'M', '962', NULL, NULL, '9012279746', '', '29', '1', NULL, NULL, '2017-09-01 08:03:22', NULL),
(201, '300', 'Tejushvi Chauhan', 'Sandeep Kumar', 'Isha', 'III-A', 'Chhutmalpur, Fancy cloth House, Roorkee Rd. ', '08-Feb-11', NULL, '12-Feb-09', 'F', '976', NULL, NULL, '9760261970', '8445428418', '33', '1', NULL, NULL, '2017-09-01 08:03:14', NULL),
(202, '664', 'Tushar Kamboj', 'Amit Kumar', 'Beenu Kamboj', 'III-B', 'Vill. Sherpur Khanazadpur, Distt. Saharanpur', '08-Apr-13', NULL, '20-Jan-09', 'M', '981', NULL, NULL, '8394816521', '', '30', '1', NULL, NULL, '2017-09-01 08:03:22', NULL),
(203, '733', 'Ujjwal', 'Birampal', 'Vimlesh Devi', 'III-B', 'Vill. & Post Khubbanpur, Distt. Haridwar', '27-May-13', NULL, '18-Mar-09', 'M', '986', NULL, NULL, '9719998019', '7830145457', '31', '1', NULL, NULL, '2017-09-01 08:03:22', NULL),
(204, '3', 'Vachan Singh Rana', 'Sandeep Rana', 'Meenakshi Rana', 'III-A', 'Village Nanka', '25/3/11', NULL, '08-Feb-09', 'M', '997', NULL, NULL, '9719432180', '7830152638', '34', '1', NULL, NULL, '2017-09-01 08:03:15', NULL),
(205, '589', 'Vaibhav Chaudhary', 'Ravinder Kumar', 'Pinky', 'III-B', 'Vill-Chaura Khurd, Post.-Gagalheri', '01-Apr-13', NULL, '28-Sep-08', 'M', '998', NULL, NULL, '9759962931', '', '32', '1', NULL, NULL, '2017-09-01 08:03:22', NULL),
(206, '617', 'Vaibhav Saini', 'Neeraj Kumar Saini', 'Deepa Saini', 'III-B', 'Alawalpur, Chhutmalpur', '03-Apr-13', NULL, '31-Mar-10', 'M', '1002', NULL, NULL, '9410688058', '8006640587', '33', '1', NULL, NULL, '2017-09-01 08:03:22', NULL),
(207, '1377', 'Vansh Saini', 'Pushpender Saini', 'Beena Devi', 'III-A', 'Vill-palli Post-Harora Distt-SRE', '09-Apr-16', NULL, '04-May-09', 'M', '1020', NULL, NULL, '7253823207', '7409842263', '35', '1', NULL, NULL, '2017-09-01 08:03:15', NULL),
(208, '538', 'Vanshika Gautam', 'Ved Pal Gautam', 'Kavita Gautam', 'III-B', 'Vill Ismailpur, Post Biharigarh', '16-Jul-12', NULL, '05-Feb-09', 'F', '1023', NULL, NULL, '9412233691', '7409569884', '34', '1', NULL, NULL, '2017-09-01 08:03:22', NULL);
INSERT INTO `studentmgmts` (`id`, `admission_no`, `student_name`, `student_father_name`, `student_mother_name`, `student_class_section`, `student_address`, `student_joining_date`, `student_images`, `student_dob`, `student_gender`, `student_username`, `student_password`, `student_email`, `student_mob1`, `student_mob2`, `student_rollno`, `sessionid`, `deleted_at`, `created_at`, `updated_at`, `code`) VALUES
(209, '418', 'Varnik Singh Laman', 'Brham Pal Singh', 'Archana Singh', 'III-B', 'Chhutmalpur, Harijan Colony, Str. No. 2, Ravidas Mandir', '10-Apr-12', NULL, '10-Mar-09', 'M', '1027', NULL, NULL, '9412141829', '8410132777', '35', '1', NULL, NULL, '2017-09-01 08:03:22', NULL),
(210, '304', 'Vedanshi', 'Komal Kumar', 'Poonam', 'III-A', 'Halwana', '18/10/11', NULL, '28/4/2008', 'F', '1039', NULL, NULL, '9720578358', '8449174444', '36', '1', NULL, NULL, '2017-09-01 08:03:15', NULL),
(211, '317', 'Vinayak Goyal', 'Vishal Goyal', 'Priyanka Goyal', 'III-B', 'Biharigarh Distt. Saharanpur', '14-Mar-12', NULL, '03-Sep-08', 'M', '1047', NULL, NULL, '0132-2784237', '9759285883', '36', '1', NULL, NULL, '2017-09-01 08:03:22', NULL),
(212, '569', 'Vishesh Yadav', 'Sorabh Kumar', 'Neeti Kumari', 'III-A', 'Vill. & Post Khajoori Akbarpur', '23-Mar-13', NULL, '17-Sep-09', 'F', '1059', NULL, NULL, '9720443529', '9758079874', '37', '1', NULL, NULL, '2017-09-01 08:03:15', NULL),
(213, '246', 'Abhinav', 'Neeraj', 'Praveen', 'IV-A', 'Village Alawalpur', '05-Dec-11', NULL, '06-Jun-08', 'M', '8', NULL, NULL, '9457073724', '9458508510', '1', '1', NULL, NULL, '2017-09-01 08:03:30', NULL),
(214, '204', 'Aditya Saini', 'Satish Kumar', 'Nisha Saini', 'IV-A', 'c/o Dr. Rishipal, Opp. Gurudwara, Biharigarh', '21/4/11', NULL, '28-Jan-08', 'M', '29', NULL, NULL, '9412080891', '9758404211', '2', '1', NULL, NULL, '2017-09-01 08:03:30', NULL),
(215, '29', 'Ankush Chaudhary', 'Pramod Kumar', 'Sarita', 'IV-A', 'Alawalpur, Post Chhutmalpur', '04-Mar-11', NULL, '14-Nov-08', 'M', '45', NULL, NULL, '9917867776', '9927172791', '3', '1', NULL, NULL, '2017-09-01 08:03:30', NULL),
(216, '1167', 'Ansh Rana', 'Late Mr. Manjeet Singh', 'Reena Rana', 'IV-A', 'Vill & Post- Bahera Sandal Singh, Distt-SRE', '08-Apr-15', NULL, '20-Jan-07', 'M', '69', NULL, NULL, '7500680804', '9456293442', '4', '1', NULL, NULL, '2017-09-01 08:03:30', NULL),
(217, '1651', 'Sonia Jethuri', 'Sukhdev Singh Jaithuri', 'Anita Jethuri', 'IX-A', 'Vill Manduwala, Post Khujnawar, Distt. Saharanpur', '10-Apr-12', NULL, '12-Apr-08', 'F', '77', NULL, NULL, '9012694269', '9917320002', '35', '1', NULL, NULL, '2017-09-06 02:24:00', NULL),
(218, '1653', 'Bhumi', 'Harish Kumar', 'Pooja', 'IV-A', 'Near Post Office, saharanpur Road, Chhutmalpur, Distt_saharanpur', '27-07-2017', NULL, '28-08-2016', 'F', '109', NULL, NULL, '7456005954', '8439268466', '6', '1', NULL, NULL, '2017-09-01 08:03:30', NULL),
(219, '287', 'Digvijay Saini', 'Sunil Saini', 'Sadhana Saini', 'IV-A', 'Vill Jattpura Post Chamarikhera', '14/7/11', NULL, '27-Aug-08', 'M', '141', NULL, NULL, '9761676037', '9627179101', '7', '1', NULL, NULL, '2017-09-01 08:03:30', NULL),
(220, '590', 'Drishya Chauhan', 'Vikas Chauhan', 'Vandana Chauhan', 'IV-A', 'Kamalpur, Post.-Chuutmalpur', '01-Apr-13', NULL, '09-Oct-08', 'F', '201', NULL, NULL, '9760832454', '9084709476', '8', '1', NULL, NULL, '2017-09-01 08:03:30', NULL),
(221, '360', 'Dushyant Pahwa', 'Varun Pahwa', 'Shikha Pahwa', 'IV-A', 'Punjabi Colony, Chhutmalpur', '02-Apr-12', NULL, '21-Nov-07', 'M', '210', NULL, NULL, '9997153718', '9634656591', '9', '1', NULL, NULL, '2017-09-01 08:03:30', NULL),
(222, '1374', 'Gaurav Pal', 'Sandeep Kumar', 'Babli Devi', 'IV-A', 'Vill-Gagalheri d.dun road, Distt-SRE', '09-Apr-16', NULL, '09-Apr-09', 'M', '228', NULL, NULL, '9690262681', '', '10', '1', NULL, NULL, '2017-09-01 08:03:30', NULL),
(223, '303', 'Gurudyal Singh', 'Phool Kumar', 'Maya Devi', 'IV-A', 'Village Manoharpur, Post Sunderpur', '10-Jul-11', NULL, '07-Mar-08', 'M', '276', NULL, NULL, '7409567764', '8650054733', '11', '1', NULL, NULL, '2017-09-01 08:03:30', NULL),
(224, '143', 'Hamna Mirza', 'Shamim Ahmad', 'Asma Parveen', 'IV-A', 'Village Fatehpur Bhado, Chhutmalpur', '04-Nov-11', NULL, '12-Oct-06', 'F', '303', NULL, NULL, '7409613529', '9719823276', '12', '1', NULL, NULL, '2017-09-01 08:03:30', NULL),
(225, '278', 'Hardik Kamboj', 'Sumit Kamboj', 'Meenu Devi', 'IV-A', 'Vill Chanchak, Chhutmalpur', '07-Apr-11', NULL, '29-Nov-07', 'M', '317', NULL, NULL, '9719445960', '', '13', '1', NULL, NULL, '2017-09-01 08:03:30', NULL),
(226, '129', 'Jessika Sharma', 'Arun Sharma', 'Deepa Sharma', 'IV-A', 'Kalalhatti, Post Chhutmalpur', '04-Aug-11', NULL, '29-May-08', 'F', '339', NULL, NULL, '9219609856', '9759693926', '14', '1', NULL, NULL, '2017-09-01 08:03:30', NULL),
(227, '210', 'Kanhiya Dhiman', 'Sunil Kumar Dhiman', 'Reetu Dhiman', 'IV-A', 'Villlage Kalalhatti, Post Chhutmalpur SRE', '25/4/11', NULL, '24-Aug-07', 'M', '347', NULL, NULL, '9259358333', '9997544369', '15', '1', NULL, NULL, '2017-09-01 08:03:30', NULL),
(228, '209', 'Kanishka Rathore', 'Himanshu Rathore', 'Tina Rathore', 'IV-A', 'Roorkee Road, Chhutmalpur,BANJARA COLONY', '25/4/11', NULL, '12-May-08', 'F', '360', NULL, NULL, '8439111051', '', '16', '1', NULL, NULL, '2017-09-01 08:03:30', NULL),
(229, '1', 'Kunal Saini', 'Ashwani Saini', 'Gopi Saini', 'IV-A', 'Alawalpur, Post Chhutmalpur', '22/3/11', NULL, '27/11/2007', 'M', '362', NULL, NULL, '9719870220', '', '17', '1', NULL, NULL, '2017-09-01 08:03:30', NULL),
(230, '594', 'Lucky', 'Birjesh Kumar', 'Padma Devi', 'IV-A', 'Vill. Cholli Shahabuddinpur', '01-Apr-13', NULL, '03-Dec-07', 'M', '387', NULL, NULL, '9719190063', '', '18', '1', NULL, NULL, '2017-09-01 08:03:30', NULL),
(231, '316', 'Manvi', 'Anokhe Lal', 'Ranjana Nirwan', 'IV-A', 'Alawalpur, Post Chhutmalpur', '14-Mar-12', NULL, '04-Oct-07', 'F', '390', NULL, NULL, '9458508607', '9458508607', '19', '1', NULL, NULL, '2017-09-01 08:03:30', NULL),
(232, '400', 'Mohd Anas', 'Basit Ali', 'Tasmeem Jahan', 'IV-A', 'Vill Chhapur Post Khubbanpur, Distt. Haridwar', '07-Apr-12', NULL, '27-May-08', 'M', '397', NULL, NULL, '9719456433', '', '20', '1', NULL, NULL, '2017-09-01 08:03:30', NULL),
(233, '308', 'Mohd Asad', 'Dilshad Ali', 'Azra Parveen', 'IV-A', 'Chhutmalpur, Muslim Colony', '25-Jan-12', NULL, '16-Aug-07', 'M', '401', NULL, NULL, '9012938470', '9897778734', '21', '1', NULL, NULL, '2017-09-01 08:03:30', NULL),
(234, '290', 'Mohd Danish', 'Boby', 'Khushnood', 'IV-A', 'Vill Gandewara', '16/7/11', NULL, '28-Dec-07', 'M', '404', NULL, NULL, '9012125751', '9927818798', '22', '1', NULL, NULL, '2017-09-01 08:03:31', NULL),
(235, '1499', 'Mohd Siraj', 'Khursheed Ahmed', 'Shaheen Parveen', 'IV-A', 'Clement Town, Near Sabji mandi, Dehtradun Distt-Dehradun', '04-04-2017', NULL, '27-02-2008', 'M', '414', NULL, NULL, '9758209026', '7417127900', '23', '1', NULL, NULL, '2017-09-01 08:03:31', NULL),
(236, '1509', 'Mohd Umar Bhuttu', 'Mohd Zulfiquar', 'Anium Ara', 'IV-A', 'Vill-Ramjanpur, Behat Road, Distt-Saharanpur', '06-04-2017', NULL, '26-06-2008', 'M', '415', NULL, NULL, '9761330007', '', '24', '1', NULL, NULL, '2017-09-01 08:03:31', NULL),
(237, '163', 'Mohd. Hamza', 'Shamoon Ali', 'Nahid', 'IV-A', 'Village Kalalhatti, Post Chhutmalpur', '14/4/11', NULL, '11-Feb-08', 'M', '421', NULL, NULL, '9760184427', '', '25', '1', NULL, NULL, '2017-09-01 08:03:31', NULL),
(238, '269', 'Mohd. Zaid', 'Shahnawaz', 'Farhana', 'IV-A', 'Vill Gandewara', '30/6/11', NULL, '', 'M', '431', NULL, NULL, '9568314242', '7500809333', '26', '1', NULL, NULL, '2017-09-01 08:03:31', NULL),
(239, '261', 'Muskan', 'Imran', 'Reshma', 'IV-A', 'Chhutmalpur, Muslim Colony', '29/5/11', NULL, '06-Sep-07', 'F', '438', NULL, NULL, '9720650030', '9719865912', '27', '1', NULL, NULL, '2017-09-01 08:03:31', NULL),
(240, '1105', 'Saarthak Kohli', 'Ashish Kumar', 'Monika Kohli', 'IV-A', 'Near Post Office, Punjabi Colony, Chhutmalpur, SRE', '04-Apr-15', NULL, '09-Jul-07', 'M', '439', NULL, NULL, '9219253134', '', '28', '1', NULL, NULL, '2017-09-01 08:03:31', NULL),
(241, '914', 'Shiva Saini', 'Anil Kumar', 'Payal Saini', 'IV-A', 'Vill-Mandawar, Post-Khubbanpur, Haridwar', '09-Apr-14', NULL, '', 'M', '441', NULL, NULL, '8859508071', '7351025875', '29', '1', NULL, NULL, '2017-09-01 08:03:31', NULL),
(242, '510', 'Shivika Choudhary', 'Yograj Singh', 'Neetu', 'IV-A', 'Vill Abha, Post Chudiyala, Distt. Saharanpur', '22-Jun-12', NULL, '15-Apr-08', 'F', '471', NULL, NULL, '9761476094', '9720652745', '30', '1', NULL, NULL, '2017-09-01 08:03:31', NULL),
(243, '792', 'Shubh Tyagi', 'Sandeep Kumar ', 'Manju tyagi', 'IV-A', 'Vill Rangail , Post Khajuri Akbarpur Dist Saharanpur ', '19-Mar-14', NULL, '14-Oct-08', 'M', '485', NULL, NULL, '9760778138', '9761884004', '31', '1', NULL, NULL, '2017-09-01 08:03:31', NULL),
(244, '821', 'Shyam Nautiyal', 'Vijesh Nautiyal', 'Neelam Nautiyal', 'IV-A', 'Vill-Brownwala, Post-Bhauwala, Distt-Dehradun', '31-Mar-14', NULL, '08-Nov-08', 'M', '503', NULL, NULL, '9568005325', '9690125481', '32', '1', NULL, NULL, '2017-09-01 08:03:31', NULL),
(245, '1398', 'Subhangi Kamboj', 'Anil Kumar', 'Poonam Kamboj', 'IV-A', 'Shiv Colony Fatehpur, Chhutmalpur, Distt-SRE', '13-Apr-16', NULL, '23-Feb-08', 'F', '514', NULL, NULL, '9927172474', '8532936207', '33', '1', NULL, NULL, '2017-09-01 08:03:31', NULL),
(246, '195', 'Vansh Kumar ', 'Ajay Kumar', 'Mamta', 'IV-A', 'Chhutmalpur, D.Dun Road', '19/4/11', NULL, '15-Mar-07', 'M', '518', NULL, NULL, '9759058106', '', '34', '1', NULL, NULL, '2017-09-01 08:03:31', NULL),
(247, '107', 'Vasu Goyal', 'Vipin Goyal', 'Mamta Jain', 'IV-A', 'Village Gangali, Post Chhutmalpur, Distt. Saharanpur', '04-Jul-11', NULL, '21-Oct-08', 'M', '528', NULL, NULL, '9927624270', '', '35', '1', NULL, NULL, '2017-09-01 08:03:31', NULL),
(248, '468', 'Yuvraj Singh', 'Sandeep Kumar', 'Anjali', 'IV-A', 'Amardeep Colony, Chhutmalpur', '20-Apr-12', NULL, '24-Jan-08', 'M', '546', NULL, NULL, '9897565782', '', '36', '1', NULL, NULL, '2017-09-01 08:03:31', NULL),
(249, '513', 'Zikra Nawaj', 'Shahnawaj', 'Shabana', 'IV-A', 'Vill Syed Mazra, Post Harora, Distt. Saharanpur', '30-Jun-12', NULL, '12-Apr-08', 'F', '558', NULL, NULL, '9837085688', '9719210761', '37', '1', NULL, NULL, '2017-09-01 08:03:31', NULL),
(250, '1575', 'Aadya Agrawal', 'Amit Kumar Agrawal', 'Rachna Agrawal', 'IV-B', 'Home No.99, Shatrughanpuri colony, Distt-Saharanpur', '08-04-2017', NULL, '11-04-2008', 'F', '579', NULL, NULL, '9719004576', '7830088400', '1', '1', NULL, NULL, '2017-09-01 08:03:37', NULL),
(251, '169', 'Aarchi Kamboj', 'Nikunj Kamboj', 'Renu Kamboj', 'IV-B', 'Village Sherpur Khanazadpur', '15/4/11', NULL, '08-Jun-08', 'F', '602', NULL, NULL, '9897452108', '', '2', '1', NULL, NULL, '2017-09-01 08:03:37', NULL),
(252, '319', 'Aastha Saini', 'Pawan Kumar Saini', 'Naresh Devi', 'IV-B', 'Dhankapur Post Chamarikhera', '17-Mar-12', NULL, '06-Oct-08', 'F', '625', NULL, NULL, '9917093708', '9045584542', '3', '1', NULL, NULL, '2017-09-01 08:03:37', NULL),
(253, '656', 'Abhinav Saini', 'Dinesh kumarSaini', 'Prachi Saini', 'IV-B', 'Shiv Colony, Fatehpur', '06-Apr-13', NULL, '', 'M', '628', NULL, NULL, '9760782807', '9927600149', '4', '1', NULL, NULL, '2017-09-01 08:03:37', NULL),
(254, '273', 'Akshay Kamboj', 'Pravind Kumar', 'Suman Lata', 'IV-B', 'Rasoolpur Kala', '07-Feb-11', NULL, '18-Jan-07', 'M', '629', NULL, NULL, '9012259325', '', '5', '1', NULL, NULL, '2017-09-01 08:03:37', NULL),
(255, '379', 'Anshuman Yadav', 'Mukesh Yadav', 'Aruna Yadav', 'IV-B', 'Vill & post Simbhalki Gujjar', '04-Apr-12', NULL, '22-Jun-08', 'M', '636', NULL, NULL, '9758645741', '9536698300', '6', '1', NULL, NULL, '2017-09-01 08:03:37', NULL),
(256, '654', 'Arpit Saini', 'Vinit Kumar Saini', 'Rubi Saini', 'IV-B', 'Vill. Mustafapur, Post Muzaffarabad', '06-Apr-13', NULL, '27-Aug-07', 'M', '637', NULL, NULL, '9997388605', '9719252528', '7', '1', NULL, NULL, '2017-09-01 08:03:37', NULL),
(257, '568', 'Avantika Saini', 'Vivek Kumar Saini', 'Shalu saini', 'IV-B', 'Chuali plot', '22-Mar-13', NULL, '26-Oct-08', 'F', '642', NULL, NULL, '9675355945', '8273711616', '8', '1', NULL, NULL, '2017-09-01 08:03:37', NULL),
(258, '627', 'Ayush Pal', 'Rajnish Pal', 'Renu Pal', 'IV-B', 'Hind Vihar Colony, Chhutmalpur', '03-Apr-13', NULL, '22-Sep-07', 'M', '655', NULL, NULL, '9927413719', '7830139056', '9', '1', NULL, NULL, '2017-09-01 08:03:37', NULL),
(259, '1546', 'Ayushi Chauhan', 'Kulwant Singh', 'Anshu', 'IV-B', 'Shiv Colony,Fatehpur, Chhutmalpur, Distt-Saharanpur', '12-04-2017', NULL, '18-02-2008', 'F', '669', NULL, NULL, '8894788344', '8894965244', '10', '1', NULL, NULL, '2017-09-01 08:03:37', NULL),
(260, '1574', 'Bhavya Agrawal', 'Amit Kumar Agrawal', 'Rachna Agrawal', 'IV-B', 'Home No.99, Shatrughanpuri colony, Distt-Saharanpur', '08-04-2017', NULL, '11-04-2008', 'F', '681', NULL, NULL, '9719004576', '7830088400', '11', '1', NULL, NULL, '2017-09-01 08:03:37', NULL),
(261, '276', 'Devansh Aggarwal', 'Pradeep Aggarwal', 'Manju Aggarwal', 'IV-B', 'Agg. Agency, Kamalpur Rd, Chhutmalpur', '07-Apr-11', NULL, '', 'M', '712', NULL, NULL, '9897119842', '9412650471', '12', '1', NULL, NULL, '2017-09-01 08:03:38', NULL),
(262, '234', 'Devansh Kamboj', 'Amit Kumar', 'Anjali Kamboj', 'IV-B', 'Arya Nagar, Chhutmalpur', '05-Jun-11', NULL, '21-Nov-07', 'M', '746', NULL, NULL, '8979959511', '9927203911', '13', '1', NULL, NULL, '2017-09-01 08:03:38', NULL),
(263, '162', 'Dhruv Chaudhary', 'Ashish Kumar', 'Rashmi Chaudhary', 'IV-B', 'Chhutmalpur, Hind Vihar Colony, Halwana Road', '14/4/11', NULL, '07-May-08', 'M', '792', NULL, NULL, '9837421910', '', '14', '1', NULL, NULL, '2017-09-01 08:03:38', NULL),
(264, '1144', 'Dikshant Chauhan', 'Mangeram', 'Neelam', 'IV-B', 'Vill & Post - Khubbanpur, Distt- Haridwar', '07-Apr-15', NULL, '', 'M', '797', NULL, NULL, '9759229126', '9761022899', '15', '1', NULL, NULL, '2017-09-01 08:03:38', NULL),
(265, '199', 'Faeiz Ali', 'Rao Usman Ali', 'Razia Begum', 'IV-B', 'Village Kheri Shikohpur', '20/4/11', NULL, '13-Dec-08', 'M', '819', NULL, NULL, '9719765983', '', '16', '1', NULL, NULL, '2017-09-01 08:03:38', NULL),
(266, '1590', 'Himani Rana', 'Sandeep Rana', 'Dimple', 'IV-B', 'Vill-Battanwala, Post-Chhutmalpur, Distt-Saharanpur', '08-04-2017', NULL, '21-01-2008', 'F', '821', NULL, NULL, '9917664176', '9761381966', '17', '1', NULL, NULL, '2017-09-01 08:03:38', NULL),
(267, '293', 'Insha Rao', 'Mehtab Ali', 'Farha Rao', 'IV-B', 'Chhutmalpur, H.No. 73, Harijan Col, St No. 1 ', '18/7/11', NULL, '18-Jun-08', 'F', '840', NULL, NULL, '9758139455', '', '18', '1', NULL, NULL, '2017-09-01 08:03:38', NULL),
(268, '145', 'Keshav Tyagi', 'Atul Kumar Tyagi ', 'Supriya Tyagi', 'IV-B', 'Bansal Vihar Colony, Chhutmalpur', '04-Nov-11', NULL, '27-Nov-07', 'M', '852', NULL, NULL, '9411079010', '', '19', '1', NULL, NULL, '2017-09-01 08:03:38', NULL),
(269, '832', 'Krishan Kumar', 'Geeta Ram Saini', 'Suman Lata', 'IV-B', 'Kamaalpur Road, Shanti Nagar, Near Indian Culture Academy, Chhutmalpur', '31-Mar-14', NULL, '23-Aug-07', 'M', '855', NULL, NULL, '9759693790', '9675562612', '20', '1', NULL, NULL, '2017-09-01 08:03:38', NULL),
(270, '282', 'Mohd. Adil', 'Mohd. Akram', 'Tasneem', 'IV-B', 'Masjid Talab Wali, Vill Gandewara', '07-Dec-11', NULL, '09-Sep-07', 'M', '868', NULL, NULL, '9837340471', '9639263316', '21', '1', NULL, NULL, '2017-09-01 08:03:38', NULL),
(271, '42', 'Nitish Kumar', 'Dilip Kumar ', 'Anita Saini', 'IV-B', 'Adarash Nagar, Chhutmalpur', '04-Apr-11', NULL, '20-Dec-07', 'M', '892', NULL, NULL, '9557337095', '8449200941', '22', '1', NULL, NULL, '2017-09-01 08:03:38', NULL),
(272, '1457', 'Prince', 'Rakesh', 'Umesh', 'IV-B', 'Vill-Alawalpur, Post-Chhutmalpur, Distt-Haridwar', '06-Jul-16', NULL, '05-Feb-06', 'M', '897', NULL, NULL, '', '', '23', '1', NULL, NULL, '2017-09-01 08:03:38', NULL),
(273, '326', 'Ravikant', 'Tejpal Singh', 'Nutan Chauhan', 'IV-B', 'Vill & Post Khubbanpur', '27-Mar-12', NULL, '05-Sep-08', 'M', '901', NULL, NULL, '9720166976', '', '24', '1', NULL, NULL, '2017-09-01 08:03:38', NULL),
(274, '119', 'Rimjhim', 'Bhajan Lal Kamboj', 'Sunita Kamboj', 'IV-B', 'Village sherpur ', '04-Jul-11', NULL, '11-Jul-08', 'F', '905', NULL, NULL, '9412355750', '9720405026', '25', '1', NULL, NULL, '2017-09-01 08:03:38', NULL),
(275, '1614', 'Sabeel', 'Mukharam', 'Naseema', 'IV-B', 'Vill- Bhainsrao, Post-Chhutmalpur. Distt-Saharanpur', '09-05-2017', NULL, '05-01-2007', 'M', '917', NULL, NULL, '9719156764', '9720722683', '26', '1', NULL, NULL, '2017-09-01 08:03:38', NULL),
(276, '864', 'Saloni Gupta', 'Rajesh Gupta', 'Mamta Gupta', 'IV-B', 'Vill. Cholli, Sahbuddipur, Distt. Haridwar', '03-Apr-14', NULL, '30-May-07', 'F', '921', NULL, NULL, '9761206794', '8394817377', '27', '1', NULL, NULL, '2017-09-01 08:03:38', NULL),
(277, '94', 'Samreen Rao', 'Rao Sajid', 'Nazma Rao', 'IV-B', 'Rao Market, Sabji Mandi, Chhutmalpur', '04-Jul-11', NULL, '12-Apr-07', 'F', '929', NULL, NULL, '8868952998', '9358018210', '28', '1', NULL, NULL, '2017-09-01 08:03:38', NULL),
(278, '422', 'Sana Chaudhary', 'Jamshed Chaudhary', 'Anjum Chaudhary', 'IV-B', 'Chhutmalpur, Harijan Colony, Str. No. 3', '10-Apr-12', NULL, '15-May-08', 'F', '942', NULL, NULL, '9760968263', '8439132606', '29', '1', NULL, NULL, '2017-09-01 08:03:38', NULL),
(279, '249', 'Sarthak Verma', 'Suresh Kumar', 'Anita', 'IV-B', 'Shiv Col. Fatehpur', '18/5/11', NULL, '16-Jun-08', 'M', '943', NULL, NULL, '9758938632', '', '30', '1', NULL, NULL, '2017-09-01 08:03:38', NULL),
(280, '295', 'Shivam Kumar', 'Rajan Kumar', 'Poonam Devi', 'IV-B', 'Vill Razapur, Post Chhutmalpur, Distt. Saharanpur', '19/7/11', NULL, '09-Aug-07', 'M', '992', NULL, NULL, '9557916824', '', '31', '1', NULL, NULL, '2017-09-01 08:03:38', NULL),
(281, '25', 'Shivam Singh', 'Gurudas Singh', 'Sulekha', 'IV-B', 'Village Syed Majra', '04-Feb-11', NULL, '16-Feb-09', 'M', '995', NULL, NULL, '9837110779', '', '32', '1', NULL, NULL, '2017-09-01 08:03:38', NULL),
(282, '1016', 'Simran Kamboj', 'Pradeep Kamboj', 'Monika', 'IV-B', 'Vill-Ganja Mazra, Post-Biharigarh, Distt-Haridwar', '11-Jul-14', NULL, '11-Oct-09', 'F', '1015', NULL, NULL, '9927556777', '', '33', '1', NULL, NULL, '2017-09-01 08:03:38', NULL),
(283, '1335', 'Suchi Saini', 'Aditya Saini', 'Vinita Singh', 'IV-B', 'Bansal Vihar Colony, D.dun Road, Chhutmalpur, Distt-SRE', '06-Apr-16', NULL, '24-Jun-09', 'F', '1017', NULL, NULL, '9719567198', '9536687092', '34', '1', NULL, NULL, '2017-09-01 08:03:38', NULL),
(284, '221', 'Utkarsh Prajapati', 'Shishupal', 'Meena Prajapati', 'IV-B', 'Vill & Post Hasanpur Madanpur, Distt. Haridwar', '28/4/11', NULL, '11-Dec-08', 'M', '1021', NULL, NULL, '8006337122', '8006337715', '35', '1', NULL, NULL, '2017-09-01 08:03:38', NULL),
(285, '167', 'Uzma Khan', 'Aijaz Haider', 'Malika', 'IV-B', 'Opp Arora Dhaba, D.Dun Rd., Chhutmalpur', '14/4/11', NULL, '15-Nov-07', 'F', '1034', NULL, NULL, '9719923879', '', '36', '1', NULL, NULL, '2017-09-01 08:03:38', NULL),
(286, '1165', 'Vansh Rana', 'Late Mr. Kulbeer Singh', 'Rekha Rana', 'IV-B', 'Vill & Post-Bahera Sandal Singh, Distt-SRE', '08-Apr-15', NULL, '21-Jan-07', 'M', '1071', NULL, NULL, '9012166530', '9456293442', '37', '1', NULL, NULL, '2017-09-01 08:03:38', NULL),
(287, '2', 'Vansh Saini', 'Praveen Saini', 'Anju Saini', 'IV-B', 'Alawalpur, Post Chhutmalpur', '22/3/11', NULL, '06-Nov-07', 'M', '1080', NULL, NULL, '9837938115', '', '38', '1', NULL, NULL, '2017-09-01 08:03:38', NULL),
(288, '567', 'Aakariti Jain', 'Anup Kumar Jain', 'Pooja Jain', 'IX-A', 'Gandhi Colony, Chhutmalpur', '22-Mar-13', NULL, '12-Nov-03', 'F', '12', NULL, NULL, '9837979726', '8923409762', '1', '1', NULL, NULL, '2017-09-01 08:03:46', NULL),
(289, '1514', 'Aakash Kumar', 'Mukesh Kumar', 'Munesh Devi', 'IX-A', 'Vill-Aurangabad, Post-Sherpur Khanazadpur, Distt-Saharanpur', '07-04-2017', NULL, '02-10-2004', 'M', '13', NULL, NULL, '9759560567', '8954168236', '2', '1', NULL, NULL, '2017-09-01 08:03:46', NULL),
(290, '651', 'Aarushi Chauhan', 'Chanchal Singh', 'Kumud', 'IX-A', 'Vill. Danda Telpura, Post Biharigarh, Distt. Haridwar U.K.', '05-Apr-13', NULL, '14-Nov-03', 'F', '33', NULL, NULL, '9719953027', '9758467477', '3', '1', NULL, NULL, '2017-09-01 08:03:46', NULL),
(291, '863', 'Abdul Mujeeb ', 'Abdul Aleem', 'Gulista Parveen', 'IX-A', 'Vill-Khaeri Shikohpur, Distt-Haridwar', '03-Apr-14', NULL, '02-Oct-03', 'M', '57', NULL, NULL, '9720484703', '9837488947', '4', '1', NULL, NULL, '2017-09-01 08:03:46', NULL),
(292, '1487', 'Abhishek Pundir', 'Lalit Kumar', 'Babita', 'IX-A', 'Vill-Behra Sandal Singh, Distt-Saharanpur', '27-03-2017', NULL, '25-01-2003', 'M', '88', NULL, NULL, '7409533607', '9758301083', '5', '1', NULL, NULL, '2017-09-01 08:03:46', NULL),
(293, '1610', 'Ahad Zeeshan', 'Mohd Zeeshan', 'Shakira', 'IX-A', 'Vill-Bhattpura, Post-Chhutmalpur, Distt-Saharanpur', '17-04-2017', NULL, '18-09-2002', 'M', '90', NULL, NULL, '9719115577', '9720909333', '6', '1', NULL, NULL, '2017-09-01 08:03:46', NULL),
(294, '677', 'Akash Pal', 'Sobharam', 'Bubly ', 'IX-A', 'Gagalheri', '09-Apr-13', NULL, '17-Jan-01', 'M', '92', NULL, NULL, '9675482076', '9837097782', '7', '1', NULL, NULL, '2017-09-01 08:03:46', NULL),
(295, '836', 'Arnav Nausran', 'Jitendra Choudhary', 'Choudhary', 'IX-A', 'Vill-Ibrahimpur, Post-Chhutmalpur, Distt-SRE', '01-Apr-14', NULL, '11-Sep-03', 'M', '108', NULL, NULL, '9719148810', '9719726862', '8', '1', NULL, NULL, '2017-09-01 08:03:46', NULL),
(296, '1583', 'Aryan', 'Anokhelal', 'Rajna', 'IX-A', 'Vill-Alawalpur, Post-Chhutmalpur, Distt-Haridwar', '19-04-2017', NULL, '', 'M', '119', NULL, NULL, '9458508607', '9457148415', '9', '1', NULL, NULL, '2017-09-01 08:03:46', NULL),
(297, '859', 'Avika Kamboj', 'Ravipal Singh Kamboj', 'Reena Devi', 'IX-A', 'Vill & Post Sherpur Khanazadpur, Distt-Saharanpur', '03-Apr-14', NULL, '10-Mar-02', 'F', '130', NULL, NULL, '9758126843', '9761715800', '10', '1', NULL, NULL, '2017-09-01 08:03:46', NULL),
(298, '1292', 'Ayush Seth', 'Prem Kumar', 'Annu Seth', 'IX-A', 'Punjabi Colony, Chhutmalpur, Distt-SRE', '25-Mar-16', NULL, '', 'M', '219', NULL, NULL, '9719102567', '9758013759', '11', '1', NULL, NULL, '2017-09-01 08:03:46', NULL),
(299, '166', 'Devansh Garg', 'Ashutosh Garg', 'Poonam Garg', 'IX-A', 'Vill-Bhagwanpur, Dist-Haridwar', '14/4/11', NULL, '05-Nov-03', 'M', '263', NULL, NULL, '9927352270', '9760305020', '12', '1', NULL, NULL, '2017-09-01 08:03:46', NULL),
(300, '1497', 'Divam Kamboj', 'Aditiya Kumar', 'Raman Kamboj', 'IX-A', 'Vill-Aurangabad, Post-Sherpur Khanazadpur, Distt-Saharanpur', '03-04-2017', NULL, '21-04-2003', 'M', '278', NULL, NULL, '8006527192', '9536992390', '13', '1', NULL, NULL, '2017-09-01 08:03:46', NULL),
(301, '74', 'Hardik', 'Vipin Kumar', 'Pooja', 'IX-A', 'Chhutmalpur, Gandhi Chowk', '04-Jun-11', NULL, '08-May-03', 'M', '288', NULL, NULL, '9759560231', '', '14', '1', NULL, NULL, '2017-09-01 08:03:46', NULL),
(302, '203', 'Himanshu', 'Pramod Kumar', 'Savita Devi', 'IX-A', 'Alawalpur, Post Chhutmalpur', '21/4/11', NULL, '15-Jan-03', 'M', '289', NULL, NULL, '9720806266', '', '15', '1', NULL, NULL, '2017-09-01 08:03:46', NULL),
(303, '1396', 'Himanshu Saini', 'Umesh Saini', 'Anju Saini', 'IX-A', 'Vill-Main Bazar Biharigarh, Distt-SRE', '12-Apr-16', NULL, '06-Jun-03', 'M', '318', NULL, NULL, '9759255930', '9720798756', '16', '1', NULL, NULL, '2017-09-01 08:03:46', NULL),
(304, '250', 'Khushi Verma', 'Suresh Kumar', 'Anita', 'IX-A', 'Shiv Col. Fatehpur', '18/5/11', NULL, '24-Jun-03', 'F', '321', NULL, NULL, '9758938632', '', '17', '1', NULL, NULL, '2017-09-01 08:03:46', NULL),
(305, '888', 'Mamoon Ansari', 'Saleem Ahmad', 'Nazma Ansari', 'IX-A', 'Amardeep Colony, SRE Road, Chhutmalpur', '05-Apr-14', NULL, '02-Jul-02', 'M', '343', NULL, NULL, '9927387866', '9719268180', '18', '1', NULL, NULL, '2017-09-01 08:03:46', NULL),
(306, '1350', 'Manvi Kamboj', 'Sanjay Kamboj', 'Pinki Kamboj', 'IX-A', 'Vill-Sherpur Khanazadpur, Distt-SRE', '06-Apr-16', NULL, '16-Jun-03', 'F', '345', NULL, NULL, '9758832040', '9761185434', '19', '1', NULL, NULL, '2017-09-01 08:03:46', NULL),
(307, '649', 'Mayoor Rana', 'Anil Kumar Rana', 'Minakshi Rana', 'IX-A', 'Danda Telpura', '05-Apr-13', NULL, '08-Dec-04', 'M', '349', NULL, NULL, '9719108066', '', '20', '1', NULL, NULL, '2017-09-01 08:03:46', NULL),
(308, '1553', 'Naba Ahsan', 'Mohd Ahsan', 'Mumtaaz', 'IX-A', 'Vill-Bhattpura, Chhutmalpur, Distt-Saharanpur', '13-04-2017', NULL, '27-01-2003', 'F', '364', NULL, NULL, '7830665742', '9756410845', '21', '1', NULL, NULL, '2017-09-01 08:03:46', NULL),
(309, '866', 'Nikhil Kamboj', 'Sukhpal Singh Kamboj', 'Bobby Kamboj', 'IX-A', 'Shiv Colony, Fatehpur Bhado, Chhutmalpur', '03-Apr-14', NULL, '23-Mar-02', 'M', '388', NULL, NULL, '9410689036', '8445221105', '22', '1', NULL, NULL, '2017-09-01 08:03:46', NULL),
(310, '1111', 'Nitish Mittal', 'Shailesh Mittal', 'Neelam Mittal', 'IX-A', 'Gandhi Colony, Chhutmalpur, SRE', '06-Apr-15', NULL, '', 'M', '405', NULL, NULL, '9758674921', '9761256587', '23', '1', NULL, NULL, '2017-09-01 08:03:46', NULL),
(311, '257', 'Pooja ', 'Babu Ram', 'Savita', 'IX-A', 'Nr Tulsi Adrash School, Fatehpur', '27/5/11', NULL, '12-Feb-03', 'F', '408', NULL, NULL, '8654281080', '', '24', '1', NULL, NULL, '2017-09-01 08:03:46', NULL),
(312, '35', 'Pratham Batra', 'Rajeev Batra', 'Poonam Batra', 'IX-A', 'Panjabi Colony,Chhutmalpur', '04-Apr-11', NULL, '23/1/2003', 'M', '426', NULL, NULL, '9927001504', '9634177262', '25', '1', NULL, NULL, '2017-09-01 08:03:46', NULL),
(313, '1460', 'Rao Aasim', 'Rao Saleem', 'Afroji', 'IX-A', 'Vill-Gudam, Post-Chhutmalpur, Distt-SRE', '11-Jul-16', NULL, '17-Feb-02', 'M', '432', NULL, NULL, '9837424141', '9719346381', '26', '1', NULL, NULL, '2017-09-01 08:03:46', NULL),
(314, '1543', 'Rao Faisal Niazi', 'Attaur Haman', 'Shabnam', 'IX-A', 'Vill-Kheri Sikohpur, Post-Chhutmalpur, Distt-Haridwar', '12-04-2017', NULL, '15-08-2017', 'M', '440', NULL, NULL, '9759787684', '8533008054', '27', '1', NULL, NULL, '2017-09-01 08:03:46', NULL),
(315, '1542', 'Rao Jariyab Ali', 'Salamt Khan', 'Kishwar Jahan', 'IX-A', 'Vill-Kheri Sikohpur, Post-Chhutmalpur, Distt-Haridwar', '04-04-2017', NULL, '25-11-2003', 'M', '446', NULL, NULL, '9719113130', '9634630466', '28', '1', NULL, NULL, '2017-09-01 08:03:46', NULL),
(316, '1599', 'Rao Murtaza', 'Rao Akram', 'Poshida Rao', 'IX-A', 'Vill-Kheri Sikohpur, Post-Chhutmalpur. Distt-Saharanpur', '14-04-2017', NULL, '12-07-2004', 'M', '458', NULL, NULL, '9759412789', '9719951182', '29', '1', NULL, NULL, '2017-09-01 08:03:46', NULL),
(317, '1540', 'Shivam Pahuja', 'Rajesh Kumar Pahuja', 'Asha Pahuja', 'IX-A', 'Shyam Hardware Sanitary Store, Punjabi Market, Chhutmalpur, Distt-Saharanpur', '11-04-2017', NULL, '10-10-2002', 'M', '472', NULL, NULL, '9837277039', '9927728010', '30', '1', NULL, NULL, '2017-09-01 08:03:47', NULL),
(318, '1333', 'Tanu Garg', 'Manoj Garg', 'Reema Garg', 'IX-A', 'Shiv Colony Fatehpur, Chhutmalpur, Distt-SRE', '06-Apr-16', NULL, '13-Sep-02', 'F', '477', NULL, NULL, '8115442223', '8738938687', '31', '1', NULL, NULL, '2017-09-01 08:03:47', NULL),
(319, '1522', 'Vishu Saini', 'Vinod Saini', 'Babli Devi', 'IX-A', 'Vill-Kurrikhera, Distt-Saharanpur', '25-03-2017', NULL, '02-06-2002', 'M', '538', NULL, NULL, '7351571664', '8755819867', '32', '1', NULL, NULL, '2017-09-01 08:03:47', NULL),
(320, '86', 'Waqar Ahmed', 'Abrar Ahmed', 'Mehraj', 'IX-A', 'Village Khujnawar', '04-Jun-11', NULL, '04-Aug-02', 'M', '555', NULL, NULL, '9758139091', '9758051610', '33', '1', NULL, NULL, '2017-09-01 08:03:47', NULL),
(321, '219', 'Zeba Malik', 'Mursleen Ahmad', 'Shabnam', 'IX-A', 'Kameshpur', '28/4/11', NULL, '24/1/2003', 'F', '557', NULL, NULL, '9412532339', '9012400986', '34', '1', NULL, NULL, '2017-09-01 08:03:47', NULL),
(322, '1533', 'Abhishek ', 'Shiv Pal', 'Parmod Devi', 'IX-B', 'Vill-Biharigarh, Distt-Saharanpur', '10-04-2017', NULL, '01-01-2001', 'M', '587', NULL, NULL, '9761926533', '9927776369', '1', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(323, '635', 'Abhishek Kumar', 'Kuldeep Kumar', 'Sonia Devi', 'IX-B', 'Vill & Post Biharigarh, Near Petrol Pump', '03-Apr-13', NULL, '15-Sep-02', 'M', '600', NULL, NULL, '9761061645', '', '2', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(324, '957', 'Aditya Chauhan', 'Om Prakash Chauhan', 'Manju Chauhan', 'IX-B', 'Vill. & Post Badheri Ghoghu', '23-Apr-14', NULL, '', 'M', '604', NULL, NULL, '9012233575', '9012101808', '3', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(325, '206', 'Anshika Saini', 'Satish Saini', 'Nisha Saini', 'IX-B', 'Opp Gurudwara, Biharigarh', '21/4/11', NULL, '01-Nov-02', 'F', '614', NULL, NULL, '9412080891', '9758404211', '4', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(326, '1534', 'Arpit Yadav', 'Rajesh Yadav', 'Meera Yadav', 'IX-B', 'Vill-Mandebansh, Post-Harora, Distt-Saharanpur', '10-04-2017', NULL, '07-03-2004', 'F', '617', NULL, NULL, '8393955761', '9758559410', '5', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(327, '53', 'Aryan Kamboj', 'Anvesh Kamboj', 'Kulwanti Devi', 'IX-B', 'Village Chanchak', '04-May-11', NULL, '26-Jul-03', 'M', '618', NULL, NULL, '9719173068', '9758731031', '6', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(328, '660', 'Aryan Kamboj', 'Manoj Kamboj', 'Meera Kamboj', 'IX-B', 'Vill. Takipur, Post Biharigarh', '06-Apr-13', NULL, '18-Nov-02', 'M', '627', NULL, NULL, '8650003531', '9720557568', '7', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(329, '377', 'Avi Chauhan', 'Sudesh Chauhan', 'Sarvesh Chauhan', 'IX-B', 'Vill & Post Khubbanpur', '04-Apr-12', NULL, '26-May-02', 'M', '630', NULL, NULL, '9759789155', '9720665279', '8', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(330, '202', 'Ayush Saini', 'Ajay Saini', 'Ritu Saini', 'IX-B', 'Chhutmalpur, Near Jain Bagh, Shiv Colony ', '21/4/11', NULL, '01-Jul-04', 'M', '653', NULL, NULL, '9412557177', '9808863336', '9', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(331, '429', 'Ayushi Dhiman', 'Sushil Kumar', 'Sangeeta', 'IX-B', 'Vill.Rehri Post-Chhutmalpur', '11-Apr-12', NULL, '12-Aug-04', 'F', '683', NULL, NULL, '7500104781', '9758857218', '10', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(332, '253', 'Bilal Ahmed', 'Irshad Ahmed', 'Rashida Parveen', 'IX-B', 'Chhutmalpur, Nai basti, Nr water Tank ', '18/5/11', NULL, '02-Feb-01', 'M', '703', NULL, NULL, '9719204013', '9058004013', '11', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(333, '1500', 'Dikshit Kumar', 'Radhe Lal', 'Sunita', 'IX-B', 'Vill-Alawalpur, Post-Chhutmalpur, Distt-Haridwar', '05-04-2017', NULL, '05-01-2005', 'M', '713', NULL, NULL, '9627123531', '9758630240', '12', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(334, '272', 'Fatima Zeinab', 'Late Sh. Khushnud ', 'Koasar Jhan', 'IX-B', 'Village Syed Majra', '07-Jan-11', NULL, '27/2/2004', 'F', '728', NULL, NULL, '9536267086', '8791877102', '13', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(335, '121', 'Harsh Kamboj', 'Pankaj Kumar', 'Neelam Kamboj', 'IX-B', 'Thapul Road, Biharigarh', '04-Jul-11', NULL, '20-Feb-02', 'M', '737', NULL, NULL, '9634650659', '9758433136', '14', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(336, '827', 'Harshit Saini', 'Sanjeev Kumar Saini', 'Sumedha Saini', 'IX-B', 'Vill& Post-Khurrampur, SRE', '31-Mar-14', NULL, '19-Apr-03', 'M', '740', NULL, NULL, '9818159744', '9627230389', '15', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(337, '201', 'Kuldeep Chauhan', 'Dharamveer Chauhan', 'Anju', 'IX-B', 'Budha Khera Pundir, Post Chamarikhera', '20/4/11', NULL, '03-Sep-02', 'M', '751', NULL, NULL, '9761488277', '7830139410', '16', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(338, '1607', 'Kunal Rana', 'Sanjeev Kumar', 'Munesh Devi', 'IX-B', 'Vill-Badheri Ghughoo, Distt-Saharanpur', '22-04-2017', NULL, '', 'M', '753', NULL, NULL, '9012233575', '', '17', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(339, '1597', 'Mantsha', 'Mohd. Gulbahar', 'Nazma', 'IX-B', 'Vill-Kalalhati, Post-Chhutmalpur. Distt-Saharanpur', '17-04-2017', NULL, '07-08-2002', 'M', '772', NULL, NULL, '9720095706', '8650280120', '18', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(340, '691', 'Mayank Dhiman', 'Anil Dhiman', 'Vimlesh Dhiman', 'IX-B', 'Vill. Gagalheri, Distt. Saharanpur', '12-Apr-13', NULL, '22-Jul-01', 'M', '779', NULL, NULL, '9719240653', '9045230070', '19', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(341, '1495', 'Meenakshi Rana', 'Neer Kumar Rana', 'Babita Rana', 'IX-B', 'Vill-Kurrikhera Post-Chanchak, Distt-Saharanpur', '03-04-2017', NULL, '27-02-2003', 'F', '786', NULL, NULL, '9719113168', '9536206181', '20', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(342, '1615', 'Mohd Arsh', 'Shahnawaj', 'Rubina', 'IX-B', 'Vill-Kalalhati, Post-Chhutmalpur. Distt-Saharanpur', '17-04-2017', NULL, '29-12-2002', 'M', '789', NULL, NULL, '9536733045', '9720724899', '21', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(343, '1216', 'Mohd Faisal', 'Tassawar Ahmad', 'Shahin', 'IX-B', 'Vill-Mahmoodpur Tiwai, Post-Harora,Distt-SRE', '20-Apr-15', NULL, '28-Oct-03', 'M', '805', NULL, NULL, '9837523933', '9927073661', '22', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(344, '1425', 'Mohd. Danish', 'Mohd. Aslam', 'Farzana', 'IX-B', 'Vill-Harora, Distt-SRE', '25-Apr-16', NULL, '15-Jun-00', 'M', '874', NULL, NULL, '9756094200', '9012660647', '23', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(345, '961', 'Prashant Pundir', 'Sher Singh Pundir', 'Brijesh Pundir', 'IX-B', 'Vill. & Post Badheri Ghoghu', '23-Apr-14', NULL, '', 'M', '875', NULL, NULL, '9917470092', '', '24', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(346, '595', 'Priyanshi Choudhary', 'Ravinder Kumar', 'Pinky', 'IX-B', 'Vill-Chaura Khurd, Post.-Gagalheri', '01-Apr-13', NULL, '08-Jun-02', 'F', '893', NULL, NULL, '9759962931', '', '25', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(347, '1538', 'Priyanshu Chauhan', 'Narender Kumar', 'Sanyogita Chauhan', 'IX-B', 'Vill-Kaluwala Pahadipur Urf Jahanpur, Distt-Saharanpur', '11-04-2017', NULL, '07-08-2004', 'M', '899', NULL, NULL, '9927374771', '7351362341, 9837781790', '26', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(348, '1598', 'Ritika Katyal', 'Surenderpal', 'Seema Katyal', 'IX-B', 'Amardeep Colony, Chhutmalpur, Distt-Saharanpur', '25-04-2017', NULL, '23-07-2001', 'F', '945', NULL, NULL, '9012980821', '9917277352', '27', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(349, '44', 'Saurabh Saini', 'Kunwarpal Saini', 'Kamlesh', 'IX-B', 'Village Alawalpur', '04-Apr-11', NULL, '', 'M', '968', NULL, NULL, '9756908662', '', '28', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(350, '309', 'Sayed Yasar Hussain', 'Rifakat Hussain', 'Mehjabi Fatima', 'IX-B', 'Vill Syed Mazra, Chhutmalpur', '04-Feb-12', NULL, '28-Apr-03', 'M', '972', NULL, NULL, '9758289055', '8859050486', '29', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(351, '636', 'Shivaji Singh', 'Pritam Singh', 'Savita Saini', 'IX-B', 'Vill. Badshahpur, Post Khushhalipur', '03-Apr-13', NULL, '25-Aug-03', 'M', '977', NULL, NULL, '9758693633', '9759469016', '30', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(352, '1496', 'Suhail', 'Mohd Vajid', 'Late. Afrose', 'IX-B', 'Vill-Pairagpur, Distt-Saharanpur', '03-04-2017', NULL, '13-10-2003', 'M', '978', NULL, NULL, '9927993719', '7078786595', '31', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(353, '1539', 'Tapan Bundel', 'Satendra Chauhan', 'Poonam Chauhan', 'IX-B', 'Vill-Kaluwala Pahadipur Urf Jahanpur, Distt-Saharanpur', '11-04-2017', NULL, '16-10-2004', 'M', '1061', NULL, NULL, '7210576967', '7983416037', '32', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(354, '1554', 'Thotcuila', 'Chihanngam Khapai', 'A Yalashung Khapai', 'IX-B', 'Doon Velly Colony, Near Thana Fathepur, Fathepur Bhado, Chhutmalpur, Distt-Saharanpur', '13-04-2017', NULL, '23-06-2003', 'F', '1062', NULL, NULL, '9634367344', '', '33', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(355, '78', 'Tisha Arora', 'Vinay Arora', 'Ruby Arora', 'IX-B', 'Chhutmalpur', '04-Jun-11', NULL, '01-Aug-03', 'F', '1064', NULL, NULL, '9897659420', '9897336449', '34', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(356, '105', 'Vivek Saini', 'Ram Kumar', 'Sudesh Saini', 'IX-B', 'Chhutmalpur, near Shiv Mandir ', '04-Jul-11', NULL, '05-Jan-04', 'M', '1073', NULL, NULL, '9837896285', '', '35', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(357, '1551', 'Yuvraj Singh Rana', 'Ghansham Singh Rana', 'Survestha Thakur', 'IX-B', 'Vill-Jeewala, Distt-Saharanpur', '06-04-2017', NULL, '', 'M', '1077', NULL, NULL, '8057614101', '8534973139', '36', '1', NULL, NULL, '2017-09-01 08:03:54', NULL),
(358, '1077', 'Aaradhy Yadav', 'Sorabh Kumar', 'Neeti Kumari', 'LKG-B', 'Vill & Post- Khajuri Akbarpur, Distt- SRE', '31-Mar-15', NULL, '10-Nov-13', 'M', '21', NULL, NULL, '9758079874', '8859924190', '1', '1', NULL, NULL, '2017-09-01 06:58:41', NULL),
(359, '1564', 'Aarav Kamboj', 'Pradeep Kumar', 'Priynaka Kamboj', 'LKG-A', 'Vill-Damodrabad, Post-Biharigarh, Distt-Saharanpur', '07-07-2017', NULL, '', 'M', '24', NULL, NULL, '9758791945', '9758762058', '1', '1', NULL, NULL, '2017-09-03 09:49:38', NULL),
(360, '1383', 'Aarv Dhiman', 'Amit Kumar', 'Manisha', 'LKG-A', 'Vill-Kishangarh Colony, Biharigarh, Distt-SRE', '11-Apr-16', NULL, '29-Jan-13', 'M', '34', NULL, NULL, '9412557168', '7037237953', '2', '1', NULL, NULL, '2017-09-03 09:49:38', NULL),
(361, '1381', 'Aashka Parveen', 'Mohd. Toseef', 'Dilkhusha', 'LKG-A', 'Dehradun Road, Chhutmalpur, Distt-SRE', '11-Apr-16', NULL, '15-Sep-12', 'F', '39', NULL, NULL, '9759036563', '8869068731', '3', '1', NULL, NULL, '2017-09-03 09:49:38', NULL),
(362, '1159', 'Abhay Pratap Singh', 'Harsh Vardhan Singh', 'Shalini Pundir', 'LKG-A', 'Vill & Post-Bahera Sandal Singh, Distt-SRE', '08-Apr-15', NULL, '27-Apr-12', 'M', '61', NULL, NULL, '9411211086', '8057000715', '4', '1', NULL, NULL, '2017-09-03 09:49:38', NULL),
(363, '1379', 'Aditya Saini', 'Amrish Saini', 'Meenakshi Saini', 'LKG-B', 'Fatehpur Bhado kalsiya road, Chhutmalpur, Distt-SRE', '04-Apr-16', NULL, '13-Nov-12', 'M', '111', NULL, NULL, '9411295795', '9012694264', '2', '1', NULL, NULL, '2017-09-01 06:58:41', NULL),
(364, '1171', 'Akshara Kamboj', 'Sumit Kamboj', 'Meenu Kamboj', 'LKG-A', 'Vill-Chanchak, Post-Sherpur Khanazadpur, Distt-SRE', '09-Apr-15', NULL, '', 'F', '138', NULL, NULL, '9719445960', '9837142164', '5', '1', NULL, NULL, '2017-09-03 09:49:38', NULL),
(365, '1469', 'Alina Rajput', 'Javed', 'Afhifa', 'LKG-A', 'Vill-Sibhalki Gujjar, Post-Chhutmalpur, Distt-Sre', '19-Jul-16', NULL, '15-05-2012', 'F', '153', NULL, NULL, '8394889929', '7500001957', '6', '1', NULL, NULL, '2017-09-03 09:49:38', NULL),
(366, '1464', 'Amritansh', 'Praveen Kumar', 'Meenu Kamboj', 'LKG-A', 'Vill-Sherpur Khanazadpur, Distt-SRE', '29-Mar-16', NULL, '12-Nov-12', 'F', '179', NULL, NULL, '9720458064', '9759339425', '7', '1', NULL, NULL, '2017-09-03 09:49:38', NULL),
(367, '1620', 'Ananya Yadav', 'Sonu Kumar', 'Radha', 'LKG-A', 'Vill-Razapur, Post-Chhutmalpur, Distt-Saharanpur', '31-05-2017', NULL, '24-03-2013', 'F', '189', NULL, NULL, '8126505498', '9627898951', '8', '1', NULL, NULL, '2017-09-03 09:49:38', NULL),
(368, '1570', 'Angad Singh Munder', 'Vinod Kumar Chaudhary', 'Gurumeet kaur', 'LKG-B', 'Vill-Hussainpur, Niwada, Post-Chhutmalpur, distt-Saharanpur', '17-04-2017', NULL, '13-04-2013', 'M', '190', NULL, NULL, '9675661154', '9690181136', '3', '1', NULL, NULL, '2017-09-01 06:58:41', NULL),
(369, '1313', 'Ansh Rana', 'Sunil Kumar', 'Seema Devi', 'LKG-B', 'Vill-Buddhakhera Pundir Post-Chhutmalpur, Distt-SRE', '01-Apr-16', NULL, '09-Aug-13', 'M', '211', NULL, NULL, '9756565904', '9758242536', '4', '1', NULL, NULL, '2017-09-01 06:58:41', NULL),
(370, '1254', 'Ansh Verma', 'Ankit Verma', 'Dolly Verma', 'LKG-B', 'Vill- chhutmalpur Roorkee Road, Distt. Saharanpur', '02-Jul-15', NULL, '06-Oct-12', 'M', '213', NULL, NULL, '9997391362', '9759309640', '5', '1', NULL, NULL, '2017-09-01 06:58:41', NULL),
(371, '1276', 'Anshika Saini', 'Manoj Kumar', 'Pooja Saini', 'LKG-B', 'Vill-Kurdikhera, dist-SRE', '10-Sep-15', NULL, '05-Jan-13', 'F', '221', NULL, NULL, '9758915246', '9927394361', '6', '1', NULL, NULL, '2017-09-01 06:58:42', NULL),
(372, '1060', 'Anushka Singh', 'Parveen Kumar', 'Rajeshwari ', 'LKG-A', 'Vill- Halwana, Post- Chhutmalpur, Distt- SRE', '25-Mar-15', NULL, '24-Nov-12', 'F', '242', NULL, NULL, '9759312574/9412849189', '9761906205', '9', '1', NULL, NULL, '2017-09-03 09:49:38', NULL),
(373, '1329', 'Apoorv Tyagi', 'Atul Kumar Tyagi', 'Supriya Tyagi', 'LKG-B', 'Bansal Vihar Colony, D.dun Road, Chhutmalpur, Distt-SRE', '05-Apr-16', NULL, '20-Dec-12', 'M', '246', NULL, NULL, '9411079010', '', '7', '1', NULL, NULL, '2017-09-01 06:58:42', NULL),
(374, '1410', 'Apoorva Dhiman', 'Mohit Dhiman', 'Reena Dhiman', 'LKG-A', 'Shiv Colony,D.dun Road,Fatehpur Chhutmalpur', '18-Apr-16', NULL, '10-Jul-12', 'F', '247', NULL, NULL, '9759575293', '9084993555', '10', '1', NULL, NULL, '2017-09-03 09:49:38', NULL),
(375, '1178', 'Aradhya Sharma', 'Mohit Kumar Sharma', 'Dolly Sharma', 'LKG-A', 'Hind Vihar Colony, Halwana Road, Chhutmalpur,SRE', '09-Apr-15', NULL, '29-Jul-12', 'F', '252', NULL, NULL, '9759561026', '9759399959', '11', '1', NULL, NULL, '2017-09-03 09:49:38', NULL),
(376, '1502', 'Arav Saini', 'Lalit Kumar', 'Ruchi Devi', 'LKG-B', 'Vill-Alawalpur, Post-Chhutmalpur, Distt-Haridwar', '05-04-2017', NULL, '29-08-2015', 'M', '253', NULL, NULL, '9719929680', '8650110300', '8', '1', NULL, NULL, '2017-09-01 06:58:42', NULL),
(377, '1378', 'Arnav Saini', 'Sandeep Saini', 'Mamta Saini', 'LKG-B', 'Fatehpur Bhado kalsiya road, Chhutmalpur, Distt-SRE', '04-Apr-16', NULL, '08-Jun-13', 'M', '266', NULL, NULL, '9927140838', '9837282155', '9', '1', NULL, NULL, '2017-09-01 06:58:42', NULL),
(378, '1390', 'Ashwani', 'Arvind Kumar', 'Ravita', 'LKG-A', 'Vill-Halwana, Dist-SRE', '12-Apr-16', NULL, '03-Jul-12', 'M', '305', NULL, NULL, '9917073583', '8057613990', '12', '1', NULL, NULL, '2017-09-03 09:49:38', NULL),
(379, '1406', 'Atiya Zebi', 'Mohd.Shahid', 'Yasmin Zebi', 'LKG-A', 'D.Dun road,chhutmalpur,Distt-Sre', '16-Apr-16', NULL, '01-Jan-12', 'F', '315', NULL, NULL, '9719802410', '9837232082', '13', '1', NULL, NULL, '2017-09-03 09:49:38', NULL),
(380, '1256', 'Aviraj Kamboj', 'Sandeep Kamboj', 'Nisha Kamboj', 'LKG-B', 'Viil+Post-Sherpur Khanazadpur,Distt.Saharanpur', '07-Jul-15', NULL, '22-Jan-12', 'M', '323', NULL, NULL, '9720737833', '', '10', '1', NULL, NULL, '2017-09-01 06:58:42', NULL),
(381, '1428', 'Avtar Singh', 'Anil Kumar', 'Neha Singh', 'LKG-B', 'Vill-Behra Sandal singh, Distt-SRE', '25-Apr-16', NULL, '24-Apr-13', 'M', '329', NULL, NULL, '7669177524', '9084616556', '11', '1', NULL, NULL, '2017-09-01 06:58:42', NULL),
(382, '1125', 'Ayush Saini', 'Rupendra Saini', 'Upasna Devi', 'LKG-A', 'Vill-Mirzapur Poul, Post-Mirzapur, Distt-SRE', '06-Apr-15', NULL, '10-Jul-12', 'F', '342', NULL, NULL, '9761231986', '8954775516', '14', '1', NULL, NULL, '2017-09-03 09:49:38', NULL),
(383, '1308', 'Azim Ahmad', 'Ajaaz Ali', 'Tarnnum Afra', 'LKG-B', 'Arya Nagar, Chhutmalpur, Distt-SRE', '01-Apr-16', NULL, '04-Jul-12', 'M', '353', NULL, NULL, '9997686586', '9720324616', '12', '1', NULL, NULL, '2017-09-01 06:58:42', NULL),
(384, '1622', 'Darsh Sharma', 'Johney Sharma', 'Sakshi Sharma', 'LKG-A', 'Vill-Chaulli, Khubbanpur, Distt-Haridwar', '03-07-2017', NULL, '11-10-2012', 'M', '374', NULL, NULL, '9897268515', '9045331377', '15', '1', NULL, NULL, '2017-09-03 09:49:38', NULL),
(385, '1110', 'Dev Mittal', 'Shailesh Mittal', 'Neelam Mittal', 'LKG-A', 'Gandhi Colony, Chhutmalpur, SRE', '06-Apr-15', NULL, '10-Dec-12', 'M', '381', NULL, NULL, '9758674921', '9761256587', '16', '1', NULL, NULL, '2017-09-03 09:49:38', NULL),
(386, '1463', 'Elma Naaz', 'Mukim Ahmed', 'Farzana Parveen', 'LKG-A', 'D.Dun road Near Bharat Dharam Kanta, chhutmalpur,Distt-Sre', '13-Jul-16', NULL, '', 'F', '417', NULL, NULL, '9917870970', '7500453731', '17', '1', NULL, NULL, '2017-09-03 09:49:38', NULL),
(387, '1405', 'Gauransh saini', 'Vipin Kumar Saini', 'Reeta Saini', 'LKG-A', 'Vill-Alawalpur, Post-Chhutmalpur, Distt-Haridwar', '13-Apr-16', NULL, '18-Dec-12', 'M', '429', NULL, NULL, '9758069238, 9758069238', '9758985880, 7409200000', '18', '1', NULL, NULL, '2017-09-03 09:49:39', NULL),
(388, '1315', 'Harshita Saini', 'Anil Kumar', 'Rekha Saini', 'LKG-B', 'Krishna College, Kamalpur, Post-Chhutmalpur, Distt-SRE', '02-Apr-16', NULL, '02-Dec-13', 'F', '462', NULL, NULL, '9368351392', '', '13', '1', NULL, NULL, '2017-09-01 06:58:42', NULL),
(389, '1212', 'Hilmi Rao', 'Rao Fasahat', 'Raziya', 'LKG-B', 'Vill & Post- Kheri Sikohpur, Distt- Haridwar', '18-Apr-15', NULL, '12-Nov-11', 'F', '470', NULL, NULL, '9759999934', '', '14', '1', NULL, NULL, '2017-09-01 06:58:42', NULL),
(390, '1400', 'Ishika', 'BhanuPratap Prajapati', 'Suman', 'LKG-B', 'Jain Bagh, Chhutmalpur, Distt-SRE', '13-Apr-16', NULL, '11-Aug-12', 'F', '488', NULL, NULL, '7078684065', '9897340485', '15', '1', NULL, NULL, '2017-09-01 06:58:42', NULL),
(391, '1532', 'Jessica Chauhan', 'Shiv Kumar', 'Komal', 'LKG-B', 'Vill-Khubbanpur, Post-Bhagwanpur, Distt-Haridwar', '10-04-2017', NULL, '08-07-2011', 'F', '502', NULL, NULL, '8909316610', '8273805334', '16', '1', NULL, NULL, '2017-09-01 06:58:42', NULL),
(392, '1423', 'Kanhayya', 'Braham Pal Singh', 'Laxmi Devi', 'LKG-A', 'Vill-Alawalpur, Post-Chhutmalpur, Distt-Haridwar', '21-Apr-16', NULL, '23-Dec-12', 'M', '513', NULL, NULL, '885918817', '8859212187', '19', '1', NULL, NULL, '2017-09-03 09:49:39', NULL),
(393, '1250', 'Lakshay Dhiman', 'Nipin Dhiman', 'Kiran Dhiman', 'LKG-A', 'Harijan colony Lane No.4 Chhutmalpur,Saharanpur', '01-Jul-15', NULL, '26-Nov-12', 'M', '562', NULL, NULL, '9627631009', '9759389001', '20', '1', NULL, NULL, '2017-09-03 09:49:39', NULL),
(394, '1266', 'Lakshay Saini', 'Vipin Saini', 'Pinki Saini', 'LKG-B', 'Vill-Mustaffapur,Post-Mizaffrabad,Disst.Saharanpur', '13-Jul-15', NULL, '22-Dec-12', 'M', '563', NULL, NULL, '9719073250', '9719249292', '17', '1', NULL, NULL, '2017-09-01 06:58:42', NULL),
(395, '1332', 'Lakshay Shiva Vashisht', 'Kamal Vashisht', 'Sangeeta Vashisht', 'LKG-B', 'Punjabi Colony, Chhutmalpur, Distt-SRE', '05-Apr-16', NULL, '12-Mar-12', 'M', '565', NULL, NULL, '8923458899', '9458508883', '18', '1', NULL, NULL, '2017-09-01 06:58:42', NULL),
(396, '1357', 'Lucky Saini', 'Sanjay Kumar Saini', 'Manoj Kumari', 'LKG-B', 'Vill-Shekhwala, Takipur Post-Sherpur, Distt-Saharanpur', '08-Apr-16', NULL, '16-Aug-12', 'M', '580', NULL, NULL, '9837512018', '9690142911', '19', '1', NULL, NULL, '2017-09-01 06:58:42', NULL),
(397, '1508', 'Manvi Saini', 'Vinit Saini', 'Sonam', 'LKG-A', 'Arya Nagar, Chhutmalpur, Distt-Saharanpur', '06-04-2017', NULL, '13-10-2012', 'F', '606', NULL, NULL, '9720224106', '8958888288', '21', '1', NULL, NULL, '2017-09-03 09:49:39', NULL),
(398, '1055', 'Mayank Pal', 'Narendra Pal', 'Savita Pal', 'LKG-B', 'Vill. & Post. Gagalheri, DDN Road, SRE', '21-Mar-15', NULL, '06-Sep-11', 'M', '615', NULL, NULL, '8923674687', '9627251014', '20', '1', NULL, NULL, '2017-09-01 06:58:42', NULL),
(399, '1455', 'Mishthi Khurana', 'Sachin Khurana', 'Kamya Khurana', 'LKG-A', 'Punjabi Colony Chhutmalpur', '07-Jul-16', NULL, '07-Aug-13', 'F', '622', NULL, NULL, '8449184444', '9359004050', '22', '1', NULL, NULL, '2017-09-03 09:49:39', NULL),
(400, '1412', 'Mohd. Ali', 'Tahir Ali', 'Rubina Ali', 'LKG-A', 'Dehradun Road ,Biharigarh,Distt.Saharanpur', '13-Nov-03', NULL, '02-Aug-11', 'M', '644', NULL, NULL, '9675194409', '9058771236', '23', '1', NULL, NULL, '2017-09-03 09:49:39', NULL),
(401, '1352', 'Naksh Kamboj', 'Kuldeep Kamboj', 'Akshma Kamboj', 'LKG-A', 'Vill-Nanakgarh Post-Biharigarh, Distt-Haridwar', '07-Apr-16', NULL, '13-Jan-13', 'M', '687', NULL, NULL, '9719953210', '9457048049', '24', '1', NULL, NULL, '2017-09-03 09:49:39', NULL),
(402, '1211', 'Navya Aggarwal', 'Manoj Aggarwal', 'Babita Aggarwal', 'LKG-A', 'Kamaalpur Road, Chhutmalpur', '18-Apr-15', NULL, '11-Nov-12', 'F', '697', NULL, NULL, '9760154765', '9634677711', '25', '1', NULL, NULL, '2017-09-03 09:49:39', NULL),
(403, '1049', 'Nayan Aggarwal', 'Sourabh Kumar', 'Neha Aggarwal', 'LKG-B', 'Bansal Vihar Colony,H.No. 5,Chhutmalpur,Near Bhartiya State Bank, SRE', '17-Mar-15', NULL, '24-Jul-12', 'M', '700', NULL, NULL, '9761623556', '9719961329', '21', '1', NULL, NULL, '2017-09-01 06:58:42', NULL),
(404, '1447', 'Prajjwal Chopra', 'Arvind Chopra', 'Richa Chopra', 'LKG-B', 'Shakti Nagar Near Madho Nagar, Saharanpur', '12-May-16', NULL, '17-Feb-13', 'M', '733', NULL, NULL, '8171651666', '9267405650', '22', '1', NULL, NULL, '2017-09-01 06:58:42', NULL),
(405, '1472', 'Pranav Goyal', 'Vijay Goyal', 'Khushali Goyal', 'LKG-B', 'Harijan Colony Gali no. 1, Chhutmalpur', '08-Sep-16', NULL, '08-Dec-12', 'M', '735', NULL, NULL, '8791182228', '8445599151', '23', '1', NULL, NULL, '2017-09-01 06:58:42', NULL),
(406, '1290', 'Prayag Kamboj', 'Pradeep Kamboj', 'Aadesh Kumari', 'LKG-B', 'Vill-Aurangabad Post-Sherpur Khanazadpur, Distt-SRE', '22-Mar-16', NULL, '16-Feb-13', 'M', '742', NULL, NULL, '9720652719', '9536412277', '24', '1', NULL, NULL, '2017-09-01 06:58:42', NULL),
(407, '1391', 'Preeti panwar', 'Anuj Panwar', 'Reeta Rani', 'LKG-B', 'Vill-Sanjay Colony Behat, Distt-SRE', '12-Apr-16', NULL, '01-Feb-12', 'F', '744', NULL, NULL, '9927261550', '9520320128', '25', '1', NULL, NULL, '2017-09-01 06:58:42', NULL),
(408, '1507', 'Priyanshi Saini', 'Vinit Saini', 'Sonam', 'LKG-B', 'Arya Nagar, Chhutmalpur, Distt-Saharanpur', '06-04-2017', NULL, '01-09-2011', 'F', '752', NULL, NULL, '9720224106', '8958888288', '26', '1', NULL, NULL, '2017-09-01 06:58:42', NULL),
(409, '1121', 'Ranveer', 'Sandeep Kumar', 'Anjali', 'LKG-A', 'Amardeep Colony, Saharanpur Road, Chhutmalpur, SRE', '06-Apr-15', NULL, '09-Aug-12', 'M', '769', NULL, NULL, '9897565782', '', '26', '1', NULL, NULL, '2017-09-03 09:49:39', NULL),
(410, '1414', 'Rao Hamza', 'Shahnawaz Rao', 'Gazala Rao', 'LKG-A', 'Rao Market Sabzi Mandi, Chhutmalpur', '20-Apr-16', NULL, '19-May-13', 'M', '784', NULL, NULL, '9758329200', '8923958325', '27', '1', NULL, NULL, '2017-09-03 09:49:39', NULL),
(411, '1360', 'Saksham Chauhan', 'Sushil Kumar', 'Reena', 'LKG-A', 'Vill-Shahid Wala Grant Post-Buggawala, Distt-Haridwar', '08-Apr-16', NULL, '17-Jul-12', 'M', '834', NULL, NULL, '9759562510', '', '28', '1', NULL, NULL, '2017-09-03 09:49:39', NULL);
INSERT INTO `studentmgmts` (`id`, `admission_no`, `student_name`, `student_father_name`, `student_mother_name`, `student_class_section`, `student_address`, `student_joining_date`, `student_images`, `student_dob`, `student_gender`, `student_username`, `student_password`, `student_email`, `student_mob1`, `student_mob2`, `student_rollno`, `sessionid`, `deleted_at`, `created_at`, `updated_at`, `code`) VALUES
(412, '1295', 'Samridh Singh Bhandari', 'Shamsher Singh Bhandari', 'Basanti Bhandari', 'LKG-B', 'Gali No-1 Harijan Colony, Chhutmalpur, Distt-SRE', '26-Mar-16', NULL, '23-Jul-12', 'M', '854', NULL, NULL, '9411744083', '9756051337', '27', '1', NULL, NULL, '2017-09-01 06:58:42', NULL),
(413, '1279', 'Satvik Kamboj', 'Amit Kamboj', 'Soniya Kamboj', 'LKG-A', 'Swagatan Cloth House, Near H.D.F.C. Bank Chhutmalpur, Distt-SRE', '16-Mar-16', NULL, '14-May-13', 'M', '870', NULL, NULL, '7533925854', '9368157031', '29', '1', NULL, NULL, '2017-09-03 09:49:39', NULL),
(414, '1603', 'Sayyed Yavar Hussain', 'Rais Hussain', 'Nahib Zenab', 'LKG-A', 'Vill-Mahmoodpur, Tiwaya Sona, Post-Harora, Distt-Saharanpur', '28-04-2017', NULL, '', 'M', '877', NULL, NULL, '8273780024', '7900725357', '30', '1', NULL, NULL, '2017-09-03 09:49:39', NULL),
(415, '1580', 'Shagun', 'Muneesh Kumar', 'Mamta', 'LKG-A', 'Vill-Mandawar, Post-Chhutmalpur, Distt-Haridwar', '18-04-2017', NULL, '', 'M', '880', NULL, NULL, '9761246959', '', '31', '1', NULL, NULL, '2017-09-03 09:49:39', NULL),
(416, '1169', 'Shanvi Sharma', 'Shree Kant Sharma', 'L. Rakhi Sharma', 'LKG-A', 'Hind Vihar Colony, Halwana Road, Chhutmalpur, SRE', '08-Apr-15', NULL, '', 'F', '881', NULL, NULL, '9368193741', '9997909192', '32', '1', NULL, NULL, '2017-09-03 09:49:39', NULL),
(417, '1129', 'Shourya Batra', 'Rahul Batra', 'Harshita Batra', 'LKG-B', 'Punjabi Colony, Chhutmalpur, SRE', '06-Apr-15', NULL, '09-Mar-12', 'M', '908', NULL, NULL, '9897234478', '8791550131', '28', '1', NULL, NULL, '2017-09-01 06:58:42', NULL),
(418, '1259', 'Shreya', 'Nitesh Pal', 'Kavita Pal', 'LKG-B', 'Vill-Gandevada,Post-Chhutmalpur,Disst-Saharanpur', '08-Jul-15', NULL, '21-Sep-12', 'F', '912', NULL, NULL, '9045228373', '8923436509', '29', '1', NULL, NULL, '2017-09-01 06:58:42', NULL),
(419, '1382', 'Shubh Yadav', 'Kuldeep Singh', 'Geeta Devi', 'LKG-A', 'Vill-Khajuri Akbarpur, Distt-SRE', '11-Apr-16', NULL, '', 'M', '918', NULL, NULL, '8954573190', '9897588848', '33', '1', NULL, NULL, '2017-09-03 09:49:39', NULL),
(420, '1168', 'Tanushree Sharma', 'Amarkant Sharma', 'Rupa Sharma', 'LKG-B', 'Hind Vihar Colony, Halwana Road, Chhutmalpur, SRE', '08-Apr-15', NULL, '02-Feb-13', 'F', '971', NULL, NULL, '8445484444', '9997909192', '30', '1', NULL, NULL, '2017-09-01 06:58:42', NULL),
(421, '1562', 'Ulfat', 'Zulfkar', 'Shaheen Jahan', 'LKG-A', 'Vill-Mukarrampur Urf Kalewala, Chhutmalpur, Distt-Saharanpyur', '04-04-2017', NULL, '21-07-2012', 'M', '987', NULL, NULL, '9411077562', '9456294521', '34', '1', NULL, NULL, '2017-09-03 09:49:39', NULL),
(422, '1316', 'Vaishnavi Goyal', 'Anoop Goyal', 'Ruchi Goyal', 'LKG-B', 'Vill-Main Bazar Biharigarh, Distt-SRE', '02-Apr-16', NULL, '29-Nov-12', 'F', '1007', NULL, NULL, '7830670041', '9897032011', '31', '1', NULL, NULL, '2017-09-01 06:58:42', NULL),
(423, '1639', 'Vanshika Yadav', 'Bahnu Pratap Singh', 'Devika Yadav', 'LKG-B', 'Thana Fatehpur, Distt-Saharanpur', '13-07-2017', NULL, '28-11-2012', 'F', '1024', NULL, NULL, '9719103007', '8191816337', '32', '1', NULL, NULL, '2017-09-01 06:58:42', NULL),
(424, '1312', 'Virat Chauhan', 'Sachin Kumar', 'Preeti Devi', 'LKG-B', 'Vill-Buddhakhera Pundir Post-Chhutmalpur, Distt-SRE', '01-Apr-16', NULL, '24-Jan-13', 'M', '1049', NULL, NULL, '9719072023', '7500840032', '33', '1', NULL, NULL, '2017-09-01 06:58:42', NULL),
(425, '1338', 'Virat Kamboj', 'Anil Kumar', 'Ekta Kamboj', 'LKG-A', 'Vill-Gherkarma Post-Biharigarh, Distt-SRE', '06-Apr-16', NULL, '02-Feb-12', 'F', '1050', NULL, NULL, '9758559338', '9761226544', '35', '1', NULL, NULL, '2017-09-03 09:49:39', NULL),
(426, '1226', 'Zakiya Rao', 'Rao Sammun', 'Sahib Rao', 'LKG-B', 'Vill- Guddam, Post- Chhutmalpur, Distt- SRE', '09-Apr-15', NULL, '07-Apr-12', 'F', '1075', NULL, NULL, '9927091670', '9720860000', '34', '1', NULL, NULL, '2017-09-01 06:58:42', NULL),
(427, '1434', 'Zehra Rao', 'Shafat Ali', 'Fauzia Rao', 'LKG-B', 'Vill & Post-Kheri Sikohpur, Distt-Haridwar', '29-Apr-16', NULL, '12-Dec-13', 'F', '1079', NULL, NULL, '9058787891', '9084999524', '35', '1', NULL, NULL, '2017-09-01 06:58:42', NULL),
(428, '1407', 'Zunaid', 'Shajid Ali', 'Sahista Urf Gufisha', 'LKG-B', 'Vill-Chauli, Sahabudin post-Khubbanpur, Distt-Haridwar', '16-Apr-16', NULL, '04-Jun-12', 'M', '1081', NULL, NULL, '9808281111', '8445663366', '36', '1', NULL, NULL, '2017-09-01 06:58:42', NULL),
(429, '1493', 'Aadya Singhal', 'Vivek Krishan Singhal', 'Megha Singhal', 'NUR-B', 'Near Telephone Exchange, Shiv Mandir, Chhutmalpur, Distt-Saharanpur', '14-03-2017', NULL, '18-10-2013', 'F', '9', NULL, NULL, '9359559715', '7599001988', '1', '1', NULL, NULL, '2017-09-03 09:50:09', NULL),
(430, '1558', 'Aarav Goyal', 'Nitin goyal', 'Archna Goyal', 'NUR-A', 'Maya Wali Colony, gali No.2, Chhutmalpur, Distt-Saharanpur', '15-04-2017', NULL, '02-03-2013', 'M', '22', NULL, NULL, '9758815235', '', '1', '1', NULL, NULL, '2017-09-03 09:50:01', NULL),
(431, '1255', 'Aarav Saini', 'Dinesh Saini', 'Prachi Saini', 'NUR-B', 'Shiv Colony, FatehpurBhado,Chhutmalpur Distt. Saharanpur', '03-Jul-15', NULL, '', 'M', '27', NULL, NULL, '9760782807', '9927600149', '2', '1', NULL, NULL, '2017-09-03 09:50:09', NULL),
(432, '1471', 'Aariz aziz', 'Gulfam Aziz', 'Ayesha Aziz', 'NUR-B', 'Niazi Cottage, Dehradun Road. Chhutmalpur', '11-Apr-16', NULL, '27-Jan-13', 'M', '30', NULL, NULL, '9719565042', '9719281895', '3', '1', NULL, NULL, '2017-09-03 09:50:09', NULL),
(433, '1362', 'Aarv Sikwal', 'Mohan', 'Deepa', 'NUR-B', 'Maharana Partap Colony Gali No.-3, Chhutmalpur, Distt-SRE', '08-Apr-16', NULL, '01-Oct-13', 'M', '36', NULL, NULL, '8006613876', '8006284253', '4', '1', NULL, NULL, '2017-09-03 09:50:09', NULL),
(434, '1473', 'Aarvi Saini', 'Sandeep Kumar', 'Meenu Saini', 'NUR-A', 'Vill.Alawalpur, Post Chhutmalpur', '17-Aug-16', NULL, '11-Nov-13', 'F', '37', NULL, NULL, '9639739090', '7409602561', '2', '1', NULL, NULL, '2017-09-03 09:50:01', NULL),
(435, '1456', 'Aasiya Daud Hussain', 'Daud Hussain', 'Sana Khushnud', 'NUR-B', 'Vill-Syed Majra Post-Harora, distt-SRE', '07-Jul-16', NULL, '', 'F', '41', NULL, NULL, '9997794728', '8439481525', '5', '1', NULL, NULL, '2017-09-03 09:50:09', NULL),
(436, '1511', 'Aastha Chauhan', 'Maneet Kumar', 'Babita Rani', 'NUR-A', 'Vill-Banjarewala Grant, Post-Tanko Sunderpur, Distt-Haridwar', '07-04-2017', NULL, '19-07-2013', 'F', '43', NULL, NULL, '9719831616', '9627924448', '3', '1', NULL, NULL, '2017-09-03 09:50:01', NULL),
(437, '1594', 'Aastha Singh', 'Bhagat Singh Chaudhary', 'Tapasya', 'NUR-A', 'Vill-Chaudaheri, Post-Chhutmalpur, distt-Saharanpur', '24-04-2017', NULL, '14-01-2014', 'M', '46', NULL, NULL, '9917997741', '8057772685', '4', '1', NULL, NULL, '2017-09-03 09:50:01', NULL),
(438, '1579', 'Aayush', 'Neetu Kumar', 'Manju', 'NUR-B', 'Vill-Mandawar, Post-Chhutmalpur, Distt-Haridwar', '18-04-2017', NULL, '', 'M', '49', NULL, NULL, '9761246959', '', '6', '1', NULL, NULL, '2017-09-03 09:50:09', NULL),
(439, '1484', 'Abdul Ahad', 'Mohd Nadeem', 'Sabina Parveen', 'NUR-A', 'Vill-Mathana, D.dun Road, Chhutmalpur, Distt-Saharanpur', '23-03-2017', NULL, '14-06-2012', 'M', '55', NULL, NULL, '9719165403', '7830209436', '5', '1', NULL, NULL, '2017-09-03 09:50:01', NULL),
(440, '1454', 'Abhigyan Singh', 'Sachin Chaudhary', 'Ishu Chaudhary', 'NUR-A', 'Sant Nagar Near Tanki Wali gali, chhutmalpur, distt-SRE', '08-Jul-16', NULL, '08-Mar-14', 'M', '64', NULL, NULL, '8864933333', '9917414185', '6', '1', NULL, NULL, '2017-09-03 09:50:01', NULL),
(441, '1453', 'Abhimanyu Saini', 'Vijay Saini', 'Aruna Saini', 'NUR-B', 'Vill-Satpura Post-Kurdikhera, Dist-SRE', '04-Jul-16', NULL, '03-Dec-12', 'M', '68', NULL, NULL, '9758314436', '9720838488', '7', '1', NULL, NULL, '2017-09-03 09:50:09', NULL),
(442, '1517', 'Abhinav Saini', 'Dinesh Kumar', 'Babita Saini', 'NUR-A', 'Amardeep Colony, Chhutmalpur, Distt-Saharanpur', '08-04-2017', NULL, '17-10-2013', 'M', '73', NULL, NULL, '9759053375', '7037201201', '7', '1', NULL, NULL, '2017-09-03 09:50:01', NULL),
(443, '1566', 'Aditya', 'Mainpal', 'Babita', 'NUR-B', 'Vill-Nanka, Post-Chhutmalpur, Distt-Saharnpur', '14-04-2017', NULL, '25-08-2012', 'M', '106', NULL, NULL, '9627050335', '8193960739', '8', '1', NULL, NULL, '2017-09-03 09:50:09', NULL),
(444, '1498', 'Akshit Yadav', 'Jitender Kumar', 'Laxmi', 'NUR-B', 'Vill-Mandebas, Distt-Saharanpur', '03-04-2017', NULL, '', 'M', '145', NULL, NULL, '9675038336', '9927147317', '9', '1', NULL, NULL, '2017-09-03 09:50:09', NULL),
(445, '1466', 'Ali', 'Shahjad Ali', 'Ishwar Jahan', 'NUR-B', 'D.dun Road New Basti, Chhutmalpur, Distt-Sre', '04-Jul-16', NULL, '17-Mar-13', 'M', '149', NULL, NULL, '9758948820', '9410242746', '10', '1', NULL, NULL, '2017-09-03 09:50:09', NULL),
(446, '1395', 'Alisha', 'Mohd. Ayub Ansari', 'Anwari', 'NUR-A', 'Vill-Harora, Distt-SRE', '11-Apr-16', NULL, '', 'F', '155', NULL, NULL, '9756496326', '', '8', '1', NULL, NULL, '2017-09-03 09:50:01', NULL),
(447, '1394', 'Altamas', 'Tanveer Alam', 'Sadaf Naaz', 'NUR-B', 'Vill-Chappur Sher Afganpur, Post-Khubbanpur, Distt-SRE', '12-Apr-16', NULL, '07-Jul-12', 'M', '161', NULL, NULL, '9997815146', '9219721537', '11', '1', NULL, NULL, '2017-09-03 09:50:09', NULL),
(448, '1618', 'Altamash', 'Inaam', 'Samreen', 'NUR-B', 'Vill-Rehri Mustakampur, Distt-Saharanpur', '16-05-2017', NULL, '25-12-2014', 'M', '162', NULL, NULL, '9927666172', '9760880948', '12', '1', NULL, NULL, '2017-09-03 09:50:09', NULL),
(449, '1626', 'Animesh Pratap singh', 'Parveen Kumar', 'Rajeshwari', 'NUR-A', 'Vill-Halwana, Post-Chhutmalpur, Distt-Saharanpur', '03-07-2017', NULL, '15-11-2015', 'M', '193', NULL, NULL, '9412849189', '8191863445', '9', '1', NULL, NULL, '2017-09-03 09:50:01', NULL),
(450, '1503', 'Ansh', 'Rajendra', 'Meenakshi', 'NUR-A', 'Fatehpur Bhado, Post-Chhutamalpur, Distt-Saharanpur', '06-04-2017', NULL, '12-03-2012', 'M', '204', NULL, NULL, '9761020410', '9720065837', '10', '1', NULL, NULL, '2017-09-03 09:50:01', NULL),
(451, '1486', 'Arnav Saini', 'Parveen Saini', 'Ruby Saini', 'NUR-B', 'Vill-Kabirpur, Post-Niyamatpur, Distt-Saharanpur', '23-03-2017', NULL, '', 'M', '264', NULL, NULL, '7500353376', '8192827108', '13', '1', NULL, NULL, '2017-09-03 09:50:09', NULL),
(452, '1359', 'Aryan Soni', 'Amit Kumar', 'Isha', 'NUR-A', 'Punjabi Colony, Chhutmalpur, Distt-SRE', '08-Apr-16', NULL, '24-Jun-13', 'M', '295', NULL, NULL, '8869047110', '8864847902', '11', '1', NULL, NULL, '2017-09-03 09:50:01', NULL),
(453, '1568', 'Ashutosh Rana', 'Rajneesh Rana', 'Sunita', 'NUR-B', 'Vill-Buddhakhera Pundir, Post-Chmarikhera, distt-Saharanpur', '11-04-2017', NULL, '19-08-2013', 'M', '304', NULL, NULL, '9557379557', '9759790381', '14', '1', NULL, NULL, '2017-09-03 09:50:10', NULL),
(454, '1451', 'Athrawa Rana', 'Yogesh Kumar', 'Monika', 'NUR-B', 'Vill-Gangali post-Chhutmalpur, Distt-SRE', '04-Jul-16', NULL, '26-Sep-13', 'M', '313', NULL, NULL, '7830343655', '9412873457', '15', '1', NULL, NULL, '2017-09-03 09:50:10', NULL),
(455, '1491', 'Aviral Saini', 'Vipin Kumar', 'Deepa Saini', 'NUR-A', 'Vill-Satpura Post-Kurrikhera, Distt-Saharanpur', '30-03-2017', NULL, '04-05-2013', 'M', '325', NULL, NULL, '9719961442', '9759240508', '12', '1', NULL, NULL, '2017-09-03 09:50:01', NULL),
(456, '1588', 'Avni', 'Dilip Kumar', 'Anita Saini', 'NUR-A', 'Aadarsh Nagar, Fatehpur, Chhutmalpur, Distt-Saharanpur', '21-04-2017', NULL, '15-01-2014', 'F', '326', NULL, NULL, '7017666686', '9634734775, 8445743467', '13', '1', NULL, NULL, '2017-09-03 09:50:01', NULL),
(457, '1567', 'Avni Khanna', 'Tarun Khanna', 'Simran Khanna', 'NUR-B', 'Home no.-119/2, Beri Bagh, Saharanpur', '15-04-2017', NULL, '20-09-2013', 'F', '327', NULL, NULL, '9927288835', '9219568299', '16', '1', NULL, NULL, '2017-09-03 09:50:10', NULL),
(458, '1629', 'Avni Saini', 'Sjashikant Saini', 'Shalu Saini', 'NUR-B', 'Fathepur, Chhutmalpur, Distt-Saharanpur', '07-07-2017', NULL, '', 'F', '328', NULL, NULL, '9761191856', '9720511928', '17', '1', NULL, NULL, '2017-09-03 09:50:10', NULL),
(459, '1304', 'Ayan Ali', 'Mohd. Naushad', 'Razia', 'NUR-A', 'Opp. Maharaja tent House, D.un Road, Chhutmalpur, Distt-SRE', '22-Mar-16', NULL, '14-Jan-13', 'M', '330', NULL, NULL, '9758583333', '9410689032', '14', '1', NULL, NULL, '2017-09-03 09:50:02', NULL),
(460, '1468', 'Ayiaan', 'Parvez Khan', 'Afsana', 'NUR-B', 'Vill-Sibhalki Gujjar, Post-Chhutmalpur, Distt-Sre', '19-Jul-16', NULL, '21-10-2013', 'M', '332', NULL, NULL, '7500002748', '7500001957', '18', '1', NULL, NULL, '2017-09-03 09:50:10', NULL),
(461, '1605', 'Ayrah', 'Anees Ahmad', 'Shabnam Praveen', 'NUR-A', 'Muslim Colony, Chhutmalpur, Distt-Saharanpur', '03-05-2017', NULL, '17-01-2013', 'F', '333', NULL, NULL, '8410510570', '9927034632', '15', '1', NULL, NULL, '2017-09-03 09:50:02', NULL),
(462, '1286', 'Ayushi Saini', 'Pradeep Kumar', 'Soniya Devi', 'NUR-A', 'Arya Nagar Kamalpur Road, Chhutmalpur, Distt-SRE', '22-Mar-16', NULL, '04-Aug-13', 'F', '351', NULL, NULL, '9412873567', '9456968406', '16', '1', NULL, NULL, '2017-09-03 09:50:02', NULL),
(463, '1656', 'Barira Rao', 'Abrar Ahmad', 'Mehraj', 'NUR-B', 'Vill-khujnawar, Distt-Saharanpur', '02-08-2017', NULL, '03-03-2013', 'F', '355', NULL, NULL, '9758015610', '9758139091', '19', '1', NULL, NULL, '2017-09-03 09:50:10', NULL),
(464, '1531', 'Dawik Kamboj', 'Anil Kumar', 'Suman Kamboj', 'NUR-A', 'Vill-Takipur, Sherpur Khanazaadpur, Distt-Saharanpur', '10-04-2017', NULL, '30-09-2013', 'M', '375', NULL, NULL, '9720557568', '9536454006', '17', '1', NULL, NULL, '2017-09-03 09:50:02', NULL),
(465, '1569', 'Dev Kamboj', 'Digvijay Singh', 'Deepka Kamboj', 'NUR-B', 'Vill-Sherpur Khanazaadpur, Distt-Saharanpur', '17-04-2017', NULL, '10-03-2014', 'M', '380', NULL, NULL, '9627055153', '', '20', '1', NULL, NULL, '2017-09-03 09:50:10', NULL),
(466, '1549', 'Dev Raj', 'Pradeep Kumar', 'Nanita', 'NUR-A', 'Vill-Aurangabad, Post-Sherpur Khanazadpur, Distt-Saharanpur', '12-04-2017', NULL, '13-11-2014', 'M', '383', NULL, NULL, '9627851189', '9675353574', '18', '1', NULL, NULL, '2017-09-03 09:50:02', NULL),
(467, '1337', 'Devanshi Kamboj', 'Anil Kumar', 'Ekta Kamboj', 'NUR-B', 'Vill-Gherkarma Post-Biharigarh, Distt-SRE', '06-Apr-16', NULL, '22-Aug-13', 'F', '393', NULL, NULL, '9758559338', '9761226544', '21', '1', NULL, NULL, '2017-09-03 09:50:10', NULL),
(468, '1448', 'Divyansh Chaudhary', 'Bhagat Singh Chaudhary', 'Preeti Chaudhary', 'NUR-A', 'Vill-Chaundheri, Post-Muazzafrabad, Distt-SRE', '05-May-16', NULL, '17-Apr-13', 'M', '410', NULL, NULL, '9761625366', '7409029115', '19', '1', NULL, NULL, '2017-09-03 09:50:02', NULL),
(469, '1611', 'Epshita Saini', 'Anuj Kumar', 'Soniya', 'NUR-B', 'Arya Nagar, Chhutmalpur, Distt-Saharanpur', '12-04-2017', NULL, '16-09-2013', 'F', '419', NULL, NULL, '9758028577', '7500153502', '22', '1', NULL, NULL, '2017-09-03 09:50:10', NULL),
(470, '1537', 'Etika Chouhan', 'Bhanu Pratap Singh', 'Preeti', 'NUR-B', 'Vill-Kamalpur, Post-Chhutmalpur, Distt-Saharanpur', '10-04-2017', NULL, '16-12-2013', 'F', '420', NULL, NULL, '9634859243', '9634859243', '23', '1', NULL, NULL, '2017-09-03 09:50:10', NULL),
(471, '1411', 'Harsh Saini', 'Anuj Kumar Saini', 'Anshu', 'NUR-B', 'Arya Nagar,Ddun Road,Near-HDFC Chhutmalpur', '18-Apr-16', NULL, '18-May-13', 'M', '454', NULL, NULL, '9758728683', '9761301065', '24', '1', NULL, NULL, '2017-09-03 09:50:10', NULL),
(472, '1462', 'Hifzan Ahmad', 'Rao Zeeshan', 'Faheem', 'NUR-A', 'Vill-Sikanderpur Bhainswal, distt-Haridwar', '12-Jul-16', NULL, '26-Jun-13', 'M', '469', NULL, NULL, '9761868666', '', '20', '1', NULL, NULL, '2017-09-03 09:50:02', NULL),
(473, '1474', 'Humera Rao', 'Rao Danish', 'Guddi Rao', 'NUR-A', 'Arya Nagar , Chhutmalpur', '20-Aug-16', NULL, '15-Nov-13', 'F', '481', NULL, NULL, '9719927806', '9761868666', '21', '1', NULL, NULL, '2017-09-03 09:50:02', NULL),
(474, '1506', 'Ismita Pegwal', 'Avnesh Kumar', 'Indra', 'NUR-A', 'Vill-Shahidwala Grant Post-Buggawala, Distt-Haridwar', '06-04-2017', NULL, '27-11-2013', 'F', '492', NULL, NULL, '9422275838', '9634131733', '22', '1', NULL, NULL, '2017-09-03 09:50:02', NULL),
(475, '1512', 'Kanak Saini', 'Parmod Kumar', 'Minakshi Saini', 'NUR-A', 'Krishna Collage, Kamalpur, Post-Chhutmalpur, Distt-Saharanpur', '07-04-2017', NULL, '13-01-2014', 'F', '512', NULL, NULL, '7055610555', '9634846462', '23', '1', NULL, NULL, '2017-09-03 09:50:02', NULL),
(476, '1595', 'Krishna Yadav', 'Vipin Kumar Yadav', 'Jyoti Yadav', 'NUR-B', 'Vill-Kutubeur Khusanne, Post-Tiwaya, distt-Saharanpur', '24-04-2017', NULL, '', 'M', '552', NULL, NULL, '9837509214', '8126075759', '25', '1', NULL, NULL, '2017-09-03 09:50:10', NULL),
(477, '1421', 'Lakshay Saini', 'Sourabh Saini', 'Rubby Saini', 'NUR-B', 'Lodhiwala,Kelawala Post-Chhutmalpur,Haridwar', '20-Apr-16', NULL, '02-Jul-13', 'M', '564', NULL, NULL, '9012272185', '9058062828', '26', '1', NULL, NULL, '2017-09-03 09:50:10', NULL),
(478, '1632', 'Mayank', 'Sukhdas', 'Priyanka', 'NUR-A', 'Vill-Sona, Post-Harora, Distt-Saharanpur', '08-05-2017', NULL, '', 'M', '612', NULL, NULL, '9761645760', '9690892077', '24', '1', NULL, NULL, '2017-09-03 09:50:02', NULL),
(479, '1655', 'Mohd Akhlad Rao', 'Abrar Ahmad', 'Mehraj', 'NUR-B', 'Vill-khujnawar, Distt-Saharanpur', '02-08-2017', NULL, '07-10-2012', 'M', '624', NULL, NULL, '9758015610', '9758139091', '27', '1', NULL, NULL, '2017-09-03 09:50:10', NULL),
(480, '1523', 'Naitik Yadav', 'Bhupendra Singh Yadav', 'Indu Yadav', 'NUR-B', 'Vill-Mandebas, Distt-Saharanpur', '08-04-2017', NULL, '09-09-2014', 'M', '686', NULL, NULL, '9759788463', '7534842832', '28', '1', NULL, NULL, '2017-09-03 09:50:10', NULL),
(481, '1327', 'Nandita Chauhan', 'Sachin Chauhan', 'Neha Chauhan', 'NUR-A', 'Vill-Khubbanpur Distt-Haridwar', '04-Apr-16', NULL, '15-Oct-13', 'F', '692', NULL, NULL, '9627700059', '8006141003', '25', '1', NULL, NULL, '2017-09-03 09:50:02', NULL),
(482, '1608', 'Pihu', 'Vinit Kumar', 'Sonia', 'NUR-B', 'Vill-Alawalpur, Post-Chhutmalpur, Distt-Haridwar', '17-04-2017', NULL, '09-10-2012', 'F', '727', NULL, NULL, '9758789804', '', '29', '1', NULL, NULL, '2017-09-03 09:50:10', NULL),
(483, '1643', 'Prabhleen Kaur', 'Puran Singh', 'Navjot Kaur', 'NUR-B', 'Vill-Badshahpur, Distt-Saharanpur', '22-07-2017', NULL, '10-03-2013', 'F', '729', NULL, NULL, '9719543654', '7300690932', '30', '1', NULL, NULL, '2017-09-03 09:50:10', NULL),
(484, '1573', 'Pranav Bansal', 'Sachin Bansal', 'Shruti Bansal', 'NUR-A', 'Harijan Colony, Gali No.3, Chhutmalpur, Distt-Saharanpur', '12-04-2017', NULL, '22-02-2014', 'M', '734', NULL, NULL, '9761922211', '9258699031', '26', '1', NULL, NULL, '2017-09-03 09:50:02', NULL),
(485, '1345', 'Puneet Galiyan', 'Jasveer Singh', 'Nisha Devi', 'NUR-A', 'Vill-Rehri Mustakampur Post-Chhutmalpur, Distt-SRE', '06-Apr-16', NULL, '14-Jan-12', 'M', '758', NULL, NULL, '9719166047', '8477859229', '27', '1', NULL, NULL, '2017-09-03 09:50:02', NULL),
(486, '1630', 'Rao Faiz', 'Rashid', 'Shama', 'NUR-A', 'Vill-Sikanderpur, Post-Bhagwanpur, Distt-Haridwar', '07-04-2017', NULL, '', 'M', '780', NULL, NULL, '9759468961', '8445165697', '28', '1', NULL, NULL, '2017-09-03 09:50:02', NULL),
(487, '1631', 'Rao Zaid', 'Ragib', 'Reshma', 'NUR-A', 'Vill-Sikanderpur, Post-Bhagwanpur, Distt-Haridwar', '07-04-2017', NULL, '', 'M', '790', NULL, NULL, '9759077810', '9759865714', '29', '1', NULL, NULL, '2017-09-03 09:50:02', NULL),
(488, '1465', 'Sameer Singh Laman', 'Sunil Kumar Laman', 'Anita Laman', 'NUR-B', 'Vill-Manduwala, Post-Khujnawar, Distt-SRE', '15-Jul-16', NULL, '29-01-2014', 'M', '850', NULL, NULL, '9720005020', '9837920080', '31', '1', NULL, NULL, '2017-09-03 09:50:10', NULL),
(489, '1490', 'Sanidhay Panwar', 'Anuj Panwar', 'Reeta Panwar', 'NUR-B', 'Sanjay Colony, Behat, Distyt-Saharanpur', '29-03-2017', NULL, '23-07-2013', 'M', '858', NULL, NULL, '9927261550', '7078505135', '32', '1', NULL, NULL, '2017-09-03 09:50:10', NULL),
(490, '1470', 'Sayyed Affan', 'Sayyed Wasi', 'Ujma', 'NUR-A', 'Opp. Union Bank Of India, D.dun Road, Chhutmalpur, Distt-SRE', '26-Jul-16', NULL, '04-May-13', 'M', '876', NULL, NULL, '9758750709', '9720027938', '30', '1', NULL, NULL, '2017-09-03 09:50:02', NULL),
(491, '1528', 'Shourya Chaudhary', 'Parveen Chaudhary', 'Rakhi Chaudhary', 'NUR-A', 'Vill-Alawalpur, Post-Chhutmalpur, Distt-Haridwar', '14-03-2017', NULL, '18-09-2013', 'M', '909', NULL, NULL, '9458588870', '8630077031', '31', '1', NULL, NULL, '2017-09-03 09:50:02', NULL),
(492, '1581', 'Siddharth', 'Joginder Kumar', 'Sangita', 'NUR-B', 'Vill-Mandawar, Post-Chhutmalpur, Distt-Haridwar', '18-04-2017', NULL, '', 'M', '924', NULL, NULL, '9761246959', '', '33', '1', NULL, NULL, '2017-09-03 09:50:10', NULL),
(493, '1480', 'Sidra rao', 'Shamim Ahmad', 'Ashma', 'NUR-A', 'Vill-Fatehpur Bhado, Post-Chhutmalpur, Distt-SRE', '01-Dec-16', NULL, '', 'F', '925', NULL, NULL, '9719823276', '7409613529', '32', '1', NULL, NULL, '2017-09-03 09:50:02', NULL),
(494, '1536', 'Syed Labbek Hussain', 'Syed Rifakat Hussain', 'Mehzabi Fatima', 'NUR-B', 'Vill-Mehmoodpur Tiwai, Syed Majra, Post-Harora, Distt-Saharanpur', '10-04-2017', NULL, '18-06-2014', 'M', '961', NULL, NULL, '8057902154', '9758289055, 9759798235', '34', '1', NULL, NULL, '2017-09-03 09:50:10', NULL),
(495, '1617', 'Tuba Rao', 'Shuban Rao', 'Sheeza Rao', 'NUR-B', 'Vill-Kheri Sikohpur, Post-Chhutmalpur. Distt-Saharanpur', '11-05-2017', NULL, '26-03-2014', 'F', '979', NULL, NULL, '', '', '35', '1', NULL, NULL, '2017-09-03 09:50:10', NULL),
(496, '1284', 'Unnati Baudh', 'Praveen Kumar', 'Mahamitra Siddharth', 'NUR-B', 'Arya Nagar, Chhutmalpur, Distt-SRE', '18-Mar-16', NULL, '23-Nov-13', 'F', '988', NULL, NULL, '9719146942', '', '36', '1', NULL, NULL, '2017-09-03 09:50:10', NULL),
(497, '1623', 'Utkarsh Rana', 'Rajiv Kumar', 'Neetu Pundir', 'NUR-B', 'Vill-Jajner, Distt-Saharanpur', '04-07-2017', NULL, '18-09-2013', 'M', '993', NULL, NULL, '8755791026', '8126192030', '37', '1', NULL, NULL, '2017-09-03 09:50:11', NULL),
(498, '1518', 'Vanika Chauhan', 'Ajay Chauhan', 'Pooja Chauhan', 'NUR-B', 'Vill-Kamalpur, Post-Chhutmalpur, Distt-Saharanpur', '08-04-2017', NULL, '', 'F', '1010', NULL, NULL, '9927901890', '9927352740', '38', '1', NULL, NULL, '2017-09-03 09:50:11', NULL),
(499, '1504', 'Vansh', 'Rajendra', 'Meenakshi', 'NUR-B', 'Fatehpur Bhado, Post-Chhutamalpur, Distt-Saharanpur', '06-04-2017', NULL, '12-03-2012', 'M', '1011', NULL, NULL, '9761020410', '9720065837', '39', '1', NULL, NULL, '2017-09-03 09:50:11', NULL),
(500, '1417', 'Vansh Kumar', 'Sushil Kumar', 'Ravita Devi', 'NUR-A', 'Vill-Aurangabad Post-Sherpur Khanazadpur, Distt-SRE', '20-Apr-16', NULL, '', 'M', '1014', NULL, NULL, '9536016847', '9720729289', '33', '1', NULL, NULL, '2017-09-03 09:50:02', NULL),
(501, '1444', 'Vihaana Pruthi', 'Ritesh Pruthi', 'Aarti Pruthi', 'NUR-B', 'Madho Nagar, Saharanpur', '10-May-16', NULL, '16-Dec-13', 'F', '1042', NULL, NULL, '9319959808', '9211297689', '40', '1', NULL, NULL, '2017-09-03 09:50:11', NULL),
(502, '1494', 'Aadhya Saini', 'Sachin Saini', 'Jyoti Saini', 'PRE NUR--', 'Near Bank Of Baroda, fatehpur Bhadoo, Chhutmalpur, Distt-Saharanpur', '02-04-2017', NULL, '', 'F', '2', NULL, NULL, '8859138982', '8395075212', '1', '1', NULL, NULL, '2017-09-01 07:58:38', NULL),
(503, '1485', 'Aarav Saini', 'Parveen Saini', 'Ruby Saini', 'PRE NUR--', 'Vill-Kabirpur, Post-Niyamatpur, Distt-Saharanpur', '23-03-2017', NULL, '28-06-2014', 'M', '26', NULL, NULL, '7500353376', '8192827108', '2', '1', NULL, NULL, '2017-09-01 07:58:38', NULL),
(504, '1628', 'Advit Dhiman', 'Priyank Dhiman', 'Anuradha Dhiman', 'PRE NUR--', 'Fatehpur, d.dun Road, Chhutmalpur, Distt-Saharanpur', '04-07-2017', NULL, '06-09-2014', 'M', '114', NULL, NULL, '9760460716', '7017337264', '3', '1', NULL, NULL, '2017-09-01 07:58:38', NULL),
(505, '1637', 'Afsheen', 'Late Alim Ahmed', 'Gulshan Parveen', 'PRE NUR--', 'Near Baal Vidhya Mandir School, Chhutmalpur, Distt-Saharanpur', '12-07-2017', NULL, '23-01-2014', 'F', '117', NULL, NULL, '9720771011', '', '4', '1', NULL, NULL, '2017-09-01 07:58:38', NULL),
(506, '1646', 'Athrav Kumar', 'Kuldeep Kumar', 'Pinki Mogha', 'PRE NUR--', 'Shiv Colony, Fatehpur, Chhutmalpur, Distt-Saharanpur', '25-07-2017', NULL, '20-10-2015', 'M', '312', NULL, NULL, '9759556644', '8057554466', '5', '1', NULL, NULL, '2017-09-01 07:58:38', NULL),
(507, '1572', 'Aviraj', 'Anuj Kumar', 'Rekha', 'PRE NUR--', 'Vill-Alawalpur, Post-Chhutmalpur, Distt-Haridwar', '17-04-2017', NULL, '09-04-2015', 'M', '322', NULL, NULL, '9458508511', '9456633351', '6', '1', NULL, NULL, '2017-09-01 07:58:38', NULL),
(508, '1638', 'Chavi Soniwal', 'Arvind Kumar', 'Rachna Paliwan', 'PRE NUR--', 'Gali No.-3, Chhutmalpur, Distt-Saharanpur', '13-07-2017', NULL, '05-02-2014', 'F', '366', NULL, NULL, '7895892986', '8273774962', '7', '1', NULL, NULL, '2017-09-01 07:58:38', NULL),
(509, '1550', 'Harshita', 'Harish Kumar', 'Pooja Saini', 'PRE NUR--', 'Arya Nagar, Chhutmalpur, Distt-Saharanpur', '13-04-2017', NULL, '08-04-2014', 'F', '461', NULL, NULL, '9927664444', '8449290000', '8', '1', NULL, NULL, '2017-09-01 07:58:38', NULL),
(510, '1644', 'M. Ishaan', 'Mohd Kamil', 'Shama', 'PRE NUR--', 'Vill-Halwana, Post-Chhutmalpur, Distt-Saharanpur', '24-07-2017', NULL, '22-10-2014', 'M', '581', NULL, NULL, '8006999979', '9720378388', '9', '1', NULL, NULL, '2017-09-01 07:58:38', NULL),
(511, '1576', 'Manav Pundir', 'Sandeep Kumar Pundir', 'Geetesh', 'PRE NUR--', 'Vill-Gangali, Post-Chhutmalpur, Distt-Saharanpur', '18-04-2017', NULL, '11-04-2014', 'M', '592', NULL, NULL, '9639266728', '7830873034', '10', '1', NULL, NULL, '2017-09-01 07:58:38', NULL),
(512, '1557', 'Mihika', 'Pawanesh Kumar', 'Gunjan', 'PRE NUR--', 'Punjabi Colony, Chhutmalpur, Distt-Saharanpur', '15-04-2017', NULL, '', 'F', '621', NULL, NULL, '9368854922', '8057977575', '11', '1', NULL, NULL, '2017-09-01 07:58:38', NULL),
(513, '1483', 'Navya Saini', 'Vinit Kumar', 'Madhu Saini', 'PRE NUR--', 'Hind Vihar Colony, Chhutmalpur, Distt-Saharanpur', '23-03-2017', NULL, '29-05-2014', 'F', '699', NULL, NULL, '9457505514', '7088117364', '12', '1', NULL, NULL, '2017-09-01 07:58:38', NULL),
(514, '1627', 'Parth Sarthi Bhardwaj', 'Shobhit Sharma', 'Dipti Kapil', 'PRE NUR--', 'Harijan Colony, Gali No.3, Chhutmalpur, Distt-Saharanpur', '06-07-2017', NULL, '11-09-2014', 'M', '722', NULL, NULL, '8923690888', '9761448520', '13', '1', NULL, NULL, '2017-09-01 07:58:38', NULL),
(515, '1488', 'Pawni Aggarwal', 'Sourabh Kumar', 'Neha Aggarwal', 'PRE NUR--', 'H. No.-05, Bansal Vihar Colony, D.dun Road, Chhutmalpur, Distt-Saharanpur', '29-03-2017', NULL, '09-10-2014', 'F', '723', NULL, NULL, '9719961329', '9917550043', '14', '1', NULL, NULL, '2017-09-01 07:58:38', NULL),
(516, '1602', 'Purvi Dawar', 'Paras Dawar', 'Priyanka Dawar', 'PRE NUR--', 'Punjabi Colony, Chhutmalpur, Distt-Saharanpur', '27-04-2017', NULL, '01-05-2015', 'F', '759', NULL, NULL, '8958229090', '9761604100', '15', '1', NULL, NULL, '2017-09-01 07:58:38', NULL),
(517, '1641', 'Samar', 'Irshad', 'Shabnam', 'PRE NUR--', 'Bhainswal (sikanderpur), Distt-Haridwar', '10-07-2017', NULL, '15-11-2013', 'M', '843', NULL, NULL, '9759172299', '9758168065', '16', '1', NULL, NULL, '2017-09-01 07:58:38', NULL),
(518, '1555', 'Samar Pahuja', 'Amit Pahuja', 'Beenu Pahuja', 'PRE NUR--', 'Punjabi Colony, Chhutmalpur, Distt-Saharanpur', '13-04-2017', NULL, '29-11-2014', 'M', '845', NULL, NULL, '8171222002', '9720744008', '17', '1', NULL, NULL, '2017-09-01 07:58:38', NULL),
(519, '1593', 'Shivangi Singh', 'Vinod Kumar', 'Sarita Rani', 'PRE NUR--', 'Vill-Alawalpur, Post-Chhutmalpur, Distt-Haridwar', '20-04-2017', NULL, '11-11-2013', 'F', '904', NULL, NULL, '9719801269', '9719646800', '18', '1', NULL, NULL, '2017-09-01 07:58:38', NULL),
(520, '1625', 'Shrishti', 'Ashwani Kumar', 'Meenu', 'PRE NUR--', 'Shiv Colony, Fatehpur, Chhutmalpur, Distt-Saharanpur', '04-07-2017', NULL, '30-11-2014', 'F', '914', NULL, NULL, '9997977782', '8865001323', '19', '1', NULL, NULL, '2017-09-01 07:58:38', NULL),
(521, '1642', 'Soni', 'Irshad', 'Shabnam', 'PRE NUR--', 'Bhainswal (sikanderpur), Distt-Haridwar', '10-07-2017', NULL, '15-11-2013', 'F', '936', NULL, NULL, '9759172299', '9758168065', '20', '1', NULL, NULL, '2017-09-01 07:58:38', NULL),
(522, '1616', 'Vanya Sharma', 'Mohit Kumar Sharma', 'Dolly Sharma', 'PRE NUR--', 'Hind Vihar Colony, Halwana Road, Chhutmalpur,SRE', '09-05-2017', NULL, '', 'F', '1025', NULL, NULL, '9759561026', '9719223332', '21', '1', NULL, NULL, '2017-09-01 07:58:38', NULL),
(523, '1489', 'Vihan Saini', 'Vipin Saini', 'Parul Saini', 'PRE NUR--', 'Vill-Dhankarpur, Post-Chamarikhera, Distt-Saharanpur', '29-03-2017', NULL, '05-06-2014', 'M', '1043', NULL, NULL, '9760056213', '9758233574', '22', '1', NULL, NULL, '2017-09-01 07:58:38', NULL),
(524, '1275', 'Aadi Jindal', 'Sumit Jindal', 'Saloni Jindal', 'UKG-A', 'Harijan Colony, Chhutmalpur', '24-Aug-15', NULL, '15-Mar-11', 'M', '3', NULL, NULL, '9639392828', '9927737835', '1', '1', NULL, NULL, '2017-09-03 09:50:26', NULL),
(525, '1082', 'Aanav Gupta', 'Vinay Gupta', 'Anu Gupta', 'UKG-A', 'Buggawala Road, Biharigarh, SRE', '01-Apr-15', NULL, '30-Sep-11', 'M', '14', NULL, NULL, '9759291281/ 8534090941', '9719169974', '2', '1', NULL, NULL, '2017-09-03 09:50:26', NULL),
(526, '1112', 'Aanvi', 'Shekhar Singh', 'Ritu Singh', 'UKG-B', 'Vill-Alawalpur, Post-Chhutmalpur, Distt-Haridwar', '06-Apr-15', NULL, '21-05-2012', 'F', '18', NULL, NULL, '9917631925', '9758626204', '1', '1', NULL, NULL, '2017-09-03 09:50:35', NULL),
(527, '846', 'Abhay Kamboj', 'Nikunj Kamboj', 'Renu Kamboj', 'UKG-B', 'Vill & Post Sherpur Khanazadpur, Distt-Saharanpur', '02-Apr-14', NULL, '02-Sep-11', 'M', '60', NULL, NULL, '9897452108', '8755363828', '2', '1', NULL, NULL, '2017-09-03 09:50:35', NULL),
(528, '843', 'Abhinav Chauhan', 'Sandeep Kumar ', 'Isha Chauhan', 'UKG-A', 'Fancy Cloth Showroom, Roorkee Road, Chhutmalpur, SRE', '01-Apr-14', NULL, '25-May-11', 'M', '71', NULL, NULL, '9760261970', '8445428418', '3', '1', NULL, NULL, '2017-09-03 09:50:26', NULL),
(529, '1404', 'Abhiraj Rana', 'Sushil Kumar', 'Kavita', 'UKG-A', 'Vill & Post Sunderpur, Distt-SRE', '13-Apr-16', NULL, '11-Feb-11', 'M', '86', NULL, NULL, '9720708851', '', '4', '1', NULL, NULL, '2017-09-03 09:50:26', NULL),
(530, '998', 'Abhya Ahuja', 'Gaurav Ahuja', 'Sujata Ahuja', 'UKG-B', 'Punjabi colony, Chhutmalpur(Roorkee Road)', '02-Jul-14', NULL, '12-Jan-12', 'F', '98', NULL, NULL, '8439524317', '9837629239', '3', '1', NULL, NULL, '2017-09-03 09:50:35', NULL),
(531, '1591', 'Aditi Rana', 'Sandeep Rana', 'Dimple', 'UKG-A', 'Vill-Battanwala, Post-Chhutmalpur, Distt-Saharanpur', '08-04-2017', NULL, '06-10-2011', 'M', '105', NULL, NULL, '9917664176', '9761381966', '5', '1', NULL, NULL, '2017-09-03 09:50:26', NULL),
(532, '1130', 'Agustasya Sharma', 'Arun Sharma', 'Deepali Sharma', 'UKG-B', 'Vill-Kalalhati, Post- Chhutmalpur, Distt-SRE', '06-Apr-15', NULL, '03-Oct-12', 'M', '118', NULL, NULL, '9759693926', '9634507306/9219609856', '4', '1', NULL, NULL, '2017-09-03 09:50:35', NULL),
(533, '926', 'Aksha Rehman', 'Ata-ur-Rehman', 'Ayesha Nizami', 'UKG-B', 'Vill. & Post Harora, Saharanpur', '15-Apr-14', NULL, '19-Oct-11', 'F', '134', NULL, NULL, '9758840606', '7500956362', '5', '1', NULL, NULL, '2017-09-03 09:50:35', NULL),
(534, '1172', 'Alina', 'Mohd. Imran', 'Reshma', 'UKG-B', 'Muslim Colony, Chhutmalpur, Distt-SRE', '09-Apr-15', NULL, '', 'F', '152', NULL, NULL, '9720650030', '7078682824', '6', '1', NULL, NULL, '2017-09-03 09:50:35', NULL),
(535, '1076', 'Altamash Rao', 'Quyyoum Rao', 'Gulshan Rao', 'UKG-A', 'Vill & Post- Kheri Shikohpur, Distt-Haridwar', '31-Mar-15', NULL, '30-Mar-12', 'M', '163', NULL, NULL, '7900481662', '8868027316', '6', '1', NULL, NULL, '2017-09-03 09:50:26', NULL),
(536, '1107', 'Anant Saini', 'Parveen Kumar', 'Anju Devi', 'UKG-B', 'Vill- Alawalpur, Post-Chhutmalpur, Distt-SRE', '04-Apr-15', NULL, '07-Aug-11', 'M', '184', NULL, NULL, '9837938115', '7409613349', '7', '1', NULL, NULL, '2017-09-03 09:50:35', NULL),
(537, '1648', 'Ananya Chaudhary', 'Amit Kumar Chaudhary', 'Pooja Malik', 'UKG-B', 'Jattari House, Biharipuram, Melrose Bye Pass, Koil, Aligarh', '06-07-2017', NULL, '04-03-2013', 'F', '186', NULL, NULL, '9310634209', '9811522797', '8', '1', NULL, NULL, '2017-09-03 09:50:35', NULL),
(538, '1086', 'Angel Rathore', 'Himanshu Rathore', 'Teena Rathore', 'UKG-B', 'Banjara Colony, Roorkee Road, Chhutmalpur,SRE', '01-Apr-15', NULL, '27-Sep-12', 'F', '191', NULL, NULL, '8439111051', '8791747806', '9', '1', NULL, NULL, '2017-09-03 09:50:35', NULL),
(539, '1402', 'Anshika Garg', 'Suresh Garg', 'Sapna Garg', 'UKG-A', 'Harijan Colony, Chhutmalpur, Distt-SRE', '13-Apr-16', NULL, '11-Mar-12', 'F', '218', NULL, NULL, '9719934672', '9719935151', '7', '1', NULL, NULL, '2017-09-03 09:50:26', NULL),
(540, '1057', 'Anvi Kamboj', 'Amit Kamboj', 'Rachna Kamboj', 'UKG-A', 'Vill- Ganja Mazra (Gaushala), Post- Biharigarh, Distt-Haridwar', '21-Mar-15', NULL, '06-Sep-11', 'F', '244', NULL, NULL, '7351807165', '8445576572', '8', '1', NULL, NULL, '2017-09-03 09:50:26', NULL),
(541, '1264', 'Apoorva Kamboj', 'Yogesh Kamboj', 'Sarmista', 'UKG-B', 'Vill-Meerpur, Post-Sherpur, Distt-SRE', '09-Jul-15', NULL, '23-Oct-11', 'F', '248', NULL, NULL, '9758832588', '9719855844', '10', '1', NULL, NULL, '2017-09-03 09:50:35', NULL),
(542, '1278', 'Arnav Chauhan', 'Nitin kumar', 'Nisha Chauhan', 'UKG-B', 'Vill-Buggawala, Distt-haridwar', '23-Jul-15', NULL, '05-May-12', 'M', '261', NULL, NULL, '8439375880', '9027566377', '11', '1', NULL, NULL, '2017-09-03 09:50:35', NULL),
(543, '1097', 'Arnav Saini', 'Sachin Saini', 'Savita Saini', 'UKG-B', 'Maharana Pratap Colony, Chhutmalpur', '', NULL, '29-Oct-11', 'M', '265', NULL, NULL, '8826291568', '9412357487/9917936457', '12', '1', NULL, NULL, '2017-09-03 09:50:35', NULL),
(544, '1058', 'Aru Saini', 'Amrish Kumar', 'Alka Devi', 'UKG-A', 'Vill-Alawalpur, Post-Chhutmalpur, Distt-Haridwar', '23-Mar-15', NULL, '17-Mar-12', 'F', '281', NULL, NULL, '9761982774', '9758929994', '9', '1', NULL, NULL, '2017-09-03 09:50:26', NULL),
(545, '1477', 'Aryansh Mishra', 'Puneet Kumar Mishra', 'Mitali Mishra', 'UKG-B', 'Vill-Chholi Sahabudin, Chhutmalpur', '11-Nov-16', NULL, '22-Nov-12', 'M', '297', NULL, NULL, '9452460147', '7839119435', '13', '1', NULL, NULL, '2017-09-03 09:50:35', NULL),
(546, '1459', 'Asjad', 'Aasif chauhan', 'Yaasmeen', 'UKG-B', 'Vill-Gudam, Post-Chhutmalpur, Distt-SRE', '11-Jul-16', NULL, '15-Mar-12', 'M', '306', NULL, NULL, '9837424141', '', '14', '1', NULL, NULL, '2017-09-03 09:50:35', NULL),
(547, '1263', 'Astha Laman', 'Sunil Kumar', 'Anita', 'UKG-A', 'Vill-Manduwala, Post-Khujnawar, Distt-SRE', '09-Jul-15', NULL, '19-Jul-12', 'F', '308', NULL, NULL, '9720005020', '9639869477', '10', '1', NULL, NULL, '2017-09-03 09:50:26', NULL),
(548, '1024', 'Atharv', 'Upender Kumar', 'Meenakshi', 'UKG-B', 'Vill. Fatehpur, Opp. Samudayik Hospital', '16-Jul-14', NULL, '15-Dec-11', 'M', '311', NULL, NULL, '9719233440', '9675687393', '15', '1', NULL, NULL, '2017-09-03 09:50:35', NULL),
(549, '817', 'Atiksh kaushik ', 'Ashish Sharma ', 'Neerja Sharma ', 'UKG-A', '163A, Hind Vihar Colony Halwana Road ,Chhutmalpur ', '31-Mar-14', NULL, '19-Sep-11', 'M', '314', NULL, NULL, '9536912345', '8869033333', '11', '1', NULL, NULL, '2017-09-03 09:50:26', NULL),
(550, '1059', 'Dhruv Veer Kamboj', 'Deepak Kumar', 'Meena Devi', 'UKG-A', 'Vill- Rasoolpur, Post- Chhutmalpur, Distt- SRE', '24-Mar-15', NULL, '25-Dec-11', 'M', '400', NULL, NULL, '9456226153', '9719755960', '12', '1', NULL, NULL, '2017-09-03 09:50:26', NULL),
(551, '1265', 'Harshit Saini', 'Pankaj Kumar', 'Hema Devi', 'UKG-B', 'Vill-Dadapatti,Post-Chhutmalpur,Disst.Saharanpur', '10-Jul-15', NULL, '27-Oct-11', 'M', '459', NULL, NULL, '9759098774', '8650006443', '16', '1', NULL, NULL, '2017-09-03 09:50:35', NULL),
(552, '1157', 'Jay Kamboj', 'Randhir Kumar', 'Vinita', 'UKG-B', 'Opp. Tulsi School, Shiv Colony, Post-Chhutmalpur, Distt-SRE', '08-Apr-15', NULL, '07-Feb-13', 'M', '500', NULL, NULL, '9675773264', '9639784535', '17', '1', NULL, NULL, '2017-09-03 09:50:35', NULL),
(553, '814', 'Kabir Kamboj', 'Puran Singh', 'Savita Kamboj', 'UKG-B', 'Vill & Post-Sherpur Khanazadpur, Distt-SRE', '31-Mar-14', NULL, '02-Jul-12', 'M', '508', NULL, NULL, '9719848821', '8954011452', '18', '1', NULL, NULL, '2017-09-03 09:50:35', NULL),
(554, '1156', 'Kanv Jain', 'Atul Jain', 'Babita Jain', 'UKG-A', 'Gandhi Colony, Chhutmalpur,SRE', '08-Apr-15', NULL, '', 'M', '519', NULL, NULL, '9927727592', '', '13', '1', NULL, NULL, '2017-09-03 09:50:26', NULL),
(555, '1321', 'Kartik Saini', 'Sandeep Saini', 'Kavita Saini', 'UKG-B', 'Vill-Dada jalalpur P-Hallumajra, Distt-Haridwar', '04-Apr-16', NULL, '', 'M', '522', NULL, NULL, '8958042756', '8906141268', '19', '1', NULL, NULL, '2017-09-03 09:50:35', NULL),
(556, '891', 'Khushi', 'Rashid', 'Haseen Bano', 'UKG-A', 'Muslim Colony, Chhutmalpur', '05-Apr-14', NULL, '07-Jan-11', 'F', '530', NULL, NULL, '9719865912', '8755979184', '14', '1', NULL, NULL, '2017-09-03 09:50:26', NULL),
(557, '1206', 'Khushi Saini', 'Sandeep Saini', 'Anjali Saini', 'UKG-B', 'Vill & Post-Sona Syed Mazra, Distt-SRE', '15-Apr-15', NULL, '', 'F', '534', NULL, NULL, '9837776217', '9719090351', '20', '1', NULL, NULL, '2017-09-03 09:50:35', NULL),
(558, '1155', 'Kirti Verma', 'Vinod Verma', 'Meenakshi Verma', 'UKG-A', 'Shiv Colony, Opp. Jain Bagh, Fatehpur, SRE', '08-Apr-15', NULL, '01-Nov-11', 'F', '541', NULL, NULL, '7500870004', '9897387662', '15', '1', NULL, NULL, '2017-09-03 09:50:26', NULL),
(559, '1050', 'Krishna Saini', 'Mukesh Saini', 'Neeta Rani', 'UKG-A', 'Vill & Post- Gagalheri, Distt-SRE', '17-Mar-15', NULL, '06-Feb-12', 'M', '549', NULL, NULL, '9528186584', '9568628362', '16', '1', NULL, NULL, '2017-09-03 09:50:26', NULL),
(560, '1649', 'Lakshika', 'Manojjwal', 'Pooja', 'UKG-A', 'Vill-Shaheed Wala Grant, Post-Buggawala, Distt-Haridwar', '01-08-2017', NULL, '11-11-2011', 'F', '566', NULL, NULL, '9758013890', '9758012748', '17', '1', NULL, NULL, '2017-09-03 09:50:26', NULL),
(561, '1119', 'Lavishka Chauhan', 'Bhanupratap', 'Preeti Chauhan', 'UKG-B', 'Vill-Kamalpur, Post-Chhutmalpur,Distt-SRE', '06-Apr-15', NULL, '15-Dec-11', 'F', '574', NULL, NULL, '9634859243', '', '21', '1', NULL, NULL, '2017-09-03 09:50:35', NULL),
(562, '1135', 'Liza Garg', 'Jitendra Gupta', 'Laxmi Gupta', 'UKG-B', 'Street No.3, Harijan Colony, Chhutmalpur, SRE', '07-Apr-15', NULL, '30-May-12', 'F', '577', NULL, NULL, '9759260537', '8650808180', '22', '1', NULL, NULL, '2017-09-03 09:50:35', NULL),
(563, '1006', 'Manan Chauhan', 'Kamal Chauhan', 'Meenakshi Chauhan', 'UKG-A', 'Vill. Telpura, Post Biharigarh', '04-Jul-14', NULL, '03-Jun-12', 'M', '588', NULL, NULL, '9759396285', '9627370637', '18', '1', NULL, NULL, '2017-09-03 09:50:26', NULL),
(564, '849', 'Manav Chaudhary', 'Sohanveer Singh', 'Geeta Chaudhary', 'UKG-B', 'Vill. Alliwala Post Muzaffarabad', '02-Apr-14', NULL, '26-Sep-11', 'M', '590', NULL, NULL, '9917550028', '9759129632', '23', '1', NULL, NULL, '2017-09-03 09:50:36', NULL),
(565, '1273', 'Manvi Rawat', 'Pushpender Singh', 'Umesh Devi', 'UKG-A', 'Vill-Patholker, Post-Nayamatpur, Distt-SRE', '30-Jul-15', NULL, '', 'F', '605', NULL, NULL, '9720228836', '', '19', '1', NULL, NULL, '2017-09-03 09:50:26', NULL),
(566, '946', 'Mohd. Arham', 'Muqeem Ahmad', 'Farhat', 'UKG-A', 'Alawalpur (D.Dun) Road, Near Masjid, Chhutmalpur', '21-Apr-14', NULL, '04-Aug-11', 'M', '649', NULL, NULL, '9719366565', '9837166350', '20', '1', NULL, NULL, '2017-09-03 09:50:26', NULL),
(567, '844', 'Mohd. Yasir', 'Sazid Ali', 'Sabiya', 'UKG-A', 'Vill-Kalalhati, Post-Chhutmalpur, Distt-SRE', '02-Apr-14', NULL, '31-Oct-11', 'M', '668', NULL, NULL, '9760035342', '9219609856', '21', '1', NULL, NULL, '2017-09-03 09:50:26', NULL),
(568, '800', 'Mohd.Shayan', 'Mahtab Ali', 'Farha', 'UKG-A', 'Harijan Colony, Lane No. 1, Chhutmalpur', '20-Mar-14', NULL, '14-Jan-12', 'M', '673', NULL, NULL, '9758139455', '9720266262', '22', '1', NULL, NULL, '2017-09-03 09:50:26', NULL),
(569, '1592', 'Navdeep Rana', 'Sandeep Rana', 'Dimple', 'UKG-A', 'Vill-Battanwala, Post-Chhutmalpur, Distt-Saharanpur', '08-04-2017', NULL, '06-10-2011', 'M', '695', NULL, NULL, '9917664176', '9761381966', '23', '1', NULL, NULL, '2017-09-03 09:50:26', NULL),
(570, '1269', 'Nidhi Singh', 'Gurudas Singh', 'Sulekha Singh', 'UKG-B', 'Vill-Sona, Post-Harora, Distt-SRE', '02-Apr-15', NULL, '', 'F', '702', NULL, NULL, '9758228730', '9837110779', '24', '1', NULL, NULL, '2017-09-03 09:50:36', NULL),
(571, '1052', 'Om Chauhan', 'Ankush Kumar', 'Sarita Chauhan', 'UKG-A', 'Vill & Post- Khhubanpur, Bhagwanpur, Distt- Haridwar', '18-Mar-15', NULL, '28-Dec-12', 'M', '715', NULL, NULL, '8006888752', '7417622232', '24', '1', NULL, NULL, '2017-09-03 09:50:26', NULL),
(572, '1051', 'Pihoo ', 'Amar pal', 'Rakhi ', 'UKG-A', 'Town Titron moh. Kanongoyan,Distt-sre.', '17-Mar-15', NULL, '06-Feb-12', 'F', '726', NULL, NULL, '', '8979217565', '25', '1', NULL, NULL, '2017-09-03 09:50:26', NULL),
(573, '1118', 'Preksha Bansal', 'Sachin Bansal', 'Shruti Bansal', 'UKG-A', 'Harijan Colony, Gali No. 3, Chhutmalpur, SRE', '06-Apr-15', NULL, '31-Jan-12', 'F', '745', NULL, NULL, '9761922211', '7417445031', '26', '1', NULL, NULL, '2017-09-03 09:50:26', NULL),
(574, '1221', 'Radhika Parashar', 'Anirudh Parashar', 'Priyanka Parashar', 'UKG-A', 'Vill-Gangali, Post-Chhutmalpur, Distt-SRE', '21-Apr-15', NULL, '19-Jun-12', 'F', '760', NULL, NULL, '', '', '27', '1', NULL, NULL, '2017-09-03 09:50:27', NULL),
(575, '1446', 'Rao Amaan', 'Rao Mannan', 'Shahjhan', 'UKG-A', 'Vill & Post-Kheri Sikohpur, Distt-Haridwar', '09-May-16', NULL, '', 'M', '775', NULL, NULL, '9758845530', '7037375820', '28', '1', NULL, NULL, '2017-09-03 09:50:27', NULL),
(576, '870', 'Rao Anas Pundir', 'Rao Tahir Pundir', 'Saira Rao', 'UKG-B', 'Vill. Kheri Shikohpur', '03-Apr-14', NULL, '29-Jun-09', 'M', '776', NULL, NULL, '9719961141', '9927369595', '25', '1', NULL, NULL, '2017-09-03 09:50:36', NULL),
(577, '1106', 'Reet Kohli', 'Ashish Kumar', 'Monika Kohli', 'UKG-B', 'Near Post Office, Punjabi Colony, Chhutmalpur, SRE', '04-Apr-15', NULL, '03-Sep-11', 'F', '795', NULL, NULL, '9219253134', '', '26', '1', NULL, NULL, '2017-09-03 09:50:36', NULL),
(578, '1132', 'Riyanshi', 'Sompal', 'Ruma Devi', 'UKG-A', 'Vill & Post- Firaheri, Distt-SRE', '06-Apr-15', NULL, '18-Apr-12', 'F', '813', NULL, NULL, '9758775410', '9761432009', '29', '1', NULL, NULL, '2017-09-03 09:50:27', NULL),
(579, '1161', 'Shelly Chauhan', 'Sushil Kumar', 'Reena ', 'UKG-A', 'Vill-Shahidwala Grant, Post-Buggawala, Distt-Haridwar', '08-Apr-15', NULL, '22-Oct-10', 'F', '888', NULL, NULL, '9759562510', '9719181616', '30', '1', NULL, NULL, '2017-09-03 09:50:27', NULL),
(580, '1179', 'Shiv Chaudhary', 'Amit Kumar', 'Ritu Chaudhary', 'UKG-B', 'Vill-Chaura Khurd, Post-Gagalheri, Distt-SRE', '09-Apr-15', NULL, '', 'M', '890', NULL, NULL, '9720753911', '9759908438', '27', '1', NULL, NULL, '2017-09-03 09:50:36', NULL),
(581, '782', 'Shiva Kamboj', 'Sumit Kumar Kamboj', 'Poonam Kamboj', 'UKG-B', 'Vill-Chanchak, Post-Sherpur Khanazadpur, Distt-SRE', '12-Mar-14', NULL, '27-Nov-11', 'M', '891', NULL, NULL, '9758840372', '9627100089', '28', '1', NULL, NULL, '2017-09-03 09:50:36', NULL),
(582, '847', 'Shubh Goyal', 'Vishal Goyal ', 'Priyanka Goyal', 'UKG-B', 'Main Market, Biharigarh', '02-Apr-14', NULL, '07-Oct-11', 'M', '916', NULL, NULL, '9897032011', '8445191635', '29', '1', NULL, NULL, '2017-09-03 09:50:36', NULL),
(583, '1647', 'Suryansh Dhiman', 'Chitranjan Dass', 'Nirdosh', 'UKG-A', 'Near Beej Godam, Saharanpur Road, Chhutmalpur, Distt-Saharanpur', '27-07-2017', NULL, '27-12-2012', 'M', '955', NULL, NULL, '9760262228', '8171460014', '31', '1', NULL, NULL, '2017-09-03 09:50:27', NULL),
(584, '1225', 'Vaachi', 'Ankur Chaudhary', 'Sapna Chaudhary', 'UKG-A', 'Vill-Chaura Khurd, Post-Chaura Dev, Distt-SRE', '30-Apr-15', NULL, '11-Nov-12', 'F', '996', NULL, NULL, '9917695040', '9719234098', '32', '1', NULL, NULL, '2017-09-03 09:50:27', NULL),
(585, '1331', 'Vaibhav Chauhan', 'Amit Kumar', 'Ranjeeta', 'UKG-B', 'Vill-Banjarawala post-Sunderpur, Distt-Haridwar', '05-Apr-16', NULL, '02-Nov-11', 'M', '999', NULL, NULL, '9719831616', '9760726729', '30', '1', NULL, NULL, '2017-09-03 09:50:36', NULL),
(586, '1084', 'Vaibhav Yadav', 'Vipin Kumar Yadav', 'Jyoti Yadav', 'UKG-A', 'Vill-Kutubpur Kusani, Post-Tiwaya, Distt-SRE', '01-Apr-15', NULL, '', 'M', '1003', NULL, NULL, '9760095005', '8192927138', '33', '1', NULL, NULL, '2017-09-03 09:50:27', NULL),
(587, '1373', 'Varuna', 'Pushpender Saini', 'Beena Saini', 'UKG-A', 'Vill-palli Post-Harora Distt-SRE', '09-Apr-16', NULL, '26-Jan-11', 'F', '1031', NULL, NULL, '7253823207', '7409842263', '34', '1', NULL, NULL, '2017-09-03 09:50:27', NULL),
(588, '783', 'Vasu Kumar', 'Sanjay', 'Poonam ', 'UKG-A', 'Vill-Chanchak, Post-Sherpur Khanazadpur, Distt-SRE', '12-Mar-14', NULL, '12-Oct-11', 'M', '1037', NULL, NULL, '9412016983', '8865077111', '35', '1', NULL, NULL, '2017-09-03 09:50:27', NULL),
(589, '1380', 'Vishavajeet Singh Chauhan', 'Ajay Chauhan', 'Pooja Chauhan', 'UKG-B', 'Vill-Kamalpur, Post-Chhutmalpur, Distt-SRE', '11-Apr-16', NULL, '', 'M', '1057', NULL, NULL, '9927901890', '9084815728', '31', '1', NULL, NULL, '2017-09-03 09:50:36', NULL),
(590, '1085', 'Yuvraj Singh', 'Vikas Singh', 'Savita Singh', 'UKG-B', 'Vill- Alawalpur, Post- Chhutmalpur, Distt-SRE', '01-Apr-15', NULL, '05-Mar-12', 'M', '1072', NULL, NULL, '9927351605', '7536926001', '32', '1', NULL, NULL, '2017-09-03 09:50:36', NULL),
(591, '1148', 'Zara Rao', 'Rao Tahir Pundir', 'Sayira Rao', 'UKG-B', 'Vill & Post- Kheri Shikohpur, Distt-Haridwar', '06-Apr-15', NULL, '', 'F', '1076', NULL, NULL, '9927369595', '9997130314', '33', '1', NULL, NULL, '2017-09-03 09:50:36', NULL),
(592, '1524', 'Aaditya Kumar', 'Sushil Kumar', 'Rachna Devi', 'V-B', 'Vill-Gandewara, Post-Chhutmalpur, Distt-Saharanpur', '08-04-2017', NULL, '18-07-2007', 'M', '5', NULL, NULL, '7500820214', '7500820714', '1', '1', NULL, NULL, '2017-09-01 08:04:10', NULL),
(593, '488', 'Aaditya Saini', 'Ravindra Saini', 'Soniya Saini', 'V-B', 'Shiv Colony, Fatehpur, Chhutmalpur', '04-May-12', NULL, '10-Jun-06', 'M', '7', NULL, NULL, '8057613763', '', '2', '1', NULL, NULL, '2017-09-01 08:04:10', NULL),
(594, '181', 'Adiba Rao', 'Abdul Kadir', 'Rahil', 'V-B', 'Opp. U.B.I. DDN Road, Chhutamapur', '18/4/11', NULL, '23-Jan-07', 'F', '101', NULL, NULL, '9319558089', '', '3', '1', NULL, NULL, '2017-09-01 08:04:10', NULL),
(595, '395', 'Aditya Saini', 'Arun Saini', 'Rakhi saini', 'V-B', 'Vill.Daddapatti, Post Hasanpur Madanpur, Distt. Haridwar', '07-Apr-12', NULL, '16-Dec-06', 'M', '110', NULL, NULL, '9675358693', '9719533528', '4', '1', NULL, NULL, '2017-09-01 08:04:10', NULL),
(596, '1585', 'Aditya Singh Chaudhary', 'Jitendra Singh Chaudhary', 'Reena Singh Chaudhary', 'V-A', 'Fatehpur, Chhutmalpur, Distt-Saharanpur', '19-04-2017', NULL, '', 'M', '113', NULL, NULL, '9412402874', '9457268412', '1', '1', NULL, NULL, '2017-09-01 08:04:03', NULL),
(597, '23', 'Aman Chaudhary', 'Sohanvir Singh', 'Anjali Chaudhary', 'V-A', 'Vill Alliwala, Chhutmalpur', '04-Feb-11', NULL, '13-Sep-07', 'M', '164', NULL, NULL, '9759877450', '', '2', '1', NULL, NULL, '2017-09-01 08:04:03', NULL),
(598, '182', 'Ansh Saini', 'Amit  Saini', 'Geeta Devi', 'V-A', 'Alawalpur Disst. Haridwar', '18/4/11', NULL, '06-Jan-09', 'M', '212', NULL, NULL, '9758278127', '9058462044', '3', '1', NULL, NULL, '2017-09-01 08:04:03', NULL),
(599, '1170', 'Anshika Sharma', 'Shree Kant Sharma', 'L. Rakhi Sharma', 'V-B', 'Hind Vihar Colony, Halwana Road, Chhutmalpur, SRE', '08-Apr-15', NULL, '', 'F', '223', NULL, NULL, '9368193741', '9997909192', '5', '1', NULL, NULL, '2017-09-01 08:04:10', NULL),
(600, '673', 'Anurag Kamboj', 'Amit Kumar', 'Beenu Kamboj', 'V-A', 'Vill. Sherpur Khanazadpur, Distt. Saharanpur', '09-Apr-13', NULL, '16-Mar-06', 'M', '236', NULL, NULL, '8394816521', '', '4', '1', NULL, NULL, '2017-09-01 08:04:03', NULL),
(601, '1128', 'Anushka Chauhan', 'Amit', 'Ranjeeta', 'V-A', 'Vill-Banjarawala, Post-Sunderpur, Distt-Haridwar', '06-Apr-15', NULL, '19-Nov-07', 'F', '238', NULL, NULL, '9760726729', '9557632419', '5', '1', NULL, NULL, '2017-09-01 08:04:03', NULL),
(602, '652', 'Arslan', 'Abdul salam', 'Sultana', 'V-B', 'Vill. Sayed Mazra, Post Harora', '06-Apr-13', NULL, '01-Oct-06', 'M', '279', NULL, NULL, '9719952654', '', '6', '1', NULL, NULL, '2017-09-01 08:04:10', NULL),
(603, '198', 'Ayan Niyazi', 'Wazid Niyazi', 'Shahina Praveen', 'V-A', 'Village Kheri Shikohpur', '20/4/11', NULL, '25-Dec-07', 'M', '331', NULL, NULL, '9719430800', '8954628242', '6', '1', NULL, NULL, '2017-09-01 08:04:03', NULL),
(604, '1363', 'Bhanu Pratap Singh', 'Rajender Singh', 'Baby', 'V-B', 'Vill-Jajner, Post- Chhutmalpur, Distt-SRE', '08-Apr-16', NULL, '28-Apr-06', 'M', '357', NULL, NULL, '9837706300', '9761414555', '7', '1', NULL, NULL, '2017-09-01 08:04:11', NULL),
(605, '378', 'Bhavya Pahwa', 'Sachin Pahwa', 'Monika Pahwa', 'V-A', 'Punjabi Colony, Chhutmalpur', '04-Apr-12', NULL, '28-May-06', 'M', '361', NULL, NULL, '8979201200', '9761010976', '7', '1', NULL, NULL, '2017-09-01 08:04:03', NULL),
(606, '475', 'Deepanshu Saini', 'Parveen Saini', 'Pinki Saini', 'V-B', 'Vill- Banwala', '23-Apr-12', NULL, '22-May-06', 'M', '377', NULL, NULL, '9759750027', '9759093881', '8', '1', NULL, NULL, '2017-09-01 08:04:11', NULL),
(607, '1134', 'Deepika Singh', 'Dheer Singh', 'Nirmla Devi', 'V-A', 'Vill-Alawalpur, Post-Chhutmalpur, Distt-Haridwar', '06-Apr-15', NULL, '13-Jan-08', 'F', '378', NULL, NULL, '7830140286', '8859189517', '8', '1', NULL, NULL, '2017-09-01 08:04:03', NULL),
(608, '56', 'Devansh Kamboj', 'Devender Kamboj', 'Monika Kamboj', 'V-A', 'Village Mahmmadpur, Post Sherpur, Distt. Saharanpur', '04-May-11', NULL, '26-Jul-07', 'M', '389', NULL, NULL, '9719802405', '7500333484', '9', '1', NULL, NULL, '2017-09-01 08:04:03', NULL),
(609, '987', 'Dishu Karanwal', 'Manghi Ram Karanwal', 'Suman Karanwal', 'V-B', 'Vill-Gangali, Post-Chhutmalpur, Dist-Saharanpur', '22-May-14', NULL, '13-Feb-07', 'F', '407', NULL, NULL, '9469239516/8006400000', '9756657531', '9', '1', NULL, NULL, '2017-09-01 08:04:11', NULL);
INSERT INTO `studentmgmts` (`id`, `admission_no`, `student_name`, `student_father_name`, `student_mother_name`, `student_class_section`, `student_address`, `student_joining_date`, `student_images`, `student_dob`, `student_gender`, `student_username`, `student_password`, `student_email`, `student_mob1`, `student_mob2`, `student_rollno`, `sessionid`, `deleted_at`, `created_at`, `updated_at`, `code`) VALUES
(610, '88', 'Farhad Rao', 'Fatehyab Khan', 'Fariza', 'V-A', 'Village Kheri Shikohpur', '04-Jun-11', NULL, '22-Nov-07', 'M', '424', NULL, NULL, '9719113130', '9012224620', '10', '1', NULL, NULL, '2017-09-01 08:04:03', NULL),
(611, '135', 'Farmaan Ali', 'Rao Parvez Khan', 'Wasima Begum', 'V-B', 'Village Kheri Shikohpur', '04-Sep-11', NULL, '27-Mar-06', 'M', '425', NULL, NULL, '9758845530', '9358161601', '10', '1', NULL, NULL, '2017-09-01 08:04:11', NULL),
(612, '21', 'Harsh Chaudhary', 'Manoj Chaudhary', 'Renu Chaudhary', 'V-A', 'Vill Alliwala', '04-Feb-11', NULL, '09-Dec-06', 'M', '444', NULL, NULL, '9837513400', '9917207406', '11', '1', NULL, NULL, '2017-09-01 08:04:03', NULL),
(613, '889', 'Hemant Bansal', 'Sanjay Bansal', 'Minakshi Bansal', 'V-B', 'Arya Nagar, Chhutmalpur', '05-Apr-14', NULL, '', 'M', '465', NULL, NULL, '9412424308', '9720738685', '11', '1', NULL, NULL, '2017-09-01 08:04:11', NULL),
(614, '1361', 'Jashanpreet', 'Kirpal Singh', 'Baljinder Kaur', 'V-B', 'Vill-Dinarpur Gagalheri, Distt-SRE', '08-Apr-16', NULL, '16-Oct-05', 'M', '496', NULL, NULL, '7830933847', '9759640211', '12', '1', NULL, NULL, '2017-09-01 08:04:11', NULL),
(615, '1351', 'Kanishka Kamboj', 'Sanjay Kamboj', 'Pinki Kamboj', 'V-B', 'Vill-Sherpur Khanazadpur, Distt-SRE', '06-Apr-16', NULL, '23-Jul-07', 'F', '516', NULL, NULL, '9758832040', '9761185434', '13', '1', NULL, NULL, '2017-09-01 08:04:11', NULL),
(616, '643', 'Kartik Singhal', 'Brijesh Singhal', 'Sarita Singhal', 'V-A', 'Chauli Sahbuddinpur', '04-Apr-13', NULL, '29-Aug-06', 'M', '524', NULL, NULL, '9756215585', '9719799959', '12', '1', NULL, NULL, '2017-09-01 08:04:03', NULL),
(617, '1146', 'Khushi Chauhan', 'Anil Chauhan', 'Lalita Chauhan', 'V-A', 'Vill-Chouharpur, Post Behat, Distt-SRE', '07-Apr-15', NULL, '', 'F', '531', NULL, NULL, '9719356843', '9719831616', '13', '1', NULL, NULL, '2017-09-01 08:04:03', NULL),
(618, '13', 'Krishna Kamboj', 'Amit Kumar', 'Rachna Devi', 'V-B', 'Village Aurangjebpur/Nanakgarh, Post Biharigarh, Distt. Haridwar', '31/3/11', NULL, '07-Jan-07', 'M', '547', NULL, NULL, '9719447331', '9758694895/9719831304', '14', '1', NULL, NULL, '2017-09-01 08:04:11', NULL),
(619, '470', 'Krrish Parle', 'Jitender Parle', 'Preeti Parle', 'V-B', 'Vill & Post Gagalehri, Distt. Saharanpur', '20-Apr-12', NULL, '14-Jun-06', 'M', '553', NULL, NULL, '9761333313', '7409077688', '15', '1', NULL, NULL, '2017-09-01 08:04:11', NULL),
(620, '40', 'Kshitij Pahuja', 'Sunil Pahuja', 'Nidhi Pahuja', 'V-A', 'Punjabi Colony, Chhutmalpur', '04-Apr-11', NULL, '17-Aug-05', 'M', '554', NULL, NULL, '9927151033', '9927126426', '14', '1', NULL, NULL, '2017-09-01 08:04:03', NULL),
(621, '323', 'Laiba Noor', 'Jamshed Ali Ansari', 'Shabana Azmy', 'V-B', 'Vill Gandewara Post Chhutamalpur, Distt. Saharanpur', '23-Mar-12', NULL, '07-Aug-06', 'F', '561', NULL, NULL, '9536982786', '7500357786', '16', '1', NULL, NULL, '2017-09-01 08:04:11', NULL),
(622, '741', 'Mantasha', 'Sanawwar Ahmed', 'Rehmani', 'V-A', 'Vill. Sayed Mazra, Post Harora', '04-Jul-13', NULL, '01-Jul-05', 'F', '599', NULL, NULL, '9927824275', '9720884778', '15', '1', NULL, NULL, '2017-09-01 08:04:03', NULL),
(623, '812', 'Mariyam Niyazi', 'Jameel Niyazi', 'Kishwar Niyazi', 'V-A', 'Vill & Post Kheri Shikohpur, Distt-Haridwar', '28-Mar-14', NULL, '', 'F', '611', NULL, NULL, '9719322706', '', '16', '1', NULL, NULL, '2017-09-01 08:04:03', NULL),
(624, '1021', 'Mohd. Danish', 'Mohd. Irfan', 'Poshida Beghum', 'V-A', 'Vill. Kameshpur, Post Chhutmalpur', '16-Jul-14', NULL, '12-Jul-07', 'M', '652', NULL, NULL, '9761214596', '9917055164', '17', '1', NULL, NULL, '2017-09-01 08:04:03', NULL),
(625, '335', 'Naren Rathore', 'Sanjay Kumar', 'Rekha Rani', 'V-A', 'Chhutmalpur, Gandhi Colony', '29-Mar-12', NULL, '26-Feb-07', 'M', '693', NULL, NULL, '9897879991', '8410510511', '18', '1', NULL, NULL, '2017-09-01 08:04:03', NULL),
(626, '1587', 'Prince Singh Chaudhary', 'Ashok Singh Chaudhary', 'Shushma Chaudhary', 'V-B', 'Vill-Alawalpur, Post-Chhutmalpur, Distt-Haridwar', '20-04-2017', NULL, '01-01-2005', 'M', '747', NULL, NULL, '9457268412', '7599646875', '17', '1', NULL, NULL, '2017-09-01 08:04:11', NULL),
(627, '224', 'Ranbeer Nirwan', 'Dhirendra Nirwan', 'Sunita Nirwan', 'V-B', 'Village Alawalpur', '29/4/11', NULL, '05-Oct-06', 'M', '768', NULL, NULL, '9927977777', '', '18', '1', NULL, NULL, '2017-09-01 08:04:11', NULL),
(628, '61', 'Rao Aarish ', 'Athar Ali', 'Rashda ', 'V-B', 'Village & Post Kheri Shikohpur', '04-May-11', NULL, '19-Mar-07', 'M', '771', NULL, NULL, '9720381673', '9719993618', '19', '1', NULL, NULL, '2017-09-01 08:04:11', NULL),
(629, '405', 'Rao Faisal', 'Rao Kausar', 'Shadma Rao', 'V-A', 'Vill & Post Harora', '09-Apr-12', NULL, '09-Feb-08', 'M', '778', NULL, NULL, '9758746832', '9897400066', '19', '1', NULL, NULL, '2017-09-01 08:04:03', NULL),
(630, '376', 'Riya Chaudhary', 'Sushil Kumar', 'Anu', 'V-B', 'Vill.Alawalpur, Post Chhutmalpur, Distt. Haridwar', '04-Apr-12', NULL, '04-May-08', 'F', '808', NULL, NULL, '9368308023', '', '20', '1', NULL, NULL, '2017-09-01 08:04:11', NULL),
(631, '1393', 'Riya Jasrotia', 'Chandel Singh', 'Anita Jasrotia', 'V-B', 'Vill-Manduwala, Post-Khujnawar, Distt-SRE', '12-Apr-16', NULL, '12-Oct-08', 'F', '811', NULL, NULL, '9917712559', '9837309010', '21', '1', NULL, NULL, '2017-09-01 08:04:11', NULL),
(632, '424', 'Saksham Sharma', 'Yogender Sharma', 'Meenakshi Sharma', 'V-A', 'Vill Sherpur Khanazadpur', '10-Apr-12', NULL, '21-Jan-07', 'M', '835', NULL, NULL, '9012874970', '9756121755', '20', '1', NULL, NULL, '2017-09-01 08:04:03', NULL),
(633, '1342', 'Sameer', 'Naresh Kumar', 'Pratima', 'V-B', 'Vill-Gherkarma Post-Biharigarh, Distt-SRE', '06-Apr-16', NULL, '24-Aug-08', 'M', '849', NULL, NULL, '9761350547', '9759103370', '22', '1', NULL, NULL, '2017-09-01 08:04:11', NULL),
(634, '6', 'Samriddhi', 'Devendra Pal', 'Luxmi', 'V-B', 'Village Gandewada, post Chhutmalpur', '25/3/11', NULL, '20-Dec-07', 'F', '853', NULL, NULL, '9927957907', '01322782277/8126589782', '23', '1', NULL, NULL, '2017-09-01 08:04:11', NULL),
(635, '58', 'Sejal Kamboj', 'Sandeep Kamboj', 'Nisha Kamboj', 'V-A', 'Village Sherpur Khanazadpur', '04-May-11', NULL, '17-Feb-07', 'F', '879', NULL, NULL, '9412437220', '', '21', '1', NULL, NULL, '2017-09-01 08:04:03', NULL),
(636, '910', 'Sharad', 'Sandeep Kumar ', 'Sangeeta', 'V-A', 'Vill. & Post Halwana', '08-Apr-14', NULL, '', 'M', '882', NULL, NULL, '9759595062', '9627478988', '22', '1', NULL, NULL, '2017-09-01 08:04:03', NULL),
(637, '869', 'Siddhant Kamboj', 'Ravinder Kumar', 'Shivani Kamboj', 'V-A', 'Vill. & Post Sherpur Khanazadpur', '03-Apr-14', NULL, '09-Nov-07', 'M', '922', NULL, NULL, '9758962429', '9720840184', '23', '1', NULL, NULL, '2017-09-01 08:04:03', NULL),
(638, '732', 'Siddharth', 'Birampal', 'Vimlesh Devi', 'V-A', 'Vill. & Post Khubbanpur, Distt. Haridwar', '27-May-13', NULL, '12-Dec-05', 'M', '923', NULL, NULL, '9719998019', '7830145457', '24', '1', NULL, NULL, '2017-09-01 08:04:03', NULL),
(639, '948', 'Simra Naaz', 'Parvez Alam', 'Ashiya Naaz', 'V-A', 'Dehradun Road, Chhutmalpur', '21-Apr-14', NULL, '31-Mar-06', 'F', '926', NULL, NULL, '9758985555', '8394926262', '25', '1', NULL, NULL, '2017-09-01 08:04:03', NULL),
(640, '156', 'Siya Khurana', 'Amit Khurana', 'Meenu Khurana ', 'V-A', 'Punjabi Colony, Chhutmalpur', '13/4/11', NULL, '28-Apr-07', 'F', '933', NULL, NULL, '9358188554', '8449174444', '26', '1', NULL, NULL, '2017-09-01 08:04:03', NULL),
(641, '260', 'Subhan Malik', 'Abdul Wahab', 'Ayesha', 'V-B', 'Chhutmalpur, Muslim Col., Sabji Mandi, Near Madarsa gate', '28/5/11', NULL, '11-Feb-07', 'M', '941', NULL, NULL, '9897758011', '', '24', '1', NULL, NULL, '2017-09-01 08:04:11', NULL),
(642, '409', 'Swayam Chauhan', 'Kamal Chauhan', 'Meenakshi Chauhan', 'V-B', 'Vill Telpura, Post Biharigarh', '09-Apr-12', NULL, '13-Feb-07', 'M', '960', NULL, NULL, '9759396285', '', '25', '1', NULL, NULL, '2017-09-01 08:04:11', NULL),
(643, '1123', 'Tushar Rana', 'Vijay Rana', 'Reena chouhan', 'V-B', 'Vill-Kamalpur, Post-Chhutmalpur,Distt-SRE', '06-Apr-15', NULL, '', 'M', '982', NULL, NULL, '9897282903', '', '26', '1', NULL, NULL, '2017-09-01 08:04:11', NULL),
(644, '1559', 'Ubed', 'Zulfkar', 'Shaheen Jahan', 'V-B', 'Vill-Mukarrampur Urf Kalewala, Chhutmalpur, Distt-Saharanpyur', '03-04-2017', NULL, '21-08-2006', 'M', '983', NULL, NULL, '9411077562', '9456294521', '27', '1', NULL, NULL, '2017-09-01 08:04:11', NULL),
(645, '176', 'Vansh Choudhary', 'Prashant Choudhary', 'Neelam Chaudhary', 'V-A', 'Village Alawalpur', '16/4/11', NULL, '09-Apr-07', 'M', '1012', NULL, NULL, '9536684113', '', '27', '1', NULL, NULL, '2017-09-01 08:04:03', NULL),
(646, '624', 'Vansh Gupta', 'Sameer Gupta', 'Dixa Gupta', 'V-A', 'Fatehpur C/O Shivalik X-Ray', '03-Apr-13', NULL, '13-Feb-07', 'M', '1013', NULL, NULL, '9897472682', '8006440815', '28', '1', NULL, NULL, '2017-09-01 08:04:03', NULL),
(647, '796', 'Vansh Saini', 'Sanjay Saini', 'Ritu Saini', 'V-A', 'Vill Lava,Post-Khubbanpur,Distt-Haridwar', '19-Mar-14', NULL, '08-Oct-07', 'M', '1019', NULL, NULL, '9837241454', '9536035264', '29', '1', NULL, NULL, '2017-09-01 08:04:03', NULL),
(648, '171', 'Yash Goyal', 'Nitin Goyal', 'Archana Goyal', 'V-A', 'Street No. 3 Maharanapratap colony, Chhutmalpur', '15/4/11', NULL, '04-Aug-07', 'M', '1065', NULL, NULL, '9758815235', '', '30', '1', NULL, NULL, '2017-09-01 08:04:03', NULL),
(649, '663', 'Yash Tyagi', 'Jitendra Kumar Tyagi', 'Parul Tyagi', 'V-A', 'Near PNB, D.Dun Road, Chhutmalpur', '08-Apr-13', NULL, '21-Feb-07', 'M', '1066', NULL, NULL, '9808333524', '9927028537', '31', '1', NULL, NULL, '2017-09-01 08:04:03', NULL),
(650, '1047', 'Yuvraj Kamboj', 'Neeraj Kumar', 'Rajni', 'V-A', 'Arya Nagar, Chhutmalpur, SRE', '11-Mar-15', NULL, '', 'M', '1070', NULL, NULL, '9758056855', '8859601146', '32', '1', NULL, NULL, '2017-09-01 08:04:03', NULL),
(651, '489', 'Aanchal Saini', 'Ravindra Saini', 'Soniya Saini', 'VI-B', 'Shiv Colony, Fatehpur, Chhutmalpur', '04-May-12', NULL, '24-May-05', 'F', '16', NULL, NULL, '8057613763', '', '1', '1', NULL, NULL, '2017-09-01 08:04:28', NULL),
(652, '389', 'Aayushi Saini', 'Neeraj Kumar', 'Deepa Saini', 'VI-B', 'Amardeep Colony, Chhutmalpur', '06-Apr-12', NULL, '09-Jul-05', 'F', '53', NULL, NULL, '9897632376', '9760370694', '2', '1', NULL, NULL, '2017-09-01 08:04:28', NULL),
(653, '215', 'Abhinav Tomar', 'Arvind Tomar', 'Seema Tomar', 'VI-B', 'Chhutmalpur, D.Dun Road, Phool Col.', '28/4/11', NULL, '31-Aug-06', 'M', '80', NULL, NULL, '9917703837', '8909590982', '3', '1', NULL, NULL, '2017-09-01 08:04:28', NULL),
(654, '408', 'Alisha Rao', 'Manawar Ali Khan', 'Rahat Rao', 'VI-B', 'Vill & Post Harora, Distt. Saharanpur', '09-Apr-12', NULL, '11-Apr-06', 'F', '157', NULL, NULL, '9837752620', '9837577974', '4', '1', NULL, NULL, '2017-09-01 08:04:28', NULL),
(655, '348', 'Ansh Goyal', 'Vivek Goyal', 'Reena Devi', 'VI-B', 'Fatehpur , Near P.N.B. Bank, Dehradun Road ', '30-Mar-12', NULL, '27-Jul-06', 'M', '207', NULL, NULL, '9897123635', '2782625', '5', '1', NULL, NULL, '2017-09-01 08:04:28', NULL),
(656, '514', 'Anshika Saini', 'Bhrampal Saini', 'Kavita Saini', 'VI-B', 'Mujaffarabad,Distt. Saharanpur', '02-Jul-12', NULL, '01-Jul-03', 'F', '220', NULL, NULL, '9215598164', '8869046797', '6', '1', NULL, NULL, '2017-09-01 08:04:28', NULL),
(657, '216', 'Anushka Nirwan', 'Dharmendra Nirwan', 'Anu Nirwan', 'VI-B', 'Village Alawalpur', '28/4/11', NULL, '20-Jun-06', 'F', '240', NULL, NULL, '9761251592', '', '7', '1', NULL, NULL, '2017-09-01 08:04:28', NULL),
(658, '709', 'Aryan Yadav', 'Pritam Yadav', 'Aruna Yadav', 'VI-B', 'Vill. & Post Chaura Dev', '19-Apr-13', NULL, '08-May-07', 'M', '296', NULL, NULL, '9675693193', '', '8', '1', NULL, NULL, '2017-09-01 08:04:28', NULL),
(659, '271', 'Astha Pundir', 'Amrish Rana', 'Sunita', 'VI-B', 'Budha Khera Pundir, Post Chamarikhera', '07-Jan-11', NULL, '15-Dec-06', 'F', '309', NULL, NULL, '9690790490', '8449164444', '9', '1', NULL, NULL, '2017-09-01 08:04:28', NULL),
(660, '333', 'Ayush Gautam', 'Vinod Kumar', 'Babita', 'VI-B', 'Vill Gandewara, Post Chhutmalpur, Distt. Saharanpur', '29-Mar-12', NULL, '16-Sep-07', 'M', '335', NULL, NULL, '9359488955', '9690629141', '10', '1', NULL, NULL, '2017-09-01 08:04:28', NULL),
(661, '318', 'Ayush Saini', 'Pawan Kumar Saini', 'Naresh Devi', 'VI-B', 'Dhankapur Post Chamarikhera', '17-Mar-12', NULL, '30-Nov-04', 'M', '344', NULL, NULL, '9917093708', '9045584542', '11', '1', NULL, NULL, '2017-09-01 08:04:28', NULL),
(662, '161', 'Bhumika Chaudhary', 'Ashish Kumar', 'Rashmi Chaudhary', 'VI-B', 'Chhutmalpur, Hind Vihar Colony, Halwana Road', '14/4/11', NULL, '12-Sep-06', 'F', '363', NULL, NULL, '9837421910', '9997811177', '12', '1', NULL, NULL, '2017-09-01 08:04:28', NULL),
(663, '450', 'Charvi Rawat', 'Pushpener Singh', 'Umesh Devi', 'VI-B', 'Vill-Pathlokar, Post-Nayamatpur ( Near-Baroli)', '16-Apr-12', NULL, '03-Feb-06', 'F', '365', NULL, NULL, '9720228836', '9761020514', '13', '1', NULL, NULL, '2017-09-01 08:04:28', NULL),
(664, '575', 'Dev Pratap Singh', 'Rajesh Kumar', 'Renu', 'VI-A', 'Vill. & Post Muzaffarabad, Near Shiv Mandir', '29-Mar-13', NULL, '14-Jan-06', 'M', '382', NULL, NULL, '9456226610', '8273383427', '1', '1', NULL, NULL, '2017-09-01 08:04:18', NULL),
(665, '8', 'Dhiraj Chauhan', 'Shree Om', 'Rajni ', 'VI-B', 'Budha Khera Pundir, Post Chamarikhera', '25/3/11', NULL, '10-Oct-06', 'M', '396', NULL, NULL, '9837139927', '9045939432', '14', '1', NULL, NULL, '2017-09-01 08:04:28', NULL),
(666, '375', 'Dipanshu Chaudhary', 'Sushil Chaudhary', 'Anu', 'VI-B', 'Vill.Alawalpur, Post Chhutmalpur, Distt. Haridwar', '04-Apr-12', NULL, '31-Jul-06', 'M', '406', NULL, NULL, '9368308023', '9286216222', '15', '1', NULL, NULL, '2017-09-01 08:04:28', NULL),
(667, '1525', 'Himanshu Goyal', 'Pawan Kumar', 'Savita Goyal', 'VI-A', 'Vill-Alawalpur, Post-Chhutmalpur, Distt-Haridwar', '08-04-2017', NULL, '06-10-2006', 'M', '473', NULL, NULL, '9758712658', '9917402994', '2', '1', NULL, NULL, '2017-09-01 08:04:18', NULL),
(668, '63', 'Husain Ahmad', 'Abdul Raouf', 'Shama Praveen', 'VI-A', 'Opp. U.B.I., DDN Road, Chhutmalpur', '04-May-11', NULL, '21-Sep-05', 'M', '482', NULL, NULL, '9759006006', '', '3', '1', NULL, NULL, '2017-09-01 08:04:18', NULL),
(669, '1519', 'Kanhiya Saini', 'Parmod Saini', 'Babli Saini', 'VI-A', 'Vill-Kurrikhera, Distt-Saharanpur', '08-04-2017', NULL, '25-06-2004', 'M', '515', NULL, NULL, '9759031736', '8410917570', '4', '1', NULL, NULL, '2017-09-01 08:04:18', NULL),
(670, '659', 'Kashish Kamboj', 'Manoj Kamboj', 'Meera Kamboj', 'VI-A', 'Vill. Takipur, Post Sherpur', '06-Apr-13', NULL, '13-Aug-05', 'F', '525', NULL, NULL, '8650003531', '9720557568', '5', '1', NULL, NULL, '2017-09-01 08:04:18', NULL),
(671, '52', 'Lakshya kamboj', 'Anuj kamboj', 'Ranjita Kamboj', 'VI-A', 'Village Chanchak, Post Sherpur Khanazadpur', '04-May-11', NULL, '11-Jan-06', 'M', '567', NULL, NULL, '9457048049', '', '6', '1', NULL, NULL, '2017-09-01 08:04:18', NULL),
(672, '57', 'Lovish Kamboj', 'Subodh Kamboj', 'Shashi Kamboj', 'VI-B', 'Village Sherpur Khanazadpur', '04-May-11', NULL, '18-Sep-05', 'M', '578', NULL, NULL, '9412437220', '', '16', '1', NULL, NULL, '2017-09-01 08:04:28', NULL),
(673, '258', 'Mahesh', 'Babu Ram', 'Savita', 'VI-A', 'Nr. Tulsi Adarsh School, Fatehpur', '27/5/11', NULL, '11-Sep-06', 'M', '584', NULL, NULL, '8057411730', '', '7', '1', NULL, NULL, '2017-09-01 08:04:18', NULL),
(674, '1324', 'Manav Dhiman', 'Anuj Dhiman', 'Sunita dhiman', 'VI-A', 'Harnaam Singh Nagar, Chhutmalpur, Distt-SRE', '04-Apr-16', NULL, '12-Dec-07', 'M', '591', NULL, NULL, '9719460697', '9058505235', '8', '1', NULL, NULL, '2017-09-01 08:04:18', NULL),
(675, '339', 'Manvender Pratap Singh', 'Sushil Pundir', 'Rekha pundir', 'VI-A', 'Jeewala, Post Muzzfarabad', '30-Mar-12', NULL, '01-Jan-06', 'M', '601', NULL, NULL, '9758873682', '9719586429', '9', '1', NULL, NULL, '2017-09-01 08:04:18', NULL),
(676, '793', 'Mayank Bhardwaj', 'Kamal Kishore Sharma', 'Suman Lata Sharma', 'VI-A', 'Vill & Post Khubbanpur, Distt-Haridwar', '19-Mar-14', NULL, '', 'M', '613', NULL, NULL, '9761336927', '9759434934', '10', '1', NULL, NULL, '2017-09-01 08:04:18', NULL),
(677, '310', 'Mohd Anas', 'Dilshad Ali', 'Azra Parveen', 'VI-A', 'Chhutmalpur, Muslim Colony', '04-Feb-12', NULL, '21-Sep-04', 'M', '626', NULL, NULL, '9897778734', '9012938470', '11', '1', NULL, NULL, '2017-09-01 08:04:18', NULL),
(678, '381', 'Mohd Junaid', 'Abdul Gaffar', 'Shabana', 'VI-A', 'Vill Gandewara Post Chhutamalpur', '04-Apr-12', NULL, '08-Jul-08', 'M', '632', NULL, NULL, '9837515813', '', '12', '1', NULL, NULL, '2017-09-01 08:04:18', NULL),
(679, '226', 'Mohd. Zaid', 'Rao Nadeem', 'Reshma', 'VI-A', 'Village Kheri Shikohpur', '29/4/11', NULL, '11-Mar-05', 'M', '670', NULL, NULL, '9761962723', '8923335689', '13', '1', NULL, NULL, '2017-09-01 08:04:18', NULL),
(680, '430', 'Mohd. Zaid', 'Suhail Arshad', 'Shazia Praveen', 'VI-B', 'Near Talab wali Gali, Vill Gandewara,', '11-Apr-12', NULL, '11-Mar-05', 'M', '671', NULL, NULL, '7895413941', '9997105989', '17', '1', NULL, NULL, '2017-09-01 08:04:28', NULL),
(681, '1127', 'Naincy Saini', 'Rupendra', 'Upasna', 'VI-B', 'Vill-Mirzapur Poul, Post-Mirzapur, Distt-SRE', '06-Apr-15', NULL, '28-Sep-05', 'F', '685', NULL, NULL, '9761231986', '8954775516', '18', '1', NULL, NULL, '2017-09-01 08:04:28', NULL),
(682, '1046', 'Paras Kamboj', 'Neeraj Kumar', 'Rajni', 'VI-B', 'Arya Nagar, Chhutmalpur, SRE', '11-Mar-15', NULL, '', 'M', '716', NULL, NULL, '9758056855', '8859601146', '19', '1', NULL, NULL, '2017-09-01 08:04:28', NULL),
(683, '325', 'Rakshit Saini', 'Mukesh Kumar', 'Mantesh Saini', 'VI-A', 'Vill Badkala, Post Chhutmalpur, Distt. Saharanpur', '27-Mar-12', NULL, '11-Feb-06', 'M', '766', NULL, NULL, '8859463348', '', '14', '1', NULL, NULL, '2017-09-01 08:04:18', NULL),
(684, '286', 'Rao Farhan Khan', 'Mr.Nanu', 'Swadyakeen', 'VI-B', 'Vill Sikanderpur Bhaiswal Bhagwanpur, Roorkee', '14/7/11', NULL, '01-Apr-06', 'M', '782', NULL, NULL, '9627117640', '', '20', '1', NULL, NULL, '2017-09-01 08:04:28', NULL),
(685, '62', 'Rao Hasnain', 'Athar Ali', 'Rashda ', 'VI-B', 'Village Kheri Shikohpur', '04-May-11', NULL, '30-Jun-05', 'M', '785', NULL, NULL, '9720381673', '9719993618', '21', '1', NULL, NULL, '2017-09-01 08:04:28', NULL),
(686, '1299', 'Ritik', 'Rajneesh Kumar', 'Paramjeet', 'VI-A', 'Vill-Takipur, Post-Sherpur Khanazadpur, Distt-SRE', '28-Mar-16', NULL, '', 'M', '799', NULL, NULL, '9758408197', '', '15', '1', NULL, NULL, '2017-09-01 08:04:18', NULL),
(687, '314', 'Sahil Alam', 'Parvej Alam', 'Parveen Begam', 'VI-B', 'Vill Gandewara Post Chhutamalpur', '13-Mar-12', NULL, '29-Oct-05', 'M', '829', NULL, NULL, '8057497640', '9690299691', '22', '1', NULL, NULL, '2017-09-01 08:04:28', NULL),
(688, '839', 'Sambhav Kamboj', 'Yogesh Kumar', 'Sarmistha Devi', 'VI-B', 'Vill-Meerpur, Post-Sherpur, Distt-SRE', '01-Apr-14', NULL, '11-Nov-06', 'M', '846', NULL, NULL, '9758832588', '97199855844', '23', '1', NULL, NULL, '2017-09-01 08:04:28', NULL),
(689, '268', 'Sameer', 'Late Abdul Sami', 'Shahjahan', 'VI-A', 'Vill Gandewara', '30/6/11', NULL, '26-Nov-05', 'M', '848', NULL, NULL, '9568314242', '9760018800', '16', '1', NULL, NULL, '2017-09-01 08:04:18', NULL),
(690, '392', 'Sangam Kamboj', 'Vinod/Manoj Kamboj', 'Deepa Kamboj', 'VI-B', 'Behind Kamboj Bakery, Chhutmalpur', '07-Apr-12', NULL, '30-Mar-05', 'M', '857', NULL, NULL, '9758292646', '7830874428', '24', '1', NULL, NULL, '2017-09-01 08:04:28', NULL),
(691, '671', 'Satyam Bhatnagar', 'Sonu Bhatnagar', 'Sarita Devi', 'VI-A', 'Roorkee Road, Chhutmalpur', '08-Apr-13', NULL, '27-Mar-05', 'M', '871', NULL, NULL, '9759562018', '9758286213', '17', '1', NULL, NULL, '2017-09-01 08:04:18', NULL),
(692, '380', 'Surbhi Yadav', 'Neeraj Yadav', 'Neelam Yadav', 'VI-A', 'Vill & post Sambhalki Gujjar', '04-Apr-12', NULL, '16-Feb-06', 'F', '953', NULL, NULL, '9675934125', '9761206843', '18', '1', NULL, NULL, '2017-09-01 08:04:18', NULL),
(693, '609', 'Suryansh Saini', 'Sunil Saini', 'Poonam Saini', 'VI-B', 'Sona Sayed Majra', '02-Apr-13', NULL, 'NA', 'M', '957', NULL, NULL, '9719474450', '', '25', '1', NULL, NULL, '2017-09-01 08:04:28', NULL),
(694, '168', 'Tasadduk', 'Aijaz Haidar', 'Malika', 'VI-A', 'Opp. Arora Dhaba, Dehradun Road, Chhutmalpur', '14/4/11', NULL, '21-Apr-05', 'M', '974', NULL, NULL, '9719923879', '', '19', '1', NULL, NULL, '2017-09-01 08:04:18', NULL),
(695, '393', 'Vaishnavi Kamboj', 'Vinod Kamboj', 'Deepa Kamboj', 'VI-B', 'Behind Kamboj Bakery, Chhutmalpur', '07-Apr-12', NULL, '27-Oct-06', 'F', '1008', NULL, NULL, '9758292646', '7830874428', '26', '1', NULL, NULL, '2017-09-01 08:04:28', NULL),
(696, '19', 'Vardan Kamboj', 'Kushal Pal Kamboj', 'Beenu Kamboj', 'VI-A', 'Village Rasoolpur kala Post Chhutmalpur', '04-Jan-11', NULL, '06-Sep-05', 'M', '1026', NULL, NULL, '9719070418', '9761866241', '20', '1', NULL, NULL, '2017-09-01 08:04:18', NULL),
(697, '275', 'Vasu Aggarwal', 'Amit Aggarwal', 'Kavita Aggarwal', 'VI-B', 'Agg. Agency, Kamalpur Rd, Chhutmalpur', '07-Apr-11', NULL, '26-Jun-05', 'M', '1033', NULL, NULL, '9897119842', '9412650471', '27', '1', NULL, NULL, '2017-09-01 08:04:28', NULL),
(698, '1291', 'Vasu Jain', 'Anup Jain', 'Pooja Jain', 'VI-A', 'Gandhi Colony, Chhutmalpur, Distt-SRE', '22-Mar-16', NULL, '05-Jul-06', 'M', '1035', NULL, NULL, '8923409762', '9837979726', '21', '1', NULL, NULL, '2017-09-01 08:04:18', NULL),
(699, '288', 'Vasu Kamboj', 'Niraj Kumar', 'Ritu Kamboj', 'VI-A', 'Chhutmalpur, Maharanapratap colony ', '15/7/11', NULL, '17-Apr-07', 'M', '1036', NULL, NULL, '7830292930', '8273203580', '22', '1', NULL, NULL, '2017-09-01 08:04:18', NULL),
(700, '1280', 'Vidit Somwal', 'Amit Kumar Chaudhary', 'Rishu', 'VI-A', 'Arya Nagar, Chhutmalpur, Distt-SRE', '16-Mar-16', NULL, '01-Dec-05', 'M', '1041', NULL, NULL, '9837547398', '9690181198', '23', '1', NULL, NULL, '2017-09-01 08:04:18', NULL),
(701, '127', 'Vishal Chaudhary', 'Ashok Kumar', 'Sanyogita', 'VI-A', 'Village Alawalpur, Post Chhutmalpur', '04-Aug-11', NULL, '29-Apr-06', 'M', '1052', NULL, NULL, '8057916621', '7830279044', '24', '1', NULL, NULL, '2017-09-01 08:04:19', NULL),
(702, '178', 'Vrinda Mittal', 'Shailendra Mittal', 'Seema Mittal', 'VI-A', 'Chhutmalpur, Gandhi Colony', '16/4/11', NULL, '17-Sep-06', 'F', '1063', NULL, NULL, '9319151659', '', '25', '1', NULL, NULL, '2017-09-01 08:04:19', NULL),
(703, '919', 'Abhinav Sharma', 'Sanjeev Sharma', 'Poonam Sharma', 'VII-B', 'Vill & Post-Khubbanpur, Distt-Haridwar', '11-Apr-14', NULL, '07-Mar-06', 'M', '78', NULL, NULL, '7830245017', '7417888319', '1', '1', NULL, NULL, '2017-09-01 08:05:29', NULL),
(704, '699', 'Abhinav Yadav', 'Sandeep Kumar', 'Baby', 'VII-B', 'Vill. & Post Chauradev', '16-Apr-13', NULL, '05-Jun-05', 'M', '82', NULL, NULL, '8006006609', '', '2', '1', NULL, NULL, '2017-09-01 08:05:29', NULL),
(705, '1560', 'Abuzar', 'Zulfikar', 'Shaheen Jahan', 'VII-A', 'Vill-Mukarrampur Urf Kalewala, Chhutmalpur, Distt-Saharanpyur', '03-04-2017', NULL, '09-08-2005', 'M', '99', NULL, NULL, '9411077562', '9456294521', '1', '1', NULL, NULL, '2017-09-01 08:04:42', NULL),
(706, '1176', 'Aditi Pundir', 'Shivender Singh', 'Seema Pundir', 'VII-B', 'Vill & Post-Bahera Sandal Singh, Distt-SRE', '08-Apr-15', NULL, '11-Feb-05', 'F', '103', NULL, NULL, '9917717300', '9756293442', '3', '1', NULL, NULL, '2017-09-01 08:05:29', NULL),
(707, '880', 'Akansha Saini ', 'Parmod Saini ', 'Uma Devi', 'VII-A', 'Sant Nagar, Batra Firm D.Dun Road Chhutmalpur', '04-Apr-14', NULL, '17-Oct-05', 'F', '124', NULL, NULL, '9927138198', '9675100539', '2', '1', NULL, NULL, '2017-09-01 08:04:42', NULL),
(708, '1586', 'Akshara Singh Chaudhary', 'Jitendra Singh Chaudhary', 'Reena Singh Chaudhary', 'VII-A', 'Fatehpur, Chhutmalpur, Distt-Saharanpur', '19-04-2017', NULL, '', 'F', '139', NULL, NULL, '9412402874', '9457268412', '3', '1', NULL, NULL, '2017-09-01 08:04:42', NULL),
(709, '406', 'Alina Rao', 'Rao Kausar', 'Shadma Rao', 'VII-B', 'Vill & Post Harora', '09-Apr-12', NULL, '01-Mar-05', 'F', '154', NULL, NULL, '9758746832', '9897400066', '4', '1', NULL, NULL, '2017-09-01 08:05:29', NULL),
(710, '1035', 'Alnoor Fatima', 'Muhammad Waheed', 'Shabana Anjum', 'VII-B', 'Vill. & Post Kheri, Distt. Haridwar', '04-Sep-14', NULL, '01-Mar-05', 'F', '159', NULL, NULL, '9359242857', '9358753890', '5', '1', NULL, NULL, '2017-09-01 08:05:29', NULL),
(711, '1341', 'Alok Mittal', 'Mahipal Mittal', 'Seema Mittal', 'VII-A', 'Vill-Sunderpur Distt-Saharanpur', '06-Apr-16', NULL, '29-Aug-05', 'M', '160', NULL, NULL, '9719703220', '9675196022', '4', '1', NULL, NULL, '2017-09-01 08:04:42', NULL),
(712, '668', 'Aman Saini', 'Arvind Saini', 'Balesh Devi', 'VII-A', 'Vill. Sona Sayed Majra', '08-Apr-13', NULL, '09-Jun-02', 'M', '167', NULL, NULL, '9759560182', '9761236004', '5', '1', NULL, NULL, '2017-09-01 08:04:42', NULL),
(713, '1354', 'Aniket Saini', 'Jitender', 'Nirmal Devi', 'VII-A', 'Vill-Daddapatti, Distt-Haridwar', '07-Apr-16', NULL, '17-Oct-05', 'M', '192', NULL, NULL, '7078004981', '9997449771', '6', '1', NULL, NULL, '2017-09-01 08:04:42', NULL),
(714, '43', 'Anmol Kamboj', 'Arun Kumar Kamboj', 'Kavarlata', 'VII-A', 'Village Rasoolpur kala Post Chhutmalpur', '04-Apr-11', NULL, '03-Nov-06', 'M', '202', NULL, NULL, '9761232279', '', '7', '1', NULL, NULL, '2017-09-01 08:04:42', NULL),
(715, '803', 'Arjun Saini', 'Bhopal Singh', 'Babita Saini', 'VII-A', 'Vill Sona Syed Mazra,post-Harora ', '21-Mar-14', NULL, '12-Jul-05', 'M', '259', NULL, NULL, '8050102347', '8650102347', '8', '1', NULL, NULL, '2017-09-01 08:04:42', NULL),
(716, '115', 'Arpit Kamboj', 'Indresh Kamboj', 'Renu Kamboj', 'VII-A', 'Village Meerpur, Post Sherpur', '04-Jul-11', NULL, '30-Oct-06', 'M', '268', NULL, NULL, '9759077155', '9058588960', '9', '1', NULL, NULL, '2017-09-01 08:04:42', NULL),
(717, '612', 'Arpit Saini', 'Sushil Saini', 'Rajni Saini', 'VII-B', 'Sona Sayed Majra', '02-Apr-13', NULL, '01-Feb-06', 'M', '274', NULL, NULL, '9719090351', '', '6', '1', NULL, NULL, '2017-09-01 08:05:29', NULL),
(718, '1183', 'Aryan Chauhan', 'Pawan Chauhan', 'Mithlesh Chauhan', 'VII-A', 'Vill & Post-Khubbanpur, Distt-Haridwar', '10-Apr-15', NULL, '', 'M', '286', NULL, NULL, '9761023215', '9536871690', '10', '1', NULL, NULL, '2017-09-01 08:04:42', NULL),
(719, '1547', 'Aryan Garg', 'Sanjay Garg', 'Neelam Garg', 'VII-B', 'Harijan Colony, Gali No.-1, Chhutmalpur, Distt-Saharanpur', '12-04-2017', NULL, '03-08-2007', 'M', '287', NULL, NULL, '9719801243', '7037731934', '7', '1', NULL, NULL, '2017-09-01 08:05:29', NULL),
(720, '1282', 'Aviral Bieber', 'Surender Kumar', 'Sunita', 'VII-A', 'Jain Bagh, Chhutmalpur, Distt-SRE', '16-Mar-16', NULL, '13-Feb-07', 'F', '324', NULL, NULL, '9759627498', '9359207270', '11', '1', NULL, NULL, '2017-09-01 08:04:42', NULL),
(721, '1389', 'Aaush Kumar', 'Sandeep Singh', 'Sangeeta', 'VII-A', 'Vill-Gangali post-Chhutmalpur, Distt-SRE', '28-Mar-16', NULL, '14-Nov-04', 'M', '340', NULL, NULL, '7830453488', '9536379449', '12', '1', NULL, NULL, '2017-09-07 06:26:27', NULL),
(722, '80', 'Chhavi Gupta', 'Shiv Kumar Gupta', 'Vandana Gupta', 'VII-A', 'Chhutmalpur, Maharana Pratap Colony, St No. 3 ', '04-Jun-11', NULL, '13-Jul-05', 'F', '368', NULL, NULL, '9927908061', '9758231815', '13', '1', NULL, NULL, '2017-09-01 08:04:42', NULL),
(723, '1541', 'Daksh Bansal', 'Mohit Bansal', 'Chhavi Bansal', 'VII-A', 'Punjabi Colony, Chhutmalpur, Distt-Saharanpur', '11-04-2017', NULL, '', 'M', '372', NULL, NULL, '9760305022', '9719441222', '14', '1', NULL, NULL, '2017-09-01 08:04:42', NULL),
(724, '977', 'Dev Rana', 'Avnish Pundir', 'Brijesh Pundir', 'VII-B', 'Vill. Mandawar, Post Chhutmalpur, Distt. Haridwar', '09-May-14', NULL, '', 'M', '384', NULL, NULL, '9045584991', '9045481291', '8', '1', NULL, NULL, '2017-09-01 08:05:29', NULL),
(725, '592', 'Fazalebari', 'Mohd. Inam', 'Imrana Khatoon', 'VII-A', 'D.Dun Road, Opp.Surya Compl. Near Monu Ice Cream, Chhutmalpur', '01-Apr-13', NULL, '04-Dec-05', 'M', '427', NULL, NULL, '8899242004', '9058197862', '15', '1', NULL, NULL, '2017-09-01 08:04:42', NULL),
(726, '496', 'Fiza Parveen', 'Ayub Ahmed', 'Anwari Begam', 'VII-B', 'Vill & Post Harora', '10-May-12', NULL, '05-Apr-05', 'F', '428', NULL, NULL, '9927812736', '', '9', '1', NULL, NULL, '2017-09-01 08:05:29', NULL),
(727, '745', 'Govind Saini', 'Rajesh Kumar', 'Suman Devi', 'VII-A', 'Vill. Lavva, Post Bhagwanpur', '05-Jul-13', NULL, '20-Oct-05', 'M', '436', NULL, NULL, '9837723111', '', '16', '1', NULL, NULL, '2017-09-01 08:04:42', NULL),
(728, '577', 'Harsh Kamboj', 'Ashvani Kamboj', 'Rani  Devi', 'VII-B', 'Ganja Majra, Post.-Biharigarh', '29-Mar-13', NULL, '12-Aug-06', 'M', '447', NULL, NULL, '9719127559', '9758035665', '10', '1', NULL, NULL, '2017-09-01 08:05:29', NULL),
(729, '1418', 'Himanshu pundir ', 'Shiv Charan Singh', 'Ashu Devi', 'VII-B', 'Vill-Battanwala post-Chamarikhera,Distt.Saharanpur', '20-Apr-16', NULL, '31-Oct-05', 'M', '475', NULL, NULL, '9837335592', '8192045120', '11', '1', NULL, NULL, '2017-09-01 08:05:30', NULL),
(730, '92', 'Isha Singh', 'Dheer Singh', 'Nirmala Devi', 'VII-B', 'Village Alawalpur', '04-Jun-11', NULL, '10-Feb-05', 'F', '486', NULL, NULL, '7830140286', '', '12', '1', NULL, NULL, '2017-09-01 08:05:30', NULL),
(731, '1501', 'Jatin Verma', 'Pravesh Kumar', 'Veena Verma', 'VII-A', 'Shiv Colony,Fatehpur, Chhutmalpur, Distt-Saharanpur', '05-04-2017', NULL, '04-10-2004', 'M', '499', NULL, NULL, '9837265344', '8445467456', '17', '1', NULL, NULL, '2017-09-01 08:04:42', NULL),
(732, '28', 'Jhalak Choudhary', 'Pramod Kumar', 'Sarita', 'VII-A', 'Village Alawalpur, Post Chhutmalpur', '04-Mar-11', NULL, '13-11-2005', 'F', '505', NULL, NULL, '9917867776', '9927172791', '18', '1', NULL, NULL, '2017-09-01 08:04:42', NULL),
(733, '1213', 'Kajal Singh', 'Inderpal', 'Raman Devi', 'VII-B', 'Vill-Alawalpur, Post-Chhutmalpur, Distt-Haridwar', '18-Apr-15', NULL, '', 'F', '510', NULL, NULL, '8006928283', '7830140286', '13', '1', NULL, NULL, '2017-09-01 08:05:30', NULL),
(734, '534', 'Khadeeja', 'Nasir Hasan', 'Shahin Perveen', 'VII-A', 'APL 416, Muslim Colony, Chhutmalpur', '11-Jul-12', NULL, '29-Sep-03', 'F', '529', NULL, NULL, '8410005706', '9045699504', '19', '1', NULL, NULL, '2017-09-01 08:04:42', NULL),
(735, '1319', 'Khushi Sharma', 'Rakesh Sharma', 'Kavita Sharma', 'VII-A', 'Vill-Muazaffarabad, Distt-SRE', '04-Apr-16', NULL, '12-Apr-07', 'F', '535', NULL, NULL, '9719428815', '7409592924', '20', '1', NULL, NULL, '2017-09-01 08:04:42', NULL),
(736, '642', 'Khushi Singhal', 'Brijesh Singhal', 'Sarita Singhal', 'VII-B', 'Chauli Sahbuddinpur', '04-Apr-13', NULL, '04-Jun-05', 'F', '536', NULL, NULL, '9756215585', '9719799959', '14', '1', NULL, NULL, '2017-09-01 08:05:30', NULL),
(737, '746', 'Krishna Saini', 'Rajesh Kumar', 'Suman Devi', 'VII-A', 'Vill. Lavva, Post Bhagwanpur', '05-Jul-13', NULL, '20-Oct-05', 'M', '550', NULL, NULL, '9837723111', '', '21', '1', NULL, NULL, '2017-09-01 08:04:42', NULL),
(738, '1392', 'Lavish Kumar', 'Vinod Kumar', 'Kiran Devi', 'VII-A', 'Vill-Ropri Junardar, Post-Kotta, Distt-SRE', '09-Apr-16', NULL, '20-Sep-04', 'M', '573', NULL, NULL, '9410823726', '8273745086', '22', '1', NULL, NULL, '2017-09-01 08:04:42', NULL),
(739, '529', 'Manas Tiwari', 'B K Tiwari', 'Seema Tiwari', 'VII-A', 'Vill Kunwhalia, Post Kanchanpur, Distt. Dewaria', '07-Jul-12', NULL, '05-Aug-05', 'M', '589', NULL, NULL, '9927712284', '8933824049', '23', '1', NULL, NULL, '2017-09-01 08:04:42', NULL),
(740, '938', 'Mohd. Abuzar', 'Abdul Gaffar', 'Shahnaaz', 'VII-A', 'Near Purani Masjid, Gagalheri, SRE', '17-Apr-14', NULL, '', 'M', '641', NULL, NULL, '9639707071', '9412648233', '24', '1', NULL, NULL, '2017-09-01 08:04:42', NULL),
(741, '85', 'Mohd. Anas', 'Abrar Ahmed', 'Mehroj', 'VII-B', 'Vill & Post Khujnawar, Distt. Saharanpur', '04-Jun-11', NULL, '03-Mar-04', 'M', '646', NULL, NULL, '9758139091', '9758051610', '15', '1', NULL, NULL, '2017-09-01 08:05:30', NULL),
(742, '252', 'Mohd. Umar', 'Nashed Ali', 'Saida Bano', 'VII-A', 'Chhutmalpur, Nai basti, Nr water Tank ', '18/5/11', NULL, '09-Dec-01', 'M', '664', NULL, NULL, '9719204013', '9045594191', '25', '1', NULL, NULL, '2017-09-01 08:04:42', NULL),
(743, '185', 'Mukul Kamboj', 'Kuldeep Singh ', 'Kavita', 'VII-B', 'Village Ganjemajra, Post Biharigarh, Dist. Haridwar', '18/4/11', NULL, '13-Sep-06', 'M', '678', NULL, NULL, '9927556777', '9456226722', '16', '1', NULL, NULL, '2017-09-01 08:05:30', NULL),
(744, '420', 'Nitin Kumar Saini', 'Sunil Kumar Saini', 'Manju Devi', 'VII-B', 'Fatehpur, Chhutmalpur', '10-Apr-12', NULL, '07-Sep-05', 'M', '711', NULL, NULL, '9927919617', '9837648387', '17', '1', NULL, NULL, '2017-09-01 08:05:30', NULL),
(745, '669', 'Paras Saini', 'Arvind Saini', 'Balesh Devi', 'VII-B', 'Vill. Sona Sayed Majra', '08-Apr-13', NULL, '28-Oct-04', 'M', '717', NULL, NULL, '9759560182', '8266006424', '18', '1', NULL, NULL, '2017-09-01 08:05:30', NULL),
(746, '886', 'Pranav Saini', 'Sushil Kumar Saini', 'Payal Saini', 'VII-B', 'Vill-Badakla, Post-Chhutmalpur, Distt-SRE', '05-Apr-14', NULL, '20-Sep-04', 'M', '736', NULL, NULL, '9759372490', '9720740909', '19', '1', NULL, NULL, '2017-09-01 08:05:30', NULL),
(747, '1481', 'Ritik', 'Ashok Sethi', 'Monika Rani', 'VII-B', 'Saharanpur, Distt Saharanpur', '18-Apr-16', NULL, '03-Apr-03', 'M', '800', NULL, NULL, '', '', '20', '1', NULL, NULL, '2017-09-01 08:05:30', NULL),
(748, '1353', 'Riyanshu Saini', 'Brij Bhushan Saini', 'Geeta Saini', 'VII-A', 'Vill+Post-Chamarikhera, Distt-SRE', '07-Apr-16', NULL, '09-Jul-04', 'F', '814', NULL, NULL, '9927045348', '9756865384', '26', '1', NULL, NULL, '2017-09-01 08:04:42', NULL),
(749, '150', 'Saliha', 'Kamrun Ali', 'Shabana ', 'VII-B', 'Village Kalalhatti, Post Chhutmalpur, Saharanpur', '12-Apr-11', NULL, '06-Jan-05', 'F', '837', NULL, NULL, '9219490170', '', '21', '1', NULL, NULL, '2017-09-01 08:05:30', NULL),
(750, '134', 'Saniya Rao', 'Rao Parvez Khan', ' ', 'VII-B', 'Village Kheri Shikohpur', '04-Sep-11', NULL, '20-Jun-04', 'F', '859', NULL, NULL, '9758845530', '9358161601', '22', '1', NULL, NULL, '2017-09-01 08:05:30', NULL),
(751, '451', 'Shivam Bhatnagar', 'Sonu Bhatnagar', 'Sarita Devi', 'VII-A', 'Chhutmalpur, Near A.H.P Inter College Roorkee Road ', '16-Apr-12', NULL, '21-Feb-04', 'M', '894', NULL, NULL, '9759562018', '9758286213', '27', '1', NULL, NULL, '2017-09-01 08:04:42', NULL),
(752, '1544', 'Srishti Chaudhary', 'Manoj Chaudhary', 'Renu Chaudhary', 'VII-A', 'Alawalpur Raod, Chhutmalpur, Distt-Saharanpur', '12-04-2017', NULL, '02-05-2005', 'F', '939', NULL, NULL, '9756424276', '8755853123', '28', '1', NULL, NULL, '2017-09-01 08:04:42', NULL),
(753, '11', 'Sumbul Fatima', 'Khushnood', 'Frahdeeba', 'VII-A', 'Arya Nagar, Chhutmalpur', '28/3/11', NULL, '19-06-2005', 'F', '949', NULL, NULL, '9837172812', '9837614891', '29', '1', NULL, NULL, '2017-09-01 08:04:43', NULL),
(754, '1582', 'Swati', 'Sushil', 'Naresho', 'VII-B', 'Vill-Gagalheri, Distt-Saharanpur', '18-04-2017', NULL, '19-06-2007', 'F', '959', NULL, NULL, '9758577424', '', '23', '1', NULL, NULL, '2017-09-01 08:05:30', NULL),
(755, '332', 'Tushar Gautam', 'Vinod Kumar', 'Babita', 'VII-B', 'Vill Gandewara, Post Chhutmalpur, Distt. Saharanpur', '29-Mar-12', NULL, '23-Mar-04', 'M', '980', NULL, NULL, '9359488955', '9837300747', '24', '1', NULL, NULL, '2017-09-01 08:05:30', NULL),
(756, '1115', 'Uday Jaiswal', 'Manoj Kumar Jaiswal', 'Anju Jaiswal', 'VII-B', 'Fatehpur Thana, Chhutmalpur, SRE', '06-Apr-15', NULL, '27-Sep-04', 'M', '984', NULL, NULL, '9719803864', '9436398940', '25', '1', NULL, NULL, '2017-09-01 08:05:30', NULL),
(757, '158', 'Urvashi Gandhi', 'Surendra Gandhi', 'Simmi Gandhi', 'VII-B', 'Punjabi Colony, Chhutmalpur', '13/4/11', NULL, '13-7-2004', 'F', '989', NULL, NULL, '9837320150', '9760363525', '26', '1', NULL, NULL, '2017-09-01 08:05:30', NULL),
(758, '1222', 'Vaishnavi Goyal', 'Vipin Goyal', 'Mamta Goyal', 'VII-A', 'Vill-Gangali, Post-Chhutmalpur, Distt-SRE', '21-Apr-15', NULL, '16-Jul-05', 'F', '1006', NULL, NULL, '9927624270', '', '30', '1', NULL, NULL, '2017-09-01 08:04:43', NULL),
(759, '89', 'Varnit Kamboj', 'Vipin Kamboj', 'Reeta Kamboj', 'VII-A', 'Village Chanchak, Post Sherpur', '04-Jun-11', NULL, '09-Feb-06', 'M', '1028', NULL, NULL, '9457048729', '9410822790', '31', '1', NULL, NULL, '2017-09-01 08:04:43', NULL),
(760, '24', 'Varun Singh', 'Gurudas Singh', 'Sulekha', 'VII-B', 'Village Syed Majra', '04-Feb-11', NULL, '30-Jul-06', 'M', '1030', NULL, NULL, '9837110779', '', '27', '1', NULL, NULL, '2017-09-01 08:05:30', NULL),
(761, '220', 'Zaid Malik', 'Mursleen Ahmad', 'Shabnam', 'VII-B', 'Vill Kameshpur', '28/4/11', NULL, '05-Dec-04', 'M', '1074', NULL, NULL, '9412532339', '9012400986', '28', '1', NULL, NULL, '2017-09-01 08:05:30', NULL),
(762, '1022', 'Abhinav Sharma', 'Rajesh Sharma', 'Shikha Sharma', 'VIII-A', 'New Bhagwati Colony, Behat Road, SRE', '16-Jul-14', NULL, '31-Jan-03', 'M', '4', NULL, NULL, '9520008080', '9319450798', '1', '1', NULL, NULL, '2017-09-01 08:05:39', NULL),
(763, '384', 'Abhinav Yadav', 'Neeraj Kumar', 'Neelam Yadav', 'VIII-A', 'Vill & post Sambhalki Gujjar', '04-Apr-12', NULL, '11-Mar-05', 'M', '48', NULL, NULL, '9675934125', '9758645741', '2', '1', NULL, NULL, '2017-09-01 08:05:39', NULL),
(764, '693', 'Abhishek Rana', 'Chandan Singh', 'Sudeshbala', 'VIII-A', 'Vill. Redi, Post Chamarikhera', '12-Apr-13', NULL, '01-Feb-06', 'M', '56', NULL, NULL, '7500682030', '9837464864', '3', '1', NULL, NULL, '2017-09-01 08:05:39', NULL),
(765, '706', 'Abhishek Saini', 'Sumit Saini', 'Santosh Saini', 'VIII-A', 'Vill. & Post Chhutmalpur', '17-Apr-13', NULL, '30-Aug-02', 'M', '79', NULL, NULL, '9837848195', '9927504311', '4', '1', NULL, NULL, '2017-09-01 08:05:39', NULL),
(766, '72', 'Akash Singhal', 'Manoj Singhal', 'Rashmi Singhal', 'VIII-A', 'Roorkee Rd. Chhutmalpur', '04-Jun-11', NULL, '08-Sep-04', 'M', '81', NULL, NULL, '9412651080', '', '5', '1', NULL, NULL, '2017-09-01 08:05:39', NULL),
(767, '1548', 'Akshit Garg', 'Sanjay Garg', 'Neelam Garg', 'VIII-A', 'Harijan Colony, Gali No.-1, Chhutmalpur, Distt-Saharanpur', '12-04-2017', NULL, '14-04-2004', 'M', '93', NULL, NULL, '9719801243', '7037731934', '6', '1', NULL, NULL, '2017-09-01 08:05:39', NULL),
(768, '743', 'Amit Saini', 'Rajesh Kumar', 'Suman Devi', 'VIII-A', 'Vill. Lavva, Post Bhagwanpur', '05-Apr-13', NULL, '19-Nov-03', 'M', '94', NULL, NULL, '9837723111', '', '7', '1', NULL, NULL, '2017-09-01 08:05:39', NULL),
(769, '401', 'Anam Tehreem', 'Basit Ali', 'Tasmeem Jaha', 'VIII-A', 'Vill Chhapur Post Khubbanpur, Distt. Haridwar', '07-Apr-12', NULL, '27-Feb-05', 'F', '131', NULL, NULL, '9719456433', '', '8', '1', NULL, NULL, '2017-09-01 08:05:39', NULL),
(770, '436', 'Anshika Sharma', 'Brijesh Sharma', 'Beena Sharma', 'VIII-A', 'Shiv Colony, near Jain Bagh, Fatehpur', '11-Apr-12', NULL, '13-Mar-05', 'F', '143', NULL, NULL, '9759999152', '8410535938', '9', '1', NULL, NULL, '2017-09-01 08:05:39', NULL),
(771, '372', 'Anubhav Dhiman', 'Vinod Kumar Dhiman', 'Poonam Dhiman', 'VIII-A', 'Vill & Post Gagalehri', '03-Apr-12', NULL, '24-Nov-04', 'M', '165', NULL, NULL, '9719951346', '9027069980', '10', '1', NULL, NULL, '2017-09-01 08:05:39', NULL),
(772, '1325', 'Anuj Kumar', 'Rishipal Singh', 'Sushma Rani', 'VIII-A', 'Vill-Aurangabad Post-Sherpur Khanazadpur, Distt-SRE', '04-Apr-16', NULL, '10-Sep-05', 'M', '174', NULL, NULL, '9759694352', '9719343643', '11', '1', NULL, NULL, '2017-09-01 08:05:39', NULL),
(773, '1505', 'Aryan Kamboj', 'Parveen Kumar', 'Rita Devi', 'VIII-A', 'Vill-Damodrabad, Post-Biharigarh, Distt-Saharanpur', '06-04-2017', NULL, '03-03-2004', 'M', '175', NULL, NULL, '9719494905', '9675382809', '12', '1', NULL, NULL, '2017-09-01 08:05:39', NULL),
(774, '1310', 'Ayush Kamboj', 'Ajay Kamboj', 'Anju Kamboj', 'VIII-A', 'Harijan Colony, Chhutmalpur, Distt-SRE', '01-Apr-16', NULL, '18-03-2002', 'M', '180', NULL, NULL, '9719444014', '', '13', '1', NULL, NULL, '2017-09-01 08:05:39', NULL),
(775, '1203', 'Chirag Chauhan', 'Parveen Kumar', 'Lata Chauhan', 'VIII-A', 'Vill-Budhwa Shaheed, Post-Buggawala, Distt-Haridwar', '15-Apr-15', NULL, '12-Oct-03', 'M', '203', NULL, NULL, '9761350315', '9758063453', '14', '1', NULL, NULL, '2017-09-01 08:05:39', NULL),
(776, '637', 'Eisha Saini', 'Pritam Singh', 'Savita Saini', 'VIII-A', 'Vill. Badshahpur, Post Khushhalipur', '03-Apr-13', NULL, '13-Jan-05', 'F', '222', NULL, NULL, '9758693633', '9759469016', '15', '1', NULL, NULL, '2017-09-01 08:05:39', NULL),
(777, '694', 'Gori Sharma', 'Neeraj Sharma', 'Suman Sharma', 'VIII-A', 'Near PNB, D.Dun Road, Chhutmalpur', '15-Apr-13', NULL, '16-Mar-04', 'F', '230', NULL, NULL, '9760083829', '', '16', '1', NULL, NULL, '2017-09-01 08:05:39', NULL),
(778, '425', 'Harsh Singh Chauhan', 'Arvind Kumar', 'Mamta Chauhan', 'VIII-A', 'V & P Khubbanpur, Haridwar', '11-Apr-12', NULL, '27-Mar-06', 'M', '231', NULL, NULL, '8006141006', '8909105320', '17', '1', NULL, NULL, '2017-09-01 08:05:39', NULL),
(779, '120', 'Harshit Kamboj', 'Ravi Pal', 'Reena Devi', 'VIII-A', 'Village Sherpur Khanazadpur', '04-Jul-11', NULL, '15-Jun-04', 'M', '239', NULL, NULL, '9758126843', '9761715800', '18', '1', NULL, NULL, '2017-09-01 08:05:39', NULL),
(780, '868', 'Janvi Kamboj', 'Ravinder Kumar', 'Shivani Kamboj', 'VIII-A', 'Vill. & Post Sherpur Khanazadpur', '03-Apr-14', NULL, '20-Sep-05', 'F', '290', NULL, NULL, '9758962429', '9720840184', '19', '1', NULL, NULL, '2017-09-01 08:05:40', NULL),
(781, '296', 'Junaid Alam', 'Ch. Meherban Alam', 'Jasmine', 'VIII-A', '34 New Col, Nr Gandhi Chowk, Chhutmalpur', '20/7/11', NULL, '04-Feb-04', 'M', '293', NULL, NULL, '9837659601', '', '20', '1', NULL, NULL, '2017-09-01 08:05:40', NULL),
(782, '1098', 'Kartik Kamboj', 'Rakesh Kamboj', 'Ravita Kamboj', 'VIII-A', 'Vill- Sherpur Khanazadpur, Post-Khas, Distt-SRE', '', NULL, '16-Aug-03', 'M', '337', NULL, NULL, '9639565771/9757740581', '9639565771', '21', '1', NULL, NULL, '2017-09-01 08:05:40', NULL),
(783, '791', 'Khushi Tyagi', 'Sandeep Kumar ', 'Manju tyagi', 'VIII-A', 'Vill Rangail , Post Khajuri Akbarpur Dist Saharanpur ', '19-Mar-14', NULL, '09-Nov-04', 'F', '352', NULL, NULL, '9760778138', '9761884004', '22', '1', NULL, NULL, '2017-09-01 08:05:40', NULL),
(784, '1237', 'Madhav Chhabra', 'Sanjay Chhabra', 'Anu Chhabra', 'VIII-A', 'New Gopal Nagar, Near Shiv Mandir, Saharanpur', '21-Apr-15', NULL, '05-Sep-03', 'M', '359', NULL, NULL, '9627758879', '7088588155', '23', '1', NULL, NULL, '2017-09-01 08:05:40', NULL),
(785, '142', 'Mohd. Hamza', 'Shamim Ahmed', 'Asma Parveen', 'VIII-A', 'Village Fatehpur', '04-Nov-11', NULL, '17-Sep-03', 'M', '369', NULL, NULL, '7409613529', '9719823276', '24', '1', NULL, NULL, '2017-09-01 08:05:40', NULL),
(786, '1529', 'Navneet Kamboj', 'Avdhesh Kamboj', 'Soniya Rani', 'VIII-A', 'Shiv Colony,Fatehpur, Chhutmalpur, Distt-Saharanpur', '10-04-2017', NULL, '02-03-2005', 'M', '370', NULL, NULL, '9927035711', '9917269080', '25', '1', NULL, NULL, '2017-09-01 08:05:40', NULL),
(787, '667', 'Nikhil Verma', 'Krishna  Verma', 'Rajni Verma', 'VIII-A', 'Shiv Colony, Dehradun Road, Fatehpur', '06-Apr-13', NULL, '05-Jul-04', 'M', '416', NULL, NULL, '9719238104', '', '26', '1', NULL, NULL, '2017-09-01 08:05:40', NULL),
(788, '1422', 'Rao Aamir', 'Abdul Aziz', 'Rabiya Rao', 'VIII-A', 'Vill-guddam,Chhutmalur', '21-Apr-16', NULL, '26-Oct-03', 'M', '423', NULL, NULL, '9719527542', '', '27', '1', NULL, NULL, '2017-09-01 08:05:40', NULL),
(789, '1654', 'Riya Dumda', 'Harish Kumar', 'Pooja', 'VIII-B', 'Near Post Office, saharanpur Road, Chhutmalpur, Distt_saharanpur', '27-07-2017', NULL, '10-01-2004', 'M', '433', NULL, NULL, '7456005954', '8439268466', '36', '1', NULL, NULL, '2017-09-06 02:20:19', NULL),
(790, '1439', 'Sagar luthra', 'Sanjeev Kumar luthra', 'Pooja Luthra', 'VIII-A', 'Prempuri Colony, Saharanpur', '03-Feb-16', NULL, '21-Jan-04', 'M', '434', NULL, NULL, '8445836138', '', '29', '1', NULL, NULL, '2017-09-01 08:05:40', NULL),
(791, '1438', 'Sahil luthra', 'Sanjeev Kumar luthra', 'Pooja Luthra', 'VIII-A', 'Prempuri Colony, Saharanpur', '03-Feb-16', NULL, '21-Jan-04', 'M', '445', NULL, NULL, '8445836138', '', '30', '1', NULL, NULL, '2017-09-01 08:05:40', NULL),
(792, '683', 'Shaurya Pratap Singh', 'Nepal Singh', 'Anju Chauhan', 'VIII-A', 'Biharigarh, Saharanpur', '10-Apr-13', NULL, '18-Feb-06', 'M', '452', NULL, NULL, '9761377187', '9675140648', '31', '1', NULL, NULL, '2017-09-01 08:05:40', NULL),
(793, '294', 'Shekhar Saini', 'Ram Kumar  ', 'Usha', 'VIII-A', 'Alawalpur, Post Chhutmalpur', '19/7/11', NULL, '13-Oct-04', 'M', '455', NULL, NULL, '8954616527', '', '32', '1', NULL, NULL, '2017-09-01 08:05:40', NULL),
(794, '1552', 'Shivam Chauhan', 'Yogesh Chauhan', 'Babli Chauhan', 'VIII-A', 'Fatehpur Bhado,Shiv Colony, Post-Chhutamalpur, Distt-Saharanpur', '13-04-2017', NULL, '29-10-2004', 'M', '456', NULL, NULL, '7500311573', '7351438913', '33', '1', NULL, NULL, '2017-09-01 08:05:40', NULL),
(795, '634', 'Shorya Kumar', 'Sanjay Kumar', 'Vijeta', 'VIII-A', 'Vill & Post Biharigarh, Near Petrol Pump', '03-Apr-13', NULL, '01-Oct-04', 'M', '463', NULL, NULL, '9761061645', '9761061645', '34', '1', NULL, NULL, '2017-09-09 03:19:16', NULL),
(796, '777', 'Sumit Choudhary', 'Virendra Choudhary', 'Babita Choudhary', 'VIII-A', 'Vill-Kheri Shikohpur, Post-Khas, Haridwar', '05-Mar-14', NULL, '02-Feb-03', 'M', '495', NULL, NULL, '9634132162', '8273732315', '35', '1', NULL, NULL, '2017-09-01 08:05:40', NULL),
(797, '126', 'Vaishali Chaudhary', 'Ashok Kumar', 'Sanyogita', 'VIII-A', 'Village Alawalpur', '04-Aug-11', NULL, '06-Jul-03', 'F', '501', NULL, NULL, '8057916621', '7830279044', '36', '1', NULL, NULL, '2017-09-01 08:05:40', NULL),
(798, '509', 'Vanshika Choudhary', 'Anuraj Singh', 'Monika', 'VIII-A', 'Vill Abha, Post Chudiyala, Distt. Saharanpur', '22-Jun-12', NULL, '05-Oct-04', 'F', '507', NULL, NULL, '9761476094', '9720652745', '37', '1', NULL, NULL, '2017-09-01 08:05:40', NULL),
(799, '897', 'Aadil Rao', 'Rao Shahid Ali', 'Zohra Begun', 'VIII-B', 'Vill & Post-Khujnawar, Distt-SRE', '07-Apr-14', NULL, '', 'M', '509', NULL, NULL, '7500656565', '9719055550', '1', '1', NULL, NULL, '2017-09-01 08:05:48', NULL),
(800, '1101', 'Aastha Yadav', 'Anil Kumar', 'Anju Rani', 'VIII-B', 'Vill-Rajapur, Post-Chhutmalpur, Distt-SRE', '04-Apr-15', NULL, '22-Oct-05', 'F', '521', NULL, NULL, '9897345257', '9837413132', '2', '1', NULL, NULL, '2017-09-01 08:05:48', NULL),
(801, '244', 'Abdul Ahad', 'Mansoor Ahmed', 'Sna Praveen', 'VIII-B', 'Chhutmalpur, Harijan Colony, Str. No. 2', '05-Oct-11', NULL, '12-Feb-05', 'M', '533', NULL, NULL, '9761350048', '7351350048', '3', '1', NULL, NULL, '2017-09-01 08:05:48', NULL),
(802, '1147', 'Aman Chauhan', 'Anil Kumar', 'Lalita', 'VIII-B', 'Vill-Chouharpur, Post-Behat, Distt-SRE', '07-Apr-15', NULL, '16-Aug-03', 'M', '537', NULL, NULL, '9719356843', '9720512085', '4', '1', NULL, NULL, '2017-09-01 08:05:48', NULL),
(803, '467', 'Amit Singh', 'Inderpal Singh', 'Raman Devi', 'VIII-B', 'Alawalpur, Post Chhutmalpur', '19-Apr-12', NULL, '10-Dec-04', 'M', '582', NULL, NULL, '8006928283', '', '5', '1', NULL, NULL, '2017-09-01 08:05:48', NULL),
(804, '211', 'Anmol Kumar Dhiman', 'Sunil Kumar Dhiman', 'Reetu Dhiman', 'VIII-B', 'Village Kalalhatti', '25/4/11', NULL, '24-May-03', 'M', '598', NULL, NULL, '9219948333', '9259358333', '6', '1', NULL, NULL, '2017-09-01 08:05:48', NULL),
(805, '54', 'Anushka Kamboj', 'Anvesh Kamboj', 'Kulwanti Devi', 'VIII-B', 'Village Chanchak', '04-May-11', NULL, '22-Dec-05', 'F', '656', NULL, NULL, '9719173068', '', '7', '1', NULL, NULL, '2017-09-01 08:05:48', NULL),
(806, '229', 'Aryan Saini', 'Raj Kumar Saini', 'Kusham', 'VIII-B', 'Village Alawalpur', '30/4/11', NULL, '', 'M', '661', NULL, NULL, '9719445824', '9675681952', '8', '1', NULL, NULL, '2017-09-01 08:05:48', NULL),
(807, '205', 'Ayushi Saini', 'Satish Saini', 'Nisha Saini', 'VIII-B', 'Opp Gurudwara, Village Biharigarh', '21/4/11', NULL, '29-3-2005', 'F', '694', NULL, NULL, '9412080891', '9758404211', '9', '1', NULL, NULL, '2017-09-01 08:05:48', NULL),
(808, '744', 'Bhavna Saini', 'Rajesh Kumar', 'Suman Devi', 'VIII-B', 'Vill. Lavva, Post Bhagwanpur', '05-Jul-13', NULL, '19-Nov-03', 'F', '696', NULL, NULL, '9837723111', '', '10', '1', NULL, NULL, '2017-09-01 08:05:48', NULL),
(809, '362', 'Chirag Gautam', 'Ravindra Gatam', 'Meena Devi', 'VIII-B', 'Kishangarh colony, Biharigarh', '02-Apr-12', NULL, '03-May-04', 'M', '704', NULL, NULL, '9719110549', '9761440365', '11', '1', NULL, NULL, '2017-09-01 08:05:48', NULL),
(810, '255', 'Fareed', 'Fayyaz', 'Shabana', 'VIII-B', 'Vill Sikanderpur', '19/5/11', NULL, '', 'M', '705', NULL, NULL, '9720632363', '9760315886', '12', '1', NULL, NULL, '2017-09-01 08:05:48', NULL),
(811, '602', 'Gautam Rathi', 'Prempal Rathi', 'Sushila devi', 'VIII-B', 'Chhutmalpur, Co-operative seed Store ', '02-Apr-13', NULL, '20-Jan-06', 'M', '706', NULL, NULL, '8859900716', '9627479261', '13', '1', NULL, NULL, '2017-09-01 08:05:48', NULL),
(812, '159', 'Harsh Gandhi', 'Surendra Gandhi', 'Simmi Gandhi', 'VIII-B', 'Punjabi Colony, Chhutmalpur', '13/4/11', NULL, '02-Jul-03', 'M', '749', NULL, NULL, '9837320150', '', '14', '1', NULL, NULL, '2017-09-01 08:05:48', NULL),
(813, '572', 'Harsh Saini', 'Satish Saini', 'Alka Saini', 'VIII-B', 'Mandawar, Post-Chhutmalpur', '28-Mar-13', NULL, '11-Oct-06', 'M', '754', NULL, NULL, '9758447501', '8445608039', '15', '1', NULL, NULL, '2017-09-01 08:05:48', NULL),
(814, '1535', 'Harshita Yadav', 'Rajesh Yadav', 'Meera Yadav', 'VIII-B', 'Vill-Mandebansh, Post-Harora, Distt-Saharanpur', '10-04-2017', NULL, '11-02-2005', 'M', '756', NULL, NULL, '8393955761', '9758559410', '16', '1', NULL, NULL, '2017-09-01 08:05:48', NULL),
(815, '1120', 'Jayant Chaudhary', 'Ajender Chaudhary', 'Lalita', 'VIII-B', 'Vill-Ibrahimpur, Post-Chhutmalpur, Distt-SRE', '06-Apr-15', NULL, '01-Mar-04', 'M', '757', NULL, NULL, '9719444347', '9761835306', '17', '1', NULL, NULL, '2017-09-01 08:05:48', NULL);
INSERT INTO `studentmgmts` (`id`, `admission_no`, `student_name`, `student_father_name`, `student_mother_name`, `student_class_section`, `student_address`, `student_joining_date`, `student_images`, `student_dob`, `student_gender`, `student_username`, `student_password`, `student_email`, `student_mob1`, `student_mob2`, `student_rollno`, `sessionid`, `deleted_at`, `created_at`, `updated_at`, `code`) VALUES
(816, '1126', 'Kajal Saini', 'Rupendra', 'Upasna', 'VIII-B', 'Vill-Mirzapur Poul, Post-Mirzapur, Distt-SRE', '06-Apr-15', NULL, '07-Aug-02', 'F', '770', NULL, NULL, '9761231986', '8954775516', '18', '1', NULL, NULL, '2017-09-01 08:05:48', NULL),
(817, '797', 'Khushi Saini', 'Sanjay Saini', 'Ritu Saini', 'VIII-B', 'Vill Lava,Post-Khubbanpur,Distt-Haridwar', '19-Mar-14', NULL, '28-Mar-05', 'F', '804', NULL, NULL, '9837241454', '9536035264', '19', '1', NULL, NULL, '2017-09-01 08:05:48', NULL),
(818, '1340', 'Mansi Yadav', 'Sudhir Kumar Yadav', 'Sapna Yadav', 'VIII-B', 'Vill-Agrsen Chowk Gagalheri, Distt-SRE', '06-Apr-16', NULL, '22-Dec-04', 'F', '806', NULL, NULL, '9672973288', '7597908113', '20', '1', NULL, NULL, '2017-09-01 08:05:48', NULL),
(819, '502', 'Mohd. Shibab', 'Mohd. Ikraam', 'Shahjahan', 'VIII-B', 'Vill Nanhera Bed Begampur', '18-May-12', NULL, '15-Aug-04', 'M', '809', NULL, NULL, '9837535395', '9529486037', '21', '1', NULL, NULL, '2017-09-01 08:05:48', NULL),
(820, '1154', 'Nashra Ansari', 'Rizwan Ansari', 'Aziza Ansari', 'VIII-B', 'Harijan Colony, Chhutmalpur, Distt-SRE  ', '08-Apr-15', NULL, '25-Feb-03', 'F', '823', NULL, NULL, '9319820580', '8273022652', '22', '1', NULL, NULL, '2017-09-01 08:05:48', NULL),
(821, '1336', 'Nikhil Kumar', 'Mukesh Kumar', 'Munesh devi', 'VIII-B', 'Vill-Aurangabad Post-Sherpur Khanazadpur, Distt-SRE', '06-Apr-16', NULL, '10-Aug-05', 'M', '831', NULL, NULL, '9627890567', '9759560567', '23', '1', NULL, NULL, '2017-09-01 08:05:48', NULL),
(822, '1164', 'Nikita Rana', 'Late Mr. Kulbeer Singh', 'Rekha Rana', 'VIII-B', 'Vill & Post-Bahera Sandal Singh, Distt-SRE', '08-Apr-15', NULL, '24-Feb-04', 'F', '833', NULL, NULL, '9012166530', '9456293442', '24', '1', NULL, NULL, '2017-09-01 08:05:48', NULL),
(823, '1527', 'Priya Goyal', 'Pawan Kumar', 'Savita Goyal', 'VIII-B', 'Vill-Alawalpur, Post-Chhutmalpur, Distt-Haridwar', '08-04-2017', NULL, '12-03-2002', 'F', '885', NULL, NULL, '9758712658', '9917402994', '25', '1', NULL, NULL, '2017-09-01 08:05:48', NULL),
(824, '329', 'Priyanshu Kamboj', 'Arvind Kumar', 'Ujala', 'VIII-B', 'Vill Taqipur, Post Sherpur Khanajadpur', '28-Mar-12', NULL, '07-Dec-04', 'M', '887', NULL, NULL, '9675820836', '7830839109', '26', '1', NULL, NULL, '2017-09-01 08:05:48', NULL),
(825, '1376', 'Priyanshu Rana', 'Meshar', 'Rajnesh', 'VIII-B', 'Vill-Satpura Post-Kurdikhera, Dist-SRE', '09-Apr-16', NULL, '', 'M', '895', NULL, NULL, '9759429719', '9758631604', '27', '1', NULL, NULL, '2017-09-01 08:05:48', NULL),
(826, '391', 'Priyanshu Saini', 'Parveen Saini', 'Pinki Saini', 'VIII-B', 'Vill & Post Banwala', '06-Apr-12', NULL, '13-Apr-04', 'M', '896', NULL, NULL, '9759750027', '9759093881', '28', '1', NULL, NULL, '2017-09-01 08:05:48', NULL),
(827, '1619', 'Ritik Vats', 'Sudhir Jha', 'Monika Devi', 'VIII-B', 'Near Shri Nagar Megzine, Putki Srinagar, Kusunda, Dhanbad, Jharkhand  ', '18-05-2017', NULL, '18-01-2005', 'M', '906', NULL, NULL, '8979790545', '', '29', '1', NULL, NULL, '2017-09-01 08:05:49', NULL),
(828, '795', 'Ritika Saini', 'Vinod Saini', 'Videsh Saini', 'VIII-B', 'Gandhi Colony, Chhutmalpur', '19-Mar-14', NULL, '05-Nov-04', 'F', '919', NULL, NULL, '9411030735', '8393931565', '30', '1', NULL, NULL, '2017-09-01 08:05:49', NULL),
(829, '396', 'Sajal Kocher', 'Anil Kocher', 'Meenu Kocher', 'VIII-B', 'Punjabi Colony, Chhutmalpur', '07-Apr-12', NULL, '11-Nov-03', 'M', '951', NULL, NULL, '9897208073', '', '31', '1', NULL, NULL, '2017-09-01 08:05:49', NULL),
(830, '605', 'Shivam Jain', 'Sandeep Jain', 'Kamini Jain', 'VIII-B', 'Chhutmalpur, Gandhi Colony', '02-Apr-13', NULL, '04-Aug-04', 'M', '1004', NULL, NULL, '9259405315', '', '32', '1', NULL, NULL, '2017-09-01 08:05:49', NULL),
(831, '122', 'Shwet Kamboj', 'Pankaj Kumar', 'Neelam Kamboj', 'VIII-B', 'Village Biharigarh', '04-Jul-11', NULL, '04-Aug-04', 'M', '1022', NULL, NULL, '9634650659', '9758433136', '33', '1', NULL, NULL, '2017-09-01 08:05:49', NULL),
(832, '12', 'Varsha Chauhan', 'Shree Om', 'Rajni ', 'VIII-B', 'Budha Khera Pundir, Post Chamarikhera', '29/3/11', NULL, '02-Aug-05', 'F', '1029', NULL, NULL, '9837139927', '9045939432', '34', '1', NULL, NULL, '2017-09-01 08:05:49', NULL),
(833, '14', 'Yashdev Saini', 'Sehdev Singh Saini', 'Reeta Saini', 'VIII-B', 'Chhutmalpur, Hind Vihar Colony', '31/3/11', NULL, '11-Jun-05', 'M', '1067', NULL, NULL, '9917284121', '9012399005', '35', '1', NULL, NULL, '2017-09-01 08:05:49', NULL),
(834, '1415', 'Aashima Choudhary', 'Sanjeev Choudhary', 'Suman', 'X-A', 'Vill-Baheri Gujjar, Saharanpur', '20-Apr-16', NULL, '20-Sep-01', 'F', '6', NULL, NULL, '9084142525', '8865912652', '1', '1', NULL, NULL, '2017-08-31 15:24:41', NULL),
(835, '712', 'Aasif', 'Mohd.Fayyaz', 'Dilshad', 'X-A', 'Vill. Kailashpur, Distt. Saharanpur', '22-Apr-13', NULL, '14-Aug-98', 'M', '15', NULL, NULL, '9997044268', '9927520525', '2', '1', NULL, NULL, '2017-08-31 15:24:41', NULL),
(836, '73', 'Aastha Singhal', 'Vipin Kumar', 'Pooja', 'X-A', 'Chhutmalpur, Gandhi Chowk', '04-Jun-11', NULL, '28-Mar-02', 'F', '38', NULL, NULL, '9759560231', '', '3', '1', NULL, NULL, '2017-08-31 15:24:41', NULL),
(837, '1294', 'Aman Rana', 'Ram Kumar Rana', 'Rani Rana', 'X-A', 'Vill-Jeewala Post-Muzaffrabad, Distt-SRE', '25-Mar-16', NULL, '03-Mar-01', 'M', '40', NULL, NULL, '7895579106', '7895579106', '4', '1', NULL, NULL, '2017-08-31 15:24:41', NULL),
(838, '1440', 'Amisha Kochar', 'Anil Kochar', 'Meenu Kochar', 'X-A', 'Punjabi Colony, Chhutmalpur, Distt-SRE', '04-May-16', NULL, '19-Feb-01', 'F', '47', NULL, NULL, '9568153318', '9897208073', '5', '1', NULL, NULL, '2017-08-31 15:24:41', NULL),
(839, '1426', 'Anishka Rana', 'Manmohan Singh Rana', 'Rajni Rana', 'X-A', 'Saharanpur Road, Chhutmalpur, Distt-SRE', '25-Apr-16', NULL, '24-Feb-04', 'F', '52', NULL, NULL, '8267969924', '8006910433', '6', '1', NULL, NULL, '2017-08-31 15:24:41', NULL),
(840, '1073', 'Anshul Saini', 'Sunderlal', 'Mithlesh Devi', 'X-A', 'Vill-Fatehpur Bhado, Shiv Colony, DDN Road, Chhutmalpur,SRE', '30-Mar-15', NULL, '10-Oct-01', 'M', '59', NULL, NULL, '9761191850', '8445733424', '7', '1', NULL, NULL, '2017-08-31 15:24:41', NULL),
(841, '1074', 'Anushtha Saini', 'Mukesh Saini', 'Susheela Saini', 'X-A', 'Opp. State Bank, DDN Road, Chhutmalpur', '31-Mar-15', NULL, '01-Oct-02', 'F', '102', NULL, NULL, '9411039492', '9411039492', '8', '1', NULL, NULL, '2017-08-31 15:24:41', NULL),
(842, '1305', 'Aqsa Zeeshan', 'Mohd. Zeeshan', 'Shakira', 'X-A', 'Vill-Bhattpura, Chhutmalpur, Distt-SRE', '29-Mar-16', NULL, '06-Mar-01', 'F', '104', NULL, NULL, '9719115577', '9720909333', '9', '1', NULL, NULL, '2017-08-31 15:24:41', NULL),
(843, '49', 'Arihant Jain', 'Naveen Jain', 'Sangeeta Jain', 'X-A', 'Kamboj colony,chhumalpur', '04-May-11', NULL, '28-Aug-02', 'M', '123', NULL, NULL, '9568064299', '9837648766', '10', '1', NULL, NULL, '2017-08-31 15:24:41', NULL),
(844, '1072', 'Arpit Kumar', 'Arvind Kumar', 'Babita ', 'X-A', 'Shiv Colony, DDN Road, Fatehpur Bhado, Chhutmalpur, SRE', '30-Mar-15', NULL, '23-05-2002', 'M', '151', NULL, NULL, '9758338119', '9536791560', '11', '1', NULL, NULL, '2017-08-31 15:24:41', NULL),
(845, '1293', 'Aryan', 'Sushil Kumar', 'Deepa Verma', 'X-A', 'Shiv Colony Fatehpur, Chhutmalpur, Distt-SRE', '22-Mar-16', NULL, '18-11-01', 'M', '166', NULL, NULL, '9758674889', '9536653585', '12', '1', NULL, NULL, '2017-08-31 15:24:41', NULL),
(846, '824', 'Aryan Kumar', 'Parvesh Kumar', 'Veena ', 'X-A', 'Shiv Colony, Fatehpur Bhado, Chhutmalpur', '31-Mar-14', NULL, '01-Dec-01', 'M', '171', NULL, NULL, '9837265344', '8445467456', '13', '1', NULL, NULL, '2017-08-31 15:24:41', NULL),
(847, '1285', 'Ashish Kamboj', 'Vipin Kumar Singh', 'Neelam Kamboj', 'X-A', 'Vill-Meerpur, Post-Sherpur, Distt-SRE', '16-Mar-16', NULL, '29-Nov-03', 'M', '172', NULL, NULL, '9719551156', '9627057210', '14', '1', NULL, NULL, '2017-08-31 15:24:42', NULL),
(848, '1287', 'Bhanu Saini', 'Narendar Saini (Late)', 'Neelam Saini', 'X-A', 'Suraj Vuhar Colony, Chhutmalpur, Distt-SRE', '22-Mar-16', NULL, '18-Feb-02', 'M', '195', NULL, NULL, '9761616708', '7078359725', '15', '1', NULL, NULL, '2017-08-31 15:24:42', NULL),
(849, '613', 'Harshit Saini', 'Sushil Saini', 'Rajni Saini', 'X-A', 'Sona Sayed Majra', '02-Apr-13', NULL, '30-May-02', 'M', '226', NULL, NULL, '9719090351', '8273370440', '16', '1', NULL, NULL, '2017-08-31 15:24:42', NULL),
(850, '1364', 'Heera Singh', 'Rajendra Singh', 'Baby', 'X-A', 'Vill-Jajner, Post- Chhutmalpur, Distt-SRE', '08-Apr-16', NULL, '27-12-2004', 'M', '243', NULL, NULL, '9837706300', '9761414555', '17', '1', NULL, NULL, '2017-08-31 15:24:42', NULL),
(851, '235', 'Himanshu Saini', 'Charan Singh', 'Lata', 'X-A', 'Village Dhalla Majra', '05-Jun-11', NULL, '30-Jul-03', 'M', '250', NULL, NULL, '9758747113', '', '18', '1', NULL, NULL, '2017-08-31 15:24:42', NULL),
(852, '1318', 'Hussain Ahmed', 'Massom Ali', 'Farzana ', 'X-A', 'Vill-Biharigarh Post-Biharigarh, Distt-SRE', '02-Apr-16', NULL, '06-Jun-02', 'M', '257', NULL, NULL, '9719393096', '9536207383', '19', '1', NULL, NULL, '2017-08-31 15:24:42', NULL),
(853, '1317', 'Kamal Rana', 'Mohkam Singh (late)', 'Sushma Devi', 'X-A', 'Vill-Roahit Nagar, Biharigarh Behind Petrol Pump, Distt-SRE', '02-Apr-16', NULL, '03-May-02', 'M', '271', NULL, NULL, '9719191984', '9761350588', '20', '1', NULL, NULL, '2017-08-31 15:24:42', NULL),
(854, '1303', 'Kartik Batra', 'Harish Batra', 'Savita Batra', 'X-A', 'Punjabi Colony, Chhutmalpur, Distt-SRE', '29-Mar-16', NULL, '27-Jan-02', 'M', '283', NULL, NULL, '7500838333', '9719370260', '21', '1', NULL, NULL, '2017-08-31 15:24:42', NULL),
(855, '138', 'Kunal Khurana', 'Naveen Khurana', 'Dheeru Khurana', 'X-A', 'Punjabi Colony Chhutmalpur', '04-Sep-11', NULL, '09-Sep-02', 'M', '285', NULL, NULL, '9319968166', '9319968167', '22', '1', NULL, NULL, '2017-08-31 15:24:42', NULL),
(856, '1311', 'Lavi Kamboj', 'Ajay Kamboj', 'Anju Kamboj', 'X-A', 'Harijan Colony, Chhutmalpur, Distt-SRE', '01-Apr-16', NULL, '16-12-2001', 'M', '292', NULL, NULL, '9719444014', '', '23', '1', NULL, NULL, '2017-08-31 15:24:42', NULL),
(857, '123', 'Manish Gandhi', 'Surendra Gandhi', 'Simmi Gandhi', 'X-A', 'Punjabi Colony Chhutmalpur', '04-Jul-11', NULL, '10-Oct-01', 'M', '301', NULL, NULL, '9837320150', '7417496005', '24', '1', NULL, NULL, '2017-08-31 15:24:42', NULL),
(858, '1467', 'Mohd Hussain', 'Mohd Imran', 'Shahjhan Parveen', 'X-A', 'D.dun Road, Chhutmalpur, Distt-Sre', '20-May-16', NULL, '17-12-2001', 'M', '310', NULL, NULL, '9917756378', '9675933802', '25', '1', NULL, NULL, '2017-08-31 15:24:42', NULL),
(859, '705', 'Mohd. Hussain', 'Mohd. Shameem', 'Asma', 'X-A', 'Vill. Fatehpur', '17-Apr-13', NULL, '30-Jul-02', 'M', '358', NULL, NULL, '9719823276', '7409623529', '26', '1', NULL, NULL, '2017-08-31 15:24:42', NULL),
(860, '1443', 'Muskan Mirza', 'Abdul Sattar', 'Firdoz', 'X-A', 'Dehradun Road, Chhutmalpur (front of Union Bank), Distt-SRE', '11-May-16', NULL, '02-Dec-01', 'F', '402', NULL, NULL, '9720909629', '8979809629', '27', '1', NULL, NULL, '2017-08-31 15:24:42', NULL),
(861, '1075', 'Nishtha Kamboj', 'Yogendra Kumar Kamboj', 'Veenam Devi', 'X-A', 'H.No.18, Bansal Vihar Colony, Chhutmalpur', '31-Mar-15', NULL, '22-Feb-03', 'F', '418', NULL, NULL, '8006337140', '8273749464', '28', '1', NULL, NULL, '2017-08-31 15:24:42', NULL),
(862, '1298', 'Rajeev Kumar', 'Sushil Kumar', 'Ravita', 'X-A', 'Fatehpur Bhado, Opp Hospital, Distt-SRE', '28-Mar-16', NULL, '10-Mar-01', 'M', '449', NULL, NULL, '9758278121', '9720381810', '29', '1', NULL, NULL, '2017-08-31 15:24:42', NULL),
(863, '1205', 'Rao Gajanfar', 'Rao Irfan', 'Shadna', 'X-A', 'Vill- Kheri Shikohpur, Post- Khas, Distt-Haridwar', '15-Apr-15', NULL, '10-Apr-02', 'M', '457', NULL, NULL, '9368402016', '9719765983', '30', '1', NULL, NULL, '2017-08-31 15:24:42', NULL),
(864, '1309', 'Rao Mehvez Ali', 'Rao Farhat Ali', 'Jubaida', 'X-A', 'Vill-Kheri Sikohpur, Post-Kheri, Distt-Haridwar', '22-Mar-16', NULL, '15-10-2001', 'M', '464', NULL, NULL, '8273430719', '9758873675', '31', '1', NULL, NULL, '2017-08-31 15:24:42', NULL),
(865, '1163', 'Saba Naaz', 'Mansoor Ali', 'Salma Khatoon', 'X-A', 'Near Inspection Bunglow, (PWD), Fatehpur Bhado, SRE', '07-Apr-15', NULL, '07-Jun-02', 'F', '476', NULL, NULL, '8859473410', '8445743792', '32', '1', NULL, NULL, '2017-08-31 15:24:42', NULL),
(866, '1288', 'Samar Chaudhary', 'Mangesh Chaudhary (Late)', 'Ravita Rani', 'X-A', 'Maharana Partap Colony Gali No.-3, Chhutmalpur, Distt-SRE', '22-Mar-16', NULL, '11-Feb-02', 'F', '483', NULL, NULL, '9758634006', '9758999375', '33', '1', NULL, NULL, '2017-08-31 15:24:42', NULL),
(867, '871', 'Sara Rao', 'Aamir Ali Khan', 'Razia Rao', 'X-A', 'Vill. Kheri Shikohpur', '03-Apr-14', NULL, '15-Mar-02', 'F', '491', NULL, NULL, '9997130314', '9927369595', '34', '1', NULL, NULL, '2017-08-31 15:24:43', NULL),
(868, '1300', 'Simran Kamboj', 'Rajneesh Kamboj', 'Paramjeet Kamboj', 'X-A', 'Vill-Takipur, Post-Sherpur Khanazadpur, Distt-SRE', '28-Mar-16', NULL, '16-08-2002', 'M', '511', NULL, NULL, '9758408197', '', '35', '1', NULL, NULL, '2017-08-31 15:24:43', NULL),
(869, '1289', 'Udit Nautiyal', 'Hukum Singh', 'Sangita Nautiyal', 'X-A', 'Gali No.-3 Harijan Colony Chhutmalpur, Distt-SRE', '22-Mar-16', NULL, '18-Sep-03', 'M', '520', NULL, NULL, '8958462869', '9927569575', '36', '1', NULL, NULL, '2017-08-31 15:24:43', NULL),
(870, '1297', 'Vaibhav Goyal', 'Sanjay Goyal', 'Santosh', 'X-A', 'Vill-Biharigarh, Distt-SRE', '28-Mar-16', NULL, '22-06-2001', 'M', '543', NULL, NULL, '8273774343', '', '37', '1', NULL, NULL, '2017-08-31 15:24:43', NULL),
(871, '1314', 'Vikas Saini', 'Pramod Kumar', 'Savita', 'X-A', 'Vill-Alawalpur, Post-Chhutmalpur, Distt-Haridwar', '01-Apr-16', NULL, '25-Jun-03', 'M', '545', NULL, NULL, '9720757049', '9759501556', '38', '1', NULL, NULL, '2017-08-31 15:24:43', NULL),
(872, '748', 'Vinay Kumar', 'Sushil Kumar (Late)', 'Kusum Lata (Late)', 'X-A', 'Vill Battan Wala, Post Chamarikhera', '08-Jul-13', NULL, '27-Jul-01', 'M', '556', NULL, NULL, '9720801519', '8954494978', '39', '1', NULL, NULL, '2017-08-31 15:24:43', NULL),
(873, '1320', 'Aaditya Rana', 'Subodh Rana', 'Prabha Rani', 'X-B', 'Vill-Gangali post-Chhutmalpur, Distt-SRE', '04-Apr-16', NULL, '15-02-2003', 'M', '571', NULL, NULL, '7253822866', '7500352187', '1', '1', NULL, NULL, '2017-09-03 09:51:05', NULL),
(874, '1152', 'Aanchal Gautam', 'Vinod Kumar', 'Babita', 'X-B', 'Vill-Gandewara, Post-Chhutmalpur, Distt-SRE', '08-Apr-15', NULL, '07-Jul-03', 'F', '583', NULL, NULL, '9837300747', '8859692704', '2', '1', NULL, NULL, '2017-09-03 09:51:05', NULL),
(875, '689', 'Aayush Yadav', 'Sandeep Yadav', 'Baby', 'X-B', 'Vill. & Post Chauradev', '11-Apr-13', NULL, '23-Nov-02', 'M', '593', NULL, NULL, '8006006609', '9675303725', '3', '1', NULL, NULL, '2017-09-03 09:51:05', NULL),
(876, '242', 'Abdul Rehman', 'Gayyur Ahmed', 'Samina Praveen', 'X-B', 'Chhutmalpur, Harijan Colony, Str. No. 2', '05-Oct-11', NULL, '18-Feb-01', 'M', '595', NULL, NULL, '9761349915', '9761350048', '4', '1', NULL, NULL, '2017-09-03 09:51:05', NULL),
(877, '1326', 'Aditi Mittal', 'Mahipal', 'Seema Mittal', 'X-B', 'Vill-Sunderpur Distt-Saharanpur', '04-Apr-16', NULL, '06-05-2002', 'F', '610', NULL, NULL, '9675196022', '9720703220', '5', '1', NULL, NULL, '2017-09-03 09:51:05', NULL),
(878, '1334', 'Aditi Pundir', 'Devendra Singh ', 'Manju Pundir', 'X-B', 'Vill+Post-Behra Sandal singh, Distt-SRE', '06-Apr-16', NULL, '01-Aug-03', 'F', '620', NULL, NULL, '7500369994', '7037977964', '6', '1', NULL, NULL, '2017-09-03 09:51:06', NULL),
(879, '1131', 'Akansha Goyal', 'Sandeep Kumar Goyal', 'Sangeeta Goyal', 'X-B', 'Vill-Fatehpur Bhado, Post-Chhutmalpur, Distt-SRE', '06-Apr-15', NULL, '11-Apr-02', 'F', '631', NULL, NULL, '9456061248', '8868926901', '7', '1', NULL, NULL, '2017-09-03 09:51:06', NULL),
(880, '59', 'Ali Raza', 'Athar Ali', 'Rashda ', 'X-B', 'Village Kheri', '04-May-11', NULL, '17-Sep-00', 'M', '657', NULL, NULL, '9720381673', '9719993618', '8', '1', NULL, NULL, '2017-09-03 09:51:06', NULL),
(881, '727', 'Amisha Pundir', 'Jitendar Pundir', 'Poonam Pundir', 'X-B', 'Gandhi Colony,Chhutmalpur', '08-May-13', NULL, '25-Oct-02', 'F', '660', NULL, NULL, '9412532336', '9457048736', '9', '1', NULL, NULL, '2017-09-03 09:51:06', NULL),
(882, '1370', 'Aryan Bhardwaj', 'Vishal Sharma', 'Meenakshi ', 'X-B', 'Vill-Dinarpur Gagalheri, Distt-SRE', '09-Apr-16', NULL, '05-Nov-03', 'M', '672', NULL, NULL, '9456224267', '9058104692', '10', '1', NULL, NULL, '2017-09-03 09:51:06', NULL),
(883, '682', 'Aswad Ali', 'Sharafat Ali', 'Shahjahan', 'X-B', '106, Mukarrampur/Kalewala', '10-Apr-13', NULL, '15-Apr-98', 'M', '675', NULL, NULL, '9411500813', '9690974394', '11', '1', NULL, NULL, '2017-09-03 09:51:06', NULL),
(884, '1323', 'Diksha Dhiman', 'Anuj Dhiman', 'Sunita Dhiman', 'X-B', 'Harnaam Singh Nagar, Chhutmalpur, Distt-SRE', '04-Apr-16', NULL, '21-Dec-03', 'F', '679', NULL, NULL, '9719460697', '9058505235', '12', '1', NULL, NULL, '2017-09-03 09:51:06', NULL),
(885, '60', 'Elma Rao', 'Athar Ali', 'Rashda ', 'X-B', 'Village Kheri', '04-May-11', NULL, '01-Aug-02', 'F', '682', NULL, NULL, '9720381673', '9719993618', '13', '1', NULL, NULL, '2017-09-03 09:51:06', NULL),
(886, '1355', 'Harsh Kamboj', 'Parveen Kamboj', 'Reeta Kamboj', 'X-B', 'Vill-Damodrabad Post-Biharigarh, Distt-SRE', '08-Apr-16', NULL, '09-02-2002', 'M', '688', NULL, NULL, '9675382809', '9719494905', '14', '1', NULL, NULL, '2017-09-03 09:51:06', NULL),
(887, '1358', 'Ishu Dhawal', 'Roopchand', 'Savita Devi', 'X-B', 'Arya Nagar Chhutmalpur, Distt-SRE', '08-Apr-16', NULL, '18-Dec-02', 'F', '709', NULL, NULL, '9358479503', '7037617565', '15', '1', NULL, NULL, '2017-09-03 09:51:06', NULL),
(888, '313', 'Km. Hina Parveen', 'Parvej Alam', 'Parveen Begam', 'X-B', 'Vill Gandewara Post Chhutamalpur', '13-Mar-12', NULL, '01-Jun-01', 'F', '710', NULL, NULL, '8057497640', '9690299691', '16', '1', NULL, NULL, '2017-09-03 09:51:06', NULL),
(889, '935', 'Km. Princi  Saini', 'Amrish Kumar', 'Balesh Devi', 'X-B', 'Vill-Dhalla Mazra, Post-Chaura Dev, Distt-SRE', '17-Apr-14', NULL, '22-Jan-03', 'F', '763', NULL, NULL, '8909759086', '8865057879', '17', '1', NULL, NULL, '2017-09-03 09:51:06', NULL),
(890, '1424', 'Madiha Ansari', 'Abdul Malik', 'Shafina Ansari', 'X-B', 'Punjabi Colony, Chhutmalpur, Distt-SRE', '04-Apr-16', NULL, '24-12-2001', 'F', '765', NULL, NULL, '9761212569', '9761213369', '18', '1', NULL, NULL, '2017-09-03 09:51:06', NULL),
(891, '544', 'Manpreet Singh', 'Harmeet Singh', 'Narender Kaur', 'X-B', 'Vill Badshahpur', '19-Jul-12', NULL, '28-Feb-02', 'M', '783', NULL, NULL, '9627324745', '', '19', '1', NULL, NULL, '2017-09-03 09:51:06', NULL),
(892, '1461', 'Mariyam', 'Shaban Ali', 'Musarrat jahan', 'X-B', 'Vill-D.dun Road Biharigarh, Dist-Sre', '11-Jul-16', NULL, '15-Mar-01', 'F', '788', NULL, NULL, '8859854215', '7017088199', '20', '1', NULL, NULL, '2017-09-03 09:51:06', NULL),
(893, '576', 'Megha Saini', 'Rajesh Kumar', 'Renu', 'X-B', 'Vill. & Post Muzaffarabad, Near Shiv Mandir', '29-Mar-13', NULL, '14-Oct-03', 'F', '802', NULL, NULL, '8273383427', '9456226610', '21', '1', NULL, NULL, '2017-09-03 09:51:06', NULL),
(894, '855', 'Mohd. Samar', 'Mohd.Tanjeb', 'Ishrat Begum ', 'X-B', 'Vill & Post Sikander Bhaiswal, Distt-Haridwar', '03-Apr-14', NULL, '18-Oct-03', 'M', '815', NULL, NULL, '8393820399', '7895275417', '22', '1', NULL, NULL, '2017-09-03 09:51:06', NULL),
(895, '1235', 'Mohd. Ziya Ul Haq', 'Mohd.Mukeem Ahmad', 'Gulista Mukeem', 'X-B', 'Dehradun Road, Nera Water Tank, Chhutmalpur,SRE', '06-Apr-15', NULL, '23-03-2003', 'M', '820', NULL, NULL, '7500323907', '', '23', '1', NULL, NULL, '2017-09-03 09:51:06', NULL),
(896, '1204', 'Mohit Rana', 'Shobha Ram Rana', 'Neelam Devi', 'X-B', 'Vill- Kurri Khera Chanchak, Post- Khas, Distt- SRE', '15-Apr-15', NULL, '21-Apr-02', 'M', '828', NULL, NULL, '9761098206', '9719177676', '24', '1', NULL, NULL, '2017-09-03 09:51:07', NULL),
(897, '1365', 'Mukul Kumar', 'Baburam', 'Babli', 'X-B', 'Near Shiv Mandir fatehpur, Post-Chhutmalpur, Distt-SRE', '09-Apr-16', NULL, '07-Dec-01', 'M', '830', NULL, NULL, '9759059233', '8954653808', '25', '1', NULL, NULL, '2017-09-03 09:51:07', NULL),
(898, '177', 'Nameera Niazi', 'Gulam Fareed', 'Shabana Fareed', 'X-B', 'D.Dun Road, Chhutmalpur', '16/4/11', NULL, '13-Mar-01', 'F', '841', NULL, NULL, '9719350213', '9719565044', '26', '1', NULL, NULL, '2017-09-03 09:51:07', NULL),
(899, '174', 'Nitin Kumar', 'Ravindra Kumar', 'Santosh', 'X-B', 'Village Rehri', '15/4/11', NULL, '24-Nov-02', 'M', '844', NULL, NULL, '9719952564', '8941888745', '27', '1', NULL, NULL, '2017-09-03 09:51:07', NULL),
(900, '100', 'Rahil Rao', 'Mohd. Quyyum Rao', 'Gulshan Parveen', 'X-B', 'Village Kheri', '04-Jul-11', NULL, '23-Oct-03', 'M', '851', NULL, NULL, '7900481662', '9675377060', '28', '1', NULL, NULL, '2017-09-03 09:51:07', NULL),
(901, '830', 'Ritik Kumar', 'Ravinder Kumar', 'Poonam Rani', 'X-B', 'Vill-Halwana, Post-Khas, Distt-SRE', '31-Mar-14', NULL, '01-Jan-00', 'M', '862', NULL, NULL, '7830453561', '9219609856', '29', '1', NULL, NULL, '2017-09-03 09:51:07', NULL),
(902, '274', 'Robin', 'Pravind Kumar', 'Suman', 'X-B', 'Rasoolpur Kala', '07-Feb-11', NULL, '20-Jun-02', 'M', '928', NULL, NULL, '9012259325', '', '30', '1', NULL, NULL, '2017-09-03 09:51:07', NULL),
(903, '698', 'Sahiba Rao', 'Mohd. Tanjeb', 'Ishrat Begum ', 'X-B', 'Vill. Sikanderpur Bhainswal, Bhagwanpur', '15-Apr-13', NULL, '15-Sep-02', 'F', '930', NULL, NULL, '9759077810', '8393820399', '31', '1', NULL, NULL, '2017-09-03 09:51:07', NULL),
(904, '1368', 'Sahil Ali', 'Sajid Hasan', 'Imrana', 'X-B', 'Gagalhri, Distt-SRE', '09-Apr-16', NULL, '14-12-2002', 'M', '931', NULL, NULL, '7533973622', '9058811028, 8126915256', '32', '1', NULL, NULL, '2017-09-03 09:51:07', NULL),
(905, '1388', 'Saloni Pundir', 'Sandeep ', 'Sangeeta ', 'X-B', 'Vill-Gangali post-Chhutmalpur, Distt-SRE', '28-Mar-16', NULL, '28-Dec-03', 'F', '952', NULL, NULL, '7830453488', '9536379449', '33', '1', NULL, NULL, '2017-09-03 09:51:07', NULL),
(906, '1343', 'Samiksha', 'Naresh Kamboj', 'Pratima Kamboj', 'X-B', 'Vill-Gherkarma Post-Biharigarh, Distt-SRE', '06-Apr-16', NULL, '19-Aug-02', 'F', '964', NULL, NULL, '9761350547', '9759103370', '34', '1', NULL, NULL, '2017-09-03 09:51:07', NULL),
(907, '1166', 'Simran Rana', 'Late Mr. Kulbir Singh', 'Rekha Rana', 'X-B', 'Vill & Post- Bahera Sandal Singh, Distt-SRE', '08-Apr-15', NULL, '20-Jan-02', 'F', '985', NULL, NULL, '9012166530', '9456293442', '35', '1', NULL, NULL, '2017-09-03 09:51:07', NULL),
(908, '1401', 'Simran Saini', 'Braham Pal Saini', 'Laxmi Saini', 'X-B', 'Vill-Alawalpur, Post-Chhutmalpur, Distt-Haridwar', '13-Apr-16', NULL, '24-09-2003', 'F', '1000', NULL, NULL, '8859178817', '8859212187', '36', '1', NULL, NULL, '2017-09-03 09:51:07', NULL),
(909, '1199', 'Sunil Rana', 'Mandresh Kumar', 'Ramesho Devi', 'X-B', 'Vill-Kurri Khera, Post-Khas, Distt-SRE', '13-Apr-15', NULL, '04-Feb-03', 'M', '1044', NULL, NULL, '9758215331', '', '37', '1', NULL, NULL, '2017-09-03 09:51:07', NULL),
(910, '1387', 'Tanishk Singh', 'Arvind Kumar', 'Mamta Devi', 'X-B', 'Vill-Khubbanpur Distt-Haridwar', '11-Apr-16', NULL, '30-12-2003', 'M', '1045', NULL, NULL, '8006141006', '8006141008', '38', '1', NULL, NULL, '2017-09-03 09:51:07', NULL),
(911, '1371', 'Vijay Pegwal', 'Ilamchand', 'Baby Pegwal', 'X-B', 'Arya Nagar Chhutmalpur, Distt-SRE', '09-Apr-16', NULL, '27-Sep-02', 'M', '1046', NULL, NULL, '9410864269', '9759416580', '39', '1', NULL, NULL, '2017-09-03 09:51:07', NULL),
(912, '370', 'Vishal Kashyap', 'Laltesh Kashyap', 'Sangeeta Kashyap', 'X-B', 'Biharigarh Distt. Saharanpur', '03-Apr-12', NULL, '', 'M', '1053', NULL, NULL, '9456761901', '9720840053', '40', '1', NULL, NULL, '2017-09-03 09:51:07', NULL),
(913, '262', 'Vishal Saini', 'Sanjay Saini', 'Rajkumari', 'X-B', 'Fatehpur, Chhutmalpur', '31/5/11', NULL, '20/6/2003', 'M', '1055', NULL, NULL, '8954629252', '9761857753', '41', '1', NULL, NULL, '2017-09-03 09:51:08', NULL),
(914, '1356', 'Vishu Dhiman', 'Vijay Dhiman', 'Rekha Devi', 'X-B', 'Vill-Shahid Wala Grant Post-Buggawala, Distt-Haridwar', '08-Apr-16', NULL, '12-Feb-01', 'M', '1060', NULL, NULL, '8394961778', '9719966080', '42', '1', NULL, NULL, '2017-09-03 09:51:08', NULL),
(915, '116', 'Aakansha Kamboj', 'Indresh Kamboj', 'Renu Kamboj', 'XI COM-B', 'Village Meerpur', '04-Jul-11', NULL, '30-Aug-01', 'F', '10', NULL, NULL, '9759077155', '9058588960', '1', '1', NULL, NULL, '2017-09-01 08:06:08', NULL),
(916, '1578', 'Aastha Kamboj', 'Sovinder Kamboj', 'Manju Kamboj', 'XI COM-B', 'Vill-Meerpur, Post-Sherpur Khanazaadpur, Distt-Saharanpur', '18-04-2017', NULL, '11-06-2000', 'F', '44', NULL, NULL, '8869890478', '', '2', '1', NULL, NULL, '2017-09-01 08:06:08', NULL),
(917, '1513', 'Abhay Sharma', 'Satya Prakash Sharma', 'Neetu Sharma', 'XI COM-B', 'Vill-Dinaarpur, Post-Gagalheri, Distt-Saharanpur', '07-04-2017', NULL, '', 'M', '62', NULL, NULL, '8057614315', '8533991122', '3', '1', NULL, NULL, '2017-09-01 08:06:08', NULL),
(918, '1633', 'Abhilasha Pundir', 'Pratap Singh', 'Mamta Pundir', 'XI COM-B', 'Vill-Mandawar, Post-Chhutmalpur, Distt-Haridwar', '05-07-2017', NULL, '15-11-2001', 'M', '66', NULL, NULL, '8923506331', '8923506331', '4', '1', NULL, NULL, '2017-09-01 08:06:08', NULL),
(919, '688', 'Abhishek', 'Girwar Saini', 'Sushma Saini', 'XI COM-B', 'Near Bank of Baroda, Fatehpur', '11-Apr-13', NULL, '16-Aug-01', 'M', '87', NULL, NULL, '9759461455', '9458258965', '5', '1', NULL, NULL, '2017-09-01 08:06:08', NULL),
(920, '1080', 'Akansha Chauhan', 'Madan Chauhan', 'Poonam Chauhan', 'XI COM-B', 'Vill- Buddhva Shahid, Post- Buggawala, Distt- Haridwar', '12-Mar-15', NULL, '20-Dec-01', 'F', '122', NULL, NULL, '9760239939', '9760195731', '6', '1', NULL, NULL, '2017-09-01 08:06:08', NULL),
(921, '452', 'Akanshit Saini', 'Anil Saini', 'Savita Saini', 'XI COM-B', 'Vill-Banwala Post-Chhutmalpur', '16-Apr-12', NULL, '18-Sep-00', 'M', '126', NULL, NULL, '9759564729', '9152585910', '7', '1', NULL, NULL, '2017-09-01 08:06:08', NULL),
(922, '1248', 'Akash Kumar', 'Satish Kumar', 'Suman Yadav', 'XI COM-B', 'Dharamveer Colony, Gagalheri, SRE', '28-May-15', NULL, '25-Oct-01', 'M', '129', NULL, NULL, '9536028136', '8859296455', '8', '1', NULL, NULL, '2017-09-01 08:06:08', NULL),
(923, '1099', 'Arpit Kamboj', 'Rakesh Kamboj', 'Ravita Kamboj', 'XI COM-B', 'Vill & Post Sherpur Khanazadpur', '03-Apr-15', NULL, '11-Apr-01', 'M', '269', NULL, NULL, '9457740581', '9639565771', '9', '1', NULL, NULL, '2017-09-01 08:06:08', NULL),
(924, '1209', 'Ashish Choudhary', 'Parmod Kumar Choudhary', 'Seema Choudhary', 'XI COM-B', 'Vill-Aliwala, Post-Muzaffrabad, Distt-SRE', '17-Apr-15', NULL, '21-May-02', 'M', '300', NULL, NULL, '9927434521', '8650549462', '10', '1', NULL, NULL, '2017-09-01 08:06:08', NULL),
(925, '650', 'Ayushi Rana', 'Anil Kumar Rana', 'Minakshi Rana', 'XI COM-B', 'Danda Telpura', '05-Apr-13', NULL, '15-Jun-02', 'F', '350', NULL, NULL, '9719108066', '9758012424', '11', '1', NULL, NULL, '2017-09-01 08:06:08', NULL),
(926, '1108', 'Mohd. Aazam', 'Rabbani', 'Taslima', 'XI COM-B', 'Vill- Sambhalki Gujjar, Post-Khas, Distt-SRE', '24-04-2017', NULL, '25-07-2001', 'm', '412', NULL, NULL, '9720484664', '9720484664', '12', '1', NULL, NULL, '2017-09-08 07:10:01', NULL),
(927, '1136', 'Ishika Pundir', 'Ghanshayam Singh Pundir', 'Sarmistha', 'XI COM-B', 'Vill-Jeewala, Post- Muzaffrabad, Distt-SRE', '06-Apr-15', NULL, '02-Dec-01', 'F', '489', NULL, NULL, '9639465982', '8057614101', '13', '1', NULL, NULL, '2017-09-01 08:06:08', NULL),
(928, '1191', 'Km. Garima Singh', 'Subhash Singh', 'Asha', 'XI COM-B', 'Vill-Ramkheri, Post-Bahera Sandal Singh, Distt-SRE', '31-Mar-15', NULL, '22-Sep-02', 'F', '542', NULL, NULL, '8126358330', '', '14', '1', NULL, NULL, '2017-09-01 08:06:08', NULL),
(929, '1045', 'Lalit Kumar', 'Sanjay Kumar', 'Poonam Devi', 'XI COM-B', 'Vill- Fatehpur, Post-Chhutmalpur, Distt-SRE', '11-Mar-15', NULL, '23-Dec-02', 'M', '568', NULL, NULL, '9837544660', '9837755640', '15', '1', NULL, NULL, '2017-09-01 08:06:08', NULL),
(930, '679', 'Manish Kashyap', 'Vijendra Kashyap', 'Babita Rani', 'XI COM-B', 'Punjabi Colony Chhutmalpur', '10-Apr-13', NULL, '18-Oct-00', 'M', '594', NULL, NULL, '9319968233', '9319049509', '16', '1', NULL, NULL, '2017-09-01 08:06:08', NULL),
(931, '1339', 'Manvi Yadav', 'Sudhir Kumar Yadav', 'Sapna Yadav', 'XI COM-B', 'Vill-Agrsen Chowk Gagalheri, Distt-SRE', '06-Apr-16', NULL, '30-Apr-02', 'F', '607', NULL, NULL, '9672973288', '7597908113', '17', '1', NULL, NULL, '2017-09-01 08:06:08', NULL),
(932, '1577', 'Manya', 'Rajiv Kumar', 'Kavita', 'XI COM-B', 'Vill-Hasanpur, Madanpur, Distt-Haridwar', '18-04-2017', NULL, '', 'F', '608', NULL, NULL, '9411484369', '9536357575', '18', '1', NULL, NULL, '2017-09-01 08:06:08', NULL),
(933, '84', 'Mayank Rastogi', 'Uday Shankar Rastogi', 'Sangeeta Rastogi', 'XI COM-B', 'Amardeep Colony, Chhutmalpur', '04-Jun-11', NULL, '08-Oct-00', 'M', '616', NULL, NULL, '9837408824', '9012693572', '19', '1', NULL, NULL, '2017-09-01 08:06:08', NULL),
(934, '1151', 'Megha Chauhan', 'Vikram Chauhan', 'Neelam Chauhan', 'XI COM-B', 'Vill-Nanka urf JaiRampur, Post-Chhutmalpur, Distt-SRE', '08-Apr-15', NULL, '01-Jan-02', 'F', '619', NULL, NULL, '9761088808', '9917124124', '20', '1', NULL, NULL, '2017-09-01 08:06:08', NULL),
(935, '921', 'Mohd. Aamir', 'Ameer Alam', 'Afroz', 'XI COM-B', 'Vill-Chhappar, Post-Khubbanpur, Dist- Haridwar', '14-Apr-14', NULL, '12-Oct-00', 'M', '639', NULL, NULL, '9917704624', '', '21', '1', NULL, NULL, '2017-09-01 08:06:08', NULL),
(936, '865', 'Mohd. Farahim', 'Attaur Rehman', 'Shabnam Niyazi', 'XI COM-B', 'Vill. & Post Kheri Shikohpur, Distt. Haridwar', '03-Apr-14', NULL, '22-Jun-01', 'M', '654', NULL, NULL, '9759787684', '8533008054', '22', '1', NULL, NULL, '2017-09-01 08:06:08', NULL),
(937, '1037', 'Mohd. Sahil Khan', 'Furkan Ali', 'Shamshida', 'XI COM-B', 'Vill - Jeewala, Post- Muzaffrabad, Distt-SRE', '09-Mar-15', NULL, '12-Apr-01', 'M', '659', NULL, NULL, '9720512009', '8057142579', '23', '1', NULL, NULL, '2017-09-01 08:06:08', NULL),
(938, '1162', 'Mohit Saini', 'Ashok Kumar', 'Anita', 'XI COM-B', '546, Dehradun Road, Near Tulsi School, Fatehpur Bhado, Chhutmalpur', '08-Apr-15', NULL, '11-Jan-01', 'M', '676', NULL, NULL, '9758123111', '', '24', '1', NULL, NULL, '2017-09-01 08:06:08', NULL),
(939, '1044', 'Namrah Rao', 'Rao Farhat Ali', 'Zubiada Rao', 'XI COM-B', 'Vill-Fatehpur Bhado, Post-Chhutmalpur, Distt-SRE', '11-Mar-15', NULL, '30-Jun-00', 'F', '689', NULL, NULL, '9758873675', '8126680566', '25', '1', NULL, NULL, '2017-09-01 08:06:08', NULL),
(940, '825', 'Nishita Saini', 'Pramod Saini', 'Rekha Saini', 'XI COM-B', 'Maharana Pratap Colony, Gali No. 1, Chhutmalpur', '31-Mar-14', NULL, '08-Jul-02', 'F', '708', NULL, NULL, '9837958781', '', '26', '1', NULL, NULL, '2017-09-01 08:06:08', NULL),
(941, '1113', 'Paras Saini', 'Sawraj Saini', 'Reeta Saini', 'XI COM-B', 'Vill & Post-Kurri Khera, Distt-SRE', '06-Apr-15', NULL, '13-Feb-01', 'M', '718', NULL, NULL, '9758962530', '7830203233', '27', '1', NULL, NULL, '2017-09-01 08:06:08', NULL),
(942, '1137', 'Pragya Saini', 'Pritam Singh', 'Manju Saini', 'XI COM-B', 'Main Road, Biharigarh, Kisan Spare Parts, Distt-SRE', '07-Apr-15', NULL, '23-Oct-01', 'F', '732', NULL, NULL, '9759255930', '9720798756', '28', '1', NULL, NULL, '2017-09-01 08:06:08', NULL),
(943, '1238', 'Raghav Chhabra', 'Sanjay Chhabra', 'Anu Chhabra', 'XI COM-B', 'New Gopal Nagar, Near Shiv Mandir, Saharanpur', '21-Apr-15', NULL, '05-Dec-00', 'M', '761', NULL, NULL, '9627758879', '7088588155', '29', '1', NULL, NULL, '2017-09-01 08:06:08', NULL),
(944, '394', 'Rahul Saini', 'Sunil Kumar', 'Sangeeta Devi', 'XI COM-B', 'Vill- Banwala Post Sunderpur', '07-Apr-12', NULL, '28-Jun-00', 'M', '764', NULL, NULL, '9719236105', '9917014256', '30', '1', NULL, NULL, '2017-09-01 08:06:09', NULL),
(945, '1526', 'Riya Goyal', 'Pawan Kumar', 'Savita Goyal', 'XI COM-B', 'Vill-Alawalpur, Post-Chhutmalpur, Distt-Haridwar', '08-04-2017', NULL, '19-12-1999', 'F', '810', NULL, NULL, '9758712658', '9917402994', '31', '1', NULL, NULL, '2017-09-01 08:06:09', NULL),
(946, '1081', 'Sagar Saini', 'Ram Kumar Singh', 'Usha', 'XI COM-B', 'Alawalpur, Chhutmalpur, Haridwar', '01-Apr-15', NULL, '26-Jul-01', 'M', '826', NULL, NULL, '8954616527', '9719445824', '32', '1', NULL, NULL, '2017-09-01 08:06:09', NULL),
(947, '1184', 'Satakshi Singh', 'Dharamender Singh', 'Kalpana', 'XI COM-B', 'Vill & Post-Rehri', '10-Apr-15', NULL, '27-Sep-01', 'F', '869', NULL, NULL, '9917758001', '9917633654', '33', '1', NULL, NULL, '2017-09-01 08:06:09', NULL),
(948, '892', 'Seejal Saini', 'Arjun Singh', 'Seema Saini', 'XI COM-B', 'Suraj Vihar Colony,Chhutmalpur', '07-Apr-14', NULL, '31-Oct-02', 'F', '878', NULL, NULL, '9897325801', '', '34', '1', NULL, NULL, '2017-09-01 08:06:09', NULL),
(949, '1214', 'Sohit Kumar', 'Suresh Chand', 'Kusum Devi', 'XI COM-B', 'Shiv Colony, Opp. Jain Bagh, Fatehpur, SRE', '20-Apr-15', NULL, '14-Aug-02', 'M', '935', NULL, NULL, '9761338375', '9758086266', '35', '1', NULL, NULL, '2017-09-01 08:06:09', NULL),
(950, '1185', 'Srishti Gach', 'Late Mr Nathi Ram', 'Anita Devi', 'XI COM-B', 'Arya Nagar, Kamalpur Road, Chhutmalpur, Distt- SRE', '10-Apr-15', NULL, '', 'F', '940', NULL, NULL, '9997777677', '', '36', '1', NULL, NULL, '2017-09-01 08:06:09', NULL),
(951, '508', 'Tabrez Alam', 'Mohd. Yusuf', 'Mehrooba', 'XI COM-B', 'Vill Chaura Khurd, Post Chaura Dev', '11-Jun-12', NULL, '25-Aug-01', 'M', '963', NULL, NULL, '9761211323', '9758186564', '37', '1', NULL, NULL, '2017-09-01 08:06:09', NULL),
(952, '1048', 'Taniya Kamboj', 'Neeraj Kamboj', 'Rajnee Kamboj', 'XI COM-B', 'Arya Nagar, Chhutmalpur, SRE', '11-Mar-15', NULL, '28-Aug-01', 'F', '967', NULL, NULL, '9758056855', '8859601146', '38', '1', NULL, NULL, '2017-09-01 08:06:09', NULL),
(953, '1236', 'Vaibhav Rawat', 'Darshan Singh Rawat', 'Soniya Rawat', 'XI COM-B', 'H. No- C3, Company Garden, SRE', '18-Apr-15', NULL, '01-Aug-01', 'M', '1001', NULL, NULL, '9412435869', '9457296252', '39', '1', NULL, NULL, '2017-09-01 08:06:09', NULL),
(954, '223', 'Vandita', 'Dhirendra Singh', 'Sunita Devi', 'XI COM-B', 'Village Alawalpur', '28/4/11', NULL, '20-Jun-02', 'F', '1009', NULL, NULL, '9927977777', '9837957777', '40', '1', NULL, NULL, '2017-09-01 08:06:09', NULL),
(955, '1153', 'Vidhi Gupta', 'Naveen Gupta', 'Meenu Gupta', 'XI COM-B', 'Harijan Colony, Street No. 4, House no. 2, Chhutmalpur,SRE', '08-Apr-15', NULL, '26-Dec-01', 'F', '1040', NULL, NULL, '9758141871', '8171814530', '41', '1', NULL, NULL, '2017-09-01 08:06:09', NULL),
(956, '1571', 'Vinny Tegwal', 'Suresh Kumar', 'Pushpa Devi', 'XI COM-B', 'Bansal Vihar, Phase-2, Fatehpur, Chhutmalpur, Distt-Saharanpur', '26-07-2017', NULL, '22-06-2002', 'F', '1048', NULL, NULL, '9759561149', '9761829552', '42', '1', NULL, NULL, '2017-09-01 08:06:09', NULL),
(957, '1601', 'Vishakha Saini', 'Jogesh Saini', 'Mamta Saini', 'XI COM-B', 'Vill-Kamalpur Road, Chhutmalpur, Distt-Saharanpur', '27-04-2017', NULL, '10-09-2001', 'F', '1051', NULL, NULL, '8057853477', '7351408500', '43', '1', NULL, NULL, '2017-09-01 08:06:09', NULL),
(958, '776', 'Zeba Malik', 'Irshad Ali', 'Varisha Begum', 'XI COM-B', 'Halwana Road, Chhutmalpur', '05-Mar-14', NULL, '06-Sep-01', 'F', '1078', NULL, NULL, '9720513506', '8923686904', '44', '1', NULL, NULL, '2017-09-01 08:06:09', NULL),
(959, '1612', 'Aayush Rana', 'Sudesh Pal Rana', 'Rajnesh Rana', 'XI SCI-A', 'Vill-Biharigarh, Post-Chhutmalpur, distt-Saharanpur', '08-05-2017', NULL, '09-01-2002', 'M', '50', NULL, NULL, '9759565107', '7627993400, 971939895', '1', '1', NULL, NULL, '2017-09-01 08:05:58', NULL),
(960, '1158', 'Abhishek Kamboj', 'Pramod Kamboj', 'Seema Kamboj', 'XI SCI-A', 'Vill-Damodrabad, Post-Biharigarh, Distt-SRE', '08-Apr-15', NULL, '13-Sep-2000', 'M', '89', NULL, NULL, '9536015906', '9720704262', '2', '1', NULL, NULL, '2017-09-01 08:05:58', NULL),
(961, '1181', 'Abhishekh Kamboj', 'Arvind Kamboj', 'Savita Kamboj', 'XI SCI-A', 'Vill-Takipur, Post-Sherpur Khanazadpur, Distt-SRE', '10-Apr-15', NULL, '2-Sep-2000', 'M', '97', NULL, NULL, '9761440690', '', '3', '1', NULL, NULL, '2017-09-01 08:05:58', NULL),
(962, '1636', 'Ajay Pegwal', 'Ilam Chand', 'Baby', 'XI SCI-A', 'Kamalpur Road, Arya Nagar, Chhutmalpur, Distt-Saharanpur', '12-07-2017', NULL, '18-11-2001', 'M', '121', NULL, NULL, '9410864269', '8273710643', '4', '1', NULL, NULL, '2017-09-01 08:05:58', NULL),
(963, '1150', 'Akansha Sharma', 'Manoj Kumar Sharma', 'Manju Sharma', 'XI SCI-A', 'Near P.J.D. Junior High School, Dehradun Road,Chhutmalpur, SRE', '08-Apr-15', NULL, '20-Jul-2002', 'F', '125', NULL, NULL, '9759642841', '9761962644', '5', '1', NULL, NULL, '2017-09-01 08:05:58', NULL),
(964, '1069', 'Akshit Saini', 'Vijendar Kumar Saini', 'Rachna Devi', 'XI SCI-A', 'Vill & Post- Biharigarh, Distt-SRE', '', NULL, '21-Jul-2001', 'M', '144', NULL, NULL, '9719168022', '9675641053', '6', '1', NULL, NULL, '2017-09-01 08:05:58', NULL),
(965, '1604', 'Aman Saini', 'Ram Kumar Saini', 'Omi Devi', 'XI SCI-A', 'Vill-Kurrikhera, Post-Jahanpur, Distt-Saharanpur', '01-05-2017', NULL, '18-08-2001', 'M', '168', NULL, NULL, '9412541669', '7617504078', '7', '1', NULL, NULL, '2017-09-01 08:05:58', NULL),
(966, '927', 'Amjad', 'Shakeel Ahmad', 'Saliman', 'XI SCI-A', 'Dinarpur Road, Gagalheri, SRE', '15-Apr-14', NULL, '5-Apr-2002', 'M', '176', NULL, NULL, '9720595969', '9758974761', '8', '1', NULL, NULL, '2017-09-01 08:05:58', NULL),
(967, '772', 'Ananya Goyal', 'Navdeep Goyal', 'Annapurna Goyal', 'XI SCI-A', 'Sant Nagar Colony,D.Dun Road,Chhutmalpur', '01-Mar-14', NULL, '27-Jul-2002', 'F', '187', NULL, NULL, '8410536649', '9758752882', '9', '1', NULL, NULL, '2017-09-01 08:05:58', NULL),
(968, '778', 'Anita Choudhary', 'Virendra Choudhary', 'Babita Choudhary', 'XI SCI-A', 'Vill-Kheri Shikohpur, Post-Khas, Haridwar', '05-Mar-14', NULL, '11-Nov-2000', 'F', '196', NULL, NULL, '9634132162', '8273732315', '10', '1', NULL, NULL, '2017-09-01 08:05:58', NULL),
(969, '1645', 'Anjali Chauhan', 'Kunwarpal Singh', 'Kusum Lata', 'XI SCI-A', 'Vill-Jajner, Post-Chhutmalpur, Distt-Saharanpur', '24-07-2017', NULL, '25-09-2003', 'F', '197', NULL, NULL, '8958499788', '7409373121', '11', '1', NULL, NULL, '2017-09-01 08:05:58', NULL),
(970, '1195', 'Ankit Saini', 'Suneet Kumar', 'Santosh', 'XI SCI-A', 'Kmaalpur Road, Chhutmalpur, Distt- SRE', '13-Apr-15', NULL, '11-Nov-1999', 'M', '200', NULL, NULL, '9837848195', '9927504311', '12', '1', NULL, NULL, '2017-09-01 08:05:58', NULL),
(971, '251', 'Anshika', 'Suresh Kumar', 'Anita', 'XI SCI-A', 'Shiv Col, Fatehpur', '18/4/11', NULL, '25-Nov-2001', 'F', '216', NULL, NULL, '9758938632', '', '13', '1', NULL, NULL, '2017-09-01 08:05:58', NULL),
(972, '1068', 'Anshu Chauhan', 'Brijesh Chauhan', 'Suman Chauhan', 'XI SCI-A', 'Vill & Post- Biharigarh, Distt-SRE', '12-Mar-15', NULL, '29-Jan-2001', 'F', '224', NULL, NULL, '9759565305', '9675438381', '14', '1', NULL, NULL, '2017-09-01 08:05:58', NULL),
(973, '1067', 'Anuraag Saini', 'Tejpal Singh', 'Rukmani Devi', 'XI SCI-A', 'Vill- Saidpura, Post- Sarsawa, Distt-SRE', '12-Mar-15', NULL, '16-May-2001', 'M', '234', NULL, NULL, '9759031780', '9536839228', '15', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(974, '773', 'Arjun Saini', 'Charan Singh', 'Usha Saini', 'XI SCI-A', 'Shiv Colony Near Nari Niketan, Fatehpur', '03-Mar-14', NULL, '7-Nov-2000', 'M', '258', NULL, NULL, '9456026179', '8954248774', '16', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(975, '1634', 'Arzoo Dhawal', 'Roopchand', 'Savita Devi', 'XI SCI-A', 'Arya Nagar, Chhutmalpur, Distt-Saharanpur', '10-07-2017', NULL, '07-11-2001', 'F', '284', NULL, NULL, '9358479503', '7037617568', '17', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(976, '139', 'Aurangzeb Ali', 'Rao Parvez Ali', 'Wasim Bano', 'XI SCI-A', 'Village Kheri', '04-Sep-11', NULL, '25-May-2001', 'M', '298', NULL, NULL, '9758845530', '9358161601', '18', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(977, '1219', 'Barkha Sethi', 'Charanjeet Sethi', 'Poonam Sethi', 'XI SCI-A', 'Vill- Sunderpur, Post- Khas, Distt- SRE', '20-Apr-15', NULL, '7-Jan-2001', 'F', '316', NULL, NULL, '9759062755', '9720433725', '19', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(978, '1609', 'Chirag Kamboj', 'Virender', 'Monika', 'XI SCI-A', 'Vill-Ganja Mazra, Post-Biharigarh, Distt-Haridwar', '08-05-2017', NULL, '25-08-2002', 'M', '356', NULL, NULL, '9758949458', '7055231791', '20', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(979, '1141', 'Faheem Ahmed', 'Naseem Ahmed', 'Heena', 'XI SCI-A', 'Bharat Petrol Pump, Chhutmalpur, SRE', '07-Apr-15', NULL, '28-Nov-1999', 'M', '371', NULL, NULL, '9758680800', '8273768475', '21', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(980, '619', 'Gaurav', 'Adeep Kumar', 'Anita', 'XI SCI-A', 'Amanat Garh', '03-Apr-13', NULL, '18-Sep-2001', 'M', '422', NULL, NULL, '9759726970', '9719242597', '22', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(981, '1650', 'Gaurav Singh Jethuri', 'Sukhdev Singh Jethuri', 'Anita Jethuri', 'XI SCI-A', 'C.H.C., Fatehpur, Distt-Saharanpur', '08-05-2017', NULL, '25-05-2002', 'M', '430', NULL, NULL, '9411081140', '8126761404', '23', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(982, '916', 'Gourav Pal', 'Suresh Pal', 'Soniya Pal', 'XI SCI-A', 'Vill-Mandawar, Haridwar', '09-Apr-14', NULL, '6-Dec-2002', 'M', '435', NULL, NULL, '9759999156', '', '24', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(983, '641', 'Harsh Kamboj', 'Jinendra Kamboj', 'Renuka', 'XI SCI-A', 'Shiv Colony, Fatehpur', '04-Apr-13', NULL, '5-Jun-2001', 'M', '448', NULL, NULL, '7830159944', '', '25', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(984, '1613', 'Harsh Pratap Chauhan', 'Anuj Chauhan', 'Neelam Chauhan', 'XI SCI-A', 'Vill-Biharigarh, Post-Chhutmalpur, distt-Saharanpur', '08-05-2017', NULL, '06-01-2001', 'M', '451', NULL, NULL, '9719136929', '8650351932', '26', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(985, '903', 'Harsh Saini', 'Mange Ram', 'Savita Saini', 'XI SCI-A', 'Vill-Mandawar, Haridwar', '07-Apr-14', NULL, '3-Feb-2001', 'M', '453', NULL, NULL, '8864842152', '', '27', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(986, '1064', 'Himanshu Sharma', 'Late Naresh Kumar Sharma', 'Babita', 'XI SCI-A', 'Maharana Pratap Colony, Gali No. 3, Chhutmalpur, Distt-SRE', '26-Mar-15', NULL, '7-Mar-2002', 'M', '478', NULL, NULL, '', '7500892007', '28', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(987, '1621', 'Isheeka kamboj', 'Akhilesh Kamboj', 'Anjana', 'XI SCI-A', 'Bansal Vihar Colony, fatehpur, chhutmalpur, Distt-Saharanpur', '03-07-2017', NULL, '04-11-2002', 'F', '487', NULL, NULL, '9720909504', '9058758020', '29', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(988, '128', 'Ishika Sharma', 'Arun Sharma', 'Deepa Sharma', 'XI SCI-A', 'Kalalhatti, Post Chhutmalpur', '04-Aug-11', NULL, '11-Mar-2003', 'F', '490', NULL, NULL, '9219609856', '9759693926', '30', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(989, '828', 'Jatin Saini', 'Sanjeev Saini', 'Sumedha Saini', 'XI SCI-A', 'Vill & Post-Khurrampur, Distt-SRE', '31-Mar-14', NULL, '15-Jan-2002', 'M', '498', NULL, NULL, '9818159744', '9627230389', '31', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(990, '837', 'Kartik Jain', 'Sandeep Jain', 'Kamini Jain', 'XI SCI-A', 'Gandhi Colony, Chhutmalpur', '01-Apr-14', NULL, '12-Jul-2001', 'M', '544', NULL, NULL, '9259405315', '9045877878', '32', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(991, '90', 'Km. Nishita Singh', 'Dheer Singh', 'Nirmala Devi', 'XI SCI-A', 'Village Alawalpur', '04-Jun-11', NULL, '8-Nov-2001', 'F', '576', NULL, NULL, '7830140286', '9286665962', '33', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(992, '1090', 'Laxmi Rathore', 'Vinod', 'Babita', 'XI SCI-A', 'Bugga Wala Road, Biharigarh, Distt- SRE', '02-Apr-15', NULL, '20-Nov-2001', 'F', '623', NULL, NULL, '9720689519', '9759128615', '34', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(993, '1606', 'Mohd  Moin', 'Ishran Ali', 'Shahnaaz', 'XI SCI-A', 'Vill-Kamalpur, Post-Chhutmalpur, Distt-Saharanpur', '06-05-2017', NULL, '07-03-2001', 'M', '635', NULL, NULL, '9758873766', '9758844966', '35', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(994, '1102', 'Mohd. Anas', 'Noor Hasan Ansari', 'Sayda ', 'XI SCI-A', 'Halwana Road, Chhutmalpur,Distt-SRE', '04-Apr-15', NULL, '1-Jan-2002', 'M', '640', NULL, NULL, '9058002290', '9058002393', '36', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(995, '1104', 'Mohd. Arish', 'Muntazir Ahmad', 'Shahin', 'XI SCI-A', 'Alawalpur Road, Near Masjid, Chhutmalpur, SRE', '04-Apr-15', NULL, '17-Mar-2002', 'M', '647', NULL, NULL, '9760218519', '9997062346', '37', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(996, '91', 'Nishant Kumar Singh', 'Dheer Singh', 'Nirmala Devi', 'XI SCI-A', 'Village Alawalpur', '04-Jun-11', NULL, '8-Nov-2001', 'M', '650', NULL, NULL, '7830140286', '9286665962', '38', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(997, '1145', 'Prachi Saini', 'Gajay Singh Saini', 'Suman Devi', 'XI SCI-A', 'Vill-Sukhedi, Post Lakhoti, Distt-SRE', '07-Apr-15', NULL, '1-Jun-2001', 'F', '707', NULL, NULL, '9759643202', '9758951662', '39', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(998, '1039', 'Raman Gupta', 'Anil  Gupta', 'Uma', 'XI SCI-A', 'Vill & Post- Behera Sandal Singh, Distt-SRE', '09-Mar-15', NULL, '10-Sep-2002', 'M', '721', NULL, NULL, '8958575147', '7500369994', '40', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(999, '1652', 'Rao Abubakar', 'Rao Saood Ahmad', 'Anees Fatima', 'XI SCI-A', 'Vill-Kheri Sikohpur, Distt-Haridwar', '24-07-2017', NULL, '11-08-2001', 'M', '731', NULL, NULL, '9759002937', '8445335477', '41', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(1000, '87', 'Rao Faiziyab Ali', 'Ikram Khan', 'Afroza', 'XI SCI-A', 'Village Kheri', '04-Jun-11', NULL, '12-Nov-2000', 'M', '767', NULL, NULL, '9719113130', '9012224620', '42', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(1001, '1584', 'Ritik Tyagi', 'Aadesh Tyagi', 'Seema Tyagi', 'XI SCI-A', 'Vill-Gagalheri, Distt-Saharanpur', '19-04-2017', NULL, '', 'M', '773', NULL, NULL, '9760366272', '9675983036', '43', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(1002, '834', 'Rittik Chaudhary', 'Ravish Kumar', 'Umlesh Devi', 'XI SCI-A', 'Vill-Roopri Junardar, Post-Kota, Distt-SRE', '31-Mar-14', NULL, '28-Jan-2002', 'M', '781', NULL, NULL, '9639191264', '9756885327', '44', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(1003, '1109', 'Sagar Yadav', 'Satish Yadav', 'Anita Yadav', 'XI SCI-A', 'Vill -Sambhalki Gujjar, Post- Khas, Distt-SRE', '04-Apr-15', NULL, '24-Sep-2002', 'M', '803', NULL, NULL, '9758728727', '9720543296', '45', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(1004, '298', 'Sanchika Mittal', 'Rajeev Kumar Mittal', 'Anjali Mittal', 'XI SCI-A', 'Punjabi Col, Chhutmalpur', '21/7/11', NULL, '14-Aug-2002', 'F', '807', NULL, NULL, '8445334751', '8449154444', '46', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(1005, '1063', 'Sandeep Pal', 'Pawan Kumar', 'Somti Devi', 'XI SCI-A', 'Vill- Tanda Hasangarh, Post-Kheri Shikohpur, Distt-Haridwar', '25-Mar-15', NULL, '18-Jul-2001', 'M', '827', NULL, NULL, '8859358653/ 9720975432', '9917800700', '47', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(1006, '774', 'Sanjeev Saini ', 'Rakesh Kumar', 'Bimlesh Devi', 'XI SCI-A', 'Shiv Colony, Fathepur', '03-Mar-14', NULL, '13-Apr-2001', 'M', '856', NULL, NULL, '9719942174', '', '48', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(1007, '1038', 'Siddhant Kumar Pegowal', 'Sewaram', 'Babita', 'XI SCI-A', 'Arya Nagar, Chhutmalpur, SRE', '09-Mar-15', NULL, '5-Apr-2000', 'M', '883', NULL, NULL, '9761142838', '9536714449', '49', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(1008, '1651', 'Sonia Jethuri', 'Sukhdev Singh Jethuri', 'Anita Jethuri', 'XI SCI-A', 'C.H.C., Fatehpur, Distt-Saharanpur', '08-05-2017', NULL, '15-09-2004', 'F', '937', NULL, NULL, '9411081140', '8126761404', '50', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(1009, '1589', 'Sourabh', 'Parmod Kumar', 'Rekha', 'XI SCI-A', 'Vill-Gagalheri, Distt-Saharanpur', '21-04-2017', NULL, '07-04-2002', 'M', '938', NULL, NULL, '9759561365', '8057889711', '51', '1', NULL, NULL, '2017-09-01 08:05:59', NULL),
(1010, '259', 'Sudhir Kumar', 'Babu ram', 'Savita Devi', 'XI SCI-A', 'Nr. Tulsi Adrash P. School, Fatehpur', '27/5/11', NULL, '20-Feb-2001', 'M', '944', NULL, NULL, '8954231080', '8951281080', '52', '1', NULL, NULL, '2017-09-01 08:06:00', NULL),
(1011, '1399', 'Aadarsh Dhiman', 'Mukesh Kumar', 'Minakshi ', 'XII COM-B', 'Vill-Kishangarh Colony, Biharigarh, Distt-SRE', '13-Apr-16', NULL, '17-Sep-99', 'M', '1', NULL, NULL, '9997447039', '9690639687', '1', '1', NULL, NULL, '2017-09-01 08:06:28', NULL),
(1012, '71', 'Aakansha Singhal', 'Manoj Singhal', 'Rashmi Singhal', 'XII COM-B', 'Roorkee Rd, Chhutmalpur', '04-Jun-11', NULL, '25-Mar-01', 'F', '11', NULL, NULL, '9412651080', '', '2', '1', NULL, NULL, '2017-09-01 08:06:28', NULL),
(1013, '885', 'Aazam Rao', 'Saud Ali', 'Kishwar', 'XII COM-B', 'Vill & Post-Khujnawar, Distt-SRE', '05-Apr-14', NULL, '08-Apr-99', 'M', '17', NULL, NULL, '7500656565', '9719055550', '3', '1', NULL, NULL, '2017-09-01 08:06:28', NULL),
(1014, '1429', 'Amrit Kaur', 'Manjeet Singh', 'Saraljeet Kaur', 'XII COM-B', 'Vill-Jyantipur Gabbli', '27-Apr-16', NULL, '13-Dec-98', 'F', '54', NULL, NULL, '9927925257', '9720985488, 9759778896', '4', '1', NULL, NULL, '2017-09-01 08:06:28', NULL),
(1015, '769', 'Anurag Saini', 'Pritam Singh Saini', 'Reeta Saini', 'XII COM-B', 'Suraj Vihar Colony, Chhutmalpur', '28-Feb-14', NULL, '13-Feb-00', 'M', '91', NULL, NULL, '9758752075/9084046940', '9759540452', '5', '1', NULL, NULL, '2017-09-01 08:06:28', NULL),
(1016, '1416', 'Ayushi Choudhary', 'Sanjeev Kumar', 'Suman', 'XII COM-B', 'Vill-Baheri Gujjar, Saharanpur', '20-Apr-16', NULL, '16-02-1999', 'F', '95', NULL, NULL, '9084142525', '8865912652', '7', '1', NULL, NULL, '2017-09-01 08:06:28', NULL);
INSERT INTO `studentmgmts` (`id`, `admission_no`, `student_name`, `student_father_name`, `student_mother_name`, `student_class_section`, `student_address`, `student_joining_date`, `student_images`, `student_dob`, `student_gender`, `student_username`, `student_password`, `student_email`, `student_mob1`, `student_mob2`, `student_rollno`, `sessionid`, `deleted_at`, `created_at`, `updated_at`, `code`) VALUES
(1017, '941', 'Hemant Kamboj', 'Pravesh Kamboj', 'Sashi Kamboj', 'XII COM-B', 'Vill-Damodrabad, Post--Biharigarh, SRE', '19-Apr-14', NULL, '28-03-1999', 'M', '96', NULL, NULL, '8954647455', '9756970035', '8', '1', NULL, NULL, '2017-09-01 08:06:28', NULL),
(1018, '1430', 'Himanshu Yadav', 'Birampal Singh', 'Sudha ', 'XII COM-B', 'Arya Nagar, Chhutmalpur, Distt-SRE', '27-Apr-16', NULL, '08-Jan-00', 'M', '112', NULL, NULL, '7830354501', '9720382024', '9', '1', NULL, NULL, '2017-09-01 08:06:28', NULL),
(1019, '501', 'Mohd. Shoaib', 'Ikraam', 'Shahjahan', 'XII COM-B', 'Vill Nanhera Bed Begampur', '18-May-12', NULL, '26-Mar-00', 'M', '132', NULL, NULL, '9837535395', '9528486037', '10', '1', NULL, NULL, '2017-09-01 08:06:28', NULL),
(1020, '937', 'Mohd. Toseef', 'Abdul Gaffar', 'Shahnaaz', 'XII COM-B', 'Near Purani Masjid, Gagalheri, SRE', '17-Apr-14', NULL, '17-Mar-98', 'M', '150', NULL, NULL, '9639707071', '9412648233', '11', '1', NULL, NULL, '2017-09-01 08:06:28', NULL),
(1021, '1600', 'Mukul Sharma', 'Brij Pal Sharma', 'Anita sharma', 'XII COM-B', 'Vill-Halwana, Post-Chhutmalpur, Distt-Saharanpur', '22-04-2017', NULL, '09-11-1999', 'M', '169', NULL, NULL, '9412525949', '9761275767', '12', '1', NULL, NULL, '2017-09-01 08:06:28', NULL),
(1022, '1435', 'Paras Singh Saini', 'Vijay Singh Saini', 'Archana Saini', 'XII COM-B', 'Vill-Bubbka Near Kalsiya, Distt-SRE', '03-Feb-16', NULL, 'G', 'M', '170', NULL, NULL, '9719681511', '9720830335', '13', '1', NULL, NULL, '2017-09-01 08:06:28', NULL),
(1023, '731', 'Rahil Chhabra', 'Rakesh Chhabra', 'Ritu Chhabra', 'XII COM-B', 'E-9, Gopal Nagar, Roorkee Camp, SRE', '14-May-13', NULL, '14-Nov-99', 'M', '173', NULL, NULL, '9897266351', '9760791424', '14', '1', NULL, NULL, '2017-09-01 08:06:28', NULL),
(1024, '896', 'Rao Afzal', 'Rao Shahid Ali', 'Zohra Begum', 'XII COM-B', 'Vill & Post-Khujnawar, Distt-SRE', '07-Apr-14', NULL, '02-May-99', 'M', '178', NULL, NULL, '7500656565', '9719055550', '15', '1', NULL, NULL, '2017-09-01 08:06:28', NULL),
(1025, '64', 'Ritik Gandhi', 'Raj Kumar Gandhi', 'Anita Gandhi', 'XII COM-B', 'Punjabi Colony, Chhutrampur', '04-May-11', NULL, '01-Oct-99', 'M', '183', NULL, NULL, '9359959651', '9557163003', '16', '1', NULL, NULL, '2017-09-01 08:06:28', NULL),
(1026, '383', 'Saloni Yadav', 'Mukesh Kumar', 'Aruna Yadav', 'XII COM-B', 'Vill & post Sambhalki Gujjar', '04-Apr-12', NULL, '11-Sep-00', 'F', '199', NULL, NULL, '9758645741', '9536698300', '17', '1', NULL, NULL, '2017-09-01 08:06:28', NULL),
(1027, '853', 'Saravjeet Singh', 'Punjab Singh', 'Navneet Kaur', 'XII COM-B', 'Vill & Post Biharigarh, Distt-SRE', '03-Apr-14', NULL, '19-Sep-99', 'M', '232', NULL, NULL, '9719529914', '9719438194', '18', '1', NULL, NULL, '2017-09-01 08:06:28', NULL),
(1028, '1420', 'Shweta Kashyap', 'Aman Kumar', 'Shashi', 'XII COM-B', 'Dinarpur Gaglheri, Distt-Saharanpur', '20-Apr-16', NULL, '08-Aug-01', 'F', '237', NULL, NULL, '', '7409553475', '19', '1', NULL, NULL, '2017-09-01 08:06:28', NULL),
(1029, '1000', 'Vaishnavi Chouhan', 'Charan Singh Chouhan', 'Usha Devi', 'XII COM-B', 'Vill-Nanka, Post-Chhutmalpur, Dist-Saharanpur', '03-Jul-14', NULL, '12-Oct-00', 'F', '254', NULL, NULL, '9758982697', '9759752697', '20', '1', NULL, NULL, '2017-09-01 08:06:28', NULL),
(1030, '357', 'Vishal Pundir', 'Jitendra Pundir', 'Poonam Pundir', 'XII COM-B', 'Chhutmalpur, Gandhi Colony', '02-Apr-12', NULL, '05-Feb-01', 'M', '294', NULL, NULL, '9412532336', '9457048736', '21', '1', NULL, NULL, '2017-09-01 08:06:28', NULL),
(1031, '1253', 'Ayush Chauhan', 'Madan Chauhan', 'Poonam Chauhan', 'XII COM-B', 'Vill-Budhva Shaheed,Post-Buggawala,Distt. Haridwar', '01-Jul-15', NULL, '08-Jun-99', 'M', '334', NULL, NULL, '9760239939', '8755461523', '6', '1', NULL, NULL, '2017-09-01 08:06:28', NULL),
(1032, '453', 'Antriksh Saini', 'Anil Saini', 'Savita Saini', 'XII SCI-A', 'Vill-Banwala Post-Chhutmalpur', '16-Apr-12', NULL, '18-Mar-99', 'M', '229', NULL, NULL, '9759564729', '9152585910', '13', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1033, '511', 'Aanchal Saini', 'Bhopal Singh', 'Babita Devi', 'XII SCI-A', 'Vill Sona Syed Mazra, Post Harora', '26-Jun-12', NULL, '09-Oct-00', 'F', '302', NULL, NULL, '9536020171', '', '1', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1034, '852', 'Abhishek Kumar', 'Ramesh Kumar', 'Santlesh Devi', 'XII SCI-A', 'Kishangarh Colony,Biharigarh,SRE', '02-Apr-14', NULL, '08-Aug-00', 'M', '319', NULL, NULL, '9675704305', '8979682958', '2', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1035, '845', 'Abhishek Saini', 'Mukesh Saini', 'Susheela Devi', 'XII SCI-A', 'DDN Road, Shiv Colony, Opp. PNB, Fatehpur', '02-Apr-14', NULL, '02-Jul-01', 'M', '348', NULL, NULL, '9411039492', '8006305629', '3', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1036, '883', 'Abhishek Saini', 'Sandeep Saini', 'Parvesh Saini', 'XII SCI-A', 'Vill-Muzaffrabad, Post-Khas, Distt-Saharanpur', '05-Apr-14', NULL, '01-Jun-99', 'M', '403', NULL, NULL, '9627539590', '', '4', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1037, '801', 'Aditya Sharma', 'Mukesh Sharma', 'Kavita Sharma', 'XII SCI-A', 'Maharan Pratap Colony, Street No.1, Chhutmalpur', '20-Mar-14', NULL, '21-Sep-99', 'M', '411', NULL, NULL, '9639500500', '', '5', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1038, '640', 'Akhilesh Saini', 'Anil Saini', 'Meena Devi', 'XII SCI-A', 'Biharigarh ', '04-Apr-13', NULL, '26-Jan-01', 'M', '437', NULL, NULL, '8869022284', '9720423381', '6', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1039, '330', 'Ali Nawaj', 'Naushad Husain', 'Shadmani', 'XII SCI-A', 'Syed Mazra, Harora', '28-Mar-12', NULL, '15-May-00', 'M', '460', NULL, NULL, '9759109815', '', '7', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1040, '806', 'Aman Sehgal', 'Vinod Sehgal', 'Kusum Devi', 'XII SCI-A', 'Jayantipur Gably, Post-Kurrikhera, Distt-Saharanpur', '26-Mar-14', NULL, '18-Feb-02', 'M', '466', NULL, NULL, '9758771452', '8475081049', '8', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1041, '1375', 'Amanpal', 'Charan Singh', 'Surmesh Pal', 'XII SCI-A', 'Vill-Kurdikhera Chanchak, Dist-SRE', '09-Apr-16', NULL, '01-Jan-00', 'M', '474', NULL, NULL, '9719035750', '9759322003', '9', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1042, '1229', 'Amit Pundir', 'Ramesh Chand', 'Rajvati Devi', 'XII SCI-A', 'Vill- Banjarawal, Post- Tanko Sunderpur, Distt- Haridwar', '01-May-15', NULL, '30-Sep-99', 'M', '479', NULL, NULL, '9758028995', '9761099532', '10', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1043, '616', 'Anant Rathore', 'Sanjay Kumar', 'Rekha Rani', 'XII SCI-A', 'Chhutmalpur, Gandhi Colony', '03-Apr-13', NULL, '06-Mar-01', 'M', '539', NULL, NULL, '9897879991', '8410510511', '11', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1044, '675', 'Ankit Rana', 'Mukesh Rana', 'Naresho', 'XII SCI-A', 'Vill. & Post Sunderpur, Saharanpur', '09-Apr-13', NULL, '28-Oct-00', 'M', '560', NULL, NULL, '9719732911', '', '12', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1045, '726', 'Anuj Kumar Rana', 'Deshraj Rana', 'Santosh Devi', 'XII SCI-A', 'Vill. & Post Kurrikhera', '06-May-13', NULL, '20-May-00', 'M', '570', NULL, NULL, '9720860780', '9720372935', '14', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1046, '1476', 'Archit  Jain', 'Rajeev Kumar Jain', 'Sangeeta Jain', 'XII SCI-A', '10/185 Chatta Jambudass , Saharanpur', '02-Sep-16', NULL, '28-Mar-00', 'M', '596', NULL, NULL, '9219691893', '9219611893', '15', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1047, '1431', 'Aryan Singh', 'Satyapal Singh', 'Premvati', 'XII SCI-A', 'Arya Nagar, Chhutmalpur, Distt-SRE', '27-Apr-16', NULL, '05-Sep-00', 'M', '597', NULL, NULL, '8126890780', '8273382363', '16', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1048, '147', 'Ashish Kumar Goyal', 'Ajay Kumar', 'Priyanka', 'XII SCI-A', 'Village Gangali', '12-Apr-11', NULL, '27-Jul-01', 'M', '638', NULL, NULL, '9927368987', '', '17', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1049, '347', 'Avi Saini', 'Praveen Kumar Saini', 'Sheetal Saini', 'XII SCI-A', 'Vill Daddapatti, Post Chhutmalpur, Distt. Saharanpur', '30-Mar-12', NULL, '18-Apr-02', 'M', '643', NULL, NULL, '9897341017', '9997712240', '18', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1050, '913', 'Diksha Mittal', 'Dinesh Kumar Mittal', 'Savita Mittal', 'XII SCI-A', 'Harnam Singh Nagar, Chhutmalpur, SRE', '09-Apr-14', NULL, '01-Nov-01', 'F', '662', NULL, NULL, '9719445255', '', '19', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1051, '851', 'Divyansh Kumar', 'Devendra Kumar', 'Sneh Lata', 'XII SCI-A', 'Kishangarh Colony,Biharigarh,SRE', '02-Apr-14', NULL, '08-Feb-01', 'M', '663', NULL, NULL, '9761215391', '', '20', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1052, '893', 'Gunjan Saini', 'Arjun Singh', 'Seema Saini', 'XII SCI-A', 'Suraj Vihar Colony,Chhutmalpur', '07-Apr-14', NULL, '23-Mar-01', 'F', '666', NULL, NULL, '9897325801', '', '21', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1053, '1296', 'Harshit Singh', 'Haripal Singh', 'Rekha Singh', 'XII SCI-A', 'Gandhi Colony, Chhutmalpur, Distt-SRE', '28-Mar-16', NULL, '02-Nov-02', 'M', '680', NULL, NULL, '9457583279', '', '22', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1054, '915', 'Himanshu Pal', 'Vedpal', 'Sushila Devi', 'XII SCI-A', 'Vill-Kamaalpur, Post-Chhutmalpur, Distt-SRE', '09-Apr-14', NULL, '25-May-00', 'M', '691', NULL, NULL, '9927558228', '9917883839', '23', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1055, '823', 'Kirti Bhardwaj', 'Sandeep Bhardwaj', 'Bharti Bhardwaj', 'XII SCI-A', 'Vill-Dinarpur, Post-Gagalheri, Distt-SRE', '31-Mar-14', NULL, '04-Aug-00', 'F', '701', NULL, NULL, '9897231754', '9548549595', '24', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1056, '950', 'Laiba', 'Tokir', 'Aamna Begam', 'XII SCI-A', 'D.Dun Road, Opp. Behat Petrol Pump, Chhutmalpur', '21-Apr-14', NULL, '15-Dec-00', 'F', '719', NULL, NULL, '9536040262', '', '25', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1057, '878', 'Lavi Kamboj', 'Jaipal Singh', 'Sulochana Devi', 'XII SCI-A', 'Vill-Damodrabad, Post-Biharigarh, Distt-SRE', '04-Apr-14', NULL, '15-Dec-99', 'M', '739', NULL, NULL, '9412480359', '9758791945', '26', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1058, '175', 'Mansi ', 'Ravindra Kumar', 'Santosh', 'XII SCI-A', 'Village Rehri', '15/4/11', NULL, '25-Mar-99', 'F', '762', NULL, NULL, '9719952564', '', '27', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1059, '22', 'Mansi Chaudhary', 'Manoj Chaudhary', 'Renu Chaudhary', 'XII SCI-A', 'Chhutmalpur', '04-Feb-11', NULL, '23-Oct-00', 'F', '774', NULL, NULL, '9837513400', '9917207406', '28', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1060, '537', 'Mohd Warid Elahi Khan', 'Masroor Khan', 'Farzana Begum', 'XII SCI-A', 'Vill & Post Sansarpur, Distt. Saharanpur', '13-Jul-12', NULL, '06-Mar-01', 'M', '794', NULL, NULL, '9927758108', '9837317549', '29', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1061, '99', 'Mohd. Ajmal', 'Mohd. Irfan', 'Shajahan', 'XII SCI-A', 'Arya Nagar, Chhutmalpur', '04-Jul-11', NULL, '19-Nov-99', 'M', '801', NULL, NULL, '8954002052', '', '30', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1062, '1302', 'Mohd. Umar Ansari', 'Mukarram Nadim Ansari', 'Afsha Nadim', 'XII SCI-A', 'Main Gali, Harijan Colony, Chhutmalpur, Distt-SRE', '29-Mar-16', NULL, '15-Nov-01', 'M', '812', NULL, NULL, '9358987297', '9358591862', '31', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1063, '165', 'Nandini Garg', 'Ashutosh Garg', 'Poonam Garg', 'XII SCI-A', 'Vill-Bhagwanpur, Dist-Haridwar', '14/4/11', NULL, '13-May-01', 'F', '824', NULL, NULL, '9927352270', '9760305020', '32', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1064, '1070', 'Sakshi Kamboj', 'Karanveer', 'Uma Devi', 'XII SCI-A', 'Vill-Gher Karma, Post-Biharigarh,Distt-SRE', '27-Mar-15', NULL, '11-Apr-99', 'F', '836', NULL, NULL, '9719979316', '', '38', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1065, '835', 'Nidhi Chaudhary', 'Ravish Kumar', 'Umlesh Devi', 'XII SCI-A', 'Vill-Roopri Junardar, Post-Kota, Distt-SRE', '31-Mar-14', NULL, '30-May-01', 'F', '838', NULL, NULL, '9639191264', '9756885327', '33', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1066, '101', 'Prashant Saini', 'Sutendra Saini', 'Babita saini', 'XII SCI-A', 'Village Dadapatti', '04-Jul-11', NULL, '01-Nov-99', 'M', '839', NULL, NULL, '9917045836', '', '34', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1067, '618', 'Reegesh', 'Adeep Kumar', 'Anita', 'XII SCI-A', 'Amanat Garh', '03-Apr-13', NULL, '14-Aug-00', 'M', '842', NULL, NULL, '9759726970', '9719242597', '35', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1068, '775', 'Riya Saini', 'Brij Bhushan Saini', 'Geeta Saini', 'XII SCI-A', 'Vill & Post Chamarikhera, Distt-Saharanpur', '03-Mar-14', NULL, '17-Jun-00', 'F', '860', NULL, NULL, '9927045348', '9756865384', '36', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1069, '98', 'Sagar Pal', 'Rakesh Pal', 'Kusum', 'XII SCI-A', 'Village Tanda Hasangarh, Post Kheri Shikohpur', '04-Jul-11', NULL, '15-Jun-00', 'M', '863', NULL, NULL, '9720065707', '', '37', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1070, '762', 'Saloni', 'Jai Kumar', 'Munesh', 'XII SCI-A', 'Vill. & Post Chamarikhera', '19-Aug-13', NULL, '26-Jun-99', 'F', '902', NULL, NULL, '9012858888', '9927496434', '39', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1071, '1458', 'Saloni Chauhan', 'Vijendra Chauhan', 'Aruna Chauhan', 'XII SCI-A', 'Vill-Kaluwala Pahadipur urf JahanpurDistt-SRE', '08-Jul-16', NULL, '11-Mar-02', 'F', '913', NULL, NULL, '9758079705', '9690820702', '40', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1072, '841', 'Sapan Singhal', 'Arun Singhal', 'Manju Singhal', 'XII SCI-A', 'Gandhi Colony, Chhutmalpur', '01-Apr-14', NULL, '21-Oct-99', 'M', '920', NULL, NULL, '9997171930', '7409061482', '41', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1073, '838', 'Shivan Sehgal', 'Deshraj Sehgal', 'Salelta Devi', 'XII SCI-A', 'Vill-Gabli, Post-Kurrikhera, Distt-SRE', '01-Apr-14', NULL, '19-Apr-01', 'M', '927', NULL, NULL, '9761175083', '9536620707', '42', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1074, '133', 'Shreya Grover', 'Bharat Bhushan Grover', 'Sangita Grover', 'XII SCI-A', 'Chhutmalpur, Grover Tailor, P. Mkt.', '04-Aug-11', NULL, '19-Mar-99', 'F', '950', NULL, NULL, '9045892482', '9045892483', '43', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1075, '46', 'Simran Kamboj', 'Nortu Singh Kamboj', 'Poonam Kamboj', 'XII SCI-A', 'Chhutmalpur', '04-May-11', NULL, '18-Sep-02', 'F', '969', NULL, NULL, '1322782542', '9536992456', '44', '1', NULL, NULL, '2017-09-08 08:03:57', NULL),
(1076, '1450', 'Sumit Chauhan', 'Vimal Chauhan', 'Lokesh Chauhan', 'XII SCI-A', 'Maharana Partap Colony Gali No.-2, Chhutmalpur, Distt-SRE', '28-Jun-16', NULL, '05-Nov-99', 'M', '970', NULL, NULL, '9719225000', '8171814446', '45', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1077, '1408', 'Tanu Kamboj', 'Ashwani Kamboj', 'Rani Devi', 'XII SCI-A', 'Vill-Ganja Majra Post-Biharigarh Distt-SRE', '16-Apr-16', NULL, '24-May-01', 'F', '973', NULL, NULL, '9719127559', '9758035665', '46', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1078, '912', 'Tanusha Yadav', 'Sanjay Yadav', 'Nershal Yadav', 'XII SCI-A', 'Shiv Vihar Colony, Dinarpur, Gagalheri', '09-Apr-14', NULL, '15-Jan-01', 'F', '1005', NULL, NULL, '9760564491', '0132-22785551', '47', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1079, '771', 'Tarushi Bansal', 'Rakesh Kumar', 'Shashi Prabha', 'XII SCI-A', 'Saharanpur Road, Chhutmalpur', '28-Feb-14', NULL, '22-Aug-00', 'F', '1032', NULL, NULL, '9808113584', '9808441612', '48', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1080, '922', 'Vashvi Chaudhary', 'Vipin Chaudhary', 'Sangeeta Chaudhary', 'XII SCI-A', 'Maharana Pratap Colony, Chhutmalpur', '14-Apr-14', NULL, '11-Jan-01', 'F', '1054', NULL, NULL, '9758229245', '7500229245', '49', '1', NULL, NULL, '2017-09-01 05:49:43', NULL),
(1081, '826', 'Vishal Saini', 'Pramod Kumar', 'Savita Saini', 'XII SCI-A', 'Vill Alawalpur, Post Chhutmalpur, Distt-Haridwar', '31-Mar-14', NULL, '23-Nov-00', 'M', '1056', NULL, NULL, '9720757049', '', '50', '1', NULL, NULL, '2017-09-01 05:49:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subjectmgmts`
--

CREATE TABLE `subjectmgmts` (
  `id` int(10) UNSIGNED NOT NULL,
  `subject_code` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subject_name` varchar(190) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class_applicable` varchar(190) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` text COLLATE utf8_unicode_ci,
  `extra` text COLLATE utf8_unicode_ci,
  `term1_unittest` varchar(190) COLLATE utf8_unicode_ci DEFAULT NULL,
  `term1_exam` varchar(190) COLLATE utf8_unicode_ci DEFAULT NULL,
  `term1_total` varchar(190) COLLATE utf8_unicode_ci DEFAULT NULL,
  `term2_unittest` varchar(190) COLLATE utf8_unicode_ci DEFAULT NULL,
  `term2_exam` varchar(190) COLLATE utf8_unicode_ci DEFAULT NULL,
  `term2_total` varchar(190) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(190) COLLATE utf8_unicode_ci DEFAULT NULL,
  `optional` varchar(500) COLLATE utf8_unicode_ci DEFAULT 'no',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `grand_total_marks` varchar(190) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_term1` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_term2` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sessionid` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `subjectmgmts`
--

INSERT INTO `subjectmgmts` (`id`, `subject_code`, `subject_name`, `class_applicable`, `code`, `extra`, `term1_unittest`, `term1_exam`, `term1_total`, `term2_unittest`, `term2_exam`, `term2_total`, `status`, `optional`, `deleted_at`, `created_at`, `updated_at`, `grand_total_marks`, `date_term1`, `date_term2`, `sessionid`) VALUES
(1, '002', 'HINDI(Read,Rect&Dict.)', 'I-A', NULL, 'N.A', '0', '50', '50', '0', '50', '50', 'Active', 'only exam', NULL, '2017-09-01 07:03:11', '2017-09-05 03:40:08', '100', '09-09-2017', NULL, '1'),
(2, '002', 'HINDI (Read,Rect. & Dict.)', 'I-B', NULL, 'N.A', '0', '50', '50', '0', '50', '50', 'Active', 'only exam', NULL, '2017-09-01 07:04:07', '2017-09-05 03:40:21', '100', '09-09-2017', NULL, '1'),
(3, '301R', 'ENGLISH(Read,Rect.&Dict.)', 'I-A', NULL, 'N.A', '0', '50', '50', '0', '50', '50', 'Active', 'both test and exam', NULL, '2017-09-01 07:06:34', '2017-09-13 10:25:55', '100', '11-09-2017', NULL, '1'),
(4, '301', 'ENGLISH(Read,Rect.&Dict.)', 'I-B', NULL, 'N.A', '0', '100', '100', '0', '100', '100', 'Active', 'both test and exam', NULL, '2017-09-01 07:06:49', '2017-09-03 09:02:30', '200', '11-09-2017', NULL, '1'),
(5, '004', 'G.K/ART', 'I-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-01 07:11:30', '2017-09-01 07:11:30', '250', '12-09-2017', NULL, '1'),
(6, '004', 'G.K/ART', 'I-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-01 07:11:44', '2017-09-01 07:11:44', '250', '12-09-2017', NULL, '1'),
(9, '083', 'COMPUTER', 'I-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-01 07:51:42', '2017-09-03 09:11:36', '250', '13-09-2017', NULL, '1'),
(10, '083', 'COMPUTER', 'I-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-01 07:51:53', '2017-09-03 09:11:51', '250', '13-09-2017', NULL, '1'),
(11, '041', 'MATHEMATICS', 'I-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-01 07:52:33', '2017-09-01 07:52:33', '250', '15-09-2017', NULL, '1'),
(12, '041', 'MATHEMATICS', 'I-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-01 07:52:40', '2017-09-01 07:52:40', '250', '15-09-2017', NULL, '1'),
(13, '301', 'ENGLISH', 'I-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-01 07:52:56', '2017-09-01 07:52:56', '250', '18-09-2017', NULL, '1'),
(14, '301', 'ENGLISH', 'I-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-01 07:53:05', '2017-09-01 07:53:05', '250', '18-09-2017', NULL, '1'),
(15, '002', 'HINDI', 'I-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-01 07:53:23', '2017-09-01 07:53:23', '250', '20-09-2017', NULL, '1'),
(16, '002', 'HINDI', 'I-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-01 07:53:31', '2017-09-01 07:53:31', '250', '20-09-2017', NULL, '1'),
(17, '086', 'E.V.S', 'I-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-01 07:53:54', '2017-09-01 07:53:54', '250', '22-09-2017', NULL, '1'),
(18, '086', 'E.V.S', 'I-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-01 07:54:02', '2017-09-01 07:54:02', '250', '22-09-2017', NULL, '1'),
(19, '301', 'ENGLISH(Read,Rect.&Dict.)', 'II-A', NULL, 'N.A', '0', '50', '50', '0', '50', '50', 'Active', 'only exam', NULL, '2017-09-01 07:57:44', '2017-09-05 03:41:11', '100', '09-09-2017', NULL, '1'),
(20, '301', 'ENGLISH(Read,Rect.&Dict.)', 'II-B', NULL, 'N.A', '0', '50', '50', '0', '50', '50', 'Active', 'only exam', NULL, '2017-09-01 07:57:56', '2017-09-05 03:42:49', '100', '09-09-2017', NULL, '1'),
(21, '002', 'HINDI(Read,Rect&Dict.)', 'II-A', NULL, 'N.A', '0', '50', '50', '0', '50', '50', 'Active', 'only exam', NULL, '2017-09-01 07:58:10', '2017-09-05 03:43:04', '100', '11-09-2017', NULL, '1'),
(22, '002', 'HINDI(Read,Rect&Dict.)', 'II-B', NULL, 'N.A', '0', '50', '50', '0', '50', '50', 'Active', 'only exam', NULL, '2017-09-01 07:58:18', '2017-09-05 03:43:47', '100', '11-09-2017', NULL, '1'),
(23, '083', 'COMPUTER', 'II-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-01 07:58:35', '2017-09-03 09:12:08', '250', '12-09-2017', NULL, '1'),
(24, '083', 'COMPUTER', 'II-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-01 07:58:43', '2017-09-03 09:12:20', '250', '12-09-2017', NULL, '1'),
(25, '004', 'G.K/ART', 'II-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-01 07:59:44', '2017-09-01 07:59:44', '250', '13-09-2017', NULL, '1'),
(26, '004', 'G.K/ART', 'II-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-01 07:59:51', '2017-09-01 07:59:51', '250', '13-09-2017', NULL, '1'),
(27, '086', 'E.V.S', 'II-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-01 08:00:06', '2017-09-01 08:00:06', '250', '15-09-2017', NULL, '1'),
(28, '086', 'E.V.S', 'II-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-01 08:00:15', '2017-09-01 08:00:15', '250', '15-09-2017', NULL, '1'),
(29, '002', 'HINDI', 'II-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-01 08:00:35', '2017-09-01 08:00:35', '250', '18-09-2017', NULL, '1'),
(30, '002', 'HINDI', 'II-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-01 08:00:44', '2017-09-01 08:00:44', '250', '18-09-2017', NULL, '1'),
(31, '301', 'ENGLISH', 'II-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-01 08:01:11', '2017-09-01 08:01:11', '250', '20-09-2017', NULL, '1'),
(32, '301', 'ENGLISH', 'II-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-01 08:01:19', '2017-09-01 08:01:19', '250', '20-09-2017', NULL, '1'),
(33, '041', 'MATHEMATICS', 'II-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-01 08:01:36', '2017-09-01 08:01:36', '250', '22-09-2017', NULL, '1'),
(34, '041', 'MATHEMATICS', 'II-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-01 08:01:45', '2017-09-01 08:01:45', '250', '22-09-2017', NULL, '1'),
(35, '002', 'HINDI(Read,Rect&Dict.)', 'III-A', NULL, 'N.A', '0', '50', '50', '0', '50', '50', 'Active', 'only exam', NULL, '2017-09-03 08:41:06', '2017-09-05 03:43:28', '100', '09-09-2017', NULL, '1'),
(36, '002', 'HINDI(Read,Rect&Dict.)', 'III-B', NULL, 'N.A', '0', '50', '50', '0', '50', '50', 'Active', 'only exam', NULL, '2017-09-03 08:41:26', '2017-09-05 03:44:09', '100', '09-09-2017', NULL, '1'),
(37, '301', 'ENGLISH(Read,Rect.&Dict.)', 'III-A', NULL, 'N.A', '0', '50', '50', '0', '50', '50', 'Active', 'only exam', NULL, '2017-09-03 08:41:48', '2017-09-05 03:44:29', '100', '11-09-2017', NULL, '1'),
(38, '301', 'ENGLISH(Read,Rect.&Dict.)', 'III-B', NULL, 'N.A', '0', '50', '50', '0', '50', '50', 'Active', 'only exam', NULL, '2017-09-03 08:42:03', '2017-09-05 03:44:57', '100', '11-09-2017', NULL, '1'),
(39, '004', 'G.K/ART', 'III-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 08:42:27', '2017-09-03 08:42:27', '250', '12-09-2017', NULL, '1'),
(40, '004', 'G.K/ART', 'III-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 08:42:37', '2017-09-03 08:42:37', '250', '12-09-2017', NULL, '1'),
(41, '083', 'COMPUTER', 'III-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 08:43:06', '2017-09-03 09:12:35', '250', '13-09-2017', NULL, '1'),
(42, '083', 'COMPUTER', 'III-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 08:43:28', '2017-09-03 09:12:47', '250', '13-09-2017', NULL, '1'),
(43, '041', 'MATHEMATICS', 'III-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 08:44:07', '2017-09-03 08:44:07', '250', '15-09-2017', NULL, '1'),
(44, '041', 'MATHEMATICS', 'III-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 08:44:16', '2017-09-03 08:44:16', '250', '15-09-2017', NULL, '1'),
(45, '086', 'SCIENCE', 'III-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 08:44:58', '2017-09-03 08:45:28', '250', '18-09-2017', NULL, '1'),
(46, '086', 'SCIENCE', 'III-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 08:45:46', '2017-09-03 08:45:46', '250', '18-09-2017', NULL, '1'),
(47, '002', 'HINDI', 'III-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 08:46:13', '2017-09-03 08:46:13', '250', '20-09-2017', NULL, '1'),
(48, '002', 'HINDI', 'III-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 08:46:23', '2017-09-03 08:46:23', '250', '20-09-2017', NULL, '1'),
(49, '301', 'ENGLISH', 'III-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 08:46:46', '2017-09-03 08:46:46', '250', '22-09-2017', NULL, '1'),
(50, '301', 'ENGLISH', 'III-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 08:46:55', '2017-09-03 08:46:55', '250', '22-09-2017', NULL, '1'),
(51, '087', 'SOCIAL STUDIES', 'III-A', NULL, NULL, '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 08:50:34', '2017-09-03 08:50:59', '250', '25-09-2017', NULL, '1'),
(52, '087', 'SOCIAL STUDIES', 'III-B', NULL, NULL, '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 08:53:38', '2017-09-03 08:53:38', '250', '25-09-2017', NULL, '1'),
(53, '301', 'ENGLISH(Read,Rect.&Dict.)', 'IV-A', NULL, 'N.A', '0', '50', '50', '0', '50', '50', 'Active', 'only exam', NULL, '2017-09-03 08:54:19', '2017-09-05 03:45:22', '100', '09-09-2017', NULL, '1'),
(54, '301', 'ENGLISH(Read,Rect.&Dict.)', 'IV-B', NULL, 'N.A', '0', '50', '50', '0', '50', '50', 'Active', 'only exam', NULL, '2017-09-03 08:54:28', '2017-09-05 03:45:40', '100', '09-09-2017', NULL, '1'),
(55, '002', 'HINDI(Read,Rect&Dict.)', 'IV-A', NULL, 'N.A', '0', '50', '50', '0', '50', '50', 'Active', 'only exam', NULL, '2017-09-03 08:55:00', '2017-09-05 03:45:56', '100', '11-09-2017', NULL, '1'),
(56, '002', 'HINDI(Read,Rect&Dict.)', 'IV-B', NULL, 'N.A', '0', '50', '50', '0', '50', '50', 'Active', 'only exam', NULL, '2017-09-03 08:55:11', '2017-09-05 03:46:15', '100', '11-09-2017', NULL, '1'),
(57, '083', 'COMPUTER', 'IV-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 08:55:34', '2017-09-03 09:13:01', '250', '12-09-2017', NULL, '1'),
(58, '083', 'COMPUTER', 'IV-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 08:55:46', '2017-09-03 09:13:10', '250', '12-09-2017', NULL, '1'),
(59, '004', 'G.K/ART', 'IV-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 08:56:11', '2017-09-03 08:56:11', '250', '13-09-2017', NULL, '1'),
(60, '004', 'G.K/ART', 'IV-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 08:56:20', '2017-09-03 08:56:20', '250', '13-09-2017', NULL, '1'),
(61, '086', 'SCIENCE', 'IV-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 08:56:59', '2017-09-03 08:56:59', '250', '15-09-2017', NULL, '1'),
(62, '086', 'SCIENCE', 'IV-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 08:57:12', '2017-09-03 08:57:12', '250', '15-09-2017', NULL, '1'),
(63, '087', 'SOCIAL STUDIES', 'IV-A', NULL, NULL, '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 08:57:35', '2017-09-03 08:57:35', '250', '18-09-2017', NULL, '1'),
(64, '087', 'SOCIAL STUDIES', 'IV-B', NULL, NULL, '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 08:57:46', '2017-09-03 08:57:46', '250', '18-09-2017', NULL, '1'),
(65, '301', 'ENGLISH', 'IV-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 08:58:30', '2017-09-03 08:58:30', '250', '20-09-2017', NULL, '1'),
(66, '301', 'ENGLISH', 'IV-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 08:58:40', '2017-09-03 08:58:40', '250', '20-09-2017', NULL, '1'),
(67, '002', 'HINDI', 'IV-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 08:59:02', '2017-09-03 08:59:02', '250', '22-09-2017', NULL, '1'),
(68, '002', 'HINDI', 'IV-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 08:59:14', '2017-09-03 08:59:14', '250', '22-09-2017', NULL, '1'),
(69, '041', 'MATHEMATICS', 'IV-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 08:59:38', '2017-09-03 08:59:38', '250', '25-09-2017', NULL, '1'),
(70, '041', 'MATHEMATICS', 'IV-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 08:59:46', '2017-09-03 08:59:46', '250', '25-09-2017', NULL, '1'),
(71, '002', 'HINDI(Read,Rect&Dict.)', 'V-A', NULL, 'N.A', '0', '50', '50', '0', '50', '50', 'Active', 'only exam', NULL, '2017-09-03 09:00:59', '2017-09-05 03:46:35', '100', '09-09-2017', NULL, '1'),
(72, '002', 'HINDI(Read,Rect&Dict.)', 'V-B', NULL, 'N.A', '0', '50', '50', '0', '50', '50', 'Active', 'only exam', NULL, '2017-09-03 09:01:09', '2017-09-05 03:46:53', '100', '09-09-2017', NULL, '1'),
(73, '301', 'ENGLISH(Read,Rect.&Dict.)', 'V-A', NULL, 'N.A', '0', '50', '50', '0', '50', '50', 'Active', 'only exam', NULL, '2017-09-03 09:01:31', '2017-09-05 03:47:13', '100', '11-09-2017', NULL, '1'),
(74, '301', 'ENGLISH(Read,Rect.&Dict.)', 'V-B', NULL, 'N.A', '0', '50', '50', '0', '50', '50', 'Active', 'only exam', NULL, '2017-09-03 09:02:06', '2017-09-05 03:47:29', '100', '11-09-2017', NULL, '1'),
(75, '004', 'G.K/ART', 'V-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:03:14', '2017-09-03 09:03:14', '250', '12-09-2017', NULL, '1'),
(76, '004', 'G.K/ART', 'V-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:03:25', '2017-09-03 09:03:25', '250', '12-09-2017', NULL, '1'),
(77, '083', 'COMPUTER', 'V-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:03:48', '2017-09-03 09:13:25', '250', '13-09-2017', NULL, '1'),
(78, '083', 'COMPUTER', 'V-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:03:59', '2017-09-03 09:13:36', '250', '13-09-2017', NULL, '1'),
(79, '041', 'MATHEMATICS', 'V-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:05:02', '2017-09-03 09:05:02', '250', '15-09-2017', NULL, '1'),
(80, '041', 'MATHEMATICS', 'V-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:05:20', '2017-09-03 09:05:20', '250', '15-09-2017', NULL, '1'),
(81, '086', 'SCIENCE', 'V-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:05:38', '2017-09-03 09:05:38', '250', '18-09-2017', NULL, '1'),
(82, '086', 'SCIENCE', 'V-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:05:48', '2017-09-03 09:05:48', '250', '18-09-2017', NULL, '1'),
(83, '002', 'HINDI', 'V-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:06:07', '2017-09-03 09:06:07', '250', '20-09-2017', NULL, '1'),
(84, '002', 'HINDI', 'V-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:06:19', '2017-09-03 09:06:19', '250', '20-09-2017', NULL, '1'),
(85, '301', 'ENGLISH', 'V-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:06:39', '2017-09-03 09:06:39', '250', '22-09-2017', NULL, '1'),
(86, '301', 'ENGLISH', 'V-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:06:48', '2017-09-03 09:06:48', '250', '22-09-2017', NULL, '1'),
(87, '087', 'SOCIAL STUDIES', 'V-A', NULL, NULL, '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:07:07', '2017-09-03 09:07:07', '250', '25-09-2017', NULL, '1'),
(88, '087', 'SOCIAL STUDIES', 'V-B', NULL, NULL, '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:07:17', '2017-09-03 09:07:17', '250', '25-09-2017', NULL, '1'),
(89, '083', 'COMPUTER', 'VI-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:08:00', '2017-09-03 09:14:48', '250', '09-09-2017', NULL, '1'),
(90, '083', 'COMPUTER', 'VI-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:08:54', '2017-09-03 09:15:00', '250', '09-09-2017', NULL, '1'),
(91, '122', 'SANSKRIT', 'VI-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:10:17', '2017-09-03 09:10:17', '250', '11-09-2017', NULL, '1'),
(92, '122', 'SANSKRIT', 'VI-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:10:38', '2017-09-03 09:10:38', '250', '11-09-2017', NULL, '1'),
(93, '004', 'G.K', 'I-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:14:03', '2017-09-13 10:30:13', '250', '12-09-2017', NULL, '1'),
(94, '004', 'G.K/ART', 'VI-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:14:29', '2017-09-03 09:14:29', '250', '12-09-2017', NULL, '1'),
(95, '301', 'ENGLISH', 'VI-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:15:53', '2017-09-03 09:15:53', '250', '15-09-2017', NULL, '1'),
(96, '301', 'ENGLISH', 'VI-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:16:03', '2017-09-03 09:16:03', '250', '15-09-2017', NULL, '1'),
(97, '041', 'MATHEMATICS', 'VI-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:16:25', '2017-09-03 09:16:25', '250', '18-09-2017', NULL, '1'),
(98, '041', 'MATHEMATICS', 'VI-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:16:37', '2017-09-03 09:16:37', '250', '18-09-2017', NULL, '1'),
(99, '087', 'SOCIAL STUDIES', 'VI-A', NULL, NULL, '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:17:00', '2017-09-03 09:17:00', '250', '20-09-2017', NULL, '1'),
(100, '087', 'SOCIAL STUDIES', 'VI-B', NULL, NULL, '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:17:16', '2017-09-03 09:17:16', '250', '20-09-2017', NULL, '1'),
(101, '002', 'HINDI', 'VI-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:17:39', '2017-09-03 09:17:39', '250', '22-09-2017', NULL, '1'),
(102, '002', 'HINDI', 'VI-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:17:51', '2017-09-03 09:17:51', '250', '22-09-2017', NULL, '1'),
(103, '086', 'SCIENCE', 'VI-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:18:16', '2017-09-03 09:18:16', '250', '25-09-2017', NULL, '1'),
(104, '086', 'SCIENCE', 'VI-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:18:27', '2017-09-03 09:18:27', '250', '25-09-2017', NULL, '1'),
(105, '004', 'G.K/ART', 'VII-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:21:10', '2017-09-03 09:21:10', '250', '09-09-2017', NULL, '1'),
(106, '004', 'G.K/ART', 'VII-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:22:51', '2017-09-03 09:22:51', '250', '09-09-2017', NULL, '1'),
(107, '083', 'COMPUTER', 'VII-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:25:05', '2017-09-03 09:25:05', '250', '11-09-2017', NULL, '1'),
(108, '083', 'COMPUTER', 'VII-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:25:40', '2017-09-03 09:25:40', '250', '11-09-2017', NULL, '1'),
(109, '122', 'SANSKRIT', 'VII-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:26:04', '2017-09-03 09:26:04', '250', '12-09-2017', NULL, '1'),
(110, '122', 'SANSKRIT', 'VII-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:26:15', '2017-09-03 09:26:15', '250', '12-09-2017', NULL, '1'),
(111, '041', 'MATHEMATICS', 'VII-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:26:47', '2017-09-03 09:26:47', '250', '15-09-2017', NULL, '1'),
(112, '041', 'MATHEMATICS', 'VII-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:26:57', '2017-09-03 09:26:57', '250', '15-09-2017', NULL, '1'),
(113, '087', 'SOCIAL STUDIES', 'VII-A', NULL, NULL, '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:27:19', '2017-09-03 09:27:19', '250', '18-09-2017', NULL, '1'),
(114, '087', 'SOCIAL STUDIES', 'VII-B', NULL, NULL, '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:27:29', '2017-09-03 09:27:29', '250', '18-09-2017', NULL, '1'),
(115, '002', 'HINDI', 'VII-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:28:26', '2017-09-03 09:28:26', '250', '20-09-2017', NULL, '1'),
(116, '002', 'HINDI', 'VII-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:28:35', '2017-09-03 09:28:35', '250', '20-09-2017', NULL, '1'),
(117, '301', 'ENGLISH', 'VII-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:29:07', '2017-09-03 09:29:07', '250', '22-09-2017', NULL, '1'),
(118, '301', 'ENGLISH', 'VII-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:29:18', '2017-09-03 09:29:18', '250', '22-09-2017', NULL, '1'),
(119, '086', 'SCIENCE', 'VII-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:29:38', '2017-09-03 09:29:38', '250', '25-09-2017', NULL, '1'),
(120, '086', 'SCIENCE', 'VII-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:29:51', '2017-09-03 09:29:51', '250', '25-09-2017', NULL, '1'),
(121, '122', 'SANSKRIT', 'VIII-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:30:20', '2017-09-03 09:30:20', '250', '09-09-2017', NULL, '1'),
(122, '122', 'SANSKRIT', 'VIII-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:30:32', '2017-09-03 09:30:32', '250', '09-09-2017', NULL, '1'),
(123, '004', 'G.K/ART', 'VIII-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:30:55', '2017-09-03 09:30:55', '250', '11-09-2017', NULL, '1'),
(124, '004', 'G.K/ART', 'VIII-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:31:05', '2017-09-03 09:31:05', '250', '11-09-2017', NULL, '1'),
(125, '083', 'COMPUTER', 'VIII-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:31:24', '2017-09-03 09:31:24', '250', '12-09-2017', NULL, '1'),
(126, '083', 'COMPUTER', 'VIII-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:31:32', '2017-09-03 09:31:32', '250', '12-09-2017', NULL, '1'),
(127, '002', 'HINDI', 'VIII-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:31:59', '2017-09-03 09:31:59', '250', '15-09-2017', NULL, '1'),
(128, '002', 'HINDI', 'VIII-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:32:07', '2017-09-03 09:32:07', '250', '15-09-2017', NULL, '1'),
(129, '087', 'SOCIAL STUDIES', 'VIII-A', NULL, NULL, '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:32:27', '2017-09-03 09:32:27', '250', '18-09-2017', NULL, '1'),
(130, '087', 'SOCIAL STUDIES', 'VIII-B', NULL, NULL, '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:32:36', '2017-09-03 09:32:36', '250', '18-09-2017', NULL, '1'),
(131, '301', 'ENGLISH', 'VIII-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:32:56', '2017-09-03 09:32:56', '250', '20-09-2017', NULL, '1'),
(132, '301', 'ENGLISH', 'VIII-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:33:06', '2017-09-03 09:33:06', '250', '20-09-2017', NULL, '1'),
(133, '086', 'SCIENCE', 'VIII-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:33:28', '2017-09-03 09:33:28', '250', '22-09-2017', NULL, '1'),
(134, '086', 'SCIENCE', 'VIII-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:33:37', '2017-09-03 09:33:37', '250', '22-09-2017', NULL, '1'),
(135, '041', 'MATHEMATICS', 'VIII-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:33:57', '2017-09-03 09:33:57', '250', '25-09-2017', NULL, '1'),
(136, '041', 'MATHEMATICS', 'VIII-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:34:07', '2017-09-03 09:34:07', '250', '25-09-2017', NULL, '1'),
(137, '083', 'COMPUTER', 'IX-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:34:39', '2017-09-03 09:34:39', '250', '09-09-2017', NULL, '1'),
(138, '083', 'COMPUTER', 'IX-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:34:48', '2017-09-03 09:34:48', '250', '09-09-2017', NULL, '1'),
(139, '087', 'SOCIAL STUDIES', 'IX-A', NULL, NULL, '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:35:15', '2017-09-03 09:35:15', '250', '15-09-2017', NULL, '1'),
(140, '087', 'SOCIAL STUDIES', 'IX-B', NULL, NULL, '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:35:23', '2017-09-03 09:35:23', '250', '15-09-2017', NULL, '1'),
(141, '041', 'MATHEMATICS', 'IX-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:35:41', '2017-09-03 09:35:41', '250', '18-09-2017', NULL, '1'),
(142, '041', 'MATHEMATICS', 'IX-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:35:49', '2017-09-03 09:35:49', '250', '18-09-2017', NULL, '1'),
(143, '002', 'HINDI', 'IX-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:36:14', '2017-09-03 09:36:14', '250', '20-09-2017', NULL, '1'),
(144, '002', 'HINDI', 'IX-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:36:25', '2017-09-03 09:36:25', '250', '20-09-2017', NULL, '1'),
(145, '301', 'ENGLISH', 'IX-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:36:49', '2017-09-03 09:36:49', '250', '22-09-2017', NULL, '1'),
(146, '301', 'ENGLISH', 'IX-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:36:57', '2017-09-03 09:36:57', '250', '22-09-2017', NULL, '1'),
(147, '086', 'SCIENCE', 'IX-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:37:20', '2017-09-03 09:37:20', '250', '25-09-2017', NULL, '1'),
(148, '086', 'SCIENCE', 'IX-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:37:29', '2017-09-03 09:37:29', '250', '25-09-2017', NULL, '1'),
(149, '043', 'CHEMISTRY', 'XI SCI-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:39:11', '2017-09-03 09:39:11', '250', '15-09-2017', NULL, '1'),
(150, '042', 'PHYSICS', 'XI SCI-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:40:01', '2017-09-03 09:40:01', '250', '18-09-2017', NULL, '1'),
(151, '301', 'ENGLISH', 'XI SCI-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:40:23', '2017-09-03 09:40:23', '250', '20-09-2017', NULL, '1'),
(152, '048', 'PHYSICAL EDUCATION', 'XI SCI-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:42:05', '2017-09-03 09:42:05', '250', '22-09-2017', NULL, '1'),
(153, '041/044', 'MATHEMATICS/BIOLOGY', 'XI SCI-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:44:32', '2017-09-03 09:53:22', '250', '25-09-2017', NULL, '1'),
(154, '030', 'ECONOMICS', 'XI COM-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:45:59', '2017-09-03 09:45:59', '250', '15-09-2017', NULL, '1'),
(155, '055', 'ACCOUNTANCY', 'XI COM-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:46:54', '2017-09-03 09:46:54', '250', '18-09-2017', NULL, '1'),
(156, '301', 'ENGLISH', 'XI COM-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:47:19', '2017-09-03 09:47:19', '250', '20-09-2017', NULL, '1'),
(157, '048', 'PHYSICAL EDUCATION', 'XI COM-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:47:46', '2017-09-03 09:47:46', '250', '22-09-2017', NULL, '1'),
(158, '054', 'BUSINESS STUDIES', 'XI COM-B', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-03 09:48:35', '2017-09-03 09:48:35', '250', '25-09-2017', NULL, '1'),
(159, '005', 'Art', 'VI-A', NULL, 'N.A', '25', '100', '125', '25', '100', '125', 'Active', 'both test and exam', NULL, '2017-09-13 10:31:18', '2017-09-13 10:31:18', '250', 'N.A', 'N.A', '1');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 1, 'pt', 'Post', '2017-08-29 15:05:41', '2017-08-29 15:05:41'),
(2, 'data_types', 'display_name_singular', 2, 'pt', 'Página', '2017-08-29 15:05:41', '2017-08-29 15:05:41'),
(3, 'data_types', 'display_name_singular', 3, 'pt', 'Utilizador', '2017-08-29 15:05:41', '2017-08-29 15:05:41'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2017-08-29 15:05:41', '2017-08-29 15:05:41'),
(5, 'data_types', 'display_name_singular', 5, 'pt', 'Menu', '2017-08-29 15:05:41', '2017-08-29 15:05:41'),
(6, 'data_types', 'display_name_singular', 6, 'pt', 'Função', '2017-08-29 15:05:41', '2017-08-29 15:05:41'),
(7, 'data_types', 'display_name_plural', 1, 'pt', 'Posts', '2017-08-29 15:05:41', '2017-08-29 15:05:41'),
(8, 'data_types', 'display_name_plural', 2, 'pt', 'Páginas', '2017-08-29 15:05:41', '2017-08-29 15:05:41'),
(9, 'data_types', 'display_name_plural', 3, 'pt', 'Utilizadores', '2017-08-29 15:05:41', '2017-08-29 15:05:41'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2017-08-29 15:05:41', '2017-08-29 15:05:41'),
(11, 'data_types', 'display_name_plural', 5, 'pt', 'Menus', '2017-08-29 15:05:41', '2017-08-29 15:05:41'),
(12, 'data_types', 'display_name_plural', 6, 'pt', 'Funções', '2017-08-29 15:05:41', '2017-08-29 15:05:41'),
(13, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2017-08-29 15:05:41', '2017-08-29 15:05:41'),
(14, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2017-08-29 15:05:41', '2017-08-29 15:05:41'),
(15, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o''nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2017-08-29 15:05:41', '2017-08-29 15:05:41'),
(16, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2017-08-29 15:05:41', '2017-08-29 15:05:41'),
(17, 'menu_items', 'title', 2, 'pt', 'Media', '2017-08-29 15:05:42', '2017-08-29 15:05:42'),
(18, 'menu_items', 'title', 3, 'pt', 'Publicações', '2017-08-29 15:05:42', '2017-08-29 15:05:42'),
(19, 'menu_items', 'title', 4, 'pt', 'Utilizadores', '2017-08-29 15:05:42', '2017-08-29 15:05:42'),
(20, 'menu_items', 'title', 5, 'pt', 'Categorias', '2017-08-29 15:05:42', '2017-08-29 15:05:42'),
(21, 'menu_items', 'title', 6, 'pt', 'Páginas', '2017-08-29 15:05:42', '2017-08-29 15:05:42'),
(22, 'menu_items', 'title', 7, 'pt', 'Funções', '2017-08-29 15:05:42', '2017-08-29 15:05:42'),
(23, 'menu_items', 'title', 8, 'pt', 'Ferramentas', '2017-08-29 15:05:42', '2017-08-29 15:05:42'),
(24, 'menu_items', 'title', 9, 'pt', 'Menus', '2017-08-29 15:05:42', '2017-08-29 15:05:42'),
(25, 'menu_items', 'title', 10, 'pt', 'Base de dados', '2017-08-29 15:05:42', '2017-08-29 15:05:42'),
(26, 'menu_items', 'title', 11, 'pt', 'Configurações', '2017-08-29 15:05:42', '2017-08-29 15:05:42');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', 'admin@admin.com', 'users/default.png', '$2y$10$UTnzuDsCwnfdsDQMm/X8AuG19HwiL8ZDQ5zHwzIN4j36dro/sanp6', 'SUcZxFNqpAtiMJh1RbHesxQ9vtvtE7TFNUbb8wx1wTDNEafz4nMs75QkK7HX', '2017-08-29 15:05:40', '2017-08-29 15:05:40'),
(2, 2, 'Vikas', 'smart_55guy@yahoo.co.in', 'users/default.png', '$2y$10$7tEwckRuCkw.RKiuQZuWaeokE6XPNmUKRBazYyLoIhD3nlUHuM9Mi', NULL, '2017-09-01 04:59:41', '2017-09-01 04:59:41'),
(3, 2, 'test', 'test@user.com', 'users/default.png', '$2y$10$H5DQEjXGWQQMV3jMHUlRC.wMsnBjyVH2hn3Qk1m5ppN2erZQ.Oyli', '5kRrBBJlQ45aNtQ4kRftf1jWR5xv9qNO2kTn5K0SMhYksbmsqmkLB5BWLSwP', '2017-09-01 06:54:42', '2017-09-01 06:54:42');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendancemgmts`
--
ALTER TABLE `attendancemgmts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `attendancemgmts_code_unique` (`code`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `classmgmts`
--
ALTER TABLE `classmgmts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `class_section` (`class_section`);

--
-- Indexes for table `coscholasticmarksmgmts`
--
ALTER TABLE `coscholasticmarksmgmts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `coscholasticmarksmgmts_code_unique` (`code`);

--
-- Indexes for table `coscholasticmgmts`
--
ALTER TABLE `coscholasticmgmts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `disciplinemarksmgmts`
--
ALTER TABLE `disciplinemarksmgmts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `disciplinemarksmgmts_code_unique` (`code`);

--
-- Indexes for table `disciplinemgmts`
--
ALTER TABLE `disciplinemgmts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `marksmgmts`
--
ALTER TABLE `marksmgmts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `marksmgmts_code_unique` (`code`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `overallmarksmgmts`
--
ALTER TABLE `overallmarksmgmts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `overallmarksmgmt_code_unique` (`code`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_groups`
--
ALTER TABLE `permission_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permission_groups_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Indexes for table `resultdatemgmts`
--
ALTER TABLE `resultdatemgmts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `resultdatemgmts_code_unique` (`code`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `sessionmanagers`
--
ALTER TABLE `sessionmanagers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `session` (`session_period`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `studentmgmts`
--
ALTER TABLE `studentmgmts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjectmgmts`
--
ALTER TABLE `subjectmgmts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendancemgmts`
--
ALTER TABLE `attendancemgmts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `classmgmts`
--
ALTER TABLE `classmgmts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `coscholasticmarksmgmts`
--
ALTER TABLE `coscholasticmarksmgmts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `coscholasticmgmts`
--
ALTER TABLE `coscholasticmgmts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `disciplinemarksmgmts`
--
ALTER TABLE `disciplinemarksmgmts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `disciplinemgmts`
--
ALTER TABLE `disciplinemgmts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `marksmgmts`
--
ALTER TABLE `marksmgmts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `overallmarksmgmts`
--
ALTER TABLE `overallmarksmgmts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `permission_groups`
--
ALTER TABLE `permission_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `resultdatemgmts`
--
ALTER TABLE `resultdatemgmts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sessionmanagers`
--
ALTER TABLE `sessionmanagers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `studentmgmts`
--
ALTER TABLE `studentmgmts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1082;
--
-- AUTO_INCREMENT for table `subjectmgmts`
--
ALTER TABLE `subjectmgmts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=160;
--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
